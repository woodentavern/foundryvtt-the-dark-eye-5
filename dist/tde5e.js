// Core utilities
import "./module/config.js"; // Load the script without importing.
import * as socket from "./module/socket.js";
import preloadHandlebarsTemplates from "./module/templates.js";
import * as settings from "./module/settings.js";
import * as effects from "./module/tokens/statusEffects.js";
import registerShortcuts from "./module/keybinds.js";
import { initializeCompendium } from "./module/compendium.js";
import { migrateData, migrateSystem } from "./module/migration.js";
import helpers from "./module/handlebarHelpers.js";
import { info, isPrimaryGm } from "./module/util.js";

// Item models & UI
import TdeItem from "./module/items/item.js";
import ActionData from "./module/items/model/actionData.js";
import VantageData from "./module/items/model/vantageData.js";
import AmmunitionData from "./module/items/model/ammunitionData.js";
import ArmorData from "./module/items/model/armorData.js";
import ArtifactAbilityData from "./module/items/model/artifactAbilityData.js";
import BlessingData from "./module/items/model/blessingData.js";
import CantripData from "./module/items/model/cantripData.js";
import CombatAbilityData from "./module/items/model/combatAbilityData.js";
import CombatTechniqueData from "./module/items/model/combatTechniqueData.js";
import SpecialAbilityModel from "./module/items/model/abstract/specialAbilityModel.js";
import ConditionData from "./module/items/model/conditionData.js";
import CreatureTypeData from "./module/items/model/creatureTypeData.js";
import CultureData from "./module/items/model/cultureData.js";
import InventoryItemData from "./module/items/model/inventoryItemData.js";
import KarmaAbilityData from "./module/items/model/karmaAbilityData.js";
import LanguageSpokenData from "./module/items/model/languageSpokenData.js";
import LanguageWrittenData from "./module/items/model/languageWrittenData.js";
import LiturgicalChantData from "./module/items/model/liturgicalChantData.js";
import MagicAbilityData from "./module/items/model/magicAbilityData.js";
import MeleeWeaponData from "./module/items/model/meleeWeaponData.js";
import RaceData from "./module/items/model/raceData.js";
import ReactionData from "./module/items/model/reactionData.js";
import ShieldData from "./module/items/model/shieldData.js";
import SkillData from "./module/items/model/skillData.js";
import SpellData from "./module/items/model/spellData.js";
import SpellGlyphAbilityData from "./module/items/model/spellGlyphAbilityData.js";
import StateData from "./module/items/model/stateData.js";
import TraditionData from "./module/items/model/traditionData.js";
import RangedWeaponData from "./module/items/model/rangedWeaponData.js";
import TraditionImprintData from "./module/items/model/traditionImprintData.js";
import TdeBaseItemSheet from "./module/items/sheets/baseItemSheet.js";
import CombatInventoryItem from "./module/items/sheets/combatInventoryItemSheet.js";

// Actor models & UI
import TdeActor from "./module/actors/actor.js";
import CreatureData from "./module/actors/model/creatureData.js";
import NpcData from "./module/actors/model/npcData.js";
import PlayerData from "./module/actors/model/playerData.js";
import TdeTokenDocument from "./module/tokens/tokenDoc.js";
import PlayerActorSheet from "./module/actors/sheets/playerSheet.js";
import NpcActorSheet from "./module/actors/sheets/npcSheet.js";
import CreatureActorSheet from "./module/actors/sheets/creatureSheet.js";
import { ImprovementLogEntry } from "./module/actors/logEntry.js";

// Message models & UI
import TdeChatMessage from "./module/chatMessage/chatMessage.js";
import ActionMessageModel from "./module/chatMessage/model/ActionMessageModel.js";
import TdeSkillCheck from "./module/ui/rolls/skillCheck.js";

// Combat models & UI
import TdeCombat from "./module/combat/combat.js";
import TdeCombatant from "./module/combat/combatant.js";
import TdeCombatTracker from "./module/combat/combatTracker.js";

// Other UI modules
import EntityLibrary from "./module/ui/library.js";
import { initializeModifierControl } from "./module/ui/controls/modifierControl.js";
import UnitControl from "./module/ui/controls/unitControl.js";
import { initializeRecharge, initializeChargeConfiguration } from "./module/ui/chargeManagement.js";
import { initializeHotbarOverlay } from "./module/ui/hotbarOverlay.js";
import TdeTremorSenseDetectionMode from "./module/canvas/tremorSenseDetectionMode.js";
import ItemTagsElement from "./module/ui/controls/itemTags.js";
import ExtendedProseMirrorElement from "./module/ui/controls/richEditor.js";
import ExtendedMultiSelectElement from "./module/ui/controls/multiSelect.js";

// Extensions for existing modules
import registerBarBrawl from "./module/integration/barBrawl.js";
import registerChatCommands from "./module/integration/commands.js";
import registerDragRuler from "./module/integration/dragRulerExtension.js";
import registerElevationRuler from "./module/integration/elevationRulerExtension.js";
import registerMonkHotbar from "./module/integration/monksHotbarExtension.js";
import registerSimpleCalendar from "./module/integration/simpleCalendarExtension.js";
import registerBabele from "./module/integration/babele.js";
import registerRegions from "./module/regions.js";

/* ------------------------------------ */
/* Initialize system                    */
/* ------------------------------------ */
Hooks.once("init", () => {
    info("Initializing the system.");

    window.customElements.define(ItemTagsElement.tagName, ItemTagsElement);
    window.customElements.define(ExtendedProseMirrorElement.tagName, ExtendedProseMirrorElement);
    window.customElements.define(ExtendedMultiSelectElement.tagName, ExtendedMultiSelectElement);
    window.NullableBoolean = value => {
        switch (value) {
        case "yes":
        case "true": return true;
        case "no":
        case "false": return false;
        default: return null;
        }
    };

    TDE5E.hasAssets = !!game.modules.get("tde5e-assets")?.active;
    settings.registerSystemSettings();

    CONFIG.Combat.initiative.formula = "1d20";
    CONFIG.Dice.rolls.push(TdeSkillCheck);
    CONFIG.ActiveEffect.legacyTransferral = false;
    Handlebars.registerHelper(helpers);
    registerRegions();

    // Assign custom classes and constants here
    // @ts-ignore
    CONFIG.Actor.documentClass = TdeActor;
    CONFIG.Actor.dataModels.creature = CreatureData;
    CONFIG.Actor.dataModels.npc = NpcData;
    CONFIG.Actor.dataModels.player = PlayerData;
    CONFIG.Actor.trackableAttributes = {
        creature: CreatureData.getTokenAttributes(),
        npc: NpcData.getTokenAttributes(),
        player: PlayerData.getTokenAttributes(),
    };

    CONFIG.Item.documentClass = TdeItem;
    CONFIG.Item.dataModels.action = ActionData;
    CONFIG.Item.dataModels.advantage = VantageData;
    CONFIG.Item.dataModels.ammunition = AmmunitionData;
    CONFIG.Item.dataModels.armor = ArmorData;
    CONFIG.Item.dataModels.artifactAbility = ArtifactAbilityData;
    CONFIG.Item.dataModels.blessing = BlessingData;
    CONFIG.Item.dataModels.cantrip = CantripData;
    CONFIG.Item.dataModels.combatAbility = CombatAbilityData;
    CONFIG.Item.dataModels.combatTechnique = CombatTechniqueData;
    CONFIG.Item.dataModels.commonAbility = SpecialAbilityModel;
    CONFIG.Item.dataModels.condition = ConditionData;
    CONFIG.Item.dataModels.creatureType = CreatureTypeData;
    CONFIG.Item.dataModels.culture = CultureData;
    CONFIG.Item.dataModels.disadvantage = VantageData;
    CONFIG.Item.dataModels.inventoryItem = InventoryItemData;
    CONFIG.Item.dataModels.karmaAbility = KarmaAbilityData;
    CONFIG.Item.dataModels.languageSpoken = LanguageSpokenData;
    CONFIG.Item.dataModels.languageWritten = LanguageWrittenData;
    CONFIG.Item.dataModels.liturgy = LiturgicalChantData;
    CONFIG.Item.dataModels.magicalAbility = MagicAbilityData;
    CONFIG.Item.dataModels.meleeWeapon = MeleeWeaponData;
    CONFIG.Item.dataModels.race = RaceData;
    CONFIG.Item.dataModels.rangedWeapon = RangedWeaponData;
    CONFIG.Item.dataModels.reaction = ReactionData;
    CONFIG.Item.dataModels.shield = ShieldData;
    CONFIG.Item.dataModels.skill = SkillData;
    CONFIG.Item.dataModels.spell = SpellData;
    CONFIG.Item.dataModels.spellGlyphAbility = SpellGlyphAbilityData;
    CONFIG.Item.dataModels.state = StateData;
    CONFIG.Item.dataModels.tradition = TraditionData;
    CONFIG.Item.dataModels.traditionImprint = TraditionImprintData;

    CONFIG.ChatMessage.documentClass = TdeChatMessage;
    CONFIG.ChatMessage.dataModels.action = ActionMessageModel;

    CONFIG.Combat.documentClass = TdeCombat;
    CONFIG.Combatant.documentClass = TdeCombatant;
    CONFIG.Token.documentClass = TdeTokenDocument;
    CONFIG.ui.combat = TdeCombatTracker;

    // Register custom system settings
    Actors.unregisterSheet("core", ActorSheet);
    Actors.registerSheet("tde5e", PlayerActorSheet, { types: ["player"] });
    Actors.registerSheet("tde5e", NpcActorSheet, { types: ["npc"] });
    Actors.registerSheet("tde5e", CreatureActorSheet, { types: ["creature"] });
    Items.unregisterSheet("core", ItemSheet);
    Items.registerSheet("tde5e", TdeBaseItemSheet);
    Items.registerSheet("tde5e", CombatInventoryItem, { types: ["meleeWeapon", "rangedWeapon", "shield", "armor"] });

    initializeCompendium(); // Initialize other components.
    initializeModifierControl(); // Initialize modifiers for items.
    UnitControl.initialize();
    initializeChargeConfiguration(); // Initialize charge configuration rendering.
    initializeRecharge(); // Initialize automatic charge management.
    initializeHotbarOverlay();
    preloadHandlebarsTemplates(); // Preload Handlebars templates
    registerShortcuts(); // Initialize global keyboard shortcuts.

    // Register extensions
    registerBarBrawl();
    registerChatCommands();
    registerMonkHotbar();
    registerDragRuler();
    registerElevationRuler();
    registerSimpleCalendar();
});

/* ------------------------------------ */
/* Setup system                         */
/* ------------------------------------ */
Hooks.once("setup", () => {
    effects.setupConditionCounters();
    registerBabele();

    // Translate static resources.
    const resources = TDE5E.units.currency.concat(TDE5E.units.weight);
    for (const resource of resources) {
        resource.label = game.i18n.localize(resource.label);
        if (resource.labelShort) resource.labelShort = game.i18n.localize(resource.labelShort);
    }

    // Add a custom vision override for tremor sense
    CONFIG.Canvas.detectionModes.feelTremor = new TdeTremorSenseDetectionMode({
        id: "feelTremor",
        label: "DETECTION.FeelTremor",
        walls: false,
        type: DetectionMode.DETECTION_TYPES.MOVE,
    });
});

/* ------------------------------------ */
/* When ready                           */
/* ------------------------------------ */
Hooks.once("ready", async () => {
    TDE5E.debugMode = settings.canUseDebugFeature(game.user.id);
    CONFIG.debug.combat = TDE5E.debugMode;
    game.socket.on("system.tde5e", socket.processMessage);
    TdeCombat.ready();

    const body = $("body");
    TdeItem.addActivationListener(body);
    EntityLibrary.addTagListener(body);

    if (game.user.isGM) {
        // Make sure nobody else is logged in when migrating data.
        if (game.users.filter(user => user.active).length === 1) {
            await migrateSystem();
            await migrateData();
        }

        ImprovementLogEntry.notifyPendingImprovements();
    }
});

/* -------------------------------------------- */
/*  Other Hooks                                 */
/* -------------------------------------------- */
Hooks.on("hotbarDrop", (_bar, data, slot) => {
    if (!data.uuid) return true;
    switch (data.type) {
    case "Actor":
        fromUuid(data.uuid).then(actor => {
            Macro.create({
                name: `${game.i18n.localize("Display")} ${actor.name}`,
                type: CONST.MACRO_TYPES.SCRIPT,
                img: actor.thumbnail,
                command: `Hotbar.toggleDocumentSheet("${data.uuid}");`,
            }).then(macro => game.user.assignHotbarMacro(macro, slot));
        });
        return false;
    case "Item":
        fromUuid(data.uuid).then(item => {
            const roll = item?.isRollable && item.isOwner && item.actor;
            const command = roll
                ? `(await fromUuid("${data.uuid}"))?.roll();`
                : `Hotbar.toggleDocumentSheet("${data.uuid}");`;
            Macro.create({
                name: `${roll ? game.i18n.localize("Roll") : game.i18n.localize("Display")} ${item.name}`,
                type: CONST.MACRO_TYPES.SCRIPT,
                img: item.thumbnail,
                command,
                "flags.tde5e.sourceItem": item.uuid,
            }).then(macro => game.user.assignHotbarMacro(macro, slot));
        });
        return false;
    case "JournalEntry":
        fromUuid(data.uuid).then(journal => {
            const firstPage = Array.from(journal.pages.values()).sort((p1, p2) => p1.sort - p2.sort)[0];
            const macroPromise = firstPage?.src ? Macro.create({
                name: `${game.i18n.localize("Display")} ${journal.name}`,
                type: CONST.MACRO_TYPES.SCRIPT,
                img: firstPage.src,
                command: `Hotbar.toggleDocumentSheet("${data.uuid}")`,
            }) : ui.hotbar._createDocumentSheetToggle(journal);
            macroPromise.then(macro => game.user.assignHotbarMacro(macro, slot));
        });
        return false;
    default: return true;
    }
});

// Chat extensions
Hooks.on("renderChatMessage", (chatMessage, html) => chatMessage.activateListeners(html));
Hooks.on("getChatLogEntryContext", (_chat, items) => {
    items.push({
        name: "tde5e.check.reassociate",
        icon: "<i class=\"fas fa-link\"></i>",
        condition: li => {
            const message = game.messages.get(li.data("messageId"));
            return message?.canUserModify(game.user, "update")
                && message.isRoll
                && message.rolls.some(r => r instanceof TdeSkillCheck);
        },
        callback: li => game.combat?.connectRoll(game.messages.get(li.data("messageId")), true),
    });
});

Hooks.on("renderActorDirectory", ImprovementLogEntry.displayPendingImprovements);
Hooks.on("renderCompendiumDirectory", EntityLibrary.extendCompendiumDirectory);
Hooks.on("renderTokenHUD", effects.filterAvailableEffects);

Hooks.on("preUpdateToken", (token, changes) => {
    // Change token actor name when token name changes.
    if ("name" in changes && !token.actorLink && token.actor) changes["delta.name"] = changes.name;
});

Hooks.on("updateToken", (token, changes) => {
    const { combatant } = token;
    if (!combatant) return;

    if ((changes.hasOwnProperty("x") || changes.hasOwnProperty("y")) && isPrimaryGm()) {
        game.combat._onMoveCombatant(combatant);
    }

    if (changes.hasOwnProperty("disposition")) {
        combatant.prepareData();
        ui.combat.render();
    }
});

Hooks.on("createCombatant", combatant => combatant.refresh());
Hooks.on("deleteCombatant", combatant => combatant.refresh());
Hooks.on("deleteCombat", combat => combat.combatants.forEach(c => c.refresh()));
