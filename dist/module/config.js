// Item CID maps
import conditions from "./items/modifier/condition.js";
import PhysicalItem from "./items/rollable/physicalItem.js";
import skills from "./items/rollable/skill.js";
import spells from "./items/rollable/spell.js";
import advantages from "./items/modifier/advantage.js";
import disadvantages from "./items/modifier/vantage.js";
import combatAbilities from "./items/modifier/combatSpecialAbility.js";
import combatStylies from "./items/modifier/combatStyle.js";
import magicAbilities from "./items/modifier/magicalSpecialAbility.js";
import karmaAbilities from "./items/modifier/karmaSpecialAbility.js";
import meleeWeapons from "./items/rollable/meleeWeapon.js";
import rangedWeapons from "./items/rollable/rangedWeapon.js";
import shields from "./items/rollable/shield.js";
import armors from "./items/rollable/armor.js";
import { LanguageSpoken, LanguageWritten } from "./items/language.js";
import races from "./items/race.js";
import creatureType from "./items/creatureType.js";
import cultures from "./items/culture.js";
import traditions from "./items/tradition.js";
import traditionImprints from "./items/modifier/traditionImprint.js";
import magictraditionImprints from "./items/modifier/magicalTraditionImprint.js";
import blessedtraditionImprints from "./items/modifier/blessedTraditionImprint.js";
import CombatTechnique from "./items/rollable/combatTechnique.js";
import meleeCombatTechniques from "./items/rollable/meleeCombatTechnique.js";
import rangedCombatTechniques from "./items/rollable/rangedCombatTechnique.js";
import maneuver from "./items/rollable/maneuver.js";
import ammunition from "./items/rollable/ammunition.js";
import inventoryItem from "./items/rollable/inventoryItem.js";
import states from "./items/modifier/state.js";
import commonAbilities from "./items/modifier/commonSpecialAbility.js";
import liturgy from "./items/rollable/liturgy.js";
import armorStyles from "./items/modifier/armorStyle.js";
import artifactAbility from "./items/modifier/artifactAbility.js";

// Core types
import PlayerActor from "./actors/player.js";
import NpcActor from "./actors/npc.js";
import CreatureActor from "./actors/creature.js";
import { LootClaimMessage } from "./chatMessage/lootClaimMessage.js";
import ActionMessage from "./chatMessage/actionMessage.js";

// Other utilities
import { PreparationPriority } from "./items/item.js";
import * as rollModifications from "./ui/rolls/rollModification.js";
import { hasVariants, promptVariant } from "./ui/variantPicker.js";
import promptReplace from "./ui/replaceDialog.js";
import * as utils from "./util.js";

window.TDE5E = {};

/** Expose modifications so modules can use them */
TDE5E.rollModifications = rollModifications;

/** Expose helpers so modules can use them */
TDE5E.helpers = utils;

/** Expose Preparation Priority so modules can use them  */
TDE5E.preparationPriority = PreparationPriority;

/** Exposes a variety of helper functions that are part of various components so modules can use them */
TDE5E.utility = {
    hasVariants,
    promptVariant,
    promptReplace,
};

/** Master data version to target for migration. */
TDE5E.dataVersion = 48;

/** Indicates whether the private asset module is installed. */
TDE5E.hasAssets = false;

/** Indicates whether the current user can use debug features. */
TDE5E.debugMode = false;

/** Indicates whether compendium indexes have been loaded. */
TDE5E.compendiumsReady = false;

/**
 * Mapping for item types to custom class maps. Unmapped types will use the default {@link TdeItem}.
 * This may optionally contain group names, which are checked before the parent type.
 */
TDE5E.itemTypeClasses = {
    condition: conditions,
    state: states,

    combatTechnique: {
        default: CombatTechnique,
        ...meleeCombatTechniques,
        ...rangedCombatTechniques,
    },

    inventoryItem,
    equipmentItem: { default: PhysicalItem },
    meleeWeapon: meleeWeapons,
    rangedWeapon: rangedWeapons,
    shield: shields,
    armor: armors,

    spell: spells,
    liturgy,
    skill: skills,
    creatureType,

    advantage: advantages,
    disadvantage: disadvantages,
    commonAbility: commonAbilities,

    combatAbility: combatAbilities,
    combatStyle: combatStylies,
    armorStyle: armorStyles,
    armorStyleExtended: armorStyles,

    artifactAbility,
    spellGlyphAbility: magicAbilities,

    magicalAbility: magicAbilities,
    karmaAbility: karmaAbilities,
    tradition: traditions,

    traditionImprint: traditionImprints,
    magicalTraditionImprint: magictraditionImprints,
    blessedTraditionImprint: blessedtraditionImprints,

    languageSpoken: { default: LanguageSpoken },
    languageWritten: { default: LanguageWritten },

    race: races,
    culture: cultures,

    action: maneuver,
    reaction: maneuver,
    ammunition,
};

/**
 *  Mappping for the chat message classes. Unmapped types will use {@link TdeChatMessage}.
 */
TDE5E.chatMessageTypeClasses = {
    lootclaim: LootClaimMessage,
    action: ActionMessage,
};

/**
 * Mapping for actor types to custom classes. Unmapped types will use {@link TdeActor}.
 */
TDE5E.actorTypeClasses = {
    base: Actor,
    player: PlayerActor,
    npc: NpcActor,
    creature: CreatureActor,
};

/**
 * Table of improvement factors, levels and costs.
 */
TDE5E.improvementTable = {
    A: [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14],
    B: [2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 4, 6, 8, 10, 12, 14, 16, 18, 20, 22, 24, 26, 28],
    C: [3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 6, 9, 12, 15, 18, 21, 24, 27, 30, 33, 36, 39, 42],
    D: [4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 8, 12, 16, 20, 24, 28, 32, 36, 40, 44, 48, 52, 56],
    E: [0, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 30, 45, 60, 75, 90, 105, 120, 135, 150, 165, 180],
};

/**
 * Arrays of available units for each unit type.
 * @typedef {object} Unit
 * @property {string} label The localization path of the unit's name.
 * @property {string?} labelShort The localization path of the unit's abbreviation.
 * @property {number} multiplier The relative value of the unit compared to its data amount.
 * @property {string} icon The image path of the unit.
 */
TDE5E.units = {
    currency: [
        {
            label: "tde5e.currency.ducats",
            icon: "systems/tde5e/assets/units/ducat.png",
            multiplier: 1000,
        },
        {
            label: "tde5e.currency.silverthalers",
            icon: "systems/tde5e/assets/units/silverthaler.png",
            multiplier: 100,
        },
        {
            label: "tde5e.currency.halers",
            icon: "systems/tde5e/assets/units/haler.png",
            multiplier: 10,
        },
        {
            label: "tde5e.currency.kreutzers",
            icon: "systems/tde5e/assets/units/kreutzer.png",
            multiplier: 1,
        },
    ],
    weight: [
        {
            label: "tde5e.measurements.stones",
            labelShort: "tde5e.measurements.stonesShort",
            icon: "systems/tde5e/assets/units/stone.png",
            multiplier: 40,
        },
        {
            label: "tde5e.measurements.ounces",
            labelShort: "tde5e.measurements.ouncesShort",
            icon: "systems/tde5e/assets/units/ounce.png",
            multiplier: 1,
        },
    ],
};

/**
 * Translation for quality in maneuver to translation key.
 */
TDE5E.maneuverQuality = {
    0: "tde5e.combat.maneuverQuality.impossible",
    1: "tde5e.combat.maneuverQuality.limited",
    2: "tde5e.combat.maneuverQuality.disadvantaged",
    3: "tde5e.combat.maneuverQuality.neutral",
    4: "tde5e.combat.maneuverQuality.advantaged",
    5: "tde5e.combat.maneuverQuality.overreached",
};

/**
 * Represents the damage types and associates the corresponding icon location and the order in which multiple damage
 *  types should be displayed in.
 */
TDE5E.damageTypeIcons = {
    pierce: {
        order: 1,
        icon: "systems/tde5e/assets/damageTypes/arrowhead.svg",
    },
    slash: {
        order: 2,
        icon: "systems/tde5e/assets/damageTypes/axe-swing.svg",
    },
    blunt: {
        order: 3,
        icon: "systems/tde5e/assets/damageTypes/hammer-drop.svg",
    },
    fire: {
        order: 4,
        icon: "systems/tde5e/assets/damageTypes/feuer.svg",
    },
    ice: {
        order: 5,
        icon: "systems/tde5e/assets/damageTypes/eis.svg",
    },
    water: {
        order: 6,
        icon: "systems/tde5e/assets/damageTypes/wasser.svg",
    },
    humus: {
        order: 7,
        icon: "systems/tde5e/assets/damageTypes/humus.svg",
    },
    ore: {
        order: 8,
        icon: "systems/tde5e/assets/damageTypes/erz.svg",
    },
    air: {
        order: 9,
        icon: "systems/tde5e/assets/damageTypes/luft.svg",
    },
    energy: {
        order: 10,
        icon: "systems/tde5e/assets/damageTypes/XXXXX.svg",
    },
    acid: {
        order: 11,
        icon: "systems/tde5e/assets/damageTypes/poison-bottle.svg",
    },
    direct: {
        order: 12,
        icon: "systems/tde5e/assets/damageTypes/pierced-body.svg",
    },
    other: {
        order: 13,
        icon: "systems/tde5e/assets/damageTypes/XXXXX.svg",
    },
};

/**
 * Describes the names for multi-level items (such as conditions and wear).
 */
TDE5E.levelSelection = {
    0: "-",
    1: "I",
    2: "II",
    3: "III",
    4: "IV",
};

/**
 * Selectable options for improvement column fields.
 */
TDE5E.improvementSelection = {
    A: "A",
    B: "B",
    C: "C",
    D: "D",
    E: "E",
};

/**
 * Selectable options for nullable boolean fields.
 */
TDE5E.nullableBooleanSelection = {
    "": "tde5e.choices.maybe",
    true: "tde5e.choices.yes",
    false: "tde5e.choices.no",
};

/**
 * Selectable options for weapon reach.
 */
TDE5E.reachSelection = {
    "": "tde5e.combatTechniqueGroups.all",
    false: "tde5e.combatTechniqueGroups.melee",
    true: "tde5e.combatTechniqueGroups.ranged",
};

/**
 * Selectable options for weapon hands.
 */
TDE5E.handsSelection = {
    1: "tde5e.item.equipment.hands.one",
    1.5: "tde5e.item.equipment.hands.oneHalf",
    2: "tde5e.item.equipment.hands.two",
};

/**
 * Describes how each level of wear modifies weapons.
 * The effects of each level are not cumulative.
 */
TDE5E.wearWeaponEffects = {
    1: { damage: -1 },
    2: { damage: -1, adrenaline: -2 },
    3: { damage: -3, adrenaline: -4 },
};

/**
 * Describes how each level of wear modifies shields.
 * The effects of each level are not cumulative.
 */
TDE5E.wearShieldEffects = {
    1: { breakingPointRating: 2 },
    2: { breakingPointRating: 4 },
    3: { breakingPointRating: 6 },
};

/**
 * Describes how each level of wear modifies armors.
 * The effects of each level are not cumulative.
 */
TDE5E.wearArmorEffects = {
    1: { protection: -1 },
    2: { protection: -1, encumbrance: 1 },
    3: { protection: -3, encumbrance: 3 },
    4: { protection: 0, encumbrance: 3 },
};

/**
 * Regular expressions for matching custom commands.
 */
TDE5E.commands = {
    rollAbility: /(g(?:m)?|b(?:lind)?|s(?:elf)?|p(?:ublic)?)?r(?:oll)?a(?:bility)?/i,
};

/**
 * Regular expressions for parsing entity properties.
 */
TDE5E.parsers = {
    damage: /(?<diceNumber>\d+)?[dw](?<diceSides>\d+)(?<damageFlat>[+-]\d+)?/i,
    damageRange: /(?<lowerBound>\d+)\W*(?<upperBound>\d+)/,
    protection: /(?<pierce>-?\d+)\/(?<slash>-?\d+)\/(?<blunt>-?\d+)/,
    range: /(?<short>\d+)\/(?<middle>\d+)\/(?<long>\d+)/,
};

/**
 * The default flag contant any item in the loot container should have.
 */
TDE5E.defaultLootItemData = Object.freeze({
    chanceToAppear: 100,
    quantityRange: "1d6",
    quantityIsPerQl: false,
    sourceUuid: null,
    unitOfMeasurement: "peaces",
    requirementText: "",
});

/**
 * The default flag contant any item in the loot container should have.
 */
TDE5E.defaultLootClaimMessageData = Object.freeze({
    items: {},
    currency: {
        kreutzers: 0,
        halers: 0,
        silverthalers: 0,
        ducats: 0,
    },
    phase: "INIT",
    source: "UNKNOWN",
    isFinished: false,
    claimed: {},
    history: [],
});

/**
 * A dictionary of trade zone abbreviations and their price factors relative to every other zone.
 * @type {object}
 */
/* eslint-disable max-len -- Retain row formatting for two-dimensional table. */
TDE5E.tradeZoneFactors = Object.freeze({
    ach: [0, 0.4, 1, 0.8, 1.4, 0.6, 0.8, 0.8, 0.6, 0.4, 0.8, 1.6, 1.2, 1, 0.8, 0.4, 0.6, 0.8, 0.6, 1.2, 1.4, 1.2, 1, 1.2, 0.4, 0.6, 1.2, 0.4, 1, 0.8, 0.8, 1.2, 1.2, 0.8],
    ala: [0.4, 0, 1, 1.2, 1.4, 0.8, 1.2, 1, 0.4, 0.4, 1, 1.8, 1.2, 1.2, 1, 0.8, 0.8, 0.4, 0.8, 1.4, 1.4, 1.4, 1.2, 1.2, 0.6, 0.8, 1.2, 0.6, 1, 1, 0.4, 1.4, 1.4, 0.8],
    alb: [1, 1, 0, 0.8, 0.4, 0.8, 0.8, 1.2, 0.6, 0.8, 1.2, 1, 0.6, 0.4, 0.4, 0.8, 1, 0.6, 1.2, 1, 0.8, 1, 0.8, 0.6, 1, 1, 0.6, 1, 0.4, 0.8, 1, 0.8, 0.4, 0.4],
    alm: [0.8, 1.2, 0.8, 0, 0.8, 0.8, 0.8, 1.2, 1, 1.2, 1.2, 1.2, 1, 0.4, 0.4, 0.4, 1, 1, 0.4, 1.2, 1.2, 1.4, 0.4, 1, 0.8, 1.2, 1, 0.8, 0.8, 0.8, 1.4, 0.8, 0.4, 0.8],
    and: [1.4, 1.4, 0.4, 0.8, 0, 0.8, 1.2, 1.2, 1, 1.2, 1.2, 1.2, 0.8, 0.4, 0.8, 1.2, 1, 1, 1.2, 1.2, 0.8, 1.2, 0.8, 0.8, 1, 1.4, 0.8, 1.2, 0.4, 0.8, 1.4, 0.8, 0.8, 0.8],
    ara: [0.6, 0.8, 0.8, 0.8, 0.8, 0, 1.2, 0.6, 0.8, 0.6, 0.6, 1, 1.4, 0.4, 1.2, 0.8, 0.4, 1, 0.4, 1, 1.6, 1, 0.4, 1.4, 0.4, 0.8, 1.2, 0.4, 1.2, 0.4, 1.2, 0.8, 0.8, 1],
    art: [0.8, 1.2, 0.8, 0.8, 1.2, 1.2, 0, 1.6, 1, 1.2, 1.6, 1.4, 1, 1.2, 0.4, 0.4, 1.4, 1, 0.8, 1.4, 1.2, 1.4, 1.2, 1, 1.2, 1.4, 1, 0.8, 0.8, 1.4, 1.4, 1.6, 1.2, 0.8],
    bor: [0.8, 1, 1.2, 1.2, 1.2, 0.6, 1.6, 0, 0.8, 0.6, 0.4, 0.8, 0.8, 0.8, 1.2, 1.2, 0.4, 1, 0.8, 0.4, 1.2, 0.4, 1, 0.8, 0.6, 0.6, 0.8, 0.8, 1.2, 0.4, 1.2, 0.4, 1.2, 1],
    bra: [0.6, 0.4, 0.6, 1, 1, 0.8, 1, 0.8, 0, 0.4, 0.8, 1.2, 0.8, 1, 0.6, 1, 0.6, 0.4, 0.8, 1.2, 1, 1.2, 1.2, 0.8, 0.6, 0.6, 0.8, 0.6, 0.6, 0.8, 0.4, 1.2, 1, 0.4],
    cha: [0.4, 0.4, 0.8, 1.2, 1.2, 0.6, 1.2, 0.6, 0.4, 0, 0.6, 1, 1, 1, 0.8, 0.8, 0.4, 0.6, 0.6, 1, 1.2, 1, 1, 1, 0.4, 0.4, 1, 0.4, 0.8, 0.6, 0.8, 1, 1.2, 0.6],
    ehe: [0.8, 1, 1.2, 1.2, 1.2, 0.6, 1.6, 0.4, 0.8, 0.6, 0, 1, 0.4, 0.8, 1.2, 1.2, 0.4, 1, 0.8, 0.8, 1.2, 0.4, 1, 0.8, 0.6, 0.6, 0.8, 0.8, 0.8, 0.4, 1.2, 0.8, 1.6, 1],
    elf: [1.6, 1.8, 1, 1.2, 1.2, 1, 1.4, 0.8, 1.2, 1, 1, 0, 0.8, 0.8, 1, 1.4, 1.2, 1.2, 1.4, 0.4, 0.8, 0.8, 1.2, 0.4, 1.4, 1.4, 0.4, 1.4, 0.8, 0.8, 1.6, 0.4, 1.2, 1],
    fir: [1.2, 1.2, 0.6, 1, 0.8, 1.4, 1, 0.8, 0.8, 1, 0.4, 0.8, 0, 1.2, 0.6, 1, 1.2, 0.8, 1.4, 0.4, 0.8, 0.4, 1.6, 0.4, 1.4, 1.4, 0.4, 1.2, 0.4, 1.2, 1.2, 0.8, 1.2, 0.6],
    gar: [1, 1.2, 0.4, 0.4, 0.4, 0.4, 1.2, 0.8, 1, 1, 0.8, 0.8, 1.2, 0, 0.8, 0.8, 0.6, 1, 0.8, 0.8, 1.2, 1.2, 0.4, 1, 0.6, 1, 0.8, 0.8, 0.8, 0.4, 1.4, 0.4, 0.4, 0.8],
    hor: [0.8, 1, 0.4, 0.4, 0.8, 1.2, 0.4, 1.2, 0.6, 0.8, 1.2, 1, 0.6, 0.8, 0, 0.4, 1, 0.6, 0.8, 1, 0.8, 1, 0.8, 0.6, 1, 1, 0.6, 0.8, 0.4, 1.2, 1, 1.2, 0.4, 0.4],
    kho: [0.4, 0.8, 0.8, 0.4, 1.2, 0.8, 0.4, 1.2, 1, 0.8, 1.2, 1.4, 1, 0.8, 0.4, 0, 1, 1, 0.4, 1.4, 1.2, 1.4, 0.8, 1, 0.8, 1, 1, 0.4, 0.8, 1, 1.2, 1.2, 0.8, 0.8],
    mar: [0.6, 0.8, 1, 1, 1, 0.4, 1.4, 0.4, 0.6, 0.4, 0.4, 1.2, 1.2, 0.6, 1, 1, 0, 0.8, 0.6, 0.8, 1.4, 0.8, 0.8, 1.2, 0.4, 0.4, 1.2, 0.6, 1, 0.4, 1, 0.8, 1, 0.8],
    men: [0.8, 0.4, 0.6, 1, 1, 1, 1, 1, 0.4, 0.6, 1, 1.2, 0.8, 1, 0.6, 1, 0.8, 0, 1.2, 1, 1, 1.2, 1.8, 1, 1, 1.2, 1, 1, 0.6, 1.4, 0.4, 1.8, 1, 0.4],
    mha: [0.6, 0.8, 1.2, 0.4, 1.2, 0.4, 0.8, 0.8, 0.8, 0.6, 0.8, 1.4, 1.4, 0.8, 0.8, 0.4, 0.6, 1.2, 0, 1.4, 1.6, 1.2, 0.4, 1.6, 0.4, 0.6, 1.4, 0.4, 1.2, 0.6, 1.2, 1.2, 0.8, 1],
    niv: [1.2, 1.4, 1, 1.2, 1.2, 1, 1.4, 0.4, 1.2, 1, 0.8, 0.4, 0.4, 0.8, 1, 1.4, 0.8, 1, 1.4, 0, 1.2, 0.4, 1.2, 0.4, 0.8, 1, 0.8, 1.2, 0.8, 0.8, 1.4, 0.4, 1.2, 1],
    ork: [1.4, 1.4, 0.8, 1.2, 0.8, 1.6, 1.2, 1.2, 1, 1.2, 1.2, 0.8, 0.8, 1.2, 0.8, 1.2, 1.4, 1, 1.6, 1.2, 0, 1.2, 1.2, 0.8, 1.4, 1.4, 0.4, 1.4, 0.4, 0.8, 1.4, 0.4, 1.2, 0.8],
    paa: [1.2, 1.4, 1, 1.4, 1.2, 1, 1.4, 0.4, 1.2, 1, 0.4, 0.8, 0.4, 1.2, 1, 1.4, 0.8, 1.2, 1.2, 0.4, 1.2, 0, 1.4, 0.8, 0.8, 1, 0.8, 1.2, 0.8, 0.8, 1.8, 0.8, 1.4, 1.2],
    ras: [1, 1.2, 0.8, 0.4, 0.8, 0.4, 1.2, 1, 1.2, 1, 1, 1.2, 1.6, 0.4, 0.8, 0.8, 0.8, 1.8, 0.4, 1.2, 1.2, 1.4, 0, 1.6, 0.8, 1.2, 1.2, 0.8, 1.2, 0.8, 1.6, 0.8, 0.8, 1.2],
    riv: [1.2, 1.2, 0.6, 1, 0.8, 1.4, 1, 0.8, 0.8, 1, 0.8, 0.4, 0.4, 1, 0.6, 1, 1.2, 1, 1.6, 0.4, 0.8, 0.8, 1.6, 0, 1.2, 1.2, 0.4, 1.2, 0.4, 0.8, 1.2, 0.8, 1, 0.6],
    shi: [0.4, 0.6, 1, 0.8, 1, 0.4, 1.2, 0.6, 0.6, 0.4, 0.6, 1.4, 1.4, 0.6, 1, 0.8, 0.4, 1, 0.4, 0.8, 1.4, 0.8, 0.8, 1.2, 0, 0.4, 1.2, 0.4, 1, 0.6, 1.2, 1, 1.4, 0.8],
    skr: [0.6, 0.8, 1, 1.2, 1.4, 0.8, 1.4, 0.6, 0.6, 0.4, 0.6, 1.4, 1.4, 1, 1, 1, 0.4, 1.2, 0.6, 1, 1.4, 1, 1.2, 1.2, 0.4, 0, 1.2, 0.6, 1, 0.6, 1.2, 1, 1.4, 1.2],
    sve: [1.2, 1.2, 0.6, 1, 0.8, 1.2, 1, 0.8, 0.8, 1, 0.8, 0.4, 0.4, 0.8, 0.6, 1, 1.2, 1, 1.4, 0.8, 0.4, 0.8, 1.2, 0.4, 1.2, 1.2, 0, 1.2, 0.4, 0.8, 1.2, 0.4, 1, 0.6],
    tha: [0.4, 0.6, 1, 0.8, 1.2, 0.4, 0.8, 0.8, 0.6, 0.4, 0.8, 1.4, 1.2, 0.8, 0.8, 0.4, 0.6, 1, 0.4, 1.2, 1.4, 1.2, 0.8, 1.2, 0.4, 0.6, 1.2, 0, 1, 0.6, 1, 1.2, 1.2, 0.8],
    tho: [1, 1, 0.4, 0.8, 0.4, 1.2, 0.8, 1.2, 0.6, 0.8, 0.8, 0.8, 0.4, 0.8, 0.4, 0.8, 1, 0.6, 1.2, 0.8, 0.4, 0.8, 1.2, 0.4, 1, 1, 0.4, 1, 0, 1.2, 1, 0.8, 0.8, 0.4],
    tob: [0.8, 1, 0.8, 0.8, 0.8, 0.4, 1.4, 0.4, 0.8, 0.6, 0.4, 0.8, 1.2, 0.4, 1.2, 1, 0.4, 1.4, 0.6, 0.8, 0.8, 0.8, 0.8, 0.8, 0.6, 0.6, 0.8, 0.6, 1.2, 0, 1.4, 0.4, 0.8, 1],
    wal: [0.8, 0.4, 1, 1.4, 1.4, 1.2, 1.4, 1.2, 0.4, 0.8, 1.2, 1.6, 1.2, 1.4, 1, 1.2, 1, 0.4, 1.2, 1.4, 1.4, 1.8, 1.6, 1.2, 1.2, 1.2, 1.2, 1, 1, 1.4, 0, 1.6, 1.4, 0.6],
    wei: [1.2, 1.4, 0.8, 0.8, 0.8, 0.8, 1.6, 0.4, 1.2, 1, 0.8, 0.4, 0.8, 0.4, 1.2, 1.2, 0.8, 1.8, 1.2, 0.4, 0.4, 0.8, 0.8, 0.8, 1, 1, 0.4, 1.2, 0.8, 0.4, 1.6, 0, 0.8, 1.4],
    zwe: [1.2, 1.4, 0.4, 0.4, 0.8, 0.8, 1.2, 1.2, 1, 1.2, 1.6, 1.2, 1.2, 0.4, 0.4, 0.8, 1, 1, 0.8, 1.2, 1.2, 1.4, 0.8, 1, 1.4, 1.4, 1, 1.2, 0.8, 0.8, 1.4, 0.8, 0, 1],
    zyk: [0.8, 0.8, 0.4, 0.8, 0.8, 1, 0.8, 1, 0.4, 0.6, 1, 1, 0.6, 0.8, 0.4, 0.8, 0.8, 0.4, 1, 1, 0.8, 1.2, 1.2, 0.6, 0.8, 1.2, 0.6, 0.8, 0.4, 1, 0.6, 1.4, 1, 0],
});
/* eslint-enable max-len -- Restore disabled rule. */

/**
 * Status effects for conditions and states that are not imported from the compendium entries.
 */
TDE5E.statusEffects = [
    {
        id: "bonus",
        icon: "systems/tde5e/assets/bonus.svg",
        label: "tde5e.genericEffects.bonus",
        flags: { tde5e: { isDefault: true } },
    },
    {
        id: "penalty",
        icon: "systems/tde5e/assets/penalty.svg",
        label: "tde5e.genericEffects.penalty",
        flags: { tde5e: { isDefault: true } },
    },
    {
        id: "rounds",
        icon: "systems/tde5e/assets/rounds.svg",
        label: "tde5e.genericEffects.rounds",
        flags: { tde5e: { isDefault: true } },
    },
    {
        id: "circleBlue",
        icon: "systems/tde5e/assets/circle_blue.svg",
        label: "tde5e.genericEffects.circleBlue",
        flags: { tde5e: { isDefault: true } },
    },
    {
        id: "circleRed",
        icon: "systems/tde5e/assets/circle_red.svg",
        label: "tde5e.genericEffects.circleRed",
        flags: { tde5e: { isDefault: true } },
    },
    {
        id: "circleGreen",
        icon: "systems/tde5e/assets/circle_green.svg",
        label: "tde5e.genericEffects.circleGreen",
        flags: { tde5e: { isDefault: true } },
    },
    {
        id: "circlePurple",
        icon: "systems/tde5e/assets/circle_purple.svg",
        label: "tde5e.genericEffects.circlePurple",
        flags: { tde5e: { isDefault: true } },
    },
    {
        id: "targeted",
        icon: "systems/tde5e/assets/targeted.svg",
        label: "tde5e.genericEffects.targeted",
        flags: { tde5e: { isDefault: true } },
    },
];

/* Default icon for a new inventoryContainer */
TDE5E.inventoryContainerIconsDefault = "fas fa-box-open";

/* Possible icons to select for a new inventoryContainer */
TDE5E.inventoryContainerIcons = [
    "fas fa-box-open", "fas fa-archive", "fas fa-th-large", "fas fa-toolbox", "fas fa-inbox", "fas fa-box",
    "fas fa-boxes", "fas fa-tools", "fas fa-screwdriver", "fas fa-hammer", "fas fa-feather-alt", "fas fa-atlas",
    "fas fa-bed", "fas fa-globe-americas", "fas fa-leaf", "fas fa-map", "fas fa-compass", "fas fa-drumstick-bite",
    "fas fa-broom", "fas fa-chair", "fas fa-cat", "fas fa-drum", "fas fa-drafting-compass", "fas fa-gem",
    "fas fa-fist-raised", "fas fa-landmark", "fas fa-balance-scale", "fas fa-briefcase-medical", "fas fa-hard-hat",
    "fas fa-hat-cowboy", "fas fa-utensils", "fas fa-brain",
];

/**
 * Lists of default items by CID for specific actor types.
 */
TDE5E.defaultAbilities = {
    skills: {
        npc: ["AKROBATIK", "ATHLETIK", "KRAFTAKT", "SELBSTBEHERRSCHUNG", "SINNESSCHARFE", "VERBERGEN", "ETIKETTE",
            "KLETTERN", "SCHWIMMEN", "MENSCHENKENNTNIS", "UBERREDEN", "WILLENSKRAFT", "EINSCHUCHTERN", "GEISSELWAFFEN",
            "KLINGENWAFFEN", "SCHLAGWAFFEN", "SCHLEUDERWAFFEN", "SCHUSSWAFFEN", "STANGENWAFFEN", "WUCHTWAFFEN"],
        creature: ["AKROBATIK", "ATHLETIK", "KRAFTAKT", "KLETTERN", "SCHWIMMEN", "SELBSTBEHERRSCHUNG",
            "SINNESSCHARFE", "VERBERGEN", "WILLENSKRAFT", "EINSCHUCHTERN", "SCHLAGWAFFEN"],
    },
    combat: {
        creature: ["EINFACHER_ANGRIFF", "EINFACHER_BLOCK", "HECHTSPRUNG", "BRAND_LOSCHEN", "WEGDUCKEN",
            "GEGNER_SCHUBSEN", "AUFSTEHEN_ODER_AUFHELFEN", "BEFREIEN", "BEWEGUNG", "NACHLADEN", "WAFFENWECHSEL",
            "SCHLACHTRUF", "WUCHTIGER_ANGRIFF", "TODLICHER_ANGRIFF"],
    },
};

/**
 * Every itemType that the system offers.
 * Can be used to identify, filter or set the type of items in the system.
 */
TDE5E.itemTypes = {
    Action: "action",
    Advantage: "advantage",
    Ammunition: "ammunition",
    Armor: "armor",
    ArtifactAbility: "artifactAbility",
    CombatAbility: "combatAbility",
    CombatTechnique: "combatTechnique",
    CommonAbility: "commonAbility",
    Condition: "condition",
    CreateType: "creatureType",
    Culture: "culture",
    Disadvantage: "disadvantage",
    EquipmentItem: "equipmentItem",
    InventoryItem: "inventoryItem",
    KarmaAbility: "karmaAbility",
    LanguageSpoken: "languageSpoken",
    LanguageWritten: "languageWritten",
    Liturgy: "liturgy",
    MagicalAbility: "magicalAbility",
    MeleeWeapon: "meleeWeapon",
    Race: "race",
    RangedWeapon: "rangedWeapon",
    Reaction: "reaction",
    Shield: "shield",
    Skill: "skill",
    Spell: "spell",
    SpellGlyphAbility: "spellGlyphAbility",
    State: "state",
    Tradition: "tradition",
    TraditionImprint: "traditionImprint",
};

TDE5E.prefixes = {
    meleeWeapon: {
        blunt: { breakingPointRating: -2, adrenaline: 0, damage: -1 },
        sluggish: { breakingPointRating: -2, adrenaline: -2, damage: 0 },
        unwieldy: { breakingPointRating: -2, adrenaline: -1, damage: 0 },
        crooked: { breakingPointRating: -2, adrenaline: 0, damage: 0 },
        heavy: { breakingPointRating: -1, adrenaline: -1, damage: 0 },
        short: { breakingPointRating: -1, adrenaline: 0, damage: 0 },
        long: { breakingPointRating: 1, adrenaline: 0, damage: 0 },
        light: { breakingPointRating: 1, adrenaline: 1, damage: 0 },
        relentless: { breakingPointRating: 2, adrenaline: 0, damage: 0 },
        balanced: { breakingPointRating: 2, adrenaline: 1, damage: 0 },
        agile: { breakingPointRating: 2, adrenaline: 2, damage: 0 },
        precise: { breakingPointRating: 2, adrenaline: 0, damage: 1 },
    },
    rangedWeapon: {
        blunt: { breakingPointRating: -2, adrenaline: 0, damage: -1 },
        sluggish: { breakingPointRating: -2, adrenaline: -2, damage: 0 },
        unwieldy: { breakingPointRating: -2, adrenaline: -1, damage: 0 },
        crooked: { breakingPointRating: -2, adrenaline: 0, damage: 0 },
        heavy: { breakingPointRating: -1, adrenaline: -1, damage: 0 },
        short: { breakingPointRating: -1, adrenaline: 0, damage: 0 },
        long: { breakingPointRating: 1, adrenaline: 0, damage: 0 },
        light: { breakingPointRating: 1, adrenaline: 1, damage: 0 },
        relentless: { breakingPointRating: 2, adrenaline: 0, damage: 0 },
        balanced: { breakingPointRating: 2, adrenaline: 1, damage: 0 },
        agile: { breakingPointRating: 2, adrenaline: 2, damage: 0 },
        precise: { breakingPointRating: 2, adrenaline: 0, damage: 1 },
    },
    shield: {
        fragile: { breakingPointRating: -3, adrenaline: 0 },
        unwieldy: { breakingPointRating: -1, adrenaline: -2 },
        heavy: { breakingPointRating: -1, adrenaline: 0 },
        thin: { breakingPointRating: 0, adrenaline: -1 },
        light: { breakingPointRating: 0, adrenaline: 1 },
        solid: { breakingPointRating: 1, adrenaline: 0 },
        balanced: { breakingPointRating: 2, adrenaline: 1 },
        agile: { breakingPointRating: 1, adrenaline: 2 },
        reinforced: { breakingPointRating: 3, adrenaline: 0 },
    },
    armor: {
        rigid: {
            breakingPointRating: -2, protection: 0, adrenaline: 0, encumbrance: 1,
        },
        sluggish: {
            breakingPointRating: -2, protection: 0, adrenaline: -2, encumbrance: 0,
        },
        uneven: {
            breakingPointRating: -2, protection: -1, adrenaline: -1, encumbrance: 0,
        },
        unstable: {
            breakingPointRating: -2, protection: -2, adrenaline: 0, encumbrance: 0,
        },
        heavy: {
            breakingPointRating: -1, protection: 0, adrenaline: -1, encumbrance: 0,
        },
        thin: {
            breakingPointRating: -1, protection: -1, adrenaline: 0, encumbrance: 0,
        },
        solid: {
            breakingPointRating: 1, protection: 1, adrenaline: 0, encumbrance: 0,
        },
        light: {
            breakingPointRating: 1, protection: 0, adrenaline: 1, encumbrance: 0,
        },
        reinforced: {
            breakingPointRating: 2, protection: 2, adrenaline: 0, encumbrance: 0,
        },
        balanced: {
            breakingPointRating: 2, protection: 1, adrenaline: 1, encumbrance: 0,
        },
        agile: {
            breakingPointRating: 2, protection: 0, adrenaline: 2, encumbrance: 0,
        },
        flexible: {
            breakingPointRating: 2, protection: 0, adrenaline: 0, encumbrance: -1,
        },
    },
};

/**
 * An object mapping a descriptive name to the internal name of the corresponding compendium.
 */
TDE5E.compendiums = {
    Skills: "tde5e.skills",
    Advantages: "tde5e.advantagesDisadvantages",
    Disadvantages: "tde5e.advantagesDisadvantages",
    CommonSA: "tde5e.mundaneSpecialAbilities",
    Conditions: "tde5e.conditions",
    States: "tde5e.states",
    Karma: "tde5e.karma",
    Magic: "tde5e.magic",
    Equipment: "tde5e.equipment",
    RaceCultureProfessions: "tde5e.raceCultureProfessions",
    Languages: "tde5e.languages",
    Combat: "tde5e.combat",
};

/**
 * Additional information for type based searches in the library.
 */
TDE5E.indexedProperties = {
    artifactAbility: {
        sources: ["tde5e.magic", "tde5e.karma"],
        additionalProperties: ["system.tradition"],
    },
    spellGlyphAbility: {
        sources: ["tde5e.magic"],
    },
    meleeWeapon: {
        sources: ["tde5e.equipment"],
        additionalProperties: ["system.group", "system.damage", "system.adrenaline", "system.application"],
    },
    rangedWeapon: {
        sources: ["tde5e.equipment"],
        additionalProperties: ["system.group", "system.damage", "system.adrenaline",
            "system.reloadTime", "system.application"],
    },
    shield: {
        sources: ["tde5e.equipment"],
        additionalProperties: ["system.breakingPointRating", "system.adrenaline"],
    },
    ammunition: {
        sources: ["tde5e.equipment"],
    },
    armor: {
        sources: ["tde5e.equipment"],
        additionalProperties: ["system.protection", "system.encumbrance", "system.adrenaline"],
    },
    state: {
        sources: ["tde5e.states", "tde5e.combat"],
    },
    condition: {
        sources: ["tde5e.conditions", "tde5e.combat"],
    },
    skill: {
        sources: ["tde5e.skills"],
        additionalProperties: ["system.check.attributes"],
    },
    combatTechnique: {
        sources: ["tde5e.skills"],
    },
    commonAbility: {
        sources: ["tde5e.mundaneSpecialAbilities"],
    },
    combatAbility: {
        sources: ["tde5e.combat"],
    },
    magicalAbility: {
        sources: ["tde5e.magic"],
    },
    karmaAbility: {
        sources: ["tde5e.karma"],
    },
    advantage: {
        sources: ["tde5e.advantagesDisadvantages"],
    },
    disadvantage: {
        sources: ["tde5e.advantagesDisadvantages"],
    },
    languageSpoken: {
        sources: ["tde5e.languages"],
    },
    languageWritten: {
        sources: ["tde5e.languages"],
    },
    spell: {
        sources: ["tde5e.magic"],
        additionalProperties: ["system.property", "system.improvement.column",
            "system.traditions", "system.checkedSkill"],
    },
    cantrip: {
        sources: ["tde5e.magic"],
        additionalProperties: ["system.property", "system.traditions"],
    },
    liturgy: {
        sources: ["tde5e.karma"],
        additionalProperties: ["system.aspects", "system.improvement.column", "system.traditions"],
    },
    blessing: {
        sources: ["tde5e.karma"],
        additionalProperties: ["system.traditions"],
    },
    inventoryItem: {
        sources: ["tde5e.equipment"],
    },
    equipmentItem: {
        sources: ["tde5e.equipment"],
    },
    race: {
        sources: ["tde5e.raceCultureProfessions"],
    },
    culture: {
        sources: ["tde5e.raceCultureProfessions"],
    },
    action: {
        sources: ["tde5e.combat"],
        additionalProperties: ["system.cost.resources.actionPoints",
            "system.prerequisites.combatStyle", "system.isAttack", "system.isRanged"],
    },
    reaction: {
        sources: ["tde5e.combat"],
        additionalProperties: ["system.cost.resources.actionPoints",
            "system.prerequisites.combatStyle", "system.target.category"],
    },
    tradition: {
        sources: ["tde5e.magic", "tde5e.karma"],
    },
    traditionImprint: {
        sources: ["tde5e.magic", "tde5e.karma"],
        additionalProperties: ["system.tradition"],
    },
    creatureType: {
        sources: ["tde5e.raceCultureProfessions"],
    },
};

/**
 * An object of itemTypes that are displayed in the inventory section of sheets.
 * For each type there is a boolean determining whether it has subtypes (e.g. groups) that have to be considered when
 *  accessing the items.
 */
TDE5E.inventoryRelatedItemTypes = {
    inventoryItem: false,
    armor: true,
    meleeWeapon: false,
    rangedWeapon: false,
    shield: false,
};

/**
 * Metadata for properties outside of the system's model.
 * @see TdeItem.getPropertyMetadata for valid configurations.
 */
TDE5E.flagMetadata = {
    "flags.tde5e.charges": { label: "tde5e.item.config.charges", locked: true },
    "flags.tde5e.charges.value": { label: "tde5e.item.config.charges", alwaysEditable: true },
};
