import TdeItem from "../items/item.js";
import * as util from "../util.js";

/**
 * Application used for searching documents in the world or compendiums.
 */
export default class EntityLibrary extends Application {
    /** Accepted document types for search in compendiums */
    static supportDocumentTypes = ["JournalEntry", "Item", "Actor"];

    /** The translated type to be searched for. */
    type;

    /** A map of translated types to their original value. */
    reverseTypes;

    /** The currently configured search parameters. */
    searchParams;

    /** The result of the most recently started search. */
    searchResult;

    /** The generator function of the most recently started search. */
    currentSearch;

    /** The amount of results to display. */
    resultCount = 20;

    /** The property path and descending flag for result sorting. */
    sortOptions;

    /** Fuzzy search terms. */
    fuzzySearch;

    /** Flag indicating whether system defined compendiums should be searched. */
    searchSystemCompendiums = true;

    /** Flag indicating whether user defined compendiums should be searched. */
    searchWorldCompendiums = true;

    /** Flag indicating whether world items & actors should be searched. */
    searchWorld = true;

    /**
     * Creates a searchable library of items, actors and journals.
     * @param {string} searchTerm An optional string that will be used to start an immediate fuzzy search.
     * @param {object} options The application options to override defaults with.
     */
    constructor(searchTerm = null, options = {}) {
        super(options);

        this.reverseTypes = Object.entries(game.i18n.translations.tde5e.types).reduce((types, [key, type]) => {
            if (typeof type === "object") {
                if (!type.name) return types;
                types[type.name] = { type: key, collection: "Item" };
            } else {
                types[type] = { type: key, collection: "Item" };
            }

            return types;
        }, {});

        this.reverseTypes[game.i18n.localize("tde5e.types.player")].collection = "Actor";
        this.reverseTypes[game.i18n.localize("tde5e.types.npc")].collection = "Actor";
        this.reverseTypes[game.i18n.localize("tde5e.types.creature")].collection = "Actor";
        this.reverseTypes[game.i18n.localize("DOCUMENT.JournalEntries")] = { type: null, collection: "JournalEntry" };

        if (searchTerm) this.fuzzySearch = searchTerm;
        this.prepareSearch();
    }

    /** @override */
    static get defaultOptions() {
        return foundry.utils.mergeObject(super.defaultOptions, {
            id: "library",
            template: "systems/tde5e/templates/library.hbs",
            resizable: true,
            width: 700,
            height: 865,
            classes: ["library"],
        });
    }

    /** @override */
    get title() {
        return game.i18n.localize("tde5e.library.title");
    }

    /**
     * Returns the reverse translated type to search for.
     * @returns {string} The type that should be searched for.
     */
    get searchType() {
        if (!this.type) return undefined;
        return this.reverseTypes[this.type];
    }

    /**
     * Extends Foundry's render method to display the current result set.
     * @override
     */
    async _render(force = false, options = {}) {
        await super._render(force, options);
        if (force) document.querySelector(`#${this.id} input[name='fuzzy-search']`).focus();
        this.refreshResults();
    }

    /** @override */
    getData() {
        const displayParams = foundry.utils.deepClone(this.searchParams);
        const sortParam = displayParams[this.sortOptions.index];
        sortParam.sortIcon = this._getSortIcon(sortParam);

        return {
            recentSearches: game.settings.get("tde5e", "recentSearches") ?? [],
            currentType: this.type,
            availableTypes: Object.keys(this.reverseTypes).sort(),
            fuzzySearch: this.fuzzySearch,
            searchSystemCompendiums: this.searchSystemCompendiums,
            searchWorldCompendiums: this.searchWorldCompendiums,
            searchWorld: this.searchWorld,
            searchProperties: displayParams,
        };
    }

    /**
     * Generates an icon that represents the current sorting options.
     * @param {object} param The search parameter to get the icon for.
     * @returns {string} An HTML string containing the icon.
     */
    _getSortIcon(param) {
        switch (param.type) {
        case "number":
            return ` <i class="fas fa-sort-numeric-${this.sortOptions.desc ? "up" : "down"}"></i>`;
        case "text":
            return ` <i class="fas fa-sort-alpha-${this.sortOptions.desc ? "up" : "down"}"></i>`;
        default:
            return ` <i class="fas fa-sort-amount-${this.sortOptions.desc ? "up" : "down"}"></i>`;
        }
    }

    /** @inheritdoc */
    activateListeners(html) {
        super.activateListeners(html);

        html.find("select[name='recent-searches']").change(ev => {
            const index = parseInt(ev.currentTarget.value, 10);
            this.loadSearch(index);
            this.prepareSearch();
            this.render(true);
        });

        html.find("a.new-window-search").click(ev => {
            ev.preventDefault();
            const id = `library-${foundry.utils.randomID()}`;
            new EntityLibrary(this.fuzzySearch, { id }).render(true);
        });

        html.find("a.reset-search").click(ev => {
            ev.preventDefault();
            this.clearSearch();
            this.prepareSearch();
            this.render(true);
        });

        html.find("input[name='fuzzy-search']").keyup(ev => {
            this.fuzzySearch = ev.currentTarget.value;
            this.search(this.searchType, this.searchParams, this.fuzzySearch);
        });

        // Rebuild parameters when the type changes.
        html.find("input[name='type-picker']").change(ev => {
            this.type = ev.currentTarget.value;
            this.prepareSearch();
            this.render(true);
        });

        // Refresh search parameters and start the search when any values change.
        html.on("keyup.library-search", "td > input", ev => {
            const input = ev.currentTarget;
            const param = this.searchParams.find(p => p.path === input.name);
            if (!param) return;

            param.value = input.value;
            if (param.value === "") delete param.value;
            else if (input.type === "number") param.value = input.valueAsNumber;
            else if (param.type === "text") param.values = this.parseQuotes(param.value.toLowerCase());

            const type = this.searchType;
            if (type || input.type === "number" || input.value.length > 1) {
                this.search(type, this.searchParams, this.fuzzySearch);
            }
        });

        html.find("input[name='system-compendium-search']").change(ev => {
            this.searchSystemCompendiums = ev.currentTarget.checked;
            this.prepareSearch();
        });
        html.find("input[name='world-compendium-search']").change(ev => {
            this.searchWorldCompendiums = ev.currentTarget.checked;
            this.prepareSearch();
        });
        html.find("input[name='world-search']").change(ev => {
            this.searchWorld = ev.currentTarget.checked;
            this.prepareSearch();
        });

        // Change sort options.
        html.on("click.library-sort", "th", ev => {
            const sortIndex = Array.prototype.indexOf.call(ev.currentTarget.parentElement.children, ev.currentTarget);
            if (this.sortOptions.index === sortIndex) this.sortOptions.desc = !this.sortOptions.desc;
            else this.sortOptions = { index: sortIndex, desc: false };
            if (this.searchResult?.value) this.searchResult.value.sorted = false;
            this.render();
        });
    }

    /**
     * Extends the Foundry's method to store the current search options locally.
     * @override
     */
    async close(options = {}) {
        this.storeSearch();
        return super.close(options);
    }

    /**
     * Persistently stores the current search settings in the user's settings.
     */
    storeSearch() {
        const search = {
            fuzzy: this.fuzzySearch,
            type: this.type,
            params: this.searchParams,
            sort: this.sortOptions,
            searchSystemCompendiums: this.searchSystemCompendiums,
            searchWorldCompendiums: this.searchWorldCompendiums,
            searchWorld: this.searchWorld,
        };

        const searchStrings = [];
        if (this.fuzzySearch) searchStrings.push(search.fuzzy);
        search.params.forEach(p => { if (p.value) searchStrings.push(p.value); });
        if (!searchStrings.length) return;

        search.title = searchStrings.join(" ");
        const recent = game.settings.get("tde5e", "recentSearches") ?? [];
        if (recent.some(s => s?.title === search.title)) return;

        recent.unshift(search);
        recent.length = Math.min(10, recent.length);
        game.settings.set("tde5e", "recentSearches", recent);
    }

    /**
     * Loads the search settings from the user's settings.
     * @param {number} index The index of the recent search. Defaults to 0, meaning the most recent search.
     */
    loadSearch(index = 0) {
        const search = (game.settings.get("tde5e", "recentSearches") ?? [])[index];
        if (!search) return;

        this.fuzzySearch = search.fuzzy;
        this.type = search.type;
        this.searchParams = search.params;
        this.sortOptions = search.sort;
        this.searchSystemCompendiums = search.searchSystemCompendiums ?? true;
        this.searchWorldCompendiums = search.searchWorldCompendiums ?? true;
        this.searchWorld = search.searchWorld ?? true;
    }

    /**
     * Resets all search options.
     */
    clearSearch() {
        this.fuzzySearch = "";
        this.type = "";
        this.searchParams = [];
        this.searchOptions = { index: 0, desc: false };
        this.searchSystemCompendiums = true;
        this.searchWorldCompendiums = true;
        this.searchWorld = true;
    }

    /**
     * Prepares the relevant properties and sources for the configured type.
     *  This may execute a search immediately if a type is set.
     */
    prepareSearch() {
        const type = this.searchType;

        // Always use name search.
        const properties = [{ path: "name", label: game.i18n.localize("tde5e.values.name"), type: "text" }];
        const sources = [];
        if (type) {
            // Type dependent search columns.
            const searchInfo = TDE5E.indexedProperties[type.type] ?? {};
            if (game.i18n.translations.tde5e?.types[type.type]?.name) {
                properties.push({
                    path: "system.group",
                    label: game.i18n.localize("tde5e.combat.group"),
                    type: "text",
                    transform: v => game.i18n.localize(`tde5e.types.${type.type}.${v}`),
                });
            }
            if (searchInfo.additionalProperties) {
                // Fetch property metadata.
                properties.push(...searchInfo.additionalProperties.map(p => {
                    const metadata = util.getGlobalMetadata(type, p);
                    metadata.path = p;
                    return metadata;
                }));
            }

            if (this.searchWorld) {
                // Add associated world collection.
                sources.push(game.collections.get(type.collection));
            }

            if (this.searchSystemCompendiums) {
                // Add compendiums defined by type.
                searchInfo.sources?.forEach(pack => {
                    pack = game.packs.get(pack);
                    if (pack?.visible) sources.push(pack);
                });
            }

            if (this.searchWorldCompendiums) {
                // Add user created compendiums.
                game.packs.forEach(pack => {
                    if (pack.documentName === type.collection
                        && !pack.collection.startsWith("tde5e.")
                        && pack.visible) {
                        sources.push(pack);
                    }
                });
            }
        } else {
            // Search everywhere.
            properties.push({ path: "type", label: game.i18n.localize("tde5e.library.typeName"), type: "text" });
            if (this.searchWorld) {
                sources.push(game.actors, game.items, game.journal);
            }

            if (this.searchSystemCompendiums || this.searchWorldCompendiums) {
                game.packs.forEach(pack => {
                    if (pack.collection.startsWith("tde5e.")) {
                        if (!this.searchSystemCompendiums) return;
                    } else if (!this.searchWorldCompendiums
                        || !this.constructor.supportDocumentTypes.includes(pack.documentName)) {
                        return;
                    }
                    if (pack.visible) sources.push(pack);
                });
            }
        }

        if (this.searchParams) {
            // Transfer values and sorting from previous search if the parameters haven't changed.
            this.searchParams.forEach((p, i) => {
                if (!util.hasValue(p.value)) return;
                const match = p.path === properties[i]?.path && p.type === properties[i].type;
                if (match) {
                    properties[i].value = p.value;
                    if (p.values) properties[i].values = p.values;
                } else if (this.sortOptions?.index === i) {
                    this.sortOptions = { index: 0, desc: false };
                }
            });
        }

        this.sortOptions ??= { index: 0, desc: false };
        this.searchParams = properties;
        this.searchSources = sources;

        // Reset search options & results.
        this.searchResult = { value: [], done: true };
        this.resultCount = 20;

        this.search(type, properties, this.fuzzySearch);
    }

    /**
     * Splits the given value at spaces, except when surrounded by quotes. Escaped quotes are ignored and unescaped.
     * @see {@link https://stackoverflow.com/a/46946633}
     * @param {string} value The string to parse.
     * @returns {string[]} An array of search terms.
     */
    parseQuotes(value) {
        return value.match(/\\?.|^$/g).reduce((p, c) => {
            // eslint-disable-next-line no-bitwise -- Intentional bit operation.
            if (c === "\"") p.quote ^= 1;
            else if (!p.quote && c === " ") p.a.push("");
            else p.a[p.a.length - 1] += c.replace(/\\(.)/, "$1");
            return p;
        }, { a: [""] }).a;
    }

    /**
     * Starts a search for documents of the given type or any supported type
     *  with the given parameters.
     * @param {string?} type The document type to search for.
     * @param {object[]} params The parameters for the search.
     * @param {string[]?} fuzzy Fuzzy search input to match in addition to parameters.
     */
    search(type, params, fuzzy = null) {
        if (fuzzy) fuzzy = this.parseQuotes(fuzzy.toLowerCase());
        if (this.currentSearch) this.currentSearch.return([]); // Cancel the current search.

        /**
         * Executes the next iteration of the current search.
         */
        async function resume() {
            if (!this.currentSearch) return;
            this.searchResult = await this.currentSearch.next();
            if (this.searchResult.value) this.displayResults(this.searchResult.value, !this.searchResult.done);
            if (!this.searchResult.done) setTimeout(resume.bind(this));
            else this.currentSearch = null;
        }

        this.displayResults([], true);
        foundry.utils.debounce(this.storeSearch.bind(this), 5000)();
        setTimeout(() => {
            // Make sure that we have enough parameters to search with.
            params = foundry.utils.deepClone(params);
            if (!params.filter(param => param.value === 0 || param.value).length && !type && !fuzzy) {
                this.displayResults([]);
                return;
            }

            this.currentSearch = this.performSearch(type, params, fuzzy);
            resume.bind(this)();
        });
    }

    /**
     * Creates a generator that will perform the search in intervals and yield
     *  to allow other tasks to run.
     * @param {string?} type The document type to search for.
     * @param {object[]} params The parameters for the search.
     * @param {string[]?} fuzzy Fuzzy search input to match in addition to parameters.
     * @yields {object} A single matching result of the search.
     * @returns {Function} A generator function that yields every 1000 entries.
     */
    async* performSearch(type, params, fuzzy) {
        const sources = this.searchSources;
        const result = [];
        let processedCount = 0;

        for (const source of sources) {
            let candidates = source;
            if (source instanceof CompendiumCollection) {
                // Skip user compendiums that don't contain the target type.
                if (type && !source.collection.startsWith("tde5e.")
                    && !source.index.some(entry => entry.type === type.type)) continue;

                // Load the index with custom fields if it wasn't already.
                if (source.documentName === "Item") {
                    candidates = await source.getIndex({ fields: params.map(p => p.path) });
                } else candidates = await source.getIndex();
            }

            for (const candidate of candidates.values()) {
                // Check the target type.
                if (type?.type && candidate.type !== type.type) continue;

                // Skip search for inaccessible documents.
                if (candidate instanceof foundry.abstract.Document
                    && !candidate.testUserPermission(game.user, "OBSERVER")) continue;

                // Check if the candidate matches the search parameters.
                try {
                    const match = this._matchCandidate(candidate, source, params, fuzzy);
                    if (match) {
                        result.push(match);
                        result.sorted = false;
                    }
                } catch (err) {
                    util.warn(`Item ${candidate.uuid ?? candidate._id} could not be searched: ${err.message}`);
                }

                if (++processedCount % 500 === 0) yield result;
            }
        }

        return result;
    }

    /**
     * Creates an array of display values for each searchable property of the
     *  given candidate.
     * Returns null if the values don't match the search parameters.
     * @param {object} candidate The candidate to create a match result for.
     * @param {CompendiumCollection | Map<string, object>} source The source that contains the candidate.
     * @param {object[]} params The parameters for the search.
     * @param {string[]?} fuzzy Fuzzy search input to match in addition to parameters.
     * @returns {object[]?} Null if the candidate doesn't match the search parameters, an array
     *  containing a value/display object for the parameters otherwise.
     */
    _matchCandidate(candidate, source, params, fuzzy) {
        const result = [];
        for (const param of params) {
            let value = foundry.utils.getProperty(candidate, param.path) ?? "";

            if (param.path === "name") {
                // For the name, add a link instead of the string.
                if (!param.value || param.values.every(p => value?.toLowerCase().includes(p))) {
                    let displayName;
                    if (source instanceof CompendiumCollection) {
                        displayName = TdeItem.createCompendiumLink(`${source.collection}.${candidate._id}`, true);
                    } else if (candidate instanceof foundry.abstract.Document) {
                        displayName = candidate.toAnchor({
                            attrs: { draggable: true },
                            classes: ["content-link"],
                            icon: "fas fa-suitcase",
                        }).outerHTML;
                    } else {
                        displayName = candidate.displayName ?? candidate.name;
                    }

                    result.push({
                        value,
                        display: displayName,
                    });

                    continue;
                } else {
                    return null;
                }
            } else if (param.path === "type") {
                // For the type, try appending the group name.
                const typeName = util.localizeType(value);
                const group = candidate.system?.group;
                let groupName;
                if (!group) {
                    groupName = "";
                } else if (typeof (group) === "object" && group.name) {
                    groupName = group.name;
                } else {
                    groupName = foundry.utils.getProperty(
                        game.i18n.translations,
                        `tde5e.types.${value}.${candidate.system.group}`,
                    );
                }

                value = groupName && groupName !== typeName ? `${typeName} (${groupName})` : typeName;
            }

            let sortValue;
            if (!Array.isArray(value)) {
                if (param.type === "number") sortValue = value; // Retain original value for sorting.
                value = [value];
            }
            if (param.transform) value = value.map(v => param.transform(v));

            let match = !(param.value === 0 || param.value);
            if (!match) {
                if (param.type === "text") {
                    if (value) {
                        // Make sure that every search term is contained in any of the values.
                        match = param.values.every(p => value.some(v => v?.toLowerCase().includes(p)));
                    }
                } else {
                    match = value.some(v => v === param.value);
                }
            }

            if (match) result.push({ value: sortValue, display: value.join(", ") });
            else return null;
        }

        if (fuzzy?.length) {
            let values = result.map(v => v.display);
            if (candidate.flags?.tde5e?.tags) values = values.concat(candidate.flags.tde5e.tags);
            if (candidate.system?.variant?.available) {
                values = values.concat(candidate.system.variant.available.map(v => v.name));
            }
            if (!fuzzy.every(k => values.some(v => v.toLowerCase().includes(k)))) return null;
        }
        return result;
    }

    /**
     * Shortcut for calling {@link displayResults} with currently stored values.
     */
    refreshResults() {
        this.displayResults(this.searchResult?.value ?? [], !this.searchResult?.done);
    }

    /**
     * Sorts the given search results based on the currently configured options.
     * @param {object[]} results The results of the search.
     */
    sortResults(results) {
        results.sort((a, b) => {
            let valueA = a[this.sortOptions.index];
            valueA = valueA.value ?? valueA.display;
            let valueB = b[this.sortOptions.index];
            valueB = valueB.value ?? valueB.display;

            if (this.sortOptions.index !== 0 && valueA === valueB) {
                // Value is even, sort by name instead.
                valueA = a[0].value;
                valueB = b[0].value;
            }

            if (typeof valueA === "string" || typeof valueB === "string") {
                valueA ??= "";
                valueB ??= "";
                return this.sortOptions.desc ? valueB.localeCompare(valueA) : valueA.localeCompare(valueB);
            }

            if (this.sortOptions.desc) {
                return valueB < valueA ? -1 : 1;
            }

            return valueA < valueB ? -1 : 1;
        });
        results.sorted = true;
    }

    /**
     * Replaces the currently displayed result table with the given results.
     * @param {object[]} results The results of the search.
     * @param {boolean} partial Flag indicating whether the search is complete.
     */
    displayResults(results, partial = false) {
        const html = this.element;
        const table = html.find("table");

        // Show or hide search indicator.
        const searchIndicator = html.find("div.loading-spinner");
        if (partial) searchIndicator.show();
        else searchIndicator.hide();

        // Remove current results.
        table.find("tr").slice(2).remove();

        // Only display the first 20.
        let droppedResults = 0;
        if (!results.sorted) this.sortResults(results);
        if (results.length > this.resultCount) {
            droppedResults = results.length - this.resultCount;
            results = results.slice(0, this.resultCount);
        }

        if (results.length) {
            // Create result HTML.
            let resultString = "";
            for (const result of results) {
                resultString += `<tr>${result.map(r => `<td>${r.display}</td>`).join("")}</tr>`;
            }
            table.append(resultString);
        } else if (!partial) {
            table.append(`<tr><td colspan='10'>${game.i18n.localize("tde5e.general.none")}</td></tr>`);
        }

        if (droppedResults) {
            // Indicate that additional results have been omitted.
            const showMore = game.i18n.format("tde5e.library.showMore", {
                addCount: Math.min(20, droppedResults),
                droppedCount: droppedResults,
            });
            table.append(`<tr><td colspan="10">
                <input class="more-results" type="button" value="${showMore}">
            </td></tr>`);

            // Display additional results when clicking the associated button.
            table.find("input.more-results").click(() => {
                this.resultCount += 20;
                this.refreshResults();
            });
        }
    }

    /**
     * Extends the rendered compendium directory to include a button that opens
     *  a library.
     * @param {SidebarDirectory} _app The compendium directory that is being rendered.
     * @param {jQuery.Element} html The HTML element of the directory.
     */
    static extendCompendiumDirectory(_app, html) {
        const header = html.find(".header-actions.action-buttons");
        header.removeClass("flexrow").addClass("flexcol");
        header.append(`<button class='open-library' type='submit'>
            <i class='fas fa-search'></i> ${game.i18n.localize("tde5e.library.title")}
        </button>`);
        header.find("button.open-library").click(() => new EntityLibrary().render(true));
    }

    /**
     * Adds listeners to the given HTML to open a search when clicking on tag links.
     * @param {jQuery.Element} html The HTML element to add the listener on.
     */
    static addTagListener(html) {
        html.on("click", "a.tag-search", async event => {
            event.preventDefault();
            const currentSearch = Object.values(ui.windows).find(app => app instanceof EntityLibrary && app.rendered);
            if (currentSearch) {
                // Append the tag and refresh the current search.
                currentSearch.fuzzySearch = currentSearch.fuzzySearch
                    ? `${currentSearch.fuzzySearch} ${event.currentTarget.innerText}`
                    : event.currentTarget.innerText;
                currentSearch.prepareSearch();
                currentSearch.render(true);
            } else {
                // Create a new search with the tag.
                new EntityLibrary(event.currentTarget.innerText).render(true);
            }
        });
    }
}
