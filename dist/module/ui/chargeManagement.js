import * as util from "../util.js";

/**
 * The local collection for all items that recharge automatically.
 * @type {foundry.utils.Collection}
 */
const trackedItems = new foundry.utils.Collection();

/**
 * Registers hooks for rendering charge configuration controls on the item sheet.
 */
export function initializeChargeConfiguration() {
    Hooks.on("renderTdeBaseItemSheet", (sheet, html) => {
        if (!sheet.item.hasCharges) return;
        renderTemplate("systems/tde5e/templates/items/components/charge-block.hbs", {
            data: sheet.item._source.flags.tde5e.charges,
            options: sheet.options,
        }).then(controls => {
            const el = document.createElement("div");
            el.innerHTML = controls;
            controls = el.firstElementChild;
            sheet.processFieldset(controls);
            html[0].querySelector("div.item-content").append(controls);
        });
    });
}

/**
 * Registers hooks for maintaining information about self recharging items.
 */
export function initializeRecharge() {
    Hooks.on("updateItem", (item, changes, options) => {
        if (!item.isOwner) return;

        const quantityInfo = item.quantityInformation;
        if (!quantityInfo) return;

        const newValue = foundry.utils.getProperty(changes, quantityInfo.path);
        if (!util.hasValue(newValue)) return;

        // Render entire hotbar if the recharge type changed.
        if (foundry.utils.getProperty(changes, "flags.tde5e.charges.recharge.type")) {
            ui.hotbar.render();
            return;
        }

        // Find hotbar slot associated with item.
        const slot = ui.hotbar._getMacrosByPage(ui.hotbar.page)
            .find(m => m.macro?.getFlag("tde5e", "sourceItem") === item.uuid);
        if (slot) {
            const el = ui.hotbar.element.find(`li.macro[data-macro-id="${slot.macro.id}"] button.consume-charge`);

            // Ensures that multiple copies of a hotbar are handled (see Monks Hotbar Extension)
            el.each((_, line) => {
                $(line).contents().last().replaceWith(newValue.toString());
            });

            if (options.autoRecharge) {
                el.addClass("recharged");
                setTimeout(() => el.removeClass("recharged"), 5000);
            }
        }

        if (options.autoRecharge) {
            util.info(game.i18n.format(
                "tde5e.integration.simpleCalendar.rechargeNotification",
                {
                    item: item.name,
                    charges: options.autoRecharge,
                },
            ), true);
        }
    });
}

/**
 * Registers hooks for automatically recharging items based on Simple Calendar timestamp changes.
 */
export function initializeCalendarRecharge() {
    Hooks.once("simple-calendar-ready", () => {
        Hooks.on("preUpdateItem", logLastUse);
    });

    Hooks.once("simple-calendar-primary-gm", () => {
        // Build collection of relevant items and keep it up to date.
        refreshTrackedItems();
        Hooks.on("updateItem", (item, changes) => {
            if (!changes.flags?.tde5e) return;
            if (foundry.utils.hasProperty(changes.flags.tde5e, "charges.recharge.type")) {
                trackedItems.set(item.uuid, item);
            } else if ("-=charges" in changes.flags.tde5e) {
                trackedItems.delete(item.uuid);
            }
        });
        Hooks.on("createItem", item => {
            if (shouldTrackItem(item)) trackedItems.set(item.uuid, item);
        });
        Hooks.on("deleteItem", item => {
            trackedItems.delete(item.uuid);
        });

        Hooks.on("simple-calendar-date-time-change", rechargeItems);
    });
}

/**
 * Replaces the item collection with a new list of all rechargeable items that are found within actors owned by the
 *  current user.
 */
function refreshTrackedItems() {
    trackedItems.clear();
    let items = [].concat(...util.getOwnedActors().map(actor => actor.items.filter(item => shouldTrackItem(item))));
    items = items.concat(game.items.filter(item => shouldTrackItem(item)));
    items.forEach(item => trackedItems.set(item.uuid, item));
}

/**
 * Checks if the given item should be tracked for recharging its quantity.
 * @param {TdeItem} item The item to check.
 * @param {object} quantityInfo An optional quantity information object. If empty, it will be resolved from the item.
 * @returns {boolean} True if the item should be tracked, false otherwise.
 */
function shouldTrackItem(item, quantityInfo = null) {
    quantityInfo ??= item.quantityInformation;
    return quantityInfo?.recharge?.type && quantityInfo.recharge.type !== "manual";
}

/**
 * Logs the timestamp of a consumed charge when the charges of the given item
 *  are being modified.
 * @param {TdeItem} item The item that is being updated.
 * @param {object} changes The changes of the update.
 */
function logLastUse(item, changes) {
    const quantityInfo = item.quantityInformation;
    if (!quantityInfo) return;

    const rechargeType = quantityInfo.recharge.type;
    if (!rechargeType || !rechargeType.startsWith("afterUsage")) return; // Skip charges with manual or fixed mode.

    // Skip update if the charge value hasn't changed.
    const newValue = foundry.utils.getProperty(changes, quantityInfo.path);
    if (!util.hasValue(newValue) || quantityInfo.value === newValue) return;

    const lastUse = quantityInfo.lastUse ?? [];
    if (rechargeType === "afterUsage") {
        // Not parallel, only store oldest date.
        if (lastUse.length > 0) return;
        if (newValue < quantityInfo.value) {
            foundry.utils.setProperty(changes, "flags.tde5e.charges.lastUse", [SimpleCalendar.api.timestamp()]);
        } else if (newValue >= quantityInfo.max) {
            foundry.utils.setProperty(changes, "flags.tde5e.charges.lastUse", []);
        }
    } else {
        // Add timestamp for every consumed charge.
        if (newValue < quantityInfo.value) {
            const now = SimpleCalendar.api.timestamp();
            for (let i = quantityInfo.value; i > newValue; i--) lastUse.push(now);
        }

        // Make sure that we only keep as many timestamps as there are charges missing.
        const maxEntries = quantityInfo.max - newValue;
        foundry.utils.setProperty(changes, "flags.tde5e.charges.lastUse", lastUse
            .slice(-maxEntries) // Drop entries at the front.
            .sort((a, b) => a - b)); // Sort numerically ascending.
    }
}

/**
 * Finds items that should be recharged after a time change and updates their charge values.
 * @param {object} data The data of the time change,
 *  @see {@link https://github.com/vigoren/foundryvtt-simple-calendar/blob/main/docs/Hooks.md#datetime-change}.
 */
async function rechargeItems(data) {
    if (data.diff <= 0) return; // Skip backwards or unchanged time.

    for (const item of trackedItems.values()) {
        try {
            // Skip items with full or no charges.
            const quantityInfo = item.quantityInformation;
            if (!quantityInfo || quantityInfo.value === quantityInfo.max) continue;

            const now = SimpleCalendar.api.timestamp();
            const interval = (quantityInfo.recharge.interval ?? 0) * 3600; // Convert hours to seconds.
            if (interval === 0) continue; // Skip invalid interval.

            let restoredCharges = 0;
            let newLastUse;
            switch (quantityInfo.recharge.type) {
            case "fixedInterval": {
                const intervalsBefore = Math.floor((now - data.diff) / interval);
                const intervalsAfter = Math.floor(now / interval);
                if (intervalsBefore === intervalsAfter) continue; // Didn't pass any intervals.

                const rolls = new Array(intervalsAfter - intervalsBefore).fill(quantityInfo.recharge.amount).join(",");
                const roll = await new Roll(`{${rolls}}`).evaluate();
                restoredCharges = roll.total;
                break;
            }
            case "afterUsage": {
                const firstUse = (quantityInfo.lastUse ?? [now])[0];
                if (firstUse + interval > now) continue; // Oldest usage is too recent.

                const passedIntervals = Math.floor((now - firstUse) / interval);
                const rolls = new Array(passedIntervals).fill(quantityInfo.recharge.amount).join(",");
                const roll = await new Roll(`{${rolls}}`).evaluate();
                restoredCharges = roll.total;

                newLastUse = quantityInfo.value + restoredCharges < quantityInfo.max
                    ? [interval * passedIntervals + firstUse] : [];
                break;
            }
            case "afterUsageParallel": {
                const lastUse = quantityInfo.lastUse ?? [];
                if (!lastUse.length) continue; // Skip items without usage timestamps.

                restoredCharges = lastUse.filter(t => t <= now - interval).length;
                if (!restoredCharges) continue;
                newLastUse = lastUse.slice(restoredCharges);
                break;
            }
            default:
                continue; // Skip items with manual recharge.
            }

            restoredCharges = Math.clamp(restoredCharges, -quantityInfo.value, quantityInfo.max - quantityInfo.value);
            if (restoredCharges) {
                const update = { [quantityInfo.path]: quantityInfo.value + restoredCharges };
                if (newLastUse) update["flags.tde5e.charges.lastUse"] = newLastUse;
                item.update(update, { autoRecharge: restoredCharges });
            }
        } catch (ex) {
            util.error(`Item ${item.name} could not be recharged automatically.`, ex);
        }
    }
}

/**
 * Debug function to reset the last use timestamps of all tracked items.
 */
export async function resetCharges() {
    if (!game.modules.get("foundryvtt-simple-calendar")?.active) {
        util.warn("Simple Calendar module not enabled. Item charges cannot be reset.", true);
        return;
    }

    const now = SimpleCalendar.api.timestamp();
    for (const item of trackedItems.values()) {
        const quantityInfo = item.quantityInformation;
        let newLastUse = null;
        switch (quantityInfo.recharge.type) {
        case "afterUsage":
            newLastUse = quantityInfo.value < quantityInfo.max ? [now] : [];
            break;
        case "afterUsageParallel":
            newLastUse = quantityInfo.value < quantityInfo.max
                ? new Array(quantityInfo.max - quantityInfo.value).fill(now)
                : [];
            break;
        default: break;
        }

        if (newLastUse) await item.update({ "flags.tde5e.charges.lastUse": newLastUse });
        else if (quantityInfo.lastUse) await item.update({ "flags.tde5e.charges.-=lastUse": null });
    }
}
