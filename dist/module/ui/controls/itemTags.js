import { localizeType } from "../../util.js";
import ExtendedDocumentTagsElement from "./documentTags.js";

/**
 * A custom HTMLElement used to render a set of associated Items referenced by UUID.
 */
export default class ItemTagsElement extends ExtendedDocumentTagsElement {
    /** @override */
    static tagName = "item-tags";

    /** @inheritdoc */
    get type() {
        return "Item";
    }

    // eslint-disable-next-line no-empty-function -- Overrides parent setter.
    set type(_value) {}

    /**
     * Restrict the element to a particular item type.
     * @type {string|null}
     */
    get itemType() {
        return this.getAttribute("type");
    }

    set itemType(value) {
        if (!value) this.removeAttribute("type");
        if (!CONFIG.Item.types.includes(value)) {
            throw new Error(`"${value}" is not a valid Item type`);
        }
        this.setAttribute("type", value);
    }

    /**
     * Restrict the element to a particular item group.
     * @type {string|null}
     */
    get itemGroup() {
        return this.getAttribute("group");
    }

    set itemGroup(value) {
        if (!value) this.removeAttribute("group");
        this.setAttribute("group", value);
    }

    /** @override */
    _buildElements() {
        const elements = super._buildElements();
        if (this.editable) {
            const { itemType, itemGroup } = this;
            let typeLabel;
            if (itemType) {
                if (itemGroup) typeLabel = game.i18n.localize(`tde5e.types.${itemType}.${itemGroup}`);
                if (!typeLabel || typeLabel.startsWith("tde5e.")) typeLabel = localizeType(itemType);
            } else {
                typeLabel = "Items";
            }

            this._placeholder.innerHTML = `<i class='fas fa-sign-in-alt'></i> ${
                game.i18n.format("tde5e.documentTags.dropHint", { type: typeLabel })}`;
        }

        return elements;
    }

    /** @inheritdoc */
    _validateDocument(item) {
        const { itemType, itemGroup } = this;
        if (itemType && item.type !== itemType) {
            throw new Error(game.i18n.format("tde5e.documentTags.badItemType", { type: localizeType(itemType) }));
        }
        if (itemGroup && item.system?.group !== itemGroup) {
            throw new Error(game.i18n.format("tde5e.documentTags.badGroup", {
                group: game.i18n.localize(`tde5e.types.${itemType}.${itemGroup}`),
            }));
        }
    }
}
