import { stopEvent } from "../../jsUtils.js";

/**
 * Replacement for the HTMLProseMirrorElement.
 */
export default class ExtendedProseMirrorElement extends foundry.applications.elements.AbstractFormInputElement {
    constructor() {
        super();

        // Initialize raw content
        this._setValue(this.getAttribute("value") || "");
        this.removeAttribute("value");

        // Initialize enriched content
        this._toggled = this.hasAttribute("toggled");
    }

    /** @override */
    static tagName = "prose-mirror-ext";

    /**
     * Is the editor in active edit mode?
     * @type {boolean}
     */
    _active = false;

    /**
     * The ProseMirror editor instance.
     * @type {ProseMirrorEditor}
     */
    _editor;

    /**
     * Current editor contents
     * @type {HTMLDivElement}
     */
    _content;

    /**
     * Does this editor function via a toggle button? Or is it always active?
     * @type {boolean}
     */
    _toggled;

    /**
     * An optional edit button which activates edit mode for the editor
     * @type {HTMLButtonElement|null}
     */
    _button = null;

    /**
     * Reveal secret text blocks to the user.
     * @type {boolean}
     */
    get showSecrets() {
        return game.user.isGM || this.hasAttribute("secrets");
    }

    set showSecrets(value) {
        this.toggleAttribute("secrets", value === true);
    }

    /* -------------------------------------------- */

    /** @override */
    disconnectedCallback() {
        this._editor?.destroy();
    }

    /** @inheritdoc */
    _getValue() {
        if (!this._toggled) this._serializeValue();
        return this._value;
    }

    /* -------------------------------------------- */

    /** @override */
    _buildElements() {
        this.classList.add("editor", "prosemirror", "inactive");
        const elements = [this._buildContentContainer()];
        if (this._toggled) {
            this._button = document.createElement("button");
            this._button.type = "button";
            this._button.className = "icon toggle";
            this._button.innerHTML = "<i class=\"fas fa-edit\"></i>";
            elements.push(this._button);
        }
        return elements;
    }

    _buildContentContainer() {
        this._content = document.createElement("div");
        this._content.className = "editor-content";
        return this._content;
    }

    /* -------------------------------------------- */

    /** @override */
    async _refresh() {
        if (!this._toggled) await this._activateEditor();
        if (!this._active) await this._prepareContent();
    }

    /* -------------------------------------------- */

    /** @override */
    _activateListeners() {
        if (this.disabled) this._content.addEventListener("click", this._onClickContent.bind(this));
        if (this._toggled) this._button.addEventListener("click", this._onClickButton.bind(this));
    }

    /**
     * Handle link clicks to bypass the disabled state.
     * @param {PointerEvent} event The event of the click.
     */
    _onClickContent(event) {
        const link = event.target.closest("a[data-link]");
        if (!link) return;

        stopEvent(event);

        const bypassEvent = new Event("click");
        Object.defineProperty(bypassEvent, "target", { writable: false, value: event.target });
        Object.defineProperty(bypassEvent, "currentTarget", { writable: false, value: link });
        TextEditor._onClickContentLink(bypassEvent);
    }

    /* -------------------------------------------- */

    /**
     * Activate the ProseMirror editor.
     * @returns {Promise<void>} A promise representing the content preparation.
     */
    async _activateEditor() {
        if (this._active || this.disabled) return;
        this._active = true;

        // If the editor was toggled, replace with raw editable content
        if (this._toggled) this._content.innerHTML = this._value;

        // Create the TextEditor instance
        try {
            const document = await fromUuid(this.dataset.documentUUID);
            this._editor = await TextEditor.create({
                engine: "prosemirror",
                plugins: this._configurePlugins(),
                fieldName: this.name,
                collaborate: this.hasAttribute("collaborate"),
                target: this._content,
                document,
            }, this._getValue());
        } catch {
            this._active = false;
            await this._prepareContent();
        }

        if (this._active) {
            if (this._button) this._button.innerHTML = "<i class=\"fas fa-floppy-disk-circle-arrow-right\"></i>";
            this.classList.add("active");
            this.classList.remove("inactive");
        }
    }

    /**
     * Deactivates the ProseMirror editor.
     */
    _deactivateEditor() {
        if (!this._active) return;
        if (this._button) this._button.innerHTML = "<i class=\"fas fa-edit\"></i>";
        this._active = false;
        this._editor?.destroy();
        this.classList.remove("active");
        this.classList.add("inactive");

        // Rebuild the content that was removed by editor destruction.
        this.prepend(this._buildContentContainer());
        this._prepareContent();
    }

    /**
     * Enriches the editor's value and replaces the rendered content.
     */
    async _prepareContent() {
        const content = await TextEditor.enrichHTML(this._value, { secrets: this.showSecrets });
        if (!this._active) {
            this._content.innerHTML = content;
            this._repositionWindow();
        }
    }

    /**
     * Recalculates the height of the parent application, if one exists.
     */
    _repositionWindow() {
        const app = ui.windows[this.closest(".window-app")?.dataset.appid];
        if (app && !app.options.height) app.setPosition({ height: "auto" });
    }

    /* -------------------------------------------- */

    /**
     * Configure ProseMirror editor plugins.
     * @returns {Record<string, Plugin>} A map of configured plugins.
     * @protected
     */
    _configurePlugins() {
        return {
            menu: ProseMirror.ProseMirrorMenu.build(ProseMirror.defaultSchema, {
                destroyOnSave: this._toggled,
                onSave: this._save.bind(this),
            }),
            keyMaps: ProseMirror.ProseMirrorKeyMaps.build(ProseMirror.defaultSchema, {
                onSave: this._save.bind(this),
            }),
        };
    }

    /* -------------------------------------------- */

    /**
     * Handle clicking the editor activation button.
     * @param {PointerEvent} event  The triggering event.
     */
    _onClickButton(event) {
        event.preventDefault();
        if (this._active) this._save();
        else this._activateEditor();
    }

    /* -------------------------------------------- */

    /**
     * Serializes and stores the currently displayed content of the editor.
     */
    _serializeValue() {
        if (!this._editor) return;
        const value = ProseMirror.dom.serializeString(this._editor.view.state.doc.content);
        if (value === this._value) return;
        this._setValue(value);
    }

    /**
     * Handle saving the editor content.
     */
    _save() {
        this._serializeValue();
        this.dispatchEvent(new Event("change", { bubbles: true, cancelable: true }));
        if (this._toggled) this._deactivateEditor();
    }

    /* -------------------------------------------- */

    /** @override */
    _toggleDisabled(disabled) {
        if (this._active && disabled) this._deactivateEditor();
        else if (this._toggled) this._button.disabled = disabled;
        else if (!this._active && !disabled && !this._toggled) this._activateEditor();
    }

    /* -------------------------------------------- */

    /**
     * Create a ExtendedProseMirrorElement using provided configuration data.
     * @param {FormInputConfig & ProseMirrorInputConfig} config The options of the element.
     * @returns {ExtendedProseMirrorElement} The new editor element.
     */
    static create(config) {
        const editor = document.createElement(ExtendedProseMirrorElement.tagName);
        editor.name = config.name;

        // Configure editor properties
        editor.toggleAttribute("toggled", config.toggled ?? false);
        editor.toggleAttribute("collaborate", config.collaborate ?? false);
        editor.toggleAttribute("secrets", config.secrets ?? false);
        if ("documentUUID" in config) editor.dataset.documentUUID = config.documentUUID;
        if (Number.isNumeric(config.height)) editor.style.height = `${config.height}px`;

        // Un-enriched content gets temporarily assigned to the value property of the element
        editor.setAttribute("value", config.value);

        return editor;
    }
}
