/**
 * @typedef {object} MultiSelectOption
 * @property {string[]} selectionOptions An array of itemids (with compendium names). E.g. spells.MY_SPELL.
 * @property {number} maxSelectionCount The number of selections that can be made at most.
 * @property {number} minSelectionCount The number of selections that have to be made at least.
 */

/**
 * Extends the base Dialog with a feature to store and manage a selection of items.
 */
class MultiSelectDialog extends Dialog {
    constructor(data, options) {
        super(data, options);

        // Array of ids with timestamps
        this.timestamps = options.selectOptions
            .filter(option => option.isSelected)
            .map(option => ({ id: option.value, timestamp: Date.now() }));
    }

    /** @override */
    activateListeners(html) {
        super.activateListeners(html);
        html.on("click", "input.multi-select-checkbox", this.ensureProperSelection.bind(this));
    }

    /**
     * A click listener that ensures that only a valid number of items can be selected.
     * @param {Event} event The change event from the select box.
     */
    ensureProperSelection(event) {
        const selectedId = event.currentTarget.defaultValue;

        const minSelect = this.options.minSelectionCount;
        const maxSelect = this.options.maxSelectionCount;
        const selectedItems = this.timestamps.map(item => item.id);
        const selectedCount = selectedItems.length;
        const isSelected = selectedItems.includes(selectedId);

        // Change previously selected item, meaning removing an item
        if (isSelected && selectedCount - 1 < minSelect) return;
        if (isSelected) {
            this.timestamps = this.timestamps.filter(item => item.id !== selectedId);
            return;
        }

        // Change unselected item, meaning adding an item
        if (selectedCount + 1 > maxSelect) {
            // Remove oldest item
            this.timestamps.sort((itemA, itemB) => itemA.timestamp - itemB.timestamp);
            const oldestItem = this.timestamps.shift();
            const oldestCheckbox = $(`.multi-select-item > input[name="${oldestItem.id}"]`);
            oldestCheckbox[0].checked = false;
        }
        this.timestamps.push({ id: selectedId, timestamp: Date.now() });
    }
}

/**
 * Opens a dialog prompting the user to choose serveral items of a selection.
 * Throws an exception if the dialog is cancelled.
 * @param {MultiSelectOption} multiSelectOption Options for selections.
 * @returns {Promise.<object>} A promise representing the item with a variant. This may be rejected if
 *  the user closes the dialog instead of submitting a choice.
 */
export default async function promptMultiSelect(multiSelectOption) {
    try {
        const selectOptions = prepareSelectionItems(multiSelectOption);

        return MultiSelectDialog.prompt({
            title: game.i18n.localize("tde5e.multiSelectionPicker.title"),
            content: await renderMultiSelection(
                selectOptions,
                multiSelectOption.minSelectionCount,
                multiSelectOption.maxSelectionCount,
            ),
            label: game.i18n.localize("tde5e.multiSelectionPicker.button"),
            callback: html => handleCallback(html),
            options: {
                selectOptions,
                minSelectionCount: multiSelectOption.minSelectionCount,
                maxSelectionCount: multiSelectOption.maxSelectionCount,
            },
        });
    } catch (e) {
        return null;
    }
}

/**
 * Processes the callback when a selection of items has been completed.
 * @param {jQuery.Element} html The HTML of the selection dialog.
 * @returns {string[]} An array of ids that were selected.
 */
function handleCallback(html) {
    const inputs = html.find(".multi-select-item>Input");

    const checkedIds = [];
    for (const input of inputs) {
        if (input.checked) checkedIds.push(input.defaultValue);
    }

    return checkedIds;
}

/**
 * Prepares a selection's input to be used and displayed.
 * @param {MultiSelectOption} multiSelectOption Options for selections.
 * @returns {object[]} An array of objects that have a name, value and isSelected.
 */
function prepareSelectionItems(multiSelectOption) {
    const selectionData = [];

    // Prepare data
    for (const id of multiSelectOption.selectionOptions) {
        const [compendiumName, cid] = id.split(".");
        const compendiumPack = game.packs.get(`tde5e.${compendiumName}`);
        const item = compendiumPack.index.contents.find(i => i.system.cid === cid);

        selectionData.push({
            name: item.name,
            value: `${compendiumPack.collection}.${item._id}`,
            isSelected: false,
        });
    }

    // Select the minimum of elements
    for (let index = 0; index < multiSelectOption.minSelectionCount; index++) {
        selectionData[index].isSelected = true;
    }

    return selectionData;
}

/**
 * Renders the multi selection picker as HTML with the potential variants of the item.
 *  This includes both the explicitly configured ones as well as instances
 *  referenced by their type.
 * @param {object} selectOptions The options available for selection.
 * @param {number} minSelections The minimum amount of selected entries.
 * @param {number} maxSelections The maximum amount of selected entries.
 * @returns {Promise.<HTMLElement>} A promise representing the rendered picker.
 */
async function renderMultiSelection(selectOptions, minSelections, maxSelections) {
    return renderTemplate("systems/tde5e/templates/multiSelectPicker.hbs", {
        displayHeight: selectOptions.length * 28,
        selectCriteria: game.i18n.format(
            "tde5e.multiSelectionPicker.description",
            { countMin: minSelections, countMax: maxSelections },
        ),
        selectOptions,
    });
}
