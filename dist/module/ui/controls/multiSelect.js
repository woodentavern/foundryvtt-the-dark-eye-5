/**
 * Fixes for FVTT's HTMLMultiSelectElement.
 */
export default class ExtendedMultiSelectElement extends foundry.applications.elements.HTMLMultiSelectElement {
    /** @override */
    static tagName = "multi-select-ext";

    /**
     * A select element used to choose options.
     * @type {HTMLSelectElement}
     */
    _select;

    /* -------------------------------------------- */

    /** @override */
    _buildElements() {
        const elements = super._buildElements();
        [, this._select] = elements;
        const placeholder = this._select.firstElementChild;
        placeholder.selected = true;
        placeholder.disabled = true;
        placeholder.hidden = true;
        placeholder.innerHTML = game.i18n.localize("tde5e.multiSelect.add");

        if (!this.editable && this._value.size === 0) {
            const none = document.createElement("span");
            none.classList.add("notes", "empty");
            none.innerHTML = game.i18n.localize("tde5e.general.none");
            elements.push(none);
        }

        return elements;
    }

    /** @override */
    _refresh() {
        if (!this._select) return; // Not yet connected
        super._refresh();
    }

    /* -------------------------------------------- */

    /** @override */
    _toggleDisabled(disabled) {
        this._select?.toggleAttribute("disabled", disabled);
        this._refresh();
    }
}
