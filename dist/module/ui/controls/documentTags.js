/**
 * @typedef {object} DocumentTagsInputConfig
 * @property {string} type A specific document type in CONST.ALL_DOCUMENT_TYPES
 * @property {boolean} single Only allow referencing a single document. In this case the submitted form value will
 *  be a single UUID string rather than an array
 * @property {number} max Only allow attaching a maximum number of documents
 */

import { stopEvent } from "../../jsUtils.js";

/**
 * A custom HTMLElement used to render a set of associated Documents referenced by UUID.
 */
export default class ExtendedDocumentTagsElement extends foundry.applications.elements.HTMLDocumentTagsElement {
    /** @override */
    static tagName = "document-tags-ext";

    /**
     * The list of tagged documents.
     * @type {HTMLDivElement}
     */
    _tags;

    /**
     * The note containing the placeholder text when no input is rendered.
     * @type {HTMLSpanElement}
     */
    _placeholder;

    /** @override */
    _initializeTags() {
        this.innerText = this.innerText.trim();
        super._initializeTags();
    }

    /* -------------------------------------------- */

    /** @override */
    _buildElements() {
        const [tags, input, button] = super._buildElements();
        this._tags = tags;
        input.hidden = true;
        button.hidden = true;

        this._placeholder = document.createElement("span");
        this._placeholder.classList.add("notes");
        if (this.editable) {
            this._placeholder.innerHTML = game.i18n.format("tde5e.documentTags.dropHint", { type: this.type });
        } else if (Object.keys(this._value).length === 0) {
            this._placeholder.innerHTML = game.i18n.localize("tde5e.general.none");
            this._placeholder.classList.add("empty");
        }
        return [tags, this._placeholder];
    }

    /* -------------------------------------------- */

    /**
     * Create an HTML string fragment for a single document tag.
     * @param {string} uuid The document UUID
     * @param {string} name The document name
     * @param {boolean} editable Is the tag editable? Defaults to true.
     * @returns {HTMLDivElement} The new element.
     */
    static renderTag(uuid, name, editable = true) {
        const linkData = {
            attrs: { draggable: true },
            classes: ["content-link"],
            dataset: {
                link: "",
                uuid,
                type: this.type,
                tooltip: uuid,
            },
            name,
        };
        if (name.endsWith(" [INVALID]")) {
            linkData.classes.push("broken");
            linkData.name = name.substring(0, name.length - 10);
        } else {
            if (name.length > 32) {
                linkData.name = TextEditor.truncateText(name, { maxLength: 32 });
                linkData.dataset.tooltip = name;
            }

            if (uuid.startsWith("Compendium.")) {
                linkData.dataset.pack = uuid.split(".").slice(1, 3).join(".");
            }
        }

        const link = TextEditor.createAnchor(linkData);
        const div = foundry.applications.elements.HTMLStringTagsElement.renderTag(uuid, "", editable);
        div.classList.add("document-tag");
        div.querySelector("span").append(link);
        if (editable) {
            const t = game.i18n.localize("ELEMENTS.DOCUMENT_TAGS.Remove");
            const a = div.querySelector("a.remove");
            a.dataset.tooltip = t;
            a.ariaLabel = t;
        }
        return div;
    }

    /* -------------------------------------------- */

    /** @override */
    _activateListeners() {
        this._tags.addEventListener("click", this._onClickTag.bind(this));
        super._activateListeners();
    }

    /* -------------------------------------------- */

    /**
     * Remove a single coefficient by clicking on its tag.
     * @param {PointerEvent} event The event of the click.
     */
    _onClickTag(event) {
        if (event.target.classList.contains("remove") || !this.disabled) return;

        const link = event.target.closest("a[data-link]");
        if (!link) return;

        stopEvent(event); // Prevent default, propagation and immediate propagation.

        // Patch a custom click event to bypass the disabled parent.
        const bypassEvent = new Event("click");
        Object.defineProperty(bypassEvent, "target", { writable: false, value: event.target });
        Object.defineProperty(bypassEvent, "currentTarget", { writable: false, value: link });
        TextEditor._onClickContentLink(bypassEvent);
    }

    /* -------------------------------------------- */

    /** @override */
    _toggleDisabled(disabled) {
        super._toggleDisabled(disabled);
        this._refresh();
    }
}
