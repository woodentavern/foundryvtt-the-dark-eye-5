import { roundTo } from "../../util.js";

/**
 * Initializes the Handlebars helper for creating modifier controls.
 */
export function initializeModifierControl() {
    Handlebars.registerHelper("modifiers", createModifierControl);
}

/**
 * Activates listeners required for an interactive modifier control.
 * @param {jQuery.Element} html The JQuery element to attach the listeners to.
 * @param {Document} doc The document of the entity that rendered the control.
 */
export function activateModifierControl(html, doc) {
    html.on("click", "span.modifier.editable", function activate(event) {
        if (event.target.tagName === "INPUT") return;

        // Determine the attributes for the path and value.
        let target;
        let value;
        if (this.dataset.itemPath) {
            target = `data-item-path="${this.dataset.itemPath}" data-refresh="true"`;

            // Resolve the item.
            const pathSegments = this.dataset.itemPath.split(".");
            const itemId = pathSegments.shift();
            const item = doc.items.get(itemId);
            if (!item) return;

            value = foundry.utils.getProperty(item._source, pathSegments.join(".")) ?? "0";
        } else {
            target = `name="${this.dataset.name}"`;
            value = foundry.utils.getProperty(doc._source, this.dataset.name) ?? "0";
        }

        // Assemble and insert the input.
        let content = `<input type="number" class="${this.dataset.inputClass}" ${target} value="${value}">`;
        if (this.innerHTML.startsWith("(")) content = `(${content})`;
        this.innerHTML = content;

        // Focus the new input.
        setTimeout(() => this.querySelector("input").select());
    });

    html.on("blur", "span.modifier > input", function deactivate() {
        if (this.defaultValue !== this.value) return; // Event will be handled by change listener.
        this.parentElement.innerHTML = this.parentElement.dataset.initial;
    });
}

/**
 * Creates a modifier display that will change to a user modifier input when clicked.
 * @param {Array[]} modifiers The name and value pairs of the modifiers.
 * @param {object} options The options of the Handlebars helper.
 * @param {string?} options.target The name of the underlying modifier input. For embedded item properties, this
 *  must contain the item's ID. Must be specified by name.
 * @param {string?} options.itemTarget The item path of the underlying modifier input for embedded items. Must be
 *  specified by name.
 * @param {boolean?} options.included Indicates whether the modifiers are already included in the displayed value.
 *  Must be specified by name.
 * @returns {string} A string containing the HTML of the control.
 */
function createModifierControl(modifiers, options) {
    // Don't display empty modifiers without input.
    if (!options.hash.target && !modifiers?.length) return "";

    // Create the modifier list.
    let modifier = 0;
    let tooltip = game.i18n.localize(options.hash.included ? "tde5e.values.modIncluded" : "tde5e.values.modSeparate");
    if (modifiers?.length) {
        for (const [description, value] of modifiers) {
            modifier += value;
            tooltip += `\n${description}: ${roundTo(value, 1).signedString()}`;
        }
    } else {
        tooltip += ` ${game.i18n.localize("tde5e.general.none")}`;
    }

    // Adjust how the modifier is displayed.
    modifier = roundTo(modifier, 1).signedString();
    if (options.hash.included) modifier = `(${modifier})`;

    // Create the attributes that will be used for the input.
    let target = "";
    if (options.hash.itemTarget) {
        target = `data-item-path="${options.hash.target}.${options.hash.itemTarget}" data-input-class="item-property"`;
    } else if (options.hash.target) {
        target = `data-name="${options.hash.target}" data-input-class="actor-property"`;
    }

    // Determine CSS classes.
    let css = "modifier";
    if (target) css += " editable";

    // Assemble the element.
    return `<span class="${css}" ${target} title="${tooltip}" data-initial="${modifier}">${modifier}</span>`;
}
