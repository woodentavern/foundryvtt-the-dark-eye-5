import { stopEvent } from "../../jsUtils.js";

export default class UnitControl {
    /**
     * Registers a Handlebars helper for creating a unit control from templates.
     */
    static async initialize() {
        Handlebars.registerHelper("currency", (...args) => UnitControl.create("currency", ...args));
        Handlebars.registerHelper("weight", (...args) => UnitControl.create("weight", ...args));
    }

    /**
     * Creates a unit control element that can be used to enter absolute or delta values for a set of units defined by
     *  the unit type. See @see TDE5E.units for configuring the units themselves.
     * @param {string} type The unit type to display.
     * @param {string} name The property name of the value source.
     * @param {number} amount The total amount of units.
     * @param {object} sheetOptions The options of the sheet.
     * @param {object} options The Handlebars options of the helper.
     * @param {boolean=} options.hash.refresh Flag indicating whether all inputs should be refreshed when the total
     *  value changes. Defaults to true. Set this to false when you are rerendering the parent upon change.
     * @param {boolean=} options.hash.editable Flag indicating whether the controls should be editable.
     *  Defaults to false.
     * @param {string?} options.hash.class Additional class list for the unit source input.
     * @returns {Handlebars.SafeString} An HTML string containing the control element.
     */
    static create(type, name, amount, sheetOptions, options) {
        const { editable } = options.hash;
        if (editable) sheetOptions.enableUnitControl = true;

        const el = document.createElement("span");
        el.classList.add("unit-control");
        if (editable) {
            if (options.hash.refresh ?? true) el.dataset.refresh = true;
            const classes = `unit-source ${options.hash.class ?? ""}`;
            el.insertAdjacentHTML(
                "afterbegin",
                `<input class="${classes}" type="hidden" name="${name}" data-dtype="Number" value="${amount}">`,
            );
        }

        const unitConfig = TDE5E.units[type];
        let units = foundry.utils.deepClone(unitConfig);
        refreshValues(amount, units);
        if (!editable) units = units.filter(unit => (unit.value ?? "0") !== "0"); // Filter empty units in read-only.
        if (!units.length) units.push(unitConfig[unitConfig.length - 1]); // Make sure to display at least one unit.
        for (const unit of units) {
            let unitEl = `<span title="${unit.label}">
                <input class="unit" type="text" data-multiplier="${unit.multiplier}" value="${unit.value ?? "0"}">`;
            if (unit.icon) unitEl += `<img src="${unit.icon}"/>`;
            else unitEl += `<span>${unit.labelShort ?? unit.label}</span>`;
            el.insertAdjacentHTML("beforeend", `${unitEl}</span>`);
        }

        return new Handlebars.SafeString(el.outerHTML);
    }

    /**
     * Activates listeners for the unit control for the given HTML element.
     * @param {jQuery.Element} html The JQuery element representing the rendered sheet.
     */
    static activate(html) {
        html.find(".unit-control").on("change", "input.unit", function changeUnit(event) {
            stopEvent(event);
            const controls = event.target.closest(".unit-control");
            const source = controls.querySelector("input.unit-source");
            const units = controls.querySelectorAll("input.unit");

            // Sanitize input since it's a text field.
            let value = this.value.trim().replace(",", ".");
            let isDelta = value.startsWith("-") || value.startsWith("+");

            if (value.startsWith("=")) {
                isDelta = false;
                value = value.substring(1);
            }

            if (isDelta && value.lastIndexOf("-") <= 0 && value.lastIndexOf("+") <= 0) {
                value = Number(value);
            } else {
                // User entered a formula, not a delta.
                isDelta = false;
                value = parseAbsoluteValue(value);
            }

            if (Number.isNaN(value)) {
                refreshValues(source.value, units);
                return;
            }

            let total;
            if (isDelta) {
                // Determine delta and add it to the source.
                const multiplier = parseInt(this.dataset.multiplier, 10);
                const newValue = Number(source.value) + Math.round(value * multiplier);

                // Don't subtract from already negative values.
                total = value < 0 && newValue < 0 ? calculateTotal(units) : newValue;
            } else {
                // Determine and set new absolute value.
                total = calculateTotal(units);
            }

            source.value = total;
            if (controls.dataset.refresh) refreshValues(source.value, units);
            source.dispatchEvent(new Event("change", { bubbles: true }));
        });
    }
}

/**
 * Parses an input string with an optional addition or subtraction into a numerical value.
 * @param {string} value The value entered by the user.
 * @returns {number} The absolute numerical value represented by the user's input.
 */
function parseAbsoluteValue(value) {
    let valueParts = value.split("+");
    if (valueParts.length > 1) {
        return valueParts.reduce((total, part) => total + Number(part), 0);
    }

    valueParts = value.split("-");
    if (valueParts.length > 1) {
        return valueParts.slice(1).reduce((total, part) => total - Number(part), Number(valueParts[0]));
    }

    return Number(value);
}

/**
 * Calculates the total amount of units contained in the given inputs, respecting their contents and multipliers.
 * @param {NodeList.<HTMLInputElement>} unitInputs An array of text inputs containing unit values.
 * @returns {number} The total amount of units contained in the inputs.
 */
function calculateTotal(unitInputs) {
    return [...unitInputs].reduce((total, input) => {
        // Sanitize input since it's a text field.
        const value = parseAbsoluteValue(input.value.trim());
        if (Number.isNaN(value)) return total;

        // Calculate multiplied value.
        total += value * parseInt(input.dataset.multiplier, 10);
        return Math.round(total);
    }, 0);
}

/**
 * Distributes the given amount of units across the target inputs, filling their values in descending multiplier order.
 * @param {number} total The total amount of units to set.
 * @param {NodeList.<HTMLInputElement> | object[]} unitInputs An array of text inputs or unit objects to fill with new
 *  values.
 */
function refreshValues(total, unitInputs) {
    const multipliers = [...unitInputs]
        .map(input => [input.multiplier ?? parseInt(input.dataset.multiplier, 10), input]) // Map with multiplier.
        .sort((e1, e2) => e2[0] - e1[0]); // Sort by multiplier descending.

    let remaining = total;
    multipliers.forEach(([multiplier, input], index) => {
        if (!remaining) {
            input.value = "0";
        } else if (index === multipliers.length - 1) {
            // This is the last input, so just set the remaining units.
            input.value = remaining.toString();
            remaining = 0;
        } else {
            // Take the highest possible integer value and subtract it from the remaining units.
            const value = Math.floor(remaining / multiplier);
            remaining -= value * multiplier;
            input.value = value.toString();
        }
    });
}
