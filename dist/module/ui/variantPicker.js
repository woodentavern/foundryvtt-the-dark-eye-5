import { romanize, error, warn } from "../util.js";

/**
 * Checks if the given item data contains variants or levels that can be prompted for.
 * @param {object} data The data of the item to check.
 * @returns {boolean} True if the item can have variants or levels, false otherwise.
 */
export function hasVariants(data) {
    if (data.system.purchase?.levels > 1) return true;
    const { variant } = data.system;
    if (!variant) return false;
    return variant.input || variant.available?.length > 0 || variant.categories?.length > 0;
}

/**
 * @typedef CustomVariant
 * @property {string} name The internal name of the property.
 * @property {string} title The localized name of the property.
 * @property {object[]} values An array of options, each containing a text and a value. If empty, a text input is used.
 * @property {function(object, object): void} callback The function to call with the item's data and the selected value.
 */

/**
 * Opens a dialog prompting the user to pick a variant of the given item.
 * Throws an exception if the dialog is cancelled.
 * @param {object} itemData The item to pick the variant of.
 * @param {Actor} actor The actor to read variants referencing a type from.
 * @param {CustomVariant[]?} additionalVariants An array of additional properties to prompt the user for.
 * @param {boolean?} allowDefault If true, will add a variant to select the base form of the item to the list of
 *  variants. Defaults to false.
 * @returns {Promise.<object>} A promise representing the item with a variant. This may be rejected if
 *  the user closes the dialog instead of submitting a choice.
 */
export async function promptVariant(itemData, actor, additionalVariants = [], allowDefault = false) {
    try {
        const variants = await renderVariants(itemData, actor, additionalVariants, allowDefault);
        if (!variants) return Promise.resolve(itemData);

        return Dialog.prompt({
            title: game.i18n.localize("tde5e.variantPicker.title"),
            content: variants,
            label: game.i18n.localize("tde5e.variantPicker.button"),
            rejectClose: false,
            callback: html => {
                const formData = new FormDataExtended(html[0].querySelector("form")).object;
                if (formData.variant) parseVariant(itemData, formData.variant, allowDefault);
                if (itemData.system.variant?.available?.length && allowDefault) itemData.system.variant.available = [];
                if (formData.custom) parseInput(itemData, formData.custom);
                if (formData.level) itemData.system.variant.level = parseInt(formData.level, 10);
                additionalVariants.forEach(variant => variant.callback(itemData, formData[variant.name]));
                return itemData;
            },
        });
    } catch {
        return null;
    }
}

/**
 * Transfers the variant data from the submitted form data into the creation data of the item.
 * @param {object} itemData The object to write the variant into.
 * @param {string} formVariant The selected variant.
 */
function parseVariant(itemData, formVariant) {
    const variantSelected = itemData.system.variant.available?.find(variant => variant.vid === formVariant);
    itemData.system.variant.available = []; // Remove the variants from the item to save space.

    if (variantSelected) {
        // Variant was selected from one of the IDs.
        itemData.system.cid += `:${variantSelected.vid}`;
        itemData.system.variant.selected = {
            vid: variantSelected.vid,
            name: variantSelected.name,
        };

        const variantData = { system: variantSelected };

        // Move flags from inner element to root level
        if (variantSelected.flags) variantData.flags = variantSelected.flags;
        if (variantSelected.img) variantData.img = variantSelected.img;

        delete variantSelected.flags;
        delete variantSelected.img;
        delete variantSelected.vid;
        delete variantSelected.name;

        foundry.utils.mergeObject(itemData, variantData);
    } else {
        // Variant was selected from a category.
        itemData.system.cid += `:${formVariant.split(".").join("_")}`;
        itemData.system.variant.selected = {
            target: formVariant,
        };
    }
}

/**
 * Transfers custom input data from the submitted form data into the creation data of the item.
 * @param {object} itemData The object to write the variant into.
 * @param {string} formInput The user input.
 */
function parseInput(itemData, formInput) {
    itemData.system.cid += `:${formInput}`;

    const variant = itemData.system.variant.selected;
    if (variant) {
        variant.name = variant.name ? `${variant.name} - ${formInput}` : formInput;
    } else {
        itemData.system.variant.selected = {
            vid: "CUSTOM",
            name: formInput,
        };
    }
}

/**
 * Renders the variant picker as HTML with the potential variants of the item.
 *  This includes both the explicitly configured ones as well as instances
 *  referenced by their type.
 * @param {object} itemData The item in its object representation.
 * @param {Actor} actor The actor to read variants referencing a type from.
 * @param {CustomVariant[]} additionalVariants An array of additional properties to prompt the user for.
 * @param {boolean?} allowDefault If true, will add a default to the variants to select the base item.
 * @returns {Promise.<HTMLElement>} A promise representing the rendered picker.
 */
async function renderVariants(itemData, actor, additionalVariants, allowDefault) {
    let variants = [];
    if (itemData.system.variant?.available?.length) {
        // Add individual options
        variants = itemData.system.variant.available.map(variant => ({ value: variant.vid, text: variant.name }));
        if (allowDefault) variants.unshift({ value: "", text: itemData.name });
    }

    if (itemData.system.variant?.categories?.length) {
        // Add options for each category based on the actor's items
        itemData.system.variant.categories.forEach(category => {
            if (category.scope === "item") {
                let items = actor.itemTypes[category.type];
                if (!items) return;
                if (category.groups?.length) items = items.filter(item => category.groups.includes(item.system.group));
                items.forEach(item => variants.push({ value: item.id, text: item.name }));
            } else if (category.scope === "translation") {
                let items = Object.entries(game.i18n.translations.tde5e[category.type] ?? {});
                if (!items) return;
                if (category.groups?.length) items = items.filter(item => category.groups.includes(item[0]));
                items.forEach(item => variants.push({ value: `tde5e.${category.type}.${item[0]}`, text: item[1] }));
            } else {
                error(`${itemData.name} has malformed categories. Cannot pick variant!`);
            }
        });
    }

    const levels = [];
    if (itemData.system.purchase?.levels > 1) {
        for (let i = 1; i <= itemData.system.purchase.levels; i++) {
            levels.push({ value: i, text: romanize(i) });
        }
    }

    const variantInput = itemData.system.variant?.input ?? false;
    if (!variants.length && !levels.length && !variantInput && !additionalVariants) {
        warn(`Item ${itemData.system.cid ?? itemData.name} has variants, but none could be resolved.`);
        return Promise.resolve();
    }
    return renderTemplate("systems/tde5e/templates/variantPicker.hbs", {
        variants,
        levels,
        name: itemData.name,
        input: variantInput,
        additionalVariants,
    });
}
