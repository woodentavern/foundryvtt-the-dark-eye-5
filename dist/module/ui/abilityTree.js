import { getOwnedActors, warn } from "../util.js";
import DocumentEdge from "./documentEdge.js";
import DocumentNode from "./documentNode.js";

/**
 * Application for displaying an ability's related items.
 */
export default class AbilityTree extends Application {
    /**
     * The UUID of the tree's root document.
     * @type {string}
     */
    documentUuid;

    /**
     * The actor to evaluate ownership for.
     * @type {TdeActor}
     */
    actor;

    /**
     * The tree's root node. May be null if the tree hasn't been traversed yet.
     * @type {DocumentNode?}
     */
    root;

    /**
     * The currently visible nodes mapped by their UUID.
     * @type {Map.<string, DocumentNode>}
     */
    nodes = new Map();

    /**
     * The currently visible edges mapped by their key.
     * @see DocumentEdge.key
     * @type {Map.<string, DocumentEdge>}
     */
    edges = new Map();

    /**
     * Creates a new ability tree for the given item.
     * @param {TdeItem} item The item to render related abilities for.
     * @param {object} options The application options to override defaults with.
     */
    constructor(item, options = {}) {
        super(options);
        this.actor = item.actor;
        this.documentUuid = item.sourceItem.uuid;
    }

    /** @inheritdoc */
    static get defaultOptions() {
        return foundry.utils.mergeObject(super.defaultOptions, {
            template: "systems/tde5e/templates/abilityTree.hbs",
            resizable: true,
            width: 700,
            height: 865,
        });
    }

    /** @inheritdoc */
    get id() {
        return `${this.constructor.name}-${this.documentUuid}`;
    }

    /** @inheritdoc */
    get title() {
        const rootName = fromUuidSync(this.documentUuid)?.name;
        const tree = game.i18n.localize("tde5e.abilityTree.name");
        return rootName ? `${tree} - ${rootName}` : tree;
    }

    /**
     * @returns {number} The currently available width for the graph.
     */
    get graphWidth() {
        return (this.position.width ?? this.options.width) - 35;
    }

    /**
     * @returns {number} The currently available height for the graph.
     */
    get graphHeight() {
        return (this.position.height ?? this.options.height) - 81;
    }

    /** @inheritdoc */
    getData() {
        this.nodes.clear();
        this.edges.clear();
        this._traverse();
        this.root = this.nodes.get(this.documentUuid);
        return {
            actor: this.actor?.uuid,
            actors: getOwnedActors().reduce((options, actor) => {
                options[actor.uuid] = actor.name;
                return options;
            }, {}),
        };
    }

    /** @inheritdoc */
    async _renderInner(data) {
        const html = await super._renderInner(data);
        const tree = html[0].querySelector(".ability-tree");
        this._renderNodes(tree);
        for (const node of this.nodes.values()) {
            node.actor = this.actor;
        }

        return html;
    }

    /**
     * Renders and positions the nodes and edges of the graph.
     * @param {HTMLElement} tree The parent element containing the nodes.
     */
    _renderNodes(tree) {
        const iterations = 60;

        this._initializePositions();
        for (let i = 0; i < 10; i++) this._applyForces(i / iterations);
        for (const edge of this.edges.values()) edge.render(tree);
        for (const node of this.nodes.values()) node.render(tree);

        // Run some more iterations after the initial rendering completes.
        setTimeout(() => this._iterate(iterations - 10, 10, iterations), 100);

        // for (let i = 0; i < iterations; i++) {
        //     setTimeout(() => this._iterate(1, i, iterations), i * 100);
        // }
    }

    /**
     * Repositions nodes when the window was resized.
     */
    _onResize() {
        setTimeout(() => this._iterate(50));
    }

    /**
     * @typedef NodeCreationContext
     * @property {string} uuid The UUID of the record to visit.
     * @property {DocumentNode} source The node that requested the creation.
     * @property {number} distance The distance from the root node.
     * @property {number} layer The layer within the tree.
     * @property {boolean} traverse Indicates whether new nodes are created for references.
     */

    /**
     * Recursively visits items using a breadth first approach.
     */
    _traverse() {
        // Target distance solved for nodes: 45 = 0.3 * (sqrt(area / nodes))
        const preferredNodeCount = (this.graphWidth * this.graphHeight) / 22500;

        // Initialize queue with root node.
        const queue = [{
            uuid: this.documentUuid,
            source: null,
            distance: 0,
            layer: 0,
            traverse: true,
        }];

        while (queue.length) {
            // Allow queueing the average of the remaining size with a bit of leeway.
            const maxCreations = ((preferredNodeCount - this.nodes.size) / queue.length) * 1.5;
            queue.push(...this._visit(queue.shift(), maxCreations));
        }
    }

    /**
     * Creates nodes and edges for items connected to the given UUID.
     * @param {NodeCreationContext} context The context for creating the node.
     * @param {number} maxCreations The amount of new references that this node may add.
     * @returns {NodeCreationContext[]} An array of creation contexts for the node's references.
     */
    _visit(context, maxCreations) {
        let node = this.nodes.get(context.uuid);
        if (node) {
            if (node.distance !== null) return [];

            // Node was reset, copy distance and layer.
            node.distance = context.distance;
            node.layer = context.layer;
        } else {
            node = new DocumentNode(context.uuid, context.distance, context.layer);
            if (!node.record) {
                warn(`Unable to create document node for UUID ${context.uuid}.`);
                return [];
            }

            this.nodes.set(context.uuid, node);
            if (context.source) this._createEdge(context.source, node, node.layer > context.source.layer);
        }

        const parents = node.record.system.parents ?? [];
        const children = node.record.children ?? node.record.references ?? [];
        node.hiddenEdges = (parents.size ?? parents.length) + (children.size ?? children.length);

        // Prepare context for new references.
        context.source = node;
        context.traverse &&= context.distance < DocumentNode.maxDistance; // Never traverse beyond maximum distance.
        const { distance, layer } = context;
        context.distance += 1;

        const parentLayer = this._getParentLayer(layer);
        context.layer = parentLayer;
        const parentCreations = this._visitChildren(parents, context);

        const childLayer = this._getChildLayer(layer);
        context.layer = childLayer;
        const childCreations = this._visitChildren(children, context);

        if (distance === 0 || parentCreations.length + childCreations.length <= maxCreations) {
            // Enough space is available, return all creations.
            return parentCreations.concat(childCreations);
        }

        if (parentLayer === layer - 1 && parentCreations.length <= maxCreations) {
            // Traversing upward and there is only enough space for parents.
            return parentCreations;
        }

        if (childLayer === layer + 1 && childCreations.length <= maxCreations) {
            // Traversing downward and there is only enough space for children.
            return childCreations;
        }

        return [];
    }

    /**
     * Determines the layer for the node's parent. The new layer is guaranteed to be lower than the node, but higher
     *  than parents that were added at a lower distance.
     * @param {number} layer The layer of the child node.
     * @returns {number} The layer of the child's parents.
     */
    _getParentLayer(layer) {
        const ceil = Math.ceil(layer);
        if (layer <= 0 && ceil === layer) return layer - 1;
        return ceil - 1 + ((layer - ceil + 1) / 2);
    }

    /**
     * Determines the layer for the node's children. The new layer is guaranteed to be higher than the node, but lower
     *  than children that were added at a lower distance.
     * @param {number} layer The layer of the parent node.
     * @returns {number} The layer of the parent's children.
     */
    _getChildLayer(layer) {
        const floor = Math.floor(layer);
        if (layer >= 0 && floor === layer) return layer + 1;
        return 1 + floor + ((layer - floor - 1) / 2);
    }

    /**
     * Creates edges and creation data for children that haven't been visited yet.
     * @param {Iterable.<string>} references The UUIDs of the children to visit.
     * @param {NodeCreationContext} context The context for creating new child nodes.
     * @returns {NodeCreationContext[]} An array of creation contexts for unvisited nodes.
     */
    _visitChildren(references, context) {
        const creations = [];
        for (const reference of references) {
            if (context.source.record.uuid === reference) continue; // Skip source node.

            const referenceNode = this.nodes.get(reference);
            if (referenceNode && referenceNode.distance !== null) {
                // Node was already visited, create an edge to it.
                this._createEdge(context.source, referenceNode, context.layer > context.source.layer);
            } else if (context.traverse) {
                creations.push({
                    uuid: reference,
                    source: context.source,
                    distance: context.distance,
                    layer: context.layer,
                    traverse: true,
                });
            }
        }

        return creations;
    }

    /**
     * Creates a new edge for the given nodes, if it doesn't exists yet. Adjusts reference counts for both nodes.
     * @param {DocumentNode} start The node that the edge starts from.
     * @param {DocumentNode} end The node that the edge ends in.
     * @param {boolean=} reverse Indicates whether start and end should be flipped. Defaults to false.
     */
    _createEdge(start, end, reverse = false) {
        const edgeKey = start.getEdgeKey(end);
        const edge = this.edges.get(edgeKey);
        if (!edge) this.edges.set(edgeKey, new DocumentEdge(reverse ? end : start, reverse ? start : end));

        start.hiddenEdges--;
        end.hiddenEdges--;
    }

    /**
     * Initializes the vertical position of all nodes based on their layer.
     */
    _initializePositions() {
        // Determine highest and lowest layer in the graph.
        let minLayer = 0;
        let maxLayer = 0;
        for (const node of this.nodes.values()) {
            minLayer = Math.min(minLayer, node.layer);
            maxLayer = Math.max(maxLayer, node.layer);
        }

        // Leave some additional space.
        minLayer -= 0.5;
        maxLayer += 0.5;

        // Map the layer onto the available window size.
        const width = this.graphWidth;
        const slope = this.graphHeight / (maxLayer - minLayer);
        for (const node of this.nodes.values()) {
            const y = slope * (node.layer - minLayer);
            node.initializePosition(width, y);
        }
    }

    /**
     * Calculates layout forces for all nodes and edges.
     * @param {number} progress The overall progress of the layout between 0 and 1.
     */
    _applyForces(progress) {
        // Calculate preferred distance between nodes.
        const width = this.graphWidth;
        const height = this.graphHeight;
        const targetDistance = 0.3 * Math.sqrt((width * height) / this.nodes.size);
        const temperature = Math.max(0.01, 0.2 - (progress / 3));

        for (const node of this.nodes.values()) node.calculateForce(this.nodes.values(), targetDistance, progress);
        for (const edge of this.edges.values()) edge.calculateForce(targetDistance, progress);
        for (const node of this.nodes.values()) node.applyForce(width, height, temperature);
    }

    /**
     * Applies previously calculated layout forces to all nodes and edges.
     * @see _applyForces
     */
    _applyPositions() {
        for (const node of this.nodes.values()) node.applyPosition();
        for (const edge of this.edges.values()) edge.applyPosition();
    }

    /**
     * Calculates forces based on the given number of iterations and applies the resulting positions.
     * @param {number=} iterations The amount of iterations to perform. Defaults to 1.
     * @param {number=} start The index of the first iteration in the current batch. Defaults to 0.
     * @param {number=} total The total amount of iterations of the batch. Defaults to the number of iterations.
     */
    _iterate(iterations = 1, start = 0, total = iterations) {
        for (let i = start; i < iterations + start; i++) this._applyForces(i / total);
        this._applyPositions();
    }

    /** @inheritdoc */
    activateListeners(html) {
        super.activateListeners(html);
        html.find("select.select-actor").change(event => this.changeActor(event.currentTarget.value));
        html.on("click", "a.skill-tree", event => this.changeItem(event.currentTarget.dataset.uuid));
        html.on("click", "a.add-item", this.addItem.bind(this));
    }

    /**
     * Changes the actor that is used to determine ability ownership for all nodes.
     * @param {string} uuid The UUID of the new actor.
     */
    async changeActor(uuid) {
        this.actor = uuid ? await fromUuid(uuid) : null;
        for (const node of this.nodes.values()) {
            node.actor = this.actor;
        }

        const el = this._element[0].querySelector("div.ability-tree");
        if (this.actor) el.classList.remove("unowned");
        else el.classList.add("unowned");
    }

    /**
     * Changes the root item of the tree.
     * @param {string} uuid The UUID of the new root.
     */
    async changeItem(uuid) {
        if (!this._element) return; // Tree is no longer visible.

        this.documentUuid = uuid;
        const newRoot = this.nodes.get(this.documentUuid);
        if (!newRoot || newRoot === this.root) return;

        // Reset distance to allow pruning.
        for (const node of this.nodes.values()) {
            node.distance = null;
        }

        // Rebuild tree.
        this.root = newRoot;
        this._traverse();

        // Prune edges of unvisited nodes.
        for (const [key, edge] of this.edges.entries()) {
            if (edge.startNode.distance !== null && edge.endNode.distance !== null) continue;
            this.edges.delete(key);
            edge.destroy();
        }

        // Prune unvisited nodes.
        for (const node of this.nodes.values()) {
            if (node.distance !== null) continue;
            this.nodes.delete(node.record.uuid);
            node.destroy();
        }

        const el = this._element[0];
        el.querySelector("h4.window-title").innerHTML = this.title;
        this._renderNodes(el.querySelector(".ability-tree"));
    }

    /**
     * Attempts to add the item represented by the clicked node to the currently selected actor.
     * @param {JQuery.Event} event The event of the button click.
     */
    async addItem(event) {
        if (!this.actor) return;

        const uuid = event.currentTarget.closest(".document-node")?.dataset.uuid;
        if (!uuid) return;

        const item = await fromUuid(uuid);
        if (!item) return;

        this.actor.createEmbeddedDocuments("Item", [game.items.fromCompendium(item)], { clearFolder: true });
        this.nodes.get(uuid)?._element?.classList.add("owned");
    }
}
