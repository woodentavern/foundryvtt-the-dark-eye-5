import TdeItem from "../items/item.js";
import { hashcode, localizeType } from "../util.js";
import Vector2D from "./vector2d.js";

/**
 * Node within an @see AbilityTree instance.
 */
export default class DocumentNode {
    /**
     * The maximum distance to the root node of the tree. Nodes with a higher distance will not be rendered.
     * @type {number}
     */
    static maxDistance = 2;

    /**
     * The resolved item or index entry represented by this node.
     * @type {TdeItem | object}
     */
    record;

    /**
     * The smallest distance to the root node.
     * @type {number}
     */
    distance;

    /**
     * The approximate layer within the tree, relative to the root node.
     * @type {number}
     */
    layer;

    /**
     * The amount of children and parents that are not connected in the current tree.
     * @type {number}
     */
    hiddenEdges = 0;

    /**
     * The element of the node. May be null if the node wasn't rendered yet.
     * @type {HTMLElement?}
     */
    _element;

    /**
     * The localized type of the node's @see record.
     * This is cached to allow approximating its width before the node was rendered.
     * @type {string}
     */
    _localizedType;

    /**
     * The initial position of the node before any forces were applied.
     * @type {Vector2D}
     */
    _initialPosition;

    /**
     * The current position of the node.
     * @type {Vector2D}
     */
    _position;

    /**
     * The current force for the node.
     * @type {Vector2D}
     */
    _force = new Vector2D(0, 0);

    /**
     * Creates a new node for the given document UUID.
     * @param {string} uuid The UUID of the node's record.
     * @param {number} distance The distance to the root node.
     * @param {number} layer The layer within the tree.
     */
    constructor(uuid, distance, layer) {
        this.distance = distance;
        this.layer = layer;
        this.record = fromUuidSync(uuid);
        if (this.record) this._localizedType = localizeType(this.record.type, this.record.system?.group);
    }

    /**
     * @returns {boolean} True if this node should display an indicator for references that are not part of the tree.
     */
    get showMissingNodes() {
        return this.hiddenEdges > 0;
    }

    /**
     * Estimates the dimensions of the node without requiring a rendered element.
     * @returns {object} An object containing half of the node's width and height.
     */
    get size() {
        let contentWidth = Math.max(this.record.name.length * 4, this._localizedType.length * 2.5, 88);
        if (this.showMissingNodes) contentWidth = Math.max(contentWidth, 36);
        return {
            halfWidth: contentWidth + 12,
            halfHeight: this.showMissingNodes ? 62 : 44,
        };
    }

    /**
     * Estimates the boundaries of the node without requiring a rendered element.
     * @returns {object} An object containing the minimum and maximum horizontal and vertical point.
     */
    get bounds() {
        const { halfWidth, halfHeight } = this.size;
        return {
            minX: this._position.x - halfWidth,
            maxX: this._position.x + halfWidth,
            minY: this._position.y - halfHeight,
            maxY: this._position.y + halfHeight,
        };
    }

    /**
     * @param {TdeActor} value The actor to use for determining item ownership.
     */
    set actor(value) {
        if (!this._element) return;
        this._element.classList.toggle("owned", this._checkOwnership(value));
    }

    /**
     * Determines if the given actor owns the item represented by this node.
     * @param {TdeActor} actor The actor to check.
     * @returns {boolean} True if the actor owns the node's item, false otherwise.
     */
    _checkOwnership(actor) {
        if (!actor) return false;
        if (this.record instanceof TdeItem && this.record.actor === actor) return true;

        const cid = this.record.system?.cid;
        return cid ? actor.getItems(this.record.type, cid).length > 0 : false;
    }

    /**
     * Creates a unique key for an edge between this and the given node. The key does not depend on the start or end of
     *  the edge.
     * @param {DocumentNode} node The node that the edge connects to.
     * @returns {string} A unique key for the edge.
     */
    getEdgeKey(node) {
        const uuids = [this.record.uuid, node.record.uuid].sort((a, b) => a.localeCompare(b));
        return uuids.join("-");
    }

    /**
     * Creates and positions an element for this node.
     * @param {HTMLElement} tree The parent element to append the node to.
     */
    render(tree) {
        if (!this._element) {
            this._buildElement();
            tree.append(this._element);
        } else {
            this._element.classList.toggle("root-node", this.distance === 0);

            const hiddenRefs = this._element.querySelector("div.hidden-edges");
            if (!hiddenRefs === this.showMissingNodes) {
                if (this.showMissingNodes) this._element.append(this._buildHiddenReferences());
                else hiddenRefs.remove();
            }
        }

        this.applyPosition();
    }

    /**
     * Creates the element that visually represents this node.
     */
    _buildElement() {
        this._element = document.createElement("div");
        this._element.classList.add("document-node");
        this._element.classList.add("animate");
        if (this.distance === 0) this._element.classList.add("root-node");
        this._element.dataset.uuid = this.record.uuid;

        // Build header link.
        const header = document.createElement("h4");
        if (this.record instanceof TdeItem) {
            header.classList.add(this.record.sheetHeaderClass);
            header.innerHTML = this.record.createLink();
        } else {
            header.classList.add("bg-brown");
            header.innerHTML = `<a class="content-link nobg" data-link data-uuid="${this.record.uuid}" draggable="true">
                    ${this.record.name}</a>`;
        }

        header.insertAdjacentHTML("beforeend", "<a class='add-item fas fa-cart-plus'></a><i class='fas fa-check'></i>");
        this._element.append(header);

        // Build description text.
        const type = document.createElement("div");
        type.classList.add("node-type");
        type.innerHTML = this._localizedType;
        this._element.append(type);

        if (this.showMissingNodes) this._element.append(this._buildHiddenReferences());
    }

    /**
     * Creates an element for nodes referenced by or from this node's record that aren't included in the current graph.
     * @returns {HTMLElement} An element representing referenced nodes not visible in the graph.
     */
    _buildHiddenReferences() {
        const refs = document.createElement("div");
        refs.classList.add("hidden-edges");

        const link = document.createElement("a");
        link.classList.add("skill-tree");
        link.dataset.uuid = this.record.uuid;
        link.title = game.i18n.localize("tde5e.abilityTree.recenter");

        const text = game.i18n.format("tde5e.abilityTree.hiddenEdges", { edges: this.hiddenEdges });
        link.innerHTML = `<i class="fas fa-diagram-project"></i> ${text}`;

        refs.append(link);
        return refs;
    }

    /**
     * Calculates and applies the initial position of this node.
     * @param {number} graphWidth The available width of the graph in pixels.
     * @param {number} y The vertical position that the node should have.
     */
    initializePosition(graphWidth, y) {
        let x = 0;
        if (this.distance === 0) {
            // Center the root node horizontally.
            x = graphWidth / 2;
        } else {
            // Use hashcode for horizontal position to ensure that it is not random.
            const hash = hashcode(this.record._id ?? this.record.uuid);
            x = graphWidth * (hash / Number.MAX_SAFE_INTEGER);
        }

        this._initialPosition = new Vector2D(x, y);
        this._position = this._initialPosition;
    }

    /**
     * Calculates repulsion forces from other nodes of the graph.
     * @param {IterableIterator.<DocumentNode>} nodes The nodes of the graph.
     * @param {number} targetDistance The preferred distance between nodes.
     * @param {number} progress The current progress of the layout between 0 and 1.
     */
    calculateForce(nodes, targetDistance, progress) {
        // Gravity towards the initial position.
        const gravityStrength = new Vector2D(0.01, 0.1);
        this._force = this._initialPosition.subtract(this._position).multiply(gravityStrength);

        for (const node of nodes) {
            if (node === this) continue;

            // Interpolate between center and box distance.
            const direction = this._position.subtract(node._position);
            const scaledDistance = (1 - progress) * direction.magnitudeSquared()
                + progress * this._getSquaredDistance(node);

            // Calculate force.
            const repulsiveForce = (targetDistance ** 2) / Math.max(8, Math.sqrt(scaledDistance));
            const repulsion = direction.normalize().multiplyScalar(repulsiveForce);
            this._force = this._force.add(repulsion);
        }
    }

    /**
     * Calculates the smallest distance between the bounding boxes of this and the given node.
     * @param {DocumentNode} node The node to calculate the distance to.
     * @returns {number} The squared distance between the nodes.
     */
    _getSquaredDistance(node) {
        const nodeBounds = node.bounds;
        const thisBounds = this.bounds;
        const deltas = [
            thisBounds.minX - nodeBounds.maxX,
            thisBounds.minY - nodeBounds.maxY,
            nodeBounds.minX - thisBounds.maxX,
            nodeBounds.minY - thisBounds.maxY,
        ];
        return deltas.reduce((total, delta) => (delta > 0 ? total + (delta ** 2) : total), 0);
    }

    /**
     * Updates the node's position based on the accumulated forces.
     * @see calculateForce
     * @param {number} maxX The width of the available layout space.
     * @param {number} maxY The height of the available layout space.
     * @param {number} temperature A factor of the available space to limit the displacement to.
     */
    applyForce(maxX, maxY, temperature) {
        // Apply cooling to stabilize graph.
        const maxVelocitySquared = new Vector2D(temperature * maxX, temperature * maxY).magnitudeSquared();
        if (this._force.magnitudeSquared() > maxVelocitySquared) {
            this._force = this._force.normalize().multiplyScalar(Math.sqrt(maxVelocitySquared));
        }

        // Update position with force.
        this._position = this._position.add(this._force).roundTo(5);
        this._force = new Vector2D(0, 0);

        // Keep position in bounds.
        const { halfWidth, halfHeight } = this.size;
        this._position.x = Math.clamp(this._position.x, halfWidth, maxX - halfWidth);
        this._position.y = Math.clamp(this._position.y, halfHeight, maxY - halfHeight);
    }

    /**
     * Repositions the node's element.
     */
    applyPosition() {
        this._element.style.transform = `translate(${this._position.x}px, ${this._position.y}px) translate(-50%, -50%)`;
    }

    /**
     * Cleans up references and elements associated with this node.
     */
    destroy() {
        this.record = null;
        this._element?.remove();
        this._element = null;
    }
}
