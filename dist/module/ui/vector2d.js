import { roundTo } from "../util.js";

/**
 * Implements vector math because FoundryVTT doesn't load the PIXI math extras.
 * @see https://github.com/pixijs/pixijs/blob/dev/src/math-extras/pointExtras.ts
 */
export default class Vector2D {
    /**
     * The x coordinate.
     * @type {number}
     */
    x;

    /**
     * The y coordinate.
     * @type {number}
     */
    y;

    /**
     * Creates a new vector.
     * @param {number?} x The x coordinate of the vector.
     * @param {number?} y The y coordinate of the vector.
     */
    constructor(x = 0, y = 0) {
        this.x = x;
        this.y = y;
    }

    /**
     * Adds the given vector to this vector.
     * @param {Vector2D} point The vector to add to this.
     * @returns {Vector2D} A new vector with the added components.
     */
    add(point) {
        return new Vector2D(this.x + point.x, this.y + point.y);
    }

    /**
     * Subtracts the given vector from this vector.
     * @param {Vector2D} point The vector to subtract from this.
     * @returns {Vector2D} A new vector with the subtracted components.
     */
    subtract(point) {
        return new Vector2D(this.x - point.x, this.y - point.y);
    }

    /**
     * Calculates the component-wise multiplier with the given vector.
     * @param {Vector2D} point The vector to multiply with this.
     * @returns {Vector2D} A new vector with the multiplied components.
     */
    multiply(point) {
        return new Vector2D(this.x * point.x, this.y * point.y);
    }

    /**
     * Multiplies each component of this vector with the scalar number.
     * @param {number} scalar The number to multiply the components with.
     * @returns {Vector2D} A new vector with the multiplied components.
     */
    multiplyScalar(scalar) {
        return new Vector2D(this.x * scalar, this.y * scalar);
    }

    /**
     * Calculates the dot product of this vector with the given vector.
     * @param {Vector2D} point The vector to calculate the dot product with.
     * @returns {number} The dot product of the vectors.
     */
    dot(point) {
        return (this.x * point.x) + (this.y * point.y);
    }

    /**
     * Calculates the cross product of this vector with the given vector.
     * @param {Vector2D} point The vector to calculate the cross product with.
     * @returns {number} The cross product of the vectors.
     */
    cross(point) {
        return (this.x * point.y) - (this.y * point.x);
    }

    /**
     * Normalizes this vector to length 1.
     * @returns {Vector2D} The normalized vector.
     */
    normalize() {
        const mag = this.magnitude();
        if (mag === 0) return this;
        return new Vector2D(this.x / mag, this.y / mag);
    }

    /**
     * @returns {number} The length of this vector.
     */
    magnitude() {
        return Math.sqrt((this.x * this.x) + (this.y * this.y));
    }

    /**
     * @returns {number} The squared length of this vector.
     */
    magnitudeSquared() {
        return (this.x * this.x) + (this.y * this.y);
    }

    /**
     * Calculates a vector projection of this vector onto the given vector.
     * @param {Vector2D} point A non-zero vector describing a line on which to project this vector.
     * @returns {Vector2D} The projection of this vector onto the given point.
     */
    project(point) {
        const projection = ((this.x * point.x) + (this.y * point.y)) / ((point.x * point.x) + (point.y * point.y));
        return new Vector2D(point.x * projection, point.y * projection);
    }

    /**
     * Reflects this vector off of a plane orthogonal to the given point.
     * @param {Vector2D} point The normal vector of the reflecting plane.
     * @returns {Vector2D} The reflection of this vector on the plane.
     */
    reflect(point) {
        const dot = (this.x * point.x) + (this.y * point.y);
        return new Vector2D(this.x - (2 * dot * point.x), this.y - (2 * dot * point.y));
    }

    /**
     * Rounds the components of this vector to the nearest number with the given amount of decimal places.
     * @param {number} decimals The amount of decimal digits to keep.
     * @returns {Vector2D} A new vector with the rounded components.
     */
    roundTo(decimals) {
        return new Vector2D(roundTo(this.x, decimals), roundTo(this.y, decimals));
    }

    /**
     * Compares the coordinates of the given vector to this vector.
     * @param {Vector2D} point The vector to compare with.
     * @returns {boolean} True if the vector's have the same coordinates, false otherwise.
     */
    equals(point) {
        return this.x === point.x && this.y === point.y;
    }
}
