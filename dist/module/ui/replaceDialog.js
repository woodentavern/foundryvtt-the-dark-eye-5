/**
 * Displays a dialog to let the user decide whether to replace an existing item with a new one.
 * @param {TdeItem} oldItem The item that should be replaced, pending confirmation.
 * @returns {Promise.<boolean>} True if the old item was replaced, false otherwise.
 */
export default async function promptReplace(oldItem) {
    try {
        const replace = await Dialog.confirm({
            title: game.i18n.localize("tde5e.replaceDialog.title"),
            content: renderReplaceDialog(oldItem),
        });

        if (replace) await oldItem.delete();
        return replace;
    } catch {
        return false;
    }
}

/**
 * Render a dialog to let the user choose whether to replace the old item with the new one.
 * @param {TdeItem} oldItem The item that should be replaced, pending confirmation.
 * @returns {string} The rendered HTML.
 */
function renderReplaceDialog(oldItem) {
    return `<p class="notes">${game.i18n.format(
        "tde5e.replaceDialog.description",
        { itemOld: `<b>${oldItem.displayName}</b>` },
    )}</p>`;
}
