import { stripHtml } from "../../util.js";

/**
 * Base class for roll modifications. This particular modification is purely
 *  informational. If you need applied behavior, use one of the derived classes.
 */
export class RollModification {
    static get MOD_PRIORITIES() {
        return {
            BEFORE_MODIFIERS: 0,
            BEFORE_ATTRIBUTES: 1,
            BEFORE_QUALITY: 2,
            AFTER_RESULT: 3,
        };
    }

    /**
     * The type of the modification that specifies how it is applied.
     * @type {string}
     */
    type = "default";

    /**
     * The ID of the modification's source.
     * @type {string}
     */
    id;

    /**
     * The name of the modifier (usually the name of the source).
     * @type {string}
     */
    name;

    /**
     * Indicates whether the name of the modifier is concealed from non-owners.
     * @type {boolean}
     */
    hidden;

    /**
     * The value of the modifier that is used for calculations.
     * @type {number}
     */
    value;

    /**
     * Indicates whether the modifier is enabled by default.
     * @type {boolean}
     */
    active;

    /**
     * Indicates whether the modifier is only applied to the final result. When set to true, the base result of the
     *  roll will not be changed.
     * @type {boolean}
     */
    deferred;

    /**
     * Indicates whether the value of the modifier can be changed by the user and should be rendered as an input
     *  element.
     * @type {boolean}
     */
    input;

    /**
     * Indicates whether the value of the modifier is a choice of more than one option. Requires the 'values' property
     *  containing an object with value to description mappings.
     * @type {boolean}
     */
    select;

    /**
     * Creates a roll modification that is displayed in the associated roll and can be individually disabled or
     *  deferred.
     * @param {object | TdeItem} source The data of the modification or the source item.
     * @param {number} value The value of the modification.
     * @param {object} options Additional modification options.
     * @param {boolean=} options.active Indicates whether the modification is enabled by default. Defaults to true.
     * @param {boolean=} options.deferred Indicates whether the modification is excluded from the base result.
     *  Defaults to false.
     * @param {boolean=} options.hidden Indicates whether the modification name is hidden from non-owners.
     * @param {boolean=} options.input Indicates whether the value of this modifier can be changed. Defaults to false.
     * @param {boolean=} options.select Indicates whether this modifier has more than one selectable value.
     *  Defaults to false.
     * @param {object?} options.values When select is true, contains the selectable options.
     */
    constructor(source, value, {
        active = true, deferred = false, hidden = false, input = false, select = false, values = null,
    } = {}) {
        if (!source) return;
        this.id = source.id ?? foundry.utils.randomID();
        this.name = source.displayName ?? source.name;
        this.hidden = hidden;
        this.value = value;
        this.active = active;
        this.deferred = deferred;
        this.input = input;
        this.select = select;
        if (select) this.values = values ?? {};
    }

    /**
     * Checks if this modification should be applied for the given priority.
     * @param {number} priority The priority to check the modification for.
     * @returns {boolean} True if the modification should be applied, false otherwise.
     */
    hasPriority(priority) {
        return priority === RollModification.MOD_PRIORITIES.BEFORE_ATTRIBUTES;
    }

    /**
     * Applies the modification to the result of the given skill check.
     * @param {object} result The result data of the check to apply the modification to.
     */
    // eslint-disable-next-line no-unused-vars -- Keep for documentation purposes.
    apply(result) {}

    /**
     * Creates the data required for displaying this modification.
     * @param {object} rollData The display data of the skill check.
     * @returns {object} The display data of this modification.
     */
    getDisplayData(rollData) {
        const data = {
            effect: game.i18n.localize(`tde5e.check.modType.${this.type}`),
            htmlClass: "modifier",
            ...this,
        };

        if (this.name && this.hidden && !rollData.owner) {
            data.name = game.i18n.localize("tde5e.general.unknown");
        }

        if (!this.input) {
            const displayValue = typeof (this.value) === "number" ? this.value.signedString() : this.value;
            data.effect += ` ${displayValue}`;
        }

        if (this.active) {
            data.htmlClass += " active";
            rollData.activeMods++;
        }

        if (rollData.locked) {
            data.htmlClass += " disabled";
            data.input = false;
        }

        if (typeof (this.value) === "number") data.htmlClass += this.value >= 0 ? " good" : " bad";
        return data;
    }

    /**
     * Creates a roll modification from the given source data.
     * @param {object} data The data of the modification.
     * @returns {RollModification} An initialized modification instance.
     */
    static fromData(data) {
        return Object.assign(this.fromType(data.type), data);
    }

    /**
     * Creates an uninitialized roll modification with the given type.
     * @param {string} type The type of the modification.
     * @returns {RollModification} An empty modification instance.
     */
    static fromType(type) {
        switch (type) {
        case "attribute": return new SingleAttributeModification();
        case "attributes": return new AttributeModification();
        case "botchRange": return new BotchRangeModification();
        case "critRange": return new CritRangeModification();
        case "custom": return new CustomModification();
        case "info": return new InfoModification();
        case "quality": return new QualityModification();
        case "rating": return new RatingModification();
        case "replace": return new ReplaceAttributeModification();
        case "result": return new ResultModification();
        case "skillPoints": return new SkillPointModification();
        case "reroll": return new RerollModification();
        default: return new RollModification();
        }
    }
}

/**
 * Informational entry that displays its value in the tooltip.
 */
export class InfoModification extends RollModification {
    /** @inheritdoc */
    type = "info";

    /** @inheritdoc */
    getDisplayData(rollData) {
        const data = super.getDisplayData(rollData);
        data.effect = this.value;
        return data;
    }
}

/**
 * Roll modification that adds the value to every attribute of the check.
 */
export class AttributeModification extends RollModification {
    /** @inheritdoc */
    type = "attributes";

    /** @override */
    apply(result) {
        result.attributes.forEach(attr => { attr.value += this.value; });
    }
}

/**
 * Roll modification that adds the value to the check's attribute defined by
 *  the index.
 */
export class SingleAttributeModification extends RollModification {
    /** @inheritdoc */
    type = "attribute";

    /**
     * The index of the modified attribute.
     * @type {number}
     */
    index;

    /**
     * @param {object | TdeItem} source The data of the modification or the source item.
     * @param {number} value The value of the modification.
     * @param {number} index The index of the modified attribute.
     * @param {object} options Additional modification options.
     * @inheritdoc
     */
    constructor(source, value, index, options) {
        super(source, value, options);
        this.index = index;
    }

    /** @override */
    apply(result) {
        result.attributes[this.index].value += this.value;
    }

    /** @override */
    getDisplayData(rollData) {
        const data = super.getDisplayData(rollData);
        data.effect = game.i18n.localize(rollData.result.attributes[this.index].name);
        return data;
    }
}

/**
 * Roll modification that replaces the attribute at the given index with the
 *  given name and value.
 */
export class ReplaceAttributeModification extends SingleAttributeModification {
    /** @inheritdoc */
    type = "replace";

    /**
     * The name of the new attribute.
     * @type {string}
     */
    attrName;

    /**
     * @param {object | TdeItem} source The data of the modification or the source item.
     * @param {number} value The unmodified value of the new attribute.
     * @param {number} index The index of the modified attribute.
     * @param {string} attr The property name of the new attribute.
     * @param {object} options Additional modification options.
     * @inheritdoc
     */
    constructor(source, value, index, attr, options) {
        super(source, value, index, options);
        this.attr = attr;
    }

    /** @override */
    apply(result) {
        const attribute = result.attributes[this.index];
        attribute.name = this.attr;
        attribute.value = this.value;
    }
}

/**
 * Roll modification that automatically succeeds or fails the check. Note that
 *  the value for this modification is a boolean, not a number.
 */
export class ResultModification extends RollModification {
    /** @inheritdoc */
    type = "result";

    /** @inheritdoc */
    hasPriority(priority) {
        return priority === RollModification.MOD_PRIORITIES.AFTER_RESULT;
    }

    /** @override */
    apply(result) {
        if (result.success !== this.value) {
            result.success = this.value;
            result.isCrit = result.isSpectacular = false;

            if (result) result.remainingSkillPoints = Math.max(1, result.remainingSkillPoints);
            else result.remainingSkillPoints = 0;
        }
    }

    /** @override */
    getDisplayData(rollData) {
        const data = super.getDisplayData(rollData);
        if (!this.value) data.htmlClass = `${data.htmlClass.substring(0, data.htmlClass.lastIndexOf(" "))} bad`;
        return data;
    }
}

/**
 * Roll modification that adds the value to the skill rating of the check.
 */
export class RatingModification extends RollModification {
    /** @inheritdoc */
    type = "rating";

    /** @override */
    apply(result) {
        result.value += this.value;
    }
}

/**
 * Roll modification that adds the value to the remaining skill points of
 *  successful checks.
 */
export class SkillPointModification extends RollModification {
    /** @inheritdoc */
    type = "skillPoints";

    /** @inheritdoc */
    hasPriority(priority) {
        return priority === RollModification.MOD_PRIORITIES.BEFORE_QUALITY;
    }

    /** @override */
    apply(result) {
        if (result.remainingSkillPoints > 0) result.remainingSkillPoints += this.value;
    }
}

/**
 * Roll modification for the critical range of the check.
 * The crit range starts at 1 and is the value you have to match or roll lower to crit roll.
 */
export class CritRangeModification extends RollModification {
    /** @inheritdoc */
    type = "critRange";

    /** @override */
    apply(result) {
        result.critRange += this.value;
    }
}

/**
 * Roll modification for the botch range of the check.
 * The botch range starts at -1 and is subtracted from 20, giving a >19 chance to botch roll.
 */
export class BotchRangeModification extends RollModification {
    /** @inheritdoc */
    type = "botchRange";

    /** @override */
    apply(result) {
        result.botchRange += this.value;
    }
}

/**
 * Roll modification for the quality of an action.
 */
export class QualityModification extends RollModification {
    /** @inheritdoc */
    type = "quality";

    /** @inheritdoc */
    hasPriority(priority) {
        return priority === RollModification.MOD_PRIORITIES.AFTER_RESULT;
    }

    /** @inheritdoc */
    apply(result) {
        result.quality ??= 0;
        result.quality += this.value;
    }
}

/**
 * Roll modification for enabling a fixed number of attribute rerolls.
 */
export class RerollModification extends RollModification {
    /** @inheritdoc */
    type = "reroll";

    /**
     * The number of available rerolls for each individual attribute.
     * @type {number[]}
     */
    attributes;

    /**
     * Indicates whether the lower or newer result will be kept.
     * @type {boolean}
     */
    keepLower;

    /**
     * @param {object | TdeItem} source The data of the modification or the source item.
     * @param {number} value The total amount of available attribute rerolls.
     * @param {number[]} attributes The number of available rerolls for each individual attribute.
     * @param {boolean} keepLower Indicates whether the lower or newer result will be kept.
     * @param {object} options Additional modification options.
     * @inheritdoc
     */
    constructor(source, value, attributes, keepLower, options) {
        super(source, value, options);
        this.attributes = attributes;
        this.keepLower = keepLower;
    }
}

/**
 * Roll modification for complex logic.
 */
export class CustomModification extends RollModification {
    /** @inheritdoc */
    type = "custom";

    /**
     * An array of @see RollModification.MOD_PRIORITIES which indicate when this modification will be applied.
     */
    priority;

    /**
     * Creates a roll modification that calls the "applyRollModification" whenever the roll passes one of the
     *  given priorities (@see RollModification.MOD_PRIORITIES for valid values). The method is called with the
     *  current result of the roll and the current priority. When the priority is an array, it may be called multiple
     *  times.
     * @param {object | TdeItem} source The data of the modification or the source item.
     * @param {string} value UUID of the item that implements the modification logic.
     * @param {string | string[]} priority The priority (or multiple priorities) indicating when the method is called.
     * @param {object} options Additional modification options.
     */
    constructor(source, value, priority, options) {
        super(source, value, options);
        if (!Array.isArray(priority)) priority = [priority];
        this.priority = priority;
    }

    /** @inheritdoc */
    hasPriority(priority) {
        return this.priority.includes(priority);
    }

    /** @inheritdoc */
    apply(result, priority, modifiers) {
        const item = fromUuidSync(this.value);
        if (item?.applyRollModification) item.applyRollModification(result, priority, modifiers);
    }

    /** @inheritdoc */
    getDisplayData(rollData) {
        const data = super.getDisplayData(rollData);
        const description = fromUuidSync(this.value)?.system?.description;
        if (description) data.effect = stripHtml(description);
        else data.effect = null;
        return data;
    }
}
