import ModifiableRoll from "./modifiableRoll.js";
import HudContextMenu from "../hudContextMenu.js";
import * as mods from "./rollModification.js";
import { warn } from "../../util.js";
import { stopEvent } from "../../jsUtils.js";

export default class TdeSkillCheck extends ModifiableRoll {
    /**
     * Skill check constructor to override the roll formula.
     * @see {@link ModifiableRoll.constructor} for additional details.
     * @param {object} item The item to roll.
     * @param {object? | number} data When the data is a number, it is interpreted as initial difficulty.
     * @override
     */
    constructor(item, data) {
        super(item, data, "d20+d20+d20");
        if (typeof data === "number") this.data.difficulty = data;
    }

    /** @inheritdoc */
    prepareData(item) {
        const data = super.prepareData(item);
        if (!data.actor) return null;

        data.skill = {
            id: item.id,
            name: item.name,
            value: item.system.improvement.value,
        };

        data.hideValue = item.system.check.hideValue;
        data.hideResult = item.system.check.hideResult;
        data.attributes = item.system.check.attributes.map(attribute => ({
            name: attribute,
            value: item.actor.system.attributes[attribute].value ?? 8,
        }));

        data.critRange = 1;
        data.botchRange = -1;
        data.difficulty = 0;
        data.deferModifiers = game.combat?.round !== 0;
        data.animatedRolls = [1, 2, 3];
        return data;
    }

    /**
     * Generates the result of the skill check.
     * @see {@link https://foundryvtt.com/api/Roll.html#evaluate} for additional details.
     * @returns {Promise.<TdeSkillCheck>} A promise representing this roll.
     * @override
     */
    async evaluate() {
        const wasRolled = this._evaluated;
        if (!wasRolled) await super.evaluate();

        const activeModifiers = this.data.modifiers.filter(mod => mod.active);
        const result = this.calculateModifiedResult(activeModifiers, wasRolled);

        if (this.data.deferModifiers) {
            const baseModifiers = activeModifiers.filter(mod => !mod.deferred);
            if (activeModifiers.length !== baseModifiers.length) {
                result.base = this.calculateModifiedResult(baseModifiers, wasRolled);
            }
        }

        this._total = result;
        return this;
    }

    /**
     * Gets the quality level of the check's result. This is required to be
     *  numeric so that Foundry will let us handle the rendering.
     * @returns {number?} The quality level if the roll was already evaluated, undefined otherwise.
     */
    get total() {
        return this._total?.qualityLevel;
    }

    /**
     * Calculates a check result using the given modifiers. The result includes
     *  final attribute values, success and critical flags and skill points.
     * @param {RollModification[]} modifiers The modifiers to apply to the result.
     * @param {boolean} wasRolled Flag indicating whether the roll was evaluated before.
     * @returns {object} The modified result of the check.
     */
    calculateModifiedResult(modifiers, wasRolled) {
        // Create preliminary result.
        const result = {
            value: this.data.skill.value,
            attributes: this.data.attributes.map((attribute, index) => ({
                name: attribute.name,
                value: attribute.value,
                roll: this.dice[index].values[0],
                isCrit: false,
                isBotch: false,
                discardedRolls: wasRolled
                    ? this.dice[index].results.filter(r => !r.active).map(r => r.result)
                    : [],
            })),
            critRange: this.data.critRange,
            botchRange: this.data.botchRange,
            success: false,
            isCrit: false,
            isSpectacular: false,
            remainingSkillPoints: 0,
            qualityLevel: 0,
        };

        // Group modifiers by type for application order.
        this._applyModsForPriority(result, modifiers, mods.RollModification.MOD_PRIORITIES.BEFORE_MODIFIERS);
        this._applyModsForPriority(result, modifiers, mods.RollModification.MOD_PRIORITIES.BEFORE_ATTRIBUTES);
        result.attributes.forEach(attr => { attr.value += this.data.difficulty; });
        result.value = Math.max(result.value, 0);

        // Check for critical success.
        const critCount = result.attributes.reduce((count, attr) => {
            const roll = attr.discardedRolls[0] ?? attr.roll;
            attr.isCrit = roll < 1 + result.critRange;
            if (attr.isCrit) count++;
            return count;
        }, 0);
        if (critCount >= 2) {
            result.success = result.isCrit = true;
            result.isSpectacular = critCount === result.attributes.length;
            result.remainingSkillPoints = Math.max(1, result.value);
            this._applyModsForPriority(result, modifiers, mods.RollModification.MOD_PRIORITIES.BEFORE_QUALITY);
            result.qualityLevel = TdeSkillCheck.skillPointsToQuality(result.remainingSkillPoints);
            this._applyModsForPriority(result, modifiers, mods.RollModification.MOD_PRIORITIES.AFTER_RESULT);
            return result;
        }

        // Check for botch failure
        const botchCount = result.attributes.reduce((count, attr) => {
            const roll = attr.discardedRolls[0] ?? attr.roll;
            attr.isBotch = !attr.isCrit && roll > 20 + result.botchRange;
            if (attr.isBotch) count++;
            return count;
        }, 0);
        if (botchCount >= 2) {
            if (!modifiers.some(mod => mod.type === "botchRange")) this.data.locked = true; // Lock natural botches.
            result.isCrit = true;
            result.isSpectacular = botchCount === result.attributes.length;

            this._applyModsForPriority(result, modifiers, mods.RollModification.MOD_PRIORITIES.BEFORE_QUALITY);
            result.qualityLevel = 0;
            this._applyModsForPriority(result, modifiers, mods.RollModification.MOD_PRIORITIES.AFTER_RESULT);
            return result;
        }

        // Calculate remaining points when neither crit nor botch
        result.remainingSkillPoints = result.attributes.reduce((rsp, attr) => {
            rsp += Math.min(0, attr.value - attr.roll);
            return rsp;
        }, result.value);

        this._applyModsForPriority(result, modifiers, mods.RollModification.MOD_PRIORITIES.BEFORE_QUALITY);

        // Make sure a success with 0 points is counted as success with 1 and clamp negative points.
        if (result.remainingSkillPoints === 0) result.remainingSkillPoints = 1;
        if (result.remainingSkillPoints < 0) result.remainingSkillPoints = 0;

        result.success = result.remainingSkillPoints > 0;
        result.qualityLevel = TdeSkillCheck.skillPointsToQuality(result.remainingSkillPoints);

        // Automatic success or failure due to modifier.
        this._applyModsForPriority(result, modifiers, mods.RollModification.MOD_PRIORITIES.AFTER_RESULT);
        return result;
    }

    /**
     * Retrieves the first available reroll for the attribute with the given index.
     * @param {number} index The index of the die to check rerolls for.
     * @returns {mods.RerollModification?} An object containing the first available reroll or null, if there is none.
     */
    getReroll(index) {
        return this.data.modifiers.find(mod => mod.type === "reroll" && mod.active
            && mod.attributes[index] > 0 && mod.value > 0);
    }

    /**
     * Rerolls a single die of the check, identified by its index. The previous
     *  result is stored within the total as a discarded roll for that attribute.
     * @param {number} index The index of the die to reroll.
     */
    async rerollSingle(index) {
        if (!this._evaluated) await this.evaluate();

        // Store the previously active roll
        const die = this.dice[index];
        const previousResult = die.results.find(result => result.active);

        // Adjust remaining rerolls.
        const reroll = this.getReroll(index);
        if (reroll) {
            reroll.value--;
            reroll.attributes[index]--;
        } else if (!game.user.isGM) {
            warn(`Rerolled result of ${this.data.skill.name} without valid reroll for index ${index}.`);
        }

        // Generate new roll
        const newResult = await die.roll();
        if (!previousResult) return;

        previousResult.rerolled = true;

        if (!reroll?.keepLower || newResult.result <= previousResult.result) {
            previousResult.active = false;
        } else {
            newResult.active = false;
        }
    }

    /**
     * Evaluates and renders the roll.
     * @see {@link https://foundryvtt.com/api/Roll.html#render} for additional details.
     * @returns {Promise.<HTMLElement>} A promise representing the rendered HTML template.
     * @override
     */
    async render(chatOptions = {}) {
        if (chatOptions.isPrivate) return "";
        if (!this._evaluated) await this.evaluate();
        if (!chatOptions.animate) this.data.animatedRolls = [];

        // Make sure that the user can modify this roll.
        let actor = await fromUuid(this.data.actor.uuid);
        if (actor instanceof TokenDocument) actor = actor.actor;
        const owner = actor?.isOwner ?? game.user.isGM;
        this.data.locked ||= !owner;

        const result = foundry.utils.deepClone(this._total);
        const canReroll = !this.data.locked && !result.isCrit;
        const displayData = {
            ...this.data,
            owner,
            result,
            activeMods: 0,
            canUseFate: canReroll
                && actor.canUseFate && actor.system.resources.fatePoints.value > 0 // Fate point available.
                && !this.data.modifiers.some(mod => mod.id === "fate"), // Not already rerolled with fate.
            animateFaster: this.data.animatedRolls.some(r => r === false),
        };

        displayData.hideResult &&= !owner;
        displayData.hideValue ||= displayData.hideResult;
        displayData.hideValue &&= !owner;

        // Replace strings with translations.
        displayData.modifiers = this.data.modifiers.map(mod => mod.getDisplayData(displayData));

        // Prepare additional HTML data.
        result.attributes.forEach((attr, index) => {
            if (attr.isCrit) attr.rollClass = result.isCrit ? "success critical" : "success";
            else if (attr.isBotch) attr.rollClass = result.isCrit ? "fail critical" : "fail";
            else attr.rollClass = "";

            const actorValue = actor?._source.system.attributes[attr.name]?.base ?? attr.value;
            if (attr.value > actorValue) attr.valueClass = "attr-increased";
            else if (attr.value < actorValue) attr.valueClass = "attr-lowered";
            else attr.valueClass = "";

            attr.canReroll = canReroll && !!this.getReroll(index);
            attr.discardedRolls?.reverse();
            attr.animate = this.data.animatedRolls?.[index];
        });

        result.html = {
            class: result.success ? "success" : "fail",
            text: TdeSkillCheck.toDisplayResult(result),
        };
        if (result.quality) result.html.class += ` ${TdeSkillCheck.modifierToQuality(result.quality)}`;
        if (result.isCrit) result.html.class += " critical";

        return renderTemplate("systems/tde5e/templates/chat/skillCheck.hbs", displayData);
    }

    /** @inheritdoc */
    activateListeners(html, message) {
        html.find("a.roll-actor-portrait").click(this.openActorSheet.bind(this));
        html.find(".animate").one("animationend", event => event.currentTarget.classList.remove("animate"));
        if (this.data.locked) return;

        super.activateListeners(html, message);
        html.find("input[name=difficulty]").change({ message }, this.changeDifficulty.bind(this));
        html.find("div.attribute-rolls").on("click", "a.hexagon-button", { message }, this.applyReroll.bind(this));

        const fateButton = html.find("a.reroll-fate");
        if (fateButton.length) {
            HudContextMenu.create(ui.chat, html.find("div.roll-result"), "", this._buildResultMenu(message));
            fateButton.click({ message }, event => {
                const result = event.currentTarget.parentElement;
                $(result).trigger("contextmenu");
                return false;
            });
        }
    }

    /**
     * Opens the sheet of the owning actor.
     */
    openActorSheet() {
        const actorId = this.data.actor?.uuid;
        if (!actorId) return;
        fromUuid(actorId).then(actor => actor?.sheet.render(true));
    }

    /**
     * Changes the difficulty of the check based on the value of the input of the given change event.
     * @param {jQuery.Event} event The JQuery event of the difficulty change. This must contain the
     *  message object in its data property.
     * @returns {Promise.<object>} A promise representing the asynchronous message update.
     */
    changeDifficulty(event) {
        const newValue = parseInt(event.currentTarget.value, 10) || 0;
        if (newValue === this.data.difficulty) return Promise.resolve();
        this.data.difficulty = newValue;
        return this.refresh(event.data.message);
    }

    /**
     * Attempts to apply a fate point for rerolling this skill check.
     * @param {object} message The message containing the roll.
     * @param {number} amount The amount of rerolls to add.
     * @returns {Promise.<object>} A promise representing the actor update.
     */
    async addFateReroll(message, amount) {
        const existingReroll = this.data.modifiers.find(mod => mod.id === "fate");
        if (existingReroll) {
            warn(`A fate point was already used for ${this.data.skill.name}.`, true);
            return Promise.resolve();
        }

        this.data.modifiers.unshift(new mods.RerollModification(
            { id: "fate", name: game.i18n.localize("tde5e.traits.fate") },
            amount,
            new Array(this.data.attributes.length).fill(1),
            false,
        ));

        const rolls = [];
        const animatedRerolls = [];
        if (amount >= this.data.attributes.length) {
            for (let i = 0; i < this.data.attributes.length; i++) {
                rolls.push(this.rerollSingle(i));
                animatedRerolls.push(i + 1);
            }
        }

        await Promise.all(rolls);
        await this.refresh(message, animatedRerolls);
        const actor = await fromUuid(this.data.actor.uuid);
        return actor?.update({
            "system.resources.fatePoints.value": actor._source.system.resources.fatePoints.value - 1,
        });
    }

    /**
     * Applies a reroll for the attribute that triggered the event.
     * @param {jQuery.Event} event The click event of the reroll button. Must contain the message object as data.
     * @returns {Promise.<object>} A promise representing the asynchronous message update.
     */
    async applyReroll(event) {
        stopEvent(event);
        const index = parseInt(event.currentTarget.dataset.index, 10);
        await this.rerollSingle(index);
        const animatedRolls = [false, false, false];
        animatedRolls[index] = 1;
        return this.refresh(event.data.message, animatedRolls);
    }

    /** @inheritdoc */
    _buildModifierMenu(message) {
        const menuItems = super._buildModifierMenu(message);

        menuItems.push({
            name: game.i18n.localize("tde5e.check.modifications.attributes"),
            icon: "<i class='far fa-plus-square'></i>",
            callback: () => {
                this.data.modifiers.push(new mods.AttributeModification(
                    { displayName: "Custom" },
                    -1,
                    { input: true },
                ));
                this.refresh(message);
            },
        });

        menuItems.push({
            name: game.i18n.localize("tde5e.check.modifications.rating"),
            icon: "<i class='far fa-plus-square'></i>",
            callback: () => {
                this.data.modifiers.push(new mods.RatingModification({ displayName: "Custom" }, 2, { input: true }));
                this.refresh(message);
            },
        });

        menuItems.push({
            name: game.i18n.localize("tde5e.check.modifications.skillPoints"),
            icon: "<i class='far fa-plus-square'></i>",
            callback: () => {
                this.data.modifiers.push(new mods.SkillPointModification(
                    { displayName: "Custom" },
                    1,
                    { input: true, deferred: true },
                ));
                this.refresh(message);
            },
        });

        for (let i = 0; i < this.data.attributes.length; i++) {
            menuItems.push({
                name: game.i18n.format(
                    "tde5e.check.modifications.attribute",
                    { attr: game.i18n.localize(`tde5e.actor.attributes.${this.data.attributes[i].name}`) },
                ),
                icon: "<i class='far fa-plus-square'></i>",
                callback: () => {
                    this.data.modifiers.push(new mods.SingleAttributeModification(
                        { displayName: "Custom" },
                        -1,
                        i,
                        { input: true },
                    ));
                    this.refresh(message);
                },
            });
        }

        menuItems.push({
            name: game.i18n.localize("tde5e.check.modifications.critRange"),
            icon: "<i class='far fa-plus-square'></i>",
            callback: () => {
                this.data.modifiers.push(new mods.CritRangeModification({ displayName: "Custom" }, 1, { input: true }));
                this.refresh(message);
            },
        });

        menuItems.push({
            name: game.i18n.localize("tde5e.check.modifications.botchRange"),
            icon: "<i class='far fa-plus-square'></i>",
            callback: () => {
                this.data.modifiers.push(new mods.BotchRangeModification(
                    { displayName: "Custom" },
                    -1,
                    { input: true },
                ));
                this.refresh(message);
            },
        });

        menuItems.push({
            name: game.i18n.localize("tde5e.check.modifications.rerollBetter"),
            icon: "<i class='far fa-plus-square'></i>",
            callback: () => {
                this.data.modifiers.push(new mods.RerollModification(
                    { displayName: "Custom" },
                    1,
                    [1, 1, 1],
                    true,
                    { input: true },
                ));
                this.refresh(message);
            },
        });

        menuItems.push({
            name: game.i18n.localize("tde5e.check.modifications.rerollNew"),
            icon: "<i class='far fa-plus-square'></i>",
            callback: () => {
                this.data.modifiers.push(new mods.RerollModification(
                    { displayName: "Custom" },
                    1,
                    [1, 1, 1],
                    false,
                    { input: true },
                ));
                this.refresh(message);
            },
        });

        return menuItems;
    }

    /**
     * Creates the array of context menu items when right clicking the result.
     * @param {ChatMessage} message The message to update when the roll changed.
     * @returns {object[]} An array of context menu items (@see {@link https://foundryvtt.com/api/ContextMenu.html}).
     */
    _buildResultMenu(message) {
        const entries = [
            {
                name: game.i18n.format("tde5e.check.reroll", { attr: game.i18n.localize("tde5e.general.all") }),
                icon: "",
                callback: () => this.addFateReroll(message, this.data.attributes.length),
            },
        ];

        for (let i = 1; i < this.data.attributes.length; i++) {
            entries.push({
                name: game.i18n.format("tde5e.check.reroll", { attr: i }),
                icon: "",
                callback: () => this.addFateReroll(message, i),
            });
        }

        if (this._total?.success) {
            entries.push({
                name: game.i18n.localize("tde5e.check.modifications.improveQl"),
                icon: "",
                callback: () => {
                    this.data.modifiers.push(new mods.SkillPointModification(
                        { id: "fate", displayName: game.i18n.localize("tde5e.aspects.fate") },
                        3,
                    ));
                    this.refresh(message);
                },
            });
        }

        return entries;
    }

    /**
     * Applies modifiers matching the given priority to the given result.
     * @param {object} result The current result of the check.
     * @param {RollModification[]} modifiers The modifiers to apply.
     * @param {number} priority The priority to apply the modifiers for.
     */
    _applyModsForPriority(result, modifiers, priority) {
        modifiers.filter(mod => mod.hasPriority(priority))
            .forEach(mod => mod.apply(result, priority, modifiers));
    }

    /** @inheritdoc */
    refresh(message, animatedRolls = []) {
        this.data.animatedRolls = animatedRolls;
        return super.refresh(message);
    }

    /**
     * Restores a skill check using its item and previous result.
     * @param {TdeItem} item The item that was rolled.
     * @param {object} result The result of the previous roll.
     * @returns {TdeSkillCheck} A new skill check containing the previously rolled result.
     */
    static fromResult(item, result) {
        const roll = new TdeSkillCheck(item);
        roll._evaluated = true;
        roll._total = result;
        roll._dice = result.attributes.map(attr => {
            const die = new Die({ faces: 20 });
            die._evaluated = true;
            die._total = attr.roll;
            die.results = attr.discardedRolls.map(discarded => ({ result: discarded, active: false }));
            die.results.push({ result: die._total, active: true });
            return die;
        });
        return roll;
    }

    /**
     * Converts a given amount of remaining points into the quality level, bounded between 0 and 6.
     * @param {number} remainingSkillPoints The amount of points remaining after deduction of attributes.
     * @returns {number} The resulting quality level.
     */
    static skillPointsToQuality(remainingSkillPoints) {
        return remainingSkillPoints <= 0 ? 0 : Math.min(6, Math.ceil(remainingSkillPoints / 3));
    }

    /**
     * Converts a maneuver modifier to a localizable quality.
     * @param {number} modifier The effective modifier to convert.
     * @returns {string} The resulting maneuver quality.
     */
    static modifierToQuality(modifier) {
        if (modifier < -3) return "impossible";
        if (modifier === -3) return "limited";
        if (modifier < 0) return "disadvantaged";
        if (modifier > 2) return "overreached";
        if (modifier > 0) return "advantaged";
        return "neutral";
    }

    /**
     * Generates a human readable success or failure description from the given result.
     * @param {object} result The result of the skill check.
     * @returns {string} The summarized result of the check.
     */
    static toDisplayResult(result) {
        if (result.success) {
            let resultString;
            if (result.isCrit) {
                resultString = game.i18n.localize("tde5e.check.critical");
            } else {
                resultString = game.i18n.localize("tde5e.check.success");
            }

            if (result.quality) {
                const quality = TdeSkillCheck.modifierToQuality(result.quality);
                resultString = `${game.i18n.localize(`tde5e.combat.maneuverQuality.${quality}`)} ${resultString}`;
            }

            return resultString;
        }

        if (result.isCrit) return game.i18n.localize("tde5e.check.botch");
        return game.i18n.localize("tde5e.check.failed");
    }
}
