import HudContextMenu from "../hudContextMenu.js";
import { redirectRollUpdate } from "../../socket.js";
import { AttributeModification, RollModification } from "./rollModification.js";
import { stopEvent } from "../../jsUtils.js";
import { tryParseInt } from "../../util.js";

export default class ModifiableRoll extends Roll {
    /**
     * Creates a new roll from a {@link TdeItem} or from existing roll data.
     * @param {TdeItem | string} item The item to create the roll from. For the copy constructor, this must
     *  be a string to match the super constructor. In that case, the roll data is not created from the item,
     *  but passed in through the data parameter.
     * @param {object?} data Data object for copy constructor usage (see above).
     * @param {string?} formula The Foundry roll formula to create the dice with.
     */
    constructor(item, data, formula) {
        if (typeof item === "string") {
            super(item, data);
            this.data.modifiers = this.data.modifiers.map(mod => RollModification.fromData(mod));
        } else {
            super(formula ?? "d20");
            this.data = this.prepareData(item);
        }
    }

    /**
     * Extracts roll data from the given item.
     * @param {TdeItem} item The item to read the data from.
     * @returns {object} The immutable roll data.
     */
    prepareData(item) {
        if (!item?.system || !item.actor) return {};
        return {
            actor: {
                uuid: item.actor.uuid,
                name: item.actor.name,
                img: item.actor.img,
            },
            modifiers: this.prepareModifiers(item),
        };
    }

    /**
     * Generates the list of potential modifiers from the actor's items and
     *  merges any overrides or custom modifiers from the item's flags.
     * @param {TdeItem} item The item to read modifiers and settings from.
     * @returns {RollModification[]} A list of modifiers with merged defaults.
     */
    prepareModifiers(item) {
        // Generate base list
        const modifiers = item.actor.items.reduce((mods, modItem) => {
            if (!modItem.canModifyRoll(item)) return mods;
            const mod = modItem.modifyRoll(item);
            if (!mod) return mods;

            if (Array.isArray(mod)) mods.push(...mod);
            else mods.push(mod);
            return mods;
        }, []);

        // Find additional token effects.
        const specialEffects = item.actor.effects.contents;

        const penalty = specialEffects.find(effect => effect.statuses.has("penalty"));
        if (penalty) {
            modifiers.push(new AttributeModification(
                { id: "penalty", displayName: penalty.name },
                (penalty.getFlag("statuscounter", "counter")?.value ?? 1) * -1,
            ));
        }

        const bonus = specialEffects.find(effect => effect.statuses.has("bonus"));
        if (bonus) {
            modifiers.push(new AttributeModification(
                { id: "bonus", displayName: bonus.name },
                bonus.getFlag("statuscounter", "counter")?.value ?? 1,
            ));
        }

        // Retreive user settings
        const defaultModifiers = item.getFlag("tde5e", "rollModifiers");
        if (!defaultModifiers) return modifiers;

        // Apply user settings
        for (const preset of defaultModifiers) {
            const modifier = modifiers.find(mod => mod.id === preset.id && mod.name === preset.name);
            if (modifier) foundry.utils.mergeObject(modifier, preset);
            else if (preset.input) modifiers.push(RollModification.fromData(preset));
        }

        return modifiers;
    }

    /**
     * Stores the current modifier configuration within the item's flags.
     *  Custom modifiers are copied entirely, auto generated modifiers only
     *  retain their active flag.
     */
    async saveModifiers() {
        const modSettings = this.data.modifiers.filter(mod => mod.id)
            .map(mod => {
                if (mod.input) return mod;
                if (mod.select) {
                    return {
                        id: mod.id, name: mod.name, active: mod.active, value: mod.value,
                    };
                }
                return { id: mod.id, name: mod.name, active: mod.active };
            });
        if (modSettings.length === 0) return;
        const item = (await fromUuid(this.data.actor.uuid))?.items.get(this.data.skill?.id);
        if (!item) ui.notifications.warn("The Dark Eye | The item of that roll no longer exists.");

        if (modSettings.length === 0) {
            ui.notifications.info(`The Dark Eye | ${this.data.skill?.name} does not have any modifiers to save.`);
        } else {
            await item.setFlag("tde5e", "rollModifiers", modSettings);
            ui.notifications.info(`The Dark Eye | Saved default modifiers for ${this.data.skill?.name}.`);
        }
    }

    /**
     * Attaches listeners to the roll's HTML, allowing the user to modify it.
     *  This method should not be called when the roll is locked.
     * @param {jQuery} html The JQuery object containing the rendered roll.
     * @param {ChatMessage} message The message to update when the roll changed.
     */
    activateListeners(html, message) {
        // Create modifier context menu.
        const modifierTable = html.find("details.modifier-header");
        HudContextMenu.create(
            ui.chat,
            html.find("div.tde5e-roll"),
            "details.modifier-header",
            this._buildModifierMenu(message),
        );

        // Connect add and save buttons
        modifierTable.find("a.fa-save").click(this.saveModifiers.bind(this));
        modifierTable.find("a.fa-plus-square").click(() => {
            modifierTable.trigger("contextmenu");
            return false;
        });

        // Connect modifier toggles.
        modifierTable.find("div.modifier").click(event => {
            const tag = event.target.tagName;
            if (tag === "INPUT" || tag === "SELECT" || tag === "OPTION") return;
            $(event.currentTarget).find("input[type='checkbox']").trigger("click");
            stopEvent(event);
        });

        // Connect modifier inputs.
        modifierTable.find("div.modifier-list").on("change", "input, select", event => {
            const index = $(event.currentTarget.parentElement).index();
            const modifier = this.data.modifiers[index];

            switch (event.currentTarget.type) {
            case "checkbox":
                modifier.active = event.currentTarget.checked;
                break;
            case "select-one": {
                modifier.value = tryParseInt(event.currentTarget.value);
                break;
            }
            case "number":
                modifier.value = parseInt(event.currentTarget.value, 10);
                break;
            case "text":
                if (!event.currentTarget.value) this.data.modifiers.splice(index, 1);
                else modifier.name = event.currentTarget.value;
                break;
            default: break;
            }

            this.refresh(message);
        });
    }

    /**
     * Extends Foundry's method to make sure that the content is set (so that
     *  it won't be overriden later).
     * @override
     */
    async toMessage(messageData = {}, options = {}) {
        if (!messageData.content) messageData.content = "";
        return super.toMessage(messageData, options);
    }

    /**
     * Reevaluates the result of the roll and updates the given chat message.
     * @param {ChatMessage} message The message to update.
     * @returns {Promise.<object>} A promise representing the asynchronous message update.
     */
    async refresh(message) {
        await this.evaluate();
        return redirectRollUpdate(message.id, this.toJSON());
    }

    /**
     * Creates the array of context menu items when right clicking the modifiers.
     * @returns {object[]} An array of context menu items (@see {@link https://foundryvtt.com/api/ContextMenu.html}).
     */
    _buildModifierMenu() {
        return [{
            name: game.i18n.localize("tde5e.check.modifications.save"),
            icon: "<i class='far fa-save'></i>",
            callback: this.saveModifiers.bind(this),
        }];
    }

    /**
     * Creates a minimal object of this roll for serialization that includes
     *  the custom data.
     * @see {@link https://foundryvtt.com/api/Roll.html#toJSON} for additional details.
     * @returns {object} The structured data of the roll.
     * @override
     */
    toJSON() {
        const serializedRoll = super.toJSON();
        serializedRoll.data = this.data;
        return serializedRoll;
    }
}
