import HudContextMenu from "./hudContextMenu.js";

/**
 * Extends the context menu to allow global positioning.
 */
export default class GlobalContextMenu extends HudContextMenu {
    /**
     * Extends the constructor to allow custom css classes to be passed down.
     * @inheritdoc
     */
    constructor(element, selector, menuItems, options = { eventName: "contextmenu" }, customCss = "") {
        super(element, selector, menuItems, options);
        this.customCss = customCss;
    }

    /**
     * Create a menu with entries adjustable by hooked functions.
     * @override
     */
    static create(app, html, selector, menuItems, { hookName = "EntryContext", ...options } = {}) {
        for (const cls of app.constructor._getInheritanceChain()) {
            Hooks.call(`get${cls.name}${hookName}`, html, menuItems);
        }

        return new GlobalContextMenu(html, selector, menuItems, options, options.css);
    }

    /**
     * Position the menu as fixed element relative to the window.
     * @override
     */
    _setPosition(html, target) {
        html.css("visibility", "hidden"); // Hide before making adjustments.

        // Adjust DOM content.
        target.addClass("context");
        html.addClass("fixed-context");
        html.addClass("reset-font");
        if (this.customCss) html.addClass(this.customCss);
        html.insertAfter(target);

        // Calculate position relative to window.
        const targetRect = target[0].getBoundingClientRect();
        html.css("left", targetRect.left + 2);
        if (targetRect.bottom > (window.innerHeight / 2)) {
            html.addClass("expand-up");
            html.css("bottom", window.innerHeight - targetRect.top + 2);
        } else {
            html.addClass("expand-down");
            html.css("top", targetRect.bottom + 2);
        }

        html.css("visibility", ""); // Unhide after we're done.
    }
}
