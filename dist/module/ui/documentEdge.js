import Vector2D from "./vector2d.js";

/**
 * Edge connecting two @see DocumentNode instances.
 */
export default class DocumentEdge {
    /**
     * The node at which the edge starts.
     * @type {DocumentNode}
     */
    startNode;

    /**
     * The node to which the edge leads.
     * @type {DocumentNode}
     */
    endNode;

    /**
     * The element of the edge. May be null if the edge wasn't rendered yet.
     * @type {HTMLElement?}
     */
    _element;

    /**
     * Creates a new edge between the given nodes.
     * @param {DocumentNode} start The start node of the edge.
     * @param {DocumentNode} end The end node of the edge.
     */
    constructor(start, end) {
        this.startNode = start;
        this.endNode = end;
    }

    /**
     * @returns {string} A unique key identifying the edge.
     */
    get key() {
        return this.startNode.getEdgeKey(this.endNode);
    }

    /**
     * Calculates attractive forces between the connected nodes and adds them to the nodes' current force.
     * @param {number} targetDistance The preferred distance between nodes.
     * @param {number} progress The current progress of the layout between 0 and 1.
     */
    calculateForce(targetDistance, progress) {
        // Interpolate between center and box distance.
        const direction = this.startNode._position.subtract(this.endNode._position);
        const scaledDistance = (1 - progress) * direction.magnitudeSquared()
            + progress * this.startNode._getSquaredDistance(this.endNode);

        // Calculate force.
        const attractiveForce = scaledDistance / targetDistance;
        const attraction = direction.normalize().multiplyScalar(attractiveForce);

        // Apply attraction to both nodes.
        this.startNode._force = this.startNode._force.subtract(attraction);
        this.endNode._force = this.endNode._force.add(attraction);
    }

    /**
     * Repositions the edge's element to visually connect the nodes.
     */
    applyPosition() {
        const from = this.startNode._position;
        const to = this.endNode._position;
        const xAxis = new Vector2D(1, 0);

        const direction = to.subtract(from);
        const angle = Math.atan2(xAxis.cross(direction), xAxis.dot(direction));

        this._element.style.transform = `translate(${from.x}px, ${from.y}px) rotate(${angle.toFixed(2)}rad)`;
        this._element.style.width = `${Math.floor(direction.magnitude())}px`;
    }

    /**
     * Creates and positions an element for this edge.
     * @param {HTMLElement} tree The parent element to append the edge to.
     */
    render(tree) {
        if (!this._element) {
            this._buildElement();
            tree.append(this._element);
        }

        this.applyPosition();
    }

    /**
     * Creates the element that visually represents this edge.
     */
    _buildElement() {
        this._element = document.createElement("div");
        this._element.classList.add("document-edge");
        this._element.classList.add("animate");
        this._element.innerHTML = "<i class='fas fa-caret-left'></i>";
    }

    /**
     * Cleans up references and elements associated with this edge.
     */
    destroy() {
        this.startNode = this.endNode = null;
        this._element?.remove();
        this._element = null;
    }
}
