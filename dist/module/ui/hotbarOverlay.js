import TdeItem from "../items/item.js";
import { on } from "../jsUtils.js";

/**
 * Registers hooks required for using the hotbar item overlay.
 */
export function initializeHotbarOverlay() {
    Hooks.on("renderHotbar", renderOverlay);
}

/**
 * Renders item controls for opening sheets, rolling or using them and manipulating their charges.
 * The controls are only rendered for the active page, but listeners are always activated.
 * @param {Hotbar} app The application object of the hotbar.
 * @param {jQuery.Element} html The rendered HTML of the hotbar.
 */
function renderOverlay(app, html) {
    const macros = app._getMacrosByPage(app.page);
    const element = html[0];
    renderControls(element, macros);

    on(element, "click", "button.consume-charge", event => changeChargeValue(event, -1), true);
    on(element, "click", "button.add-charge", event => changeChargeValue(event, 1), true);
    on(element, "click", "button.roll-item", event => getMacroItem(event)?.roll(), true);
    on(element, "click", "button.use-item", event => getMacroItem(event)?.activate(), true);
    on(element, "click", "button.open-sheet", event => getMacroItem(event)?.sheet?.render(true), true);
}

/**
 * Renders controls to allow multiple interactions on one macro slot.
 * @param {HTMLElement} html The rendered HTML of the hotbar.
 * @param {object[]} macros The macros to render the controls for.
 */
export function renderControls(html, macros) {
    for (const slot of macros) {
        const itemUuid = slot.macro?.getFlag("tde5e", "sourceItem");
        if (!itemUuid) continue;

        const item = fromUuidSync(itemUuid);
        if (!item || !(item instanceof TdeItem)) continue;

        const quantityInfo = item.quantityInformation;
        const canRoll = item.isRollable;
        const canUse = item.isLightSource;
        if (!(quantityInfo || canRoll || canUse)) continue;

        if (quantityInfo) quantityInfo.manual = (quantityInfo.recharge?.type ?? "manual") === "manual";

        const element = html.querySelector(`li.macro[data-macro-id="${slot.macro.id}"]`);
        renderTemplate("systems/tde5e/templates/items/hotbarOverlay.hbs", {
            canRoll,
            canUse,
            quantity: quantityInfo,
            item,
        }).then(text => {
            const overlay = document.createElement("div");
            overlay.classList.add("macro-overlay");
            overlay.dataset.item = item.uuid;
            overlay.innerHTML = text;
            element.append(overlay);
        });
    }
}

/**
 * Modifies the current charge value of the item identified by the given event.
 * @param {jQuery.Event} event The event of the button click.
 * @param {number} value The differential value change to apply.
 */
function changeChargeValue(event, value) {
    const item = getMacroItem(event);
    if (!item) return;

    const quantityInfo = item.quantityInformation;
    if (quantityInfo && (value < 0 ? quantityInfo.value > 0 : quantityInfo.value < quantityInfo.max)) {
        item.update({ [quantityInfo.path]: quantityInfo.value + value });
    }
}

/**
 * Stops bubbling of the given event and resolves the item associated with its source macro.
 * @param {jQuery.Event} event The JQuery event of the button click.
 * @returns {Document?} A document if the macro UUID is valid, null otherwise.
 */
function getMacroItem(event) {
    event.preventDefault();
    event.stopPropagation();
    event.stopImmediatePropagation();
    return fromUuidSync(event.delegateTarget.closest("div.macro-overlay").dataset.item);
}
