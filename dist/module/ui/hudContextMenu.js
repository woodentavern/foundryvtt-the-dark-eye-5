import { stopEvent } from "../jsUtils.js";

/**
 * Extension for the ContextMenu class to add the menu to the target's parent
 *  container instead of its children. This allows using the menu on buttons,
 *  icons or other elements that do not function as a container.
 * Unlike the original ContextMenu, this does not change the parent's position,
 *  so you must make sure that it is using 'position: relative' layout.
 * @see {@link https://foundryvtt.com/api/ContextMenu.html} for additional details.
 */
export default class HudContextMenu extends ContextMenu {
    /**
     * Contains the element that the menu was created from.
     * @type {jQuery.Element}
     */
    target;

    /**
     * Create a menu with entries adjustable by hooked functions.
     * @override
     */
    static create(app, html, selector, menuItems, { hookName = "EntryContext", ...options } = {}) {
        for (const cls of app.constructor._getInheritanceChain()) {
            Hooks.call(`get${cls.name}${hookName}`, html, menuItems);
        }

        return new HudContextMenu(html, selector, menuItems, options);
    }

    /**
     * Check if the parent (instead of the element itself) contains the menu.
     * @override
     */
    bind() {
        if (this.selector === null) return;
        this.element.on(this.eventName, this.selector, event => {
            event.preventDefault();
            if (this.toggle($(event.currentTarget)) === false) stopEvent(event);
        });
    }

    /**
     * Position the menu after the target (instead of inside) and adjust the
     *  layout accordingly.
     * @override
     */
    _setPosition(html, target) {
        const targetRect = target[0].getBoundingClientRect();
        const parentRect = target[0].parentElement.getBoundingClientRect();

        // Insert after target and get the context bounds
        html.css("visibility", "hidden");
        html.insertAfter(target);
        const contextRect = html[0].getBoundingClientRect();

        // Determine whether to expand down or expand up
        const bottomHalf = targetRect.bottom > (window.innerHeight / 2);
        this._expandUp = bottomHalf && ((parentRect.bottom - targetRect.bottom) < contextRect.height);

        // Display the menu
        if (this._expandUp) {
            html.addClass("expand-up");
            html.css("bottom", parentRect.bottom - targetRect.top);
        } else {
            html.addClass("expand-down");
            html.css("top", targetRect.bottom - parentRect.top);
        }
        html.addClass("reset-font");
        html.css("left", targetRect.left - parentRect.left + 2);
        html.css("visibility", "");
        target.addClass("context");
    }

    /**
     * Manually toggles the state of this context menu, hiding it if it is
     *  currently displayed or rendering it otherwise.
     * @param {jQuery.Element} element The element that the context menu should be displayed on.
     * @returns {boolean} False if the menu was rendered, true otherwise.
     */
    toggle(element) {
        // Remove existing context UI
        $(".context").removeClass("context");

        // Close the current context
        if ($.contains(element.parent()[0], this.menu[0])) {
            this.close();
        } else {
            // If the new target element is different
            this.render(element);
            this.target = element;
            ui.context = this;
            return false;
        }

        return true;
    }

    /**
     * Replace Foundry's method to bypass private fields.
     * @override
     */
    activateListeners(html) {
        html.on("click", "li.context-item", this.onClickItem.bind(this));
    }

    /**
     * Handle clicks on menu items.
     * @param {jQuery.Event} event The JQuery event of the click.
     */
    onClickItem(event) {
        event.preventDefault();
        event.stopPropagation();
        const li = event.currentTarget;
        const item = this.menuItems.find(i => i.element === li);
        item?.callback(this.target);
        this.close();
    }
}
