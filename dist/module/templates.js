/**
 * Define a set of template paths to pre-load
 * Pre-loaded templates are compiled and cached for fast access when rendering
 * @returns {Promise} A promise representing the template loading.
 */
export default async function preloadHandlebarsTemplates() {
    // Define template paths to load
    const templatePaths = [
        // Item sheet partials region-group
        "systems/tde5e/templates/items/components/editor-group.hbs",
        "systems/tde5e/templates/items/components/region-group.hbs",
        "systems/tde5e/templates/items/components/purchase-group.hbs",
        "systems/tde5e/templates/items/components/wear-group.hbs",
        "systems/tde5e/templates/items/components/prefix-group.hbs",
        "systems/tde5e/templates/items/components/damage-group.hbs",
        "systems/tde5e/templates/items/components/enhancement-group.hbs",
        "systems/tde5e/templates/items/components/shortLongText-group.hbs",
        "systems/tde5e/templates/items/components/castableCheck-group.hbs",
        "systems/tde5e/templates/items/components/improvable-group.hbs",
        "systems/tde5e/templates/items/components/blacklist-group.hbs",
        "systems/tde5e/templates/items/components/charge-block.hbs",
        "systems/tde5e/templates/items/components/commonUncommonVantages-block.hbs",
        "systems/tde5e/templates/items/components/priceAndWeight-group.hbs",
        "systems/tde5e/templates/items/components/maneuver-cost-group.hbs",
        "systems/tde5e/templates/items/components/maneuver-prerequisites-group.hbs",

        // Displays claimable ChatItems
        "systems/tde5e/templates/chat/lootClaimMessage.hbs",
        "systems/tde5e/templates/chat/lootClaim-sheet.hbs",
        "systems/tde5e/templates/chat/components/lootClaim-block-hidden.hbs",
        "systems/tde5e/templates/chat/components/lootClaim-block-shown.hbs",

        // Displays 3d20 checks
        "systems/tde5e/templates/chat/skillCheck.hbs",

        // Actor sheet partials
        "systems/tde5e/templates/actors/components/navigation.hbs",
        "systems/tde5e/templates/actors/parts/player-header.hbs",
        "systems/tde5e/templates/actors/parts/magic-pages.hbs",
        "systems/tde5e/templates/actors/parts/changelog.hbs",
        "systems/tde5e/templates/actors/components/changelog-footer.hbs",
        "systems/tde5e/templates/actors/components/biography.hbs",
        "systems/tde5e/templates/actors/components/biography-creature.hbs",
        "systems/tde5e/templates/actors/components/biography-minimal.hbs",
        "systems/tde5e/templates/actors/components/ability-block.hbs",
        "systems/tde5e/templates/actors/components/improvable-ability-block.hbs",
        "systems/tde5e/templates/actors/components/attribute-table.hbs",
        "systems/tde5e/templates/actors/components/derived-value-table.hbs",
        "systems/tde5e/templates/actors/components/condition-table.hbs",
        "systems/tde5e/templates/actors/components/experience.hbs",
        "systems/tde5e/templates/actors/components/skill-table.hbs",

        "systems/tde5e/templates/actors/components/combat-loadout-block.hbs",
        "systems/tde5e/templates/actors/components/combat-loadout-block-minimal.hbs",
        "systems/tde5e/templates/actors/components/combat-loadout-melee-group.hbs",
        "systems/tde5e/templates/actors/components/combat-loadout-ranged-group.hbs",
        "systems/tde5e/templates/actors/components/combat-loadout-shield-group.hbs",

        "systems/tde5e/templates/actors/components/combat-maneuver-group.hbs",
        "systems/tde5e/templates/actors/components/container-table.hbs",

        "systems/tde5e/templates/actors/components/weapon-table.hbs",
        "systems/tde5e/templates/actors/components/unused-weapon-block.hbs",

        "systems/tde5e/templates/actors/components/state-block.hbs",
        "systems/tde5e/templates/actors/components/ammunition-block.hbs",

        "systems/tde5e/templates/actors/components/armor-block.hbs",
        "systems/tde5e/templates/actors/components/armor-block-minimal.hbs",
        "systems/tde5e/templates/actors/components/armor-group.hbs",
        "systems/tde5e/templates/actors/components/armor-group-minimal.hbs",

        "systems/tde5e/templates/actors/components/tradition-header.hbs",
        "systems/tde5e/templates/actors/components/creature-description.hbs",
        "systems/tde5e/templates/actors/components/creature-specialrules.hbs",
        "systems/tde5e/templates/actors/components/creature-facts.hbs",
        "systems/tde5e/templates/actors/components/spell-block.hbs",
        "systems/tde5e/templates/actors/components/note-block.hbs",
        "systems/tde5e/templates/actors/components/container-table-default.hbs",
        "systems/tde5e/templates/actors/components/container-table-loot.hbs",
        "systems/tde5e/templates/actors/components/container-table-trade.hbs",

        // Item sheet partials
        "systems/tde5e/templates/items/item.hbs",

        // Combat partials
        "systems/tde5e/templates/combat/combatant.hbs",
    ];

    // Load the template parts
    return loadTemplates(templatePaths);
}
