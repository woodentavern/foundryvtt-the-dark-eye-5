import { hasValue, capitalize } from "./util.js";

/**
 * An object containing all handler bar helpers for this game system.
 */
export default {
    attributeOptions,
    boolToChoice,
    concat,
    costCount,
    createDetailState,
    hasTextContent,
    every,
    fetchTranslation,
    getProperty,
    gmOnly,
    group,
    hasValue,
    includes,
    isCustomImage,
    itemTypeName,
    joinLocalized,
    length,
    repeat,
    repeatJoin,
    textHeight,
};

/**
 * Translates the itemType to a locailized singular version.
 * @param {string} type The itemType of an item
 * @returns {string} The tranlated name of the itemType as singular.
 */
function itemTypeName(type) {
    const name = capitalize(type.toLowerCase());
    return game.i18n.localize(`ITEM.Type${name}`);
}

/**
 * Determines whether the given image path is not the default image.
 * @param {string} imagePath An image path inside of foundry.
 * @returns {boolean} True, if the image path is the mystery-man image, false otherwise.
 */
function isCustomImage(imagePath) {
    return imagePath && imagePath !== "icons/svg/mystery-man.svg";
}

/**
 * Joins together all elements into a single string.
 * @param  {...any} args Any collection of elements.
 * @returns {string} A single string containing all elements.
 */
function concat(...args) {
    return args.slice(0, -1).join("");
}

/**
 * For a given bool return choice.yes if true, choice.no if false and choice.maybe if value is null
 * @param  {boolean?} value The input value.
 * @returns {string} A single string containing the choice.
 */
function boolToChoice(value) {
    if (!hasValue(value)) return game.i18n.localize("tde5e.choices.maybe");
    return value ? game.i18n.localize("tde5e.choices.yes") : game.i18n.localize("tde5e.choices.no");
}

/**
 * Returns the blocks content if the current user is a GM.
 * Otherwise an empty string is returned.
 * @param {object} block The block wrapper from handlebars.
 * @returns {string} The block HTML or an empty string.
 */
function gmOnly(block) {
    return game.user.isGM ? block.fn(this) : block.inverse(this);
}

/**
 * Creates the attributes for a closable detail box with its state inferred from the given set.
 * @param {string} id A unique ID to identify the box.
 * @param {Set<string>} closedDetails A set of IDs that should be closed.
 * @returns {Handlebars.SafeString} an HTML string containing the detail element.
 */
function createDetailState(id, closedDetails) {
    return new Handlebars.SafeString(`data-details-id="${id}" ${closedDetails.has(id) ? "" : "open"}`);
}

/**
 * Checks if the given string contains any non-markup text.
 * @param {string} content The content to check.
 * @returns {boolean} True if the context is empty, false otherwise.
 */
function hasTextContent(content) {
    return content && content !== "<p></p>";
}

/**
 * Estimates the height of the given text when displayed in a text area element.
 * @param {string} text The text content to estimate the height for.
 * @returns {Handlebars.SafeString} An HTML string containing a height styling attribute.
 */
function textHeight(text) {
    if (!text) return new Handlebars.SafeString("");
    const height = Math.max(50, text.split("\n").length * 18 + 10);
    return new Handlebars.SafeString(`style="min-height: ${height}px;"`);
}

/**
 * Retrieves the translations located at the given path.
 * @param {string} path The localization path.
 * @param {object} options The options of the helper.
 * @param {boolean} options.hash.allowNoSelection If true, adds a blank item to the translations.
 * @param {boolean} options.hash.keepName If true, the name element won't be removed.
 * @returns {Array[]} The translation map for the given path.
 */
function fetchTranslation(path, options) {
    let translations = foundry.utils.getProperty(game.i18n.translations, path) ?? {};
    const { allowNoSelection, keepName } = options.hash;
    if (!allowNoSelection && keepName) return translations;

    translations = foundry.utils.deepClone(translations);
    if (allowNoSelection) translations[""] = "-";
    if (!keepName) delete translations.name;
    return translations;
}

/**
 * Creates a set of translated select options for the attributes of the system.
 * @param {object} options The options of the helper.
 * @param {boolean} options.hash.long Indicates whether the short or long attribute strings should be used.
 * @returns {Handlebars.SafeString} An HTML string containing the select options.
 */
function attributeOptions(options) {
    const long = options.hash.long ?? false;
    const path = long ? "tde5e.actor.attributes" : "tde5e.actor.attributesShort";
    const attributes = foundry.utils.deepClone(foundry.utils.getProperty(game.i18n.translations, path) ?? {});
    delete attributes.name;
    delete attributes.any;
    return HandlebarsHelpers.selectOptions(attributes, options);
}

/**
 * Joins the given elements with the given delimiter into one string.
 * @param {Array} values The elements to join.
 * @param {object} options The options of the helper.
 * @param {string} options.hash.delimiter The delimiter to join the content with. Defaults to '/'.
 * @param {string} options.hash.prefix A localization prefix that is prepended to the value.
 * @returns {string} A string containing the joined elements.
 */
function joinLocalized(values, options) {
    return values
        .map(v => game.i18n.localize((options.hash.prefix ?? "") + v))
        .join(options.hash.delimiter ?? "/");
}

/**
 * Repeats the content contained in the block. Supports the use of @index, @last and @first.
 * @param {number} times The amount of times to repeat the content.
 * @param {object} block The block information provided by Handlebars.
 * @returns {string} An HTML string containing the repeated block.
 */
function repeat(times, block) {
    block.hash.delimiter = "";
    return repeatJoin.call(this, times, block);
}

/**
 * Repeats the content contained in the block and joins the repetitions with the given delimiter.
 * Supports the use of @index, @last and @first.
 * @param {number} times The amount of times to repeat the content.
 * @param {object} block The block information provided by Handlebars.
 * @param {string} block.hash.delimiter The delimiter to join the content with. Defaults to '/'.
 * @returns {string} An HTML string containing the repeated, joined block.
 */
function repeatJoin(times, block) {
    let result = "";
    const delimiter = block.hash.delimiter ?? "/";
    const data = Handlebars.createFrame(block.data);

    for (let i = 0; i < times; i++) {
        data.index = i;
        data.first = i === 0;
        const isLast = i === times - 1;
        data.last = isLast;
        result += block.fn(this, { data });
        if (!isLast) result += delimiter;
    }

    return result;
}

/**
 * Creates a group element around a html content.
 * If the group does not contain any fieldsets, an empty string will be returned.
 * @param {string} titleKey The i18n key for the header of the group.
 * @param {object} block The block information provided by Handlebars.
 * @returns {string} An HTML string containing the block with a group or an empty string.
 */
function group(titleKey, block) {
    const content = block.fn(this);
    const hasFieldset = content.includes("fieldset");
    if (hasFieldset) {
        const localizedTitle = game.i18n.localize(titleKey);
        return `<div class="group"><h3>${localizedTitle}</h3>${content}</div>`;
    }
    return block.inverse(this);
}

/**
 * Calculates the number of cost elements in the array based on the information of the purchase-element.
 * @param {object} purchase The purchase element in the data of an item.
 * @returns {number} The number of cost elements in the array.
 */
function costCount(purchase) {
    if (!purchase) return 0;
    return Math.max(purchase.maxTimeBuyable ?? 1, purchase.levels ?? 1, purchase.cost?.length ?? 0);
}

/**
 * Takes a value and any number of subsequent values and checks whether the original matches
 *  any of the following ones.
 * @param {string | number} original Any value.
 * @param  {...any} values A list of values that can contain the original as well.
 * @returns {boolean} True if original occures again in values.
 */
function includes(original, ...values) {
    const data = values.slice(0, -1);
    if (data.length === 1 && data[0] === original) return true;
    return data.some(arg => arg === original);
}

/**
 * Checkes whether all values in the given array are also contained in the given elememt data.
 * Only returns true iff all values in the source match all values in the target and they have
 *  the same number of elements. The order of elements is ignored.
 * @param {Array} original An array of values.
 * @param  {...any} values A list of values.
 * @returns {boolean} True iff all values in original are also in values and both array are of the same length.
 */
function every(original, ...values) {
    // TODO Rename of remove, this is not what a JS every method does.
    if (!original.length) return true;
    const data = values.slice(0, -1);
    if (!Array.isArray(original)) original = [original];
    return data.length === original.length
        && data.every(item => original.indexOf(item) > -1);
}

/**
 * Returns the length of the givebn array or string
 * @param {Array} array Any object length is defined on.
 * @returns {number} the length of the data given.
 */
function length(array) {
    if (!array) return 0;
    return array.length;
}

/**
 * Replicates the behaviour of the getProperty method in FoundryVTT.
 * Prefer Handlebars <i>lookup<i> if possible.
 * @param {object} object The data source.
 * @param {string} path The JPath in the object.
 * @returns {*} The value of path in the given object or null/undefined.
 */
function getProperty(object, path) {
    if (!object) return null;
    return foundry.utils.getProperty(object, path);
}
