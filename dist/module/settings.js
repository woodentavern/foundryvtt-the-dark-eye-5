import { migrateData, migrateSystem } from "./migration.js";
import { resetCharges } from "./ui/chargeManagement.js";

/**
 * Registers the settings used and provided by the system.
 */
export function registerSystemSettings() {
    game.settings.register("tde5e", "systemMigrationVersion", {
        name: "System Migration Version",
        scope: "world",
        config: false,
        type: String,
        default: "0.0",
    });

    game.settings.register("tde5e", "dataMigrationVersion", {
        name: "Data Migration Version",
        scope: "world",
        config: false,
        type: Number,
        default: 0,
    });

    game.settings.register("tde5e", "loadoutView", {
        name: "Loadout View",
        hint: "Settings for the display of the owned sheets' loadout",
        scope: "client",
        config: false,
        type: Object,
        default: [],
    });

    game.settings.register("tde5e", "closedDetails", {
        name: "Closed details",
        hint: "Setting for identifying detail/summary elements that the user closed.",
        scope: "client",
        config: false,
        type: Object,
        default: {},
    });

    game.settings.register("tde5e", "recentSearches", {
        name: "Recent library searches",
        hint: "Stores the most recently used search settings for the library.",
        scope: "client",
        config: false,
        type: Array,
        default: [],
    });

    game.settings.register("tde5e", "improving", {
        name: "Improve actors",
        hint: "Allow players to improve their actors.",
        scope: "world",
        config: false,
        type: Boolean,
        default: false,
        onChange: value => {
            game.actors
                .filter(actor => actor.hasPlayerOwner && actor.getFlag("tde5e", "improving") !== value)
                .forEach(actor => actor.setFlag("tde5e", "improving", value));
            ui.actors.render();
        },
    });

    game.settings.register("tde5e", "animations", {
        name: game.i18n.localize("tde5e.settings.animations.name"),
        hint: game.i18n.localize("tde5e.settings.animations.hint"),
        scope: "client",
        config: true,
        type: Boolean,
        default: true,
    });

    game.settings.register("tde5e", "autoforwardCombat", {
        name: game.i18n.localize("tde5e.settings.autoforwardCombat.name"),
        hint: game.i18n.localize("tde5e.settings.autoforwardCombat.hint"),
        scope: "world",
        config: true,
        type: Boolean,
        default: true,
    });

    game.settings.register("tde5e", "debugOptions", {
        name: game.i18n.localize("tde5e.settings.debugMode.name"),
        hint: game.i18n.localize("tde5e.settings.debugMode.hint"),
        scope: "world",
        config: true,
        type: String,
        choices: {
            none: "tde5e.settings.debugMode.options.none",
            gmOnly: "tde5e.settings.debugMode.options.gmOnly",
            all: "tde5e.settings.debugMode.options.all",
        },
        default: "none",
        requiresReload: true,
    });

    // Extend configuration window with custom buttons.
    Hooks.on("renderSettingsConfig", (_, html, options) => {
        if (!TDE5E.debugMode || !options.canConfigure) return;

        renderTemplate("systems/tde5e/templates/debugSettings.hbs").then(text => {
            const systemSettings = html.find("section[data-category='system']");
            systemSettings.append(text);
            systemSettings.find("button.tde5e-force-migration").click(async () => {
                await migrateSystem(true);
                return migrateData(true);
            });
            systemSettings.find("button.tde5e-reset-charges").click(resetCharges);
        });
    });
}

/**
 * Determines whether a user has access to debug features.
 * @param {string} userId Any user-Id.
 * @returns {boolean} True, if the user should be able to use debug features, false otherwise.
 */
export function canUseDebugFeature(userId) {
    if (!userId) return false;
    const setting = game.settings.get("tde5e", "debugOptions");
    switch (setting) {
    case "none": return false;
    case "all": return true;
    case "gmOnly": return game.users.get(userId)?.isGM ?? false;
    default: return false;
    }
}
