import * as util from "./util.js";

/**
 * Specifies the structure of an internal socket request.
 * @typedef {object} SocketRequest
 * @property {string} type The type of the request, which determines how it is processed,
 *  @see {REQUEST_TYPE}.
 * @property {string} sourceUser The ID of the user that sent the request.
 * @property {string?} targetUser The ID of the user that the message is for.
 *  Is only respected when SOCKET_REQUEST_TARGET_TYPE is SPECIFIC_USER.
 * @property {string} targetType An identifier for clients that should process this request,
 *  @see {RECIEVER_TYPE}.
 * @property {object} content The specific data required for processing the request.
 */

/**
 * Defines valid socket request types.
 */
const REQUEST_TYPE = {
    // Error reporting.
    REPORT_ERROR: "reportError",
    // Redirected updates that the owner couldn't perform due to lacking permissions.
    UPDATE_COMBAT: "combatUpdate",
    UPDATE_COMBAT_POINTS: "combatPointUpdate",
    UPDATE_ROLL: "updateRoll",
    UPDATE_NOTES: "updateNotes",
    TRANSFER_ITEM: "transferItem",
    PLAYER_CLAIM_ITEM: "playerClaimsItem",
    PLAYER_CLAIM_CURRENCY: "playerClaimsCurrency",
    PLAYER_DELETE_CLAIM: "deletesClaims",
};

/**
 * Defines valid targets for the request.
 */
const RECIEVER_TYPE = {
    /** Everybody. */
    ALL: "all",
    /** Gamemasters. */
    GM: "gm",
    /** Users that are not gamemasters. */
    NON_GM: "notGm",
    /** First gamemaster of the session. */
    FIRST_GM: "firstGm",
    /** Specific user  */
    SPECIFIC_USER: "user",
};

/**
 * Sends a system socket request to a list of users.
 * @param {string} messageType The type of the message. @see REQUEST_TYPE
 * @param {object} content The content of the message to send.
 * @param {string[]?} targets An optional list of recipient user IDs.
 *  If none is given, the message will be broadcasted to all users.
 */
function sendMessage(messageType, content, targets) {
    const data = {
        type: messageType,
        sourceUser: game.user.id,
        content,
    };

    if (!targets) {
        // No specific targets, send to everyone.
        game.socket.emit("system.tde5e", data);
    } else if (targets.length) {
        if (targets.length === 1 && game.user.id === targets[0]) processMessage(data); // Message for self, run handler.
        else game.socket.emit("system.tde5e", data, { recipients: targets }); // Send to targets.
    } else {
        // Targets are set but empty, don't send anything.
        util.warn(`Message with type ${messageType} sent without targets, skipping.`);
    }
}

/**
 * Builds an array of target user IDs based on the given reciever type.
 * @param {string} type The reciever type of the message. @see RECIEVER_TYPE
 * @param {string?} userId An optional user ID if the target is a specific user.
 * @returns {string[]} An array of active users matching the target type or null for all users.
 */
function getTargets(type, userId) {
    switch (type) {
    case RECIEVER_TYPE.GM:
        return game.users.filter(user => user.isGM && user.active).map(user => user.id);
    case RECIEVER_TYPE.FIRST_GM: {
        if (game.user.isGM) return [game.user.id];
        const target = util.getPrimaryGm();
        if (target) return [target.id];
        util.error("You cannot make that change without a GM present.", false, true);
        return [];
    }
    case RECIEVER_TYPE.NON_GM:
        return game.users.filter(user => !user.isGM && user.active).map(user => user.id);
    case RECIEVER_TYPE.SPECIFIC_USER:
        if (userId && game.users.get(userId)?.active) return [userId];
        util.error(`User ${userId} is not online or does not exist.`, true, false);
        return [];
    default: return null;
    }
}

/**
 * Retrieves the processing function for the given message type.
 * @param {string} messageType The type of the message.
 * @returns {Function} The function to be used to process the message content.
 */
function getExecutingFunction(messageType) {
    switch (messageType) {
    case REQUEST_TYPE.REPORT_ERROR: return displayReportedError;
    case REQUEST_TYPE.UPDATE_COMBAT: return processPhaseEnd;
    case REQUEST_TYPE.UPDATE_COMBAT_POINTS: return processActionPointClaim;
    case REQUEST_TYPE.UPDATE_ROLL: return processRollUpdate;
    case REQUEST_TYPE.UPDATE_NOTES: return processNoteUpdate;
    case REQUEST_TYPE.TRANSFER_ITEM: return processItemTransfer;
    case REQUEST_TYPE.PLAYER_CLAIM_CURRENCY: return null;
    case REQUEST_TYPE.PLAYER_CLAIM_ITEM: return processItemClaim;
    case REQUEST_TYPE.PLAYER_DELETE_CLAIM: return processItemClaimDeletion;
    default:
        util.error(`Received unknown socket request type ${messageType}`, true, false);
        return null;
    }
}

/**
 * Process the given message data by performing the action associated with the
 *  request type.
 * @param {SocketRequest} data The content of the message, including metadata.
 */
export function processMessage(data) {
    const func = getExecutingFunction(data.type);
    if (!func) return;
    func(data.sourceUser, data.content);
}

/**
 * Displays the error contained in a report message.
 * @param {string} _sourceUser The ID of the user that sent the error.
 * @param {object} content The content of the message.
 */
function displayReportedError(_sourceUser, content) {
    util.error(content.message, true, true);
}

/**
 * Requests the deletion of a claim for an item. The quantity is optional and will remove
 *  all of the item'c claim if not specified.
 * @param {string} chatMessageId The ID of the chat message containing the loot table.
 * @param {string} itemId The ID of the item to claim.
 * @param {number?} quantity The claimed amount to subtract from the item.
 */
export function requestItemClaimDeletion(chatMessageId, itemId, quantity = null) {
    sendMessage(REQUEST_TYPE.PLAYER_DELETE_CLAIM, {
        chatMessageId,
        itemId,
        quantity,
    }, getTargets(RECIEVER_TYPE.FIRST_GM));
}

/**
 * Processes a request from a player to delete a claim for an item.
 * @param {string} sourceUser The ID of the user that requested the claim.
 * @param {object} content The content of the message.
 */
function processItemClaimDeletion(sourceUser, content) {
    const message = game.messages.get(content.chatMessageId);
    if (!message) util.error(`Received socket player delete claim failed to find message ${content.chatMessageId}.`);
    else message.removeLootItemClaim(content.itemId, sourceUser, content.quantity);
}

/**
 * Requeststhe claim of an item's quanity.
 * @param {string} chatMessageId The ID of the chat message containing the loot table.
 * @param {string} itemId The ID of the item to claim.
 * @param {number} quantity The claimed amount to claim from the item.
 * @param {string} displayName The displayed name of the item.
 */
export function requestItemClaim(chatMessageId, itemId, quantity, displayName) {
    sendMessage(REQUEST_TYPE.PLAYER_CLAIM_ITEM, {
        chatMessageId,
        itemId,
        quantity,
        displayName,
    }, getTargets(RECIEVER_TYPE.FIRST_GM));
}

/**
 * Processes a request from a player to claim an item from a LootClaimMessage.
 * @param {string} sourceUser The ID of the user that requested the claim.
 * @param {object} content The content of the message.
 */
function processItemClaim(sourceUser, content) {
    const message = game.messages.get(content.chatMessageId);
    if (!message) util.error(`Received socket player claim failed to find message ${content.chatMessageId}.`);
    else message.claimLootItem(content.itemId, sourceUser, content.displayName, content.quantity);
}

/**
 * Redirects a combat phase change from the local client to a GM client to
 *  bypass permission issues.
 * @param {string} combatId The ID of the combat to update.
 * @param {string} phase The combat phase to change to.
 */
export function redirectPhaseEnd(combatId, phase) {
    sendMessage(REQUEST_TYPE.UPDATE_COMBAT, {
        combatId,
        data: phase,
    }, getTargets(RECIEVER_TYPE.FIRST_GM));
}

/**
 * Processes a redirected phase update. The update will not be performed if the
 *  user does not own the combatant of the current turn.
 * @param {string} sourceUser The ID of the user that requested the update.
 * @param {object} content The content of the message.
 * @returns {Promise} A promise representing the combat update.
 */
function processPhaseEnd(sourceUser, content) {
    const combat = game.combats.get(content.combatId);
    if (!combat) return Promise.resolve();

    switch (content.data) {
    case "preparation": return combat.endPreparation();
    case "distribution": return combat.endDistribution();
    case "actions":
        if (!game.user.isGM && !combat.combatant.players.some(user => user.id === sourceUser)) {
            reportError("You do not own the current turn.", sourceUser);
            return Promise.resolve();
        }
        return combat.endActions();
    default: return Promise.resolve();
    }
}

/**
 * Redirects a group action point claim from the local client to a GM client to bypass permissions.
 * @param {string} combatId The ID of the combat to update.
 * @param {number} groupIndex The index of the group to claim points from.
 * @param {number} deltaPoints The amount of points to claim.
 */
export function redirectActionPointClaim(combatId, groupIndex, deltaPoints) {
    sendMessage(
        REQUEST_TYPE.UPDATE_COMBAT_POINTS,
        { combatId, data: { groupIndex, deltaPoints } },
        getTargets(RECIEVER_TYPE.FIRST_GM),
    );
}

/**
 * Processes a redirected group action point update.
 * @param {string} _sourceUser The ID of the user that requested the update.
 * @param {object} content The content of the message.
 * @returns {Promise} A promise representing the combat update.
 */
function processActionPointClaim(_sourceUser, content) {
    const combat = game.combats.get(content.combatId);
    if (combat) return combat.modifyGroupPoints(content.data.groupIndex, content.data.deltaPoints);
    return Promise.resolve();
}

/**
 * Redirects changes to a roll on a message from the local client to a GM client
 *  to bypass permission issues.
 * @param {string} messageId The ID of the message to update.
 * @param {object} roll The data of the roll.
 */
export function redirectRollUpdate(messageId, roll) {
    sendMessage(REQUEST_TYPE.UPDATE_ROLL, {
        messageId,
        data: roll,
    }, getTargets(RECIEVER_TYPE.FIRST_GM));
}

/**
 * Processes a redirected roll update. The update will not be performed if the
 *  message was not sent by the request's user.
 * @param {string} sourceUser The ID of the user that requested the update.
 * @param {object} content The content of the message.
 * @returns {Promise} A promise representing the update.
 */
async function processRollUpdate(sourceUser, content) {
    const message = game.messages.get(content.messageId);
    if (!message?.isRoll) return Promise.resolve();

    const actorId = message.rolls[0].data?.actor?.uuid;
    if (!actorId) return Promise.resolve();

    // Validate ownership.
    const actor = await fromUuid(actorId);
    if (!actor) return Promise.resolve();
    if (!actor?.canUserModify(game.users.get(sourceUser), "update")) {
        reportError("You are not the author of that message.", sourceUser);
        return Promise.resolve();
    }

    const roll = Roll.fromData(content.data);
    return message.update({ rolls: [roll] });
}

/**
 * Redirects changes to an NPC's notes to allow users with limited permissions
 *  to edit them.
 * Note that this will always update the base actor, not the token actor.
 * @param {string} actorId The ID of the actor to update.
 * @param {object} data The data of the update.
 */
export function redirectNoteUpdate(actorId, data) {
    sendMessage(REQUEST_TYPE.UPDATE_NOTES, {
        actorId,
        data,
    }, getTargets(RECIEVER_TYPE.FIRST_GM));
}

/**
 * Processes a redirected note update. The update will not be performed if the
 *  sender doesn't have at least limited permissions.
 * @param {string} sourceUser The ID of the user that requested the update.
 * @param {object} content The content of the message.
 * @returns {Promise} A promise representing the update.
 */
async function processNoteUpdate(sourceUser, content) {
    const actor = game.actors.get(content.actorId);
    if (!actor) return Promise.resolve();

    // Validate ownership.
    if (!actor.testUserPermission(game.users.get(sourceUser), "LIMITED")) {
        reportError("You do not have the permissions to update that actor.", sourceUser);
        return Promise.resolve();
    }

    return actor.update(content.data);
}

/**
 * Requests the transfer of an item from its current parent to another actor.
 * Note that token actors are not valid targets for this operation.
 * @param {string} itemUuid The UUID of the item to transfer.
 * @param {string} actorId The ID of the actor that should receive the item.
 */
export function requestItemTransfer(itemUuid, actorId) {
    sendMessage(REQUEST_TYPE.TRANSFER_ITEM, {
        itemUuid,
        actorUuid: actorId,
        actorId,
    }, getTargets(RECIEVER_TYPE.FIRST_GM));
}

/**
 * Processes an item transfer request. The transfer will not be performed if
 *  the source user does not own the source item.
 * @param {string} sourceUser The ID of the user that requested the transfer.
 * @param {object} content The content of the message.
 */
async function processItemTransfer(sourceUser, content) {
    const targetActor = game.actors.get(content.actorId);
    if (!targetActor) return;

    const item = await fromUuid(content.itemUuid);
    if (!item) return;

    // Validate ownership.
    if (!item.testUserPermission(game.users.get(sourceUser), "OWNER")) {
        reportError("You lack the permissions to transfer that item.", sourceUser);
        return;
    }

    // Transfer item as transaction.
    try {
        const data = item.toObject();
        data.system.container = 0;
        await targetActor.createEmbeddedDocuments(item.documentName, [data]);
        await item.delete();
        util.info(`Item '${item.name}' was transfered to actor '${targetActor.name}'.`, true);
    } catch (ex) {
        util.error(
            `Transfer of item ${content.itemUuid} to actor ${targetActor.name} failed, check data consistency.`,
            ex,
            true,
        );
    }
}
