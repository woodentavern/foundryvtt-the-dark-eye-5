import LootClaim from "./sheets/lootClaimSheet.js";
import { hasValue, info, warn } from "../util.js";
import TdeChatMessage from "./chatMessage.js";
import { createTypedChatMessage } from "../chat.js";
import TdeItem from "../items/item.js";

export const LOOT_CLAIM_PHASE = Object.freeze({
    /** The claim was created, the GM can roll items and get the results for checks from players */
    INIT: "INIT",
    /** Players can claim loot for their associated characters */
    CLAIM: "CLAIM",
    /** Short phase. Items are distriubited to the associated actors. */
    DISTRIBUTE: "DISTRIBUTE",
    /** Claim is done, internal data will be pruned. */
    FINNISHED: "FINNISHED",
});

export class LootClaimMessage extends TdeChatMessage {
    /** @override */
    async getHTML() {
        const messageData = this.getData();
        this.content = await renderTemplate("systems/tde5e/templates/chat/lootClaimMessage.hbs", messageData);
        return super.getHTML();
    }

    /** @override */
    getData() {
        const data = this.toObject(false);
        const isWhisper = this.whisper.length;

        return {
            messageId: this.id,
            message: data,
            author: this.user,
            alias: this.alias,
            isGM: game.user.isGM,
            cssClass: [
                this.type === CONST.CHAT_MESSAGE_TYPES.IC ? "ic" : null,
                this.type === CONST.CHAT_MESSAGE_TYPES.EMOTE ? "emote" : null,
                isWhisper ? "whisper" : null,
                this.blind ? "blind" : null,
            ].filterJoin(" "),
            isWhisper: this.whisper.some(id => id !== game.user.id),
            whisperTo: this.whisper.map(u => {
                const user = game.users.get(u);
                return user ? user.name : null;
            }).filterJoin(", "),
        };
    }

    /** @override */
    prepareData() {
        const messageLootData = this.flags.tde5e.loot;

        this.visibleItems = [];
        this.hiddenItems = [];
        this.allItemsEvaluated = true;
        this.allItemsClaimed = true;
        this.playersCanClaim = this.flags.tde5e.loot.phase === LOOT_CLAIM_PHASE.CLAIM;

        Object.entries(messageLootData.items)
            .sort(([, itemA], [, itemB]) => itemA.name.localeCompare(itemB.name))
            .forEach(([, item]) => {
                const itemLootData = foundry.utils.deepClone(item.flags.tde5e.loot);

                const hasBeenEvaluated = hasValue(itemLootData.evaluate);
                const cls = TdeItem.resolveClass(item.type, item.system?.cid, item.system?.group);
                itemLootData.symbol = `<img class="symbol-img" src="${cls.defaultIconPath}">`;
                itemLootData.hasBeenEvaludated = hasBeenEvaluated;

                itemLootData.hasRequirements = !!itemLootData.requirementText?.length;
                itemLootData.requiresCheck = itemLootData.quantityIsPerQl ?? false;
                itemLootData.hasItems = (hasBeenEvaluated)
                    ? itemLootData.evaluate.hasItems
                    : false;
                itemLootData.uom = game.i18n.localize(`tde5e.measurements.${itemLootData.unitOfMeasurement}`);
                itemLootData.remainingQuantity = (hasBeenEvaluated)
                    ? itemLootData.evaluate.quantity - itemLootData.claim.totalQuantity
                    : 0;

                if (!hasBeenEvaluated) this.allItemsEvaluated = false;
                if (hasBeenEvaluated && itemLootData.remainingQuantity > 0) this.allItemsClaimed = false;

                item.loot = itemLootData;
                if (hasBeenEvaluated) this.visibleItems.push(item);
                else this.hiddenItems.push(item);
            });
    }

    /** @override */
    async _preCreate(data, options, user) {
        // TODO: Deduplicate items
        // const lootData = data.flags.tde5e.loot;
        return super._preCreate(data, options, user);
    }

    /**
     *  Get the embedded item for the given id.
     * @param {string} itemId The id of the item to get.
     * @returns {object} The item data.
     */
    getLootItem(itemId) {
        return this.flags.tde5e.loot.items[itemId];
    }

    /**
     * Removes the amount specified from the claimed amount for the item
     *  by the user. If no amount is specified, the entire claim will be removed.
     * @param {string} itemId The id of the item to claim.
     * @param {string} userId The userId of the claiment.
     * @param {number} value The amount to claim.
     */
    removeLootItemClaim(itemId, userId, value = null) {
        if (!userId || !itemId) return;
        if (value === 0) return;
        const itemData = this.getLootItem(itemId);
        const claimData = itemData.flags.tde5e.loot.claim;

        const claim = claimData.items.find(entry => entry.userId === userId);
        if (value) {
            claim.quantity -= value;
            if (claim.quantity < 0) claimData.items = claimData.items.filter(item => item.userId !== userId);
        } else {
            claimData.items = claimData.items.filter(item => item.userId !== userId);
        }

        claimData.totalQuantity = claimData.items
            .map(c => c.quantity)
            .reduce((prev, cur) => prev + cur, 0);

        this.updateEmbeddedLootItem(
            itemId,
            { "flags.tde5e.loot.claim": claimData },
            { diff: false },
        );
    }

    /**
     * Claims an amount of an item for the user.
     * Will only claim what is available. If the requested amount is higher, the
     *  remaining amopunt is claimed instead.
     * @param {string} itemId The id of the item to claim.
     * @param {string} userId The userId of the claiment.
     * @param {string} displayName The displayname to use for the user. If none is
     *  specified the userId will be used.
     * @param {number} value The amount to claim.
     */
    claimLootItem(itemId, userId, displayName, value) {
        if (!(value instanceof Number) || value < 1) return;
        if (!userId || !itemId) return;
        if (!displayName) displayName = userId;

        const itemData = this.getLootItem(itemId);
        const { remainingQuantity } = itemData.loot;
        const claimAmount = Math.min(remainingQuantity, value);

        const existingEntry = itemData.loot.claim.items.filter(item => item.userId === userId);

        if (existingEntry.length) existingEntry[0].quantity += claimAmount;
        else itemData.loot.claim.items.push({ userId, displayName, quantity: claimAmount });
        itemData.loot.claim.totalQuantity += claimAmount;

        this.updateEmbeddedLootItem(
            itemId,
            { "flags.tde5e.loot.claim": itemData.loot.claim },
            { diff: false },
        );
    }

    /**
     * Facade for the update method of the message.
     * Any data property will have their properties extendes by the root path of the message
     *  making it easy to simply supply the embedded item's data object.
     * @param {string} id The id of the item to update.
     * @param {object} data The data of the item to update.
     * @param {object} options The options for any update.
     * @returns {Promise} A promise representing the update.
     */
    async updateEmbeddedLootItem(id, data, options = {}) {
        if (!id) return warn("Cannot update loot item, id was not specified!");

        // Append path to root keys of update
        const pathPrefix = `flags.tde5e.loot.items.${id}.`;
        const newUpdate = Object.entries(data)
            .reduce((prev, [key, value]) => {
                prev[pathPrefix + key] = value;
                return prev;
            }, {});

        return this.update(newUpdate, options);
    }

    /**
     * Attempts to open the claim to the players. This can only be done if the
     *  claim is not already open. This will also make the message visible to everyone.
     * This will update the phase of the claim.
     * @returns {boolean} True, if the claim was opened. False, otherwise.
     */
    openClaim() {
        if (this.flags.tde5e.loot.phase === LOOT_CLAIM_PHASE.INIT) {
            this.update({
                "flags.tde5e.loot.phase": LOOT_CLAIM_PHASE.CLAIM,
                whisper: [],
            }, { render: true });
            return true;
        }
        return false;
    }

    /**
     * Evaluates all given item ids inside of the message.
     * This adds an evaluate object in the loot flag that contains the resulting values.
     * @param {string[]} itemIds An array of internal item ids.
     * @param {object} options Optional fields.
     * @param {boolean} options.force Forces the evaluation, even if the item was evaluated before.
     * @param {number} options.checkQl Specifies the associated QL. This is used to determine whether a required check
     *  was rolled and if the quantity is dependent on the QL, will multiply the result by said QL.
     * @returns {Promise} The update result for this message or null if no update was performed.
     */
    async evaluteItems(itemIds, options = {}) {
        if (!itemIds) return Promise.resolve();
        if (!Array.isArray(itemIds)) itemIds = [itemIds];

        const updates = {};
        const allItems = [].concat(this.visibleItems).concat(this.hiddenItems);
        const itemsToEval = allItems.filter(item => itemIds.includes(item._id));

        for (const item of itemsToEval) {
            const lootData = item.flags.tde5e.loot;
            // TODO: If force, update claims as well. If claims cannot be satisfied, clear all. Otherwise leave as is.
            if (!hasValue(lootData.evaluate) || options.force) {
                lootData.evaluate = {};
                const rolledChance = Math.random() * 100;
                const lootRollSuccess = rolledChance <= lootData.chanceToAppear;
                const fullfillsRequirements = !lootData.requirementText?.length || hasValue(options.checkQl);
                const canApplyQl = !lootData.quantityIsPerQl || hasValue(options.checkQl);
                info(`Item ${item._id} chance: ${rolledChance} <= ${lootData.chanceToAppear} : ${lootRollSuccess}`);

                if (fullfillsRequirements && canApplyQl) {
                    if (hasValue(options.checkQl)) lootData.checkQl = options.checkQl;
                    lootData.evaluate.hasItems = lootRollSuccess;
                    lootData.evaluate.quantity = 0;

                    if (lootRollSuccess) {
                        const rollResult = await new Roll(lootData.quantityRange).evaluate();

                        info(`Item ${item._id} rolled ${lootData.quantityRange} as ${rollResult.total}`);
                        lootData.evaluate.quantity = rollResult.total;
                        lootData.evaluate.hasItems = rollResult.total > 0;
                        if (lootData.quantityIsPerQl) lootData.evaluate.quantity *= options.checkQl;
                    }

                    const baseKey = `flags.tde5e.loot.items.${item._id}.flags.tde5e.loot`;
                    foundry.utils.setProperty(updates, `${baseKey}.evaluate`, lootData.evaluate);
                    foundry.utils.setProperty(updates, `${baseKey}.claim`, { totalQuantity: 0, items: [] });
                }
            }
        }

        const numberOfUpdated = Object.keys(updates).length;
        if (numberOfUpdated > 0) {
            info(`Updated items in lootClaim message ${this.id}`);
            return this.update(updates, { diff: false });
        }
        return Promise.resolve();
    }

    /** @override */
    activateListeners(html) {
        html.on("click", "a.claim-link", this.onClickClaimLink.bind(this));
    }

    /**
     * Opens the claim-application for the the caller.
     * @param {JQuery.event} event The click event.
     */
    onClickClaimLink(event) {
        event.preventDefault();
        // Set the improvement sheet on the actor so it can be accessed later.
        this._renderLootClaimSheet();
    }

    /**
     * Renders the associated sheet. If no sheet has been created a new one
     *  is instanced and stored.
     */
    _renderLootClaimSheet() {
        if (!this.lootClaim) this.lootClaim = new LootClaim(this);
        this.lootClaim.render(true);
    }

    /** @override */
    _onUpdate(change, options, userId) {
        super._onUpdate(change, options, userId);

        // Redirect item update to lootClaimSheet
        if (this.lootClaim?.rendered) this._renderLootClaimSheet();
    }

    /** @override */
    lockRoll() {}

    /**
     * Creates and sends a ClaimableChatMessage to chat.
     * The message contains the itemData of a single item or an array of item-data objects.
     * The items given MUST have the quantity property under their data element and should
     * have a value greater than 0 for it or they will not be claimable.
     * @param {string} source The name of the loot's source.
     * @param {object[] | object} itemData A single item-data object or an array of items.
     * @param {object} currency An object containing currency amounts.
     * @param {number?} currency.kreutzers Any none-negative amount of currency of this type.
     * @param {number?} currency.halers Any none-negative amount of currency of this type.
     * @param {number?} currency.silverthalers Any none-negative amount of currency of this type.
     * @param {number?} currency.ducats Any none-negative amount of currency of this type.
     */
    static createLootClaimMessage(source, itemData, {
        kreutzers = 0, halers = 0, silverthalers = 0, ducats = 0,
    } = {}) {
        itemData ??= [];

        const items = ((itemData instanceof Array) ? itemData : [itemData])
            .reduce((sum, item) => {
                const itemDup = foundry.utils.deepClone(item);
                foundry.utils.setProperty(itemDup, "ownership", { default: CONST.DOCUMENT_OWNERSHIP_LEVELS.OBSERVER });
                foundry.utils.setProperty(itemDup, "flags.tde5e.loot.sourceUuid");
                sum[item._id] = itemDup;
                return sum;
            }, {});

        const lootData = foundry.utils.deepClone(TDE5E.defaultLootClaimMessageData);
        lootData.items = items;
        lootData.source = source ?? lootData.source;
        lootData.currency = {
            kreutzers, halers, silverthalers, ducats,
        };

        const messageData = {
            type: CONST.CHAT_MESSAGE_TYPES.OTHER,
            user: game.user.id,
            whisper: [game.user.id],
            flags: { tde5e: { loot: lootData, type: "lootclaim" } },
        };

        createTypedChatMessage(messageData, "lootclaim")
            .then(message => message._renderLootClaimSheet(true));
    }
}
