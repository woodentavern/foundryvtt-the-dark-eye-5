import TdeChatMessage from "./chatMessage.js";

export default class ActionMessage extends TdeChatMessage {
    async getHTML() {
        const html = await super.getHTML();
        html[0].querySelector("div.message-content")?.insertAdjacentHTML("afterbegin", this.system.item.displayName);
        return html;
    }
}
