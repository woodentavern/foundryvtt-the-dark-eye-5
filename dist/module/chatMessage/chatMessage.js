import ModifiableRoll from "../ui/rolls/modifiableRoll.js";
import TdeSkillCheck from "../ui/rolls/skillCheck.js";
import { isPrimaryGm } from "../util.js";

export default class TdeChatMessage extends ChatMessage {
    /**
     * Indicates whether the next render of the message may use animations.
     * @type {boolean}
     */
    allowAnimation = false;

    constructor(data, context = {}, typed = false) {
        if (!typed) {
            const type = data.flags?.tde5e?.type ?? data.type;
            const cls = TdeChatMessage.resolveClass(type);
            return new cls(data, context, true);
        }

        super(data, context);
    }

    /**
     * Activates listeners for the givens ChatMessage's html.
     * @param {JQuery.element} html HTML of the ui element.
     */
    activateListeners(html) {
        if (this.rolls[0] instanceof ModifiableRoll) {
            if (!this.isContentVisible || !this.isRoll) return;
            if (this.rolls[0].locked || !(this.rolls[0] instanceof TdeSkillCheck)) return;
            if (Date.now() - new Date(this.timestamp) > 3600000 && isPrimaryGm()) this.lockRoll(true);
            else this.rolls[0].activateListeners(html, this);
        }
    }

    /**
     * Locks the roll contained within this message so that it can no longer
     *  be modified.
     * @returns {Promise.<object>} A promise representing the asynchronous message update.
     */
    async lockRoll() {
        const roll = this.rolls[0];
        roll.data.locked = true;
        return this.update({ rolls: [roll.toJSON()] });
    }

    /** @inheritdoc */
    async _renderRollHTML(isPrivate) {
        let html = "";
        for (const roll of this.rolls) {
            html += await roll.render({ isPrivate, animate: this.allowAnimation });
        }

        this.allowAnimation = false;
        return html;
    }

    /** @inheritdoc */
    _onCreate(...args) {
        this.allowAnimation ||= game.settings.get("tde5e", "animations");
        super._onCreate(...args);
    }

    /** @inheritdoc */
    _onUpdate(...args) {
        this.allowAnimation ||= game.settings.get("tde5e", "animations");
        super._onUpdate(...args);
    }

    /**
     * Resolves the correct runtime class for a chatmessage with the given type.
     * @param {string} type The type of the chatMessage.
     * @returns {object} The class associated with the data.
     */
    static resolveClass(type) {
        return TDE5E.chatMessageTypeClasses[type]
            ?? CONFIG.ChatMessage.documentClass;
    }
}
