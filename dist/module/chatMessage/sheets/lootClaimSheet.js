import TdeItem from "../../items/item.js";
import { requestItemClaim, requestItemClaimDeletion } from "../../socket.js";
import { hasValue, info } from "../../util.js";

export const LOOT_CLAIM_PHASE = Object.freeze({
    /** The claim was created, the GM can roll items and get the results for checks from players */
    INIT: 0,
    /** Players can claim loot for their associated characters */
    CLAIM: 1,
    /** Short phase. Items are distriubited to the associated actors. */
    DISTRIBUTE: 2,
    /** Claim is done, internal data will be pruned. */
    FINNISHED: 3,
});

export default class LootClaimSheet extends FormApplication {
    constructor(message, options = {}) {
        super(message, options);
        this.options.id += message.id;
    }

    /**
     * Changes the ID and template of the dialog.
     * @returns {object} The merged default options of the dialog.
     * @override
     */
    static get defaultOptions() {
        return foundry.utils.mergeObject(super.defaultOptions, {
            id: "loot-claim-",
            template: "systems/tde5e/templates/chat/lootClaim-sheet.hbs",
            width: 550,
            height: 600,
            title: "TODO LOOT CLAIM TITLE",
            resizable: true,
            submitOnChange: false,
            closeOnSubmit: false,
            classes: ["lootClaim-sheet"],
        });
    }

    /**
     * @override
     */
    getData() {
        const data = this.object.flags.tde5e.loot;
        return {
            data,
            visibleItems: this.object.visibleItems,
            hiddenItems: this.object.hiddenItems,
            isGM: game.user.isGM,
            playersCanClaim: this.object.playersCanClaim,
            allItemsClaimed: this.object.allItemsClaimed,
            allItemsEvaluated: this.object.allItemsEvaluated,
            isOwner: this.object.isOwner,
        };
    }

    /**
     * @override
     */
    activateListeners(html) {
        super.activateListeners(html);
        html.find("a.evaluate-item").click(this.onClickEvaluateItem.bind(this));
        html.find("a.reevaluate-item").click(this.onClickEvaluateItem.bind(this));
        html.find("a.delete-claim").click(this.onClickDeleteClaim.bind(this));
        html.find("a.claim-item").click(this.onClickClaimItem.bind(this));
        html.find("a.show-item").click(this.onClickShowItem.bind(this));

        html.find("a.evaluate-simple-items").click(this.onClickEvaluateSimple.bind(this));
        html.find("a.evaluate-all-items").click(this.onClickEvaluateAll.bind(this));
        html.find("a.abort-claim").click(this.onClickAbortClaim.bind(this));
        html.find("a.distribute-items").click(this.onClickDistributeItems.bind(this));
        html.find("a.open-claim").click(this.onClickOpenClaim.bind(this));
        html.find("a.end-claim").click(this.onClickEndClaim.bind(this));
        html.find("a.modify-quantity").click(this.onClickModifyQuantity.bind(this));
    }

    /**
     * Evaluates the item that was clicked to be evaluated.
     * @param {JQuery.event} event The click event.
     * @param {boolean?} force Default:false. If true, will overwrite existing evaluation if present.
     */
    onClickEvaluateItem(event, force = false) {
        event.preventDefault();
        const target = event.currentTarget;
        const itemId = target?.dataset?.id;
        if (!itemId) return;
        const options = { force };
        const qlInput = $(`#ql-${itemId}`);
        if (qlInput.length === 1) options.checkQl = parseInt(qlInput[0].value, 10);

        this.object.evaluteItems([itemId], options);
    }

    /**
     * Reevaluates the item that was clicked to be evaluated.
     * @param {JQuery.event} event The click event.
     */
    onClickReevaluateItem(event) {
        this.onClickEvaluateItem(event, true);
    }

    /**
     * Displays item that was clciked to be rendered.
     * @param {JQuery.event} event The click event.
     */
    onClickShowItem(event) {
        event.preventDefault();
        const target = event.currentTarget;
        const itemId = target?.dataset?.id;
        const itemData = this.object.getLootItem(itemId);
        // TODO: Handle update/edit on opened item
        const itemObj = new TdeItem(itemData, {});
        itemObj.sheet.render(true);
    }

    /**
     * Opens the claim to players so they claim items.
     * Can only be used by a GM or the owner of the message.
     * Only works if the claim wasn't open before.
     * @param {JQuery.event} event  The click event.
     */
    onClickOpenClaim(event) {
        event.preventDefault();

        const result = Dialog.confirm({
            title: game.i18n.localize("tde5e.lootClaim.dialog.confirmOpenClaimTitle"),
            content: game.i18n.localize("tde5e.lootClaim.dialog.confirmOpenClaimContent"),
        });

        result.then(confirmed => {
            if (confirmed) {
                const success = this.object.openClaim();
                if (success) info(`LootClaim ${this.object.id} opened`);
            }
        });
    }

    /**
     * Cancles the claim and deletes all unassigned items.
     * Can only be used by a GM or the owner of the message.
     * @param {JQuery.event} event  The click event.
     */
    onClickAbortClaim(event) {
        event.preventDefault();

        const result = Dialog.confirm({
            title: game.i18n.localize("tde5e.lootClaim.dialog.confirmAbortClaimTitle"),
            content: game.i18n.localize("tde5e.lootClaim.dialog.confirmAbortClaimContent"),
        });

        result.then(confirmed => {
            if (confirmed) {
                const success = this.object.endClaim(true);
                if (success) info(`LootClaim ${this.object.id} aborted`);
            }
        });
    }

    /**
     *  Cancles the claim and deletes all unassigned items.
     * @param {JQuery.event} event  The click event.
     */
    onClickEndClaim(event) {
        event.preventDefault();

        const result = Dialog.confirm({
            title: game.i18n.localize("tde5e.lootClaim.dialog.confirmAbortClaimTitle"),
            content: game.i18n.localize("tde5e.lootClaim.dialog.confirmAbortClaimContent"),
        });

        result.then(confirmed => {
            if (confirmed) {
                const success = this.object.endClaim(false);
                if (success) info(`LootClaim ${this.object.id} aborted`);
            }
        });
    }

    /**
     * Modifies the quantity after evaluation.
     * Can only be used by a GM or the owner of the message.
     * @param {JQuery.event} event  The click event.
     */
    onClickModifyQuantity(event) {
        event.preventDefault();
        const target = event.currentTarget;
        const itemId = target?.dataset?.id;
        if (!itemId) return;
        const { value } = $(`#claim-${itemId}`)[0];
        if (!hasValue(value)) return;
        this.object.updateEmbeddedLootItem(
            itemId,
            { "flags.tde5e.loot.evaluate.quantity": value },
        );
    }

    /**
     * Evaluates all items that have no requirement and can simply be rolled.
     * Can only be used by a GM or the owner of the message.
     * @param {JQuery.event} event  The click event.
     */
    onClickEvaluateSimple(event) {
        event.preventDefault();
        const ids = this.object.hiddenItems
            .filter(item => !item.flags.tde5e.loot.requirement && !item.flags.tde5e.loot.quantityIsPerQl)
            .map(item => item._id);

        this.object.evaluteItems(ids, {});
    }

    onClickEvaluateAll(event) {
        event.preventDefault();
        // TODO:
    }

    onClickDistributeItems(event) {
        event.preventDefault();
        // TODO:
    }

    /**
     * Claims the amount of an item that was clicked on.
     * Call is redirected to a GM if the iser does not have permission.
     * @param {JQuery.event} event  The click event.
     */
    onClickClaimItem(event) {
        event.preventDefault();
        const target = event.currentTarget;
        const itemId = target?.dataset?.id;
        if (!itemId) return;
        const value = $(`#claim-${itemId}`)[0].value ?? 0;
        const displayName = game.user.character?.name;

        if (game.user.isGM || this.object.testUserPermission(this.object, "OWNER")) {
            const userId = game.user.id;
            this.object.claimLootItem(itemId, userId, displayName, parseInt(value, 10));
        } else {
            requestItemClaim(this.object.id, itemId, value, displayName);
        }
    }

    /**
     * Deletes an item that was clicked on.
     * Call is redirected to a GM if the iser does not have permission.
     * @param {JQuery.event} event  The click event.
     */
    onClickDeleteClaim(event) {
        event.preventDefault();
        const target = event.currentTarget;
        const itemId = target?.dataset?.itemId;
        const userId = target?.dataset?.userId;
        if (!itemId || !userId) return;

        if (game.user.isGM || this.object.testUserPermission(this.object, "OWNER")) {
            this.object.removeLootItemClaim(itemId, game.user.id, null);
        } else {
            requestItemClaimDeletion(this.object.id, itemId);
        }
    }

    /** @override */
    _updateObject() {}
}
