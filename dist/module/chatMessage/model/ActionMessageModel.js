import ItemUuidField from "../../items/model/partial/itemUuidField.js";
import ActionModifierModel from "./partial/ActionModifierModel.js";

export default class ActionMessageModel extends foundry.abstract.DataModel {
    /** @inheritdoc */
    static defineSchema() {
        const { fields } = foundry.data;
        return {
            item: new ItemUuidField(),
            modifiers: new fields.ArrayField(new fields.EmbeddedDataField(ActionModifierModel)),
        };
    }
}
