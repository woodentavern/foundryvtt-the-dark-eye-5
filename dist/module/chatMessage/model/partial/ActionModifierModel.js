import ItemUuidField from "../../../items/model/partial/itemUuidField.js";

export default class ActionModifierModel extends foundry.abstract.DataModel {
    /** @inheritdoc */
    static defineSchema() {
        const { fields } = foundry.data;
        return {
            type: new fields.StringField({ choices: ["quality", "reaction"], initial: "quality" }),
            source: new ItemUuidField({ required: false, default: null }),
            steps: new fields.SchemaField({
                all: new fields.BooleanField(),
                values: new fields.ArrayField(new fields.NumberField()),
            }),
            value: new fields.ObjectField({ required: false }),
        };
    }

    canModifyStep(step) {
        const isIncluded = this.steps.values.includes(step);
        return this.steps.all ? !isIncluded : isIncluded;
    }
}
