/* eslint-disable no-await-in-loop -- Updates should be performed sequentially (not in parallel) */
import * as util from "./util.js";

/** Cache for already loaded compendium entities. */
let compendiumCache = {};

/**
 * Migrates system data to a newer version, if one is available.
 * @param {boolean=} force Indicates whether the migration should occur regardless of system versions.
 *  Defaults to false.
 */
export async function migrateSystem(force = false) {
    const targetVersion = game.system.version;
    const currentVersion = game.settings.get("tde5e", "systemMigrationVersion");

    if (force || foundry.utils.isNewerVersion(targetVersion, currentVersion)) {
        util.warn(`Migrating system from version ${currentVersion} to ${targetVersion}.`, true);
        await migrateDocuments("Actor", migrateActor);
        await migrateDocuments("Item", migrateItem);

        util.warn("System migration complete.", true);

        game.settings.set("tde5e", "systemMigrationVersion", targetVersion);
        util.info(`Migrated world system version from ${currentVersion} to ${targetVersion}.`);
    }
}

/**
 * Migrates all instances of the given document type within the world or its compendiums.
 * @param {string} type The type name of the document.
 * @param {Function<Document, object>} migrator An asynchronous function to migrate instances of the document.
 */
async function migrateDocuments(type, migrator) {
    // Migrate world documents.
    const worldCollection = game.collections.get(type);
    const worldData = game.data[foundry.documents[`Base${type}`].collectionName];
    for (const doc of worldCollection) {
        await migrator(doc, worldData.find(d => d._id === doc.id));
    }

    // Migrate world compendiums.
    const worldCompendiums = game.packs
        .filter(pack => pack.metadata.packageType === "world" && pack.documentName === type);
    for (const pack of worldCompendiums) {
        await migrateCompendium(pack, migrator);
    }
}

/**
 * Migrates all document instances within the given compendium.
 * @param {CompendiumCollection} pack The compendium to migrate.
 * @param {Function<Document, object>} migrator An asynchronous function to migrate the document instances.
 */
export async function migrateCompendium(pack, migrator) {
    try {
        // Make sure that the pack can be updated.
        const wasLocked = pack.locked;
        if (wasLocked) await pack.configure({ locked: false });

        // Load the compendium documents.
        const compendiumCollection = await pack.getDocuments();

        // Fetch the raw data from the database.
        const backendResponse = await SocketInterface.dispatch("modifyDocument", {
            type: pack.documentName,
            action: "get",
            operation: {
                broadcast: false,
                modifiedTime: Date.now(),
                pack: pack.collection,
                parent: null,
                query: {},
            },
        });
        const compendiumData = backendResponse.result;

        // Migrate the documents.
        for (const doc of compendiumCollection) {
            await migrator(doc, compendiumData.find(d => d._id === doc.id));
        }

        // Restore the lock.
        if (wasLocked) await pack.configure({ locked: true });
    } catch (ex) {
        util.error(`Failed to migrate compendium ${pack.collection}. Check the console for details.`, ex);
    }
}

/**
 * Migrates the given actor and its embedded items.
 * @param {TdeActor} actor The actor to migrate.
 * @param {object} rawData The database entry of the actor.
 */
export async function migrateActor(actor, rawData) {
    try {
        // Migrate the actor itself.
        const changes = actor.migrate(rawData) ?? {};
        const changeCount = Object.keys(changes).length;
        if (changeCount) {
            await actor.update(changes, { migration: true });
            util.debug(`Migrated ${changeCount} properties in actor ${actor.uuid}.`);
        }

        // Migrate embedded items.
        const itemChanges = actor.items.reduce((updates, item) => {
            const itemData = rawData.items.find(i => i._id === item.id);
            if (!itemData) {
                util.error(`Failed to migrate item ${item.uuid} because it doesn't have source data.`);
                return updates;
            }

            const update = item.migrate(itemData);
            if (Object.keys(update).length) {
                update._id = item.id;
                updates.push(update);
            }
            return updates;
        }, []);
        if (itemChanges.length) {
            await actor.updateEmbeddedDocuments("Item", itemChanges, { migration: true });
            util.debug(`Migrated ${itemChanges.length} items in actor ${actor.uuid}.`);
        }
    } catch (ex) {
        util.error(`Failed to migrate actor ${actor.uuid}. Check the console for details.`, ex);
    }
}

/**
 * Migrates the given item.
 * @param {TdeItem} item The item to migrate.
 * @param {object} rawData The database entry of the item.
 */
export async function migrateItem(item, rawData) {
    try {
        const changes = item.migrate(rawData) ?? {};
        const changeCount = Object.keys(changes).length;
        if (changeCount) {
            await item.update(changes, { migration: true });
            util.debug(`Migrated ${changeCount} properties in item ${item.uuid}.`);
        } else if (!item._stats.systemVersion) {
            await item.update(item.toObject(), { diff: false, recursive: false, migration: true });
            util.debug(`Forced version migration for item ${item.uuid}.`);
        }
    } catch (ex) {
        util.error(`Failed to migrate item ${item.uuid}. Check the console for details.`, ex);
    }
}

/**
 * Migrates all compendium entities to a newer version, if one is available.
 * @param {boolean=} force Indicates whether the migration should occur regardless of data versions. Defaults to false.
 */
export async function migrateData(force = false) {
    // Compare the current and target data version.
    const targetVersion = TDE5E.dataVersion;
    const currentVersion = game.settings.get("tde5e", "dataMigrationVersion");

    if (force || currentVersion < targetVersion) {
        util.warn(`Migrating data from version ${currentVersion} to ${targetVersion}.`, true);
        for (const actor of game.actors) await migrateActorData(actor);
        util.warn("Data migration complete.", true);

        game.settings.set("tde5e", "dataMigrationVersion", targetVersion);
        util.info(`Migrated world data version from ${currentVersion} to ${targetVersion}.`);
    }

    // Clear the cache so we don't waste memory.
    compendiumCache = {};
}

/**
 * Updates every item property within the given actor that has a compendium ID
 *  but differs from the compendium item with that ID.
 * @param {TdeActor} actor The actor to migrate.
 */
export async function migrateActorData(actor) {
    for (const item of actor.items) await migrateEmbeddedItemData(item, actor);
}

/**
 * Updates an item property within in an actor that has a compendium ID
 *  but differs from the compendium item with that ID.
 * @param {TdeItem} item The embedded item.
 * @param {TdeActor} actor The actor to migrate.
 */
export async function migrateEmbeddedItemData(item, actor) {
    const sourceItem = await findSourceItem(item);
    if (!sourceItem) return;

    const itemData = item.toObject();
    const compendiumItemData = sourceItem.toObject();
    delete compendiumItemData._stats; // Ignore FoundryVTT stats.

    // Process variants
    if (!applyItemVariant(itemData, compendiumItemData)) return;

    // Determine which properties to replace.
    const diffData = foundry.utils.diffObject(itemData, compendiumItemData);

    // Ignore empty flags.
    if (diffData.flags && foundry.utils.isEmpty(diffData.flags)) delete diffData.flags;
    const flatDiff = foundry.utils.flattenObject(diffData);

    // Ignore protected properties.
    for (const property of Object.keys(flatDiff)) {
        if (property.startsWith("system.") || ["name", "type"].includes(property)) {
            const metadata = item.getPropertyMetadata(property);
            if (!metadata.noMigration) continue;
        }

        delete flatDiff[property];
    }

    const props = Object.keys(flatDiff);
    if (!props.length) return; // Don't update without changes.
    util.debug(
        `Migrating ${props.length} properties in item ${item.name} of actor ${actor.name} (${props.join(", ")}).`,
    );
    await item.update(flatDiff, { diff: false, migration: true });
}

/**
 * Attempts fo find the compendium source of the given item using its own source flag or the global sources list.
 * @param {TdeItem} item The item to find the source for.
 * @returns {TdeItem?} The compendium source of the item or null if there is none.
 */
async function findSourceItem(item) {
    if (!item.cid) return null; // Skip items without CID.
    let sources = [];
    if (item.compendiumId) sources.push(item.compendiumId);
    sources.push(...(TDE5E.indexedProperties[item.type]?.sources ?? []));

    // Reduce unnecessary lookups for duplicate sources
    sources = util.distinct(sources);

    const sourceCid = item.cid.split(":")[0]; // Remove variant from CID for lookups.
    for (const source of sources) {
        if (!compendiumCache.hasOwnProperty(source)) {
            compendiumCache[source] = await game.packs.get(source)?.getDocuments();
        }

        const sourceItem = compendiumCache[source]?.find(i => i.cid === sourceCid);
        if (sourceItem) return sourceItem;
        util.warn(`Item ${sourceCid} could not be found in compendium ${source}, trying other sources.`);
    }

    util.info(`Item (ID: ${item._id}, Parent: ${item.parent?.name}, Type: ${item.type}, CID: ${item.cid}) no `
        + "compendium reference anymore. Please delete and insert the item again.", false, util.logStyle.migration);
    return null;
}

/**
 *  Checks if a variant is present and if so applies it to the newItem so
 *  that the existing item can be cleanly overriden.
 * @param {object} itemData The data of the item as it was present in the game.
 * @param {object} newItem The data of the item in the latest version.
 * @returns {boolean} True if migration can continue, false if current item cannot be migrated.
 */
function applyItemVariant(itemData, newItem) {
    if (itemData.system.variant?.selected) newItem.system.variant.selected = itemData.system.variant.selected;
    if (itemData.system.variant?.selected?.vid) {
        // Set variant for dif calculation
        if (itemData.system.variant.selected.vid === "CUSTOM") return true;
        const selectedVariant = newItem.system.variant.available
            ?.find(variant => variant.vid === itemData.system.variant.selected.vid);
        if (!selectedVariant) {
            util.warn(`Attempt to migrate item ${itemData.system.cid} failed because variant `
                + `${itemData.system.variant.selected.vid} was not found.`);
            return false;
        }

        delete selectedVariant.name;
        delete selectedVariant.vid;
        const variantUpdate = { system: selectedVariant };

        // add flags to the right position
        if (selectedVariant.flags) {
            const { flags } = selectedVariant;
            delete variantUpdate.system.flags;
            variantUpdate.flags = flags;
        }

        // Merge variant into item data
        foundry.utils.mergeObject(newItem, variantUpdate);
    }

    return true;
}
