/**
 * Parses keys with an array syntax within the given form data.
 * @param {object} data The flattened data to parse.
 * @param {object?} source Optional data source to merge existing array data that is not complete in the form.
 *  No merging will be performed if this is empty.
 * @param {boolean=} mergeAll Indicates whether missing array entries should be merged from the source data.
 *  Note that this flag prevents adding or removing elements.
 */
export function parseArrays(data, source, mergeAll = false) {
    const unsortedData = {};

    for (const key of Object.keys(data)) {
        const indexInfo = extractIndex(key);
        if (!indexInfo) continue;
        const [prefix, index, suffix] = indexInfo;

        if (Number.isNaN(index)) {
            // No index means no sorting, so we can just push into the data directly.
            const value = data[key];
            data[prefix] = Array.isArray(value) ? value : [value];
        } else {
            // Create temporary array if it doesn't exist yet.
            let target = unsortedData[prefix];
            if (!target) unsortedData[prefix] = target = [];

            if (!suffix) {
                // For simple values, push the value itself.
                target.push([index, data[key]]);
            } else {
                // For complex objects, use the existing object or create a new one.
                const entry = target.find(e => e[0] === index);
                if (!entry) {
                    target.push([index, { [suffix]: data[key] }]);
                } else {
                    entry[1][suffix] = data[key];
                }
            }
        }

        delete data[key];
    }

    for (const [path, array] of Object.entries(unsortedData)) {
        const sourceArray = source ? foundry.utils.getProperty(source, path) : null;
        if (mergeAll && Array.isArray(sourceArray)) {
            // Copy the old array and merge in new values at the root level.
            const property = foundry.utils.deepClone(sourceArray);
            data[path] = property;
            property.forEach((oldValue, i) => {
                const entry = array.find(e => e[0] === i);
                if (!entry) return;
                const value = entry[1];
                if (typeof (oldValue) === "object") {
                    parseArrays(value, oldValue, mergeAll);
                    foundry.utils.mergeObject(oldValue, value);
                } else {
                    property[i] = value;
                }
            });
        } else {
            // Build new array using merged entries.
            data[path] = array // Write to key field.
                .sort((a, b) => a[0] - b[0]) // Sort by index.
                .map(e => { // Map to value and parse arrays recursively.
                    const property = e[1];
                    if (property && typeof (property) === "object") {
                        const sourceEntry = sourceArray?.[e[0]];
                        parseArrays(property, sourceEntry);
                        if (sourceEntry) foundry.utils.mergeObject(property, sourceEntry, { overwrite: false });
                    }
                    return property;
                });
        }
    }
}

/**
 * Extracts array syntax information from the given path.
 * @param {string} path The path to extract the index from.
 * @returns {string[]?} An array containing the prefix, the numerical index and the suffix or null if the path doesn't
 *  contain a valid index.
 */
export function extractIndex(path) {
    // Check if the path contains array syntax.
    const startIndex = path.indexOf("[");
    if (startIndex === -1) return null;

    // Extract and validate the prefix, index and suffix.
    const endIndex = path.indexOf("]", startIndex + 1);
    const index = parseInt(path.substring(startIndex + 1, endIndex), 10);
    const prefix = path.substring(0, startIndex);
    const suffix = path.substring(endIndex + 2); // May be empty.
    if (endIndex === -1 || !prefix || (suffix && Number.isNaN(index))) return null;
    return [prefix, index, suffix];
}

/**
 * Removes property metadata from the given object if the value wasn't written before and matches the global metadata.
 * @param {object} data The flattened data to filter.
 * @param {TdeItem} item The item to read existing metadata from.
 */
export function removeDefaultMetadata(data, item) {
    for (const key of Object.keys(data)) {
        if (!key.startsWith("flags.tde5e.propertyMetadata.")) continue;
        if (foundry.utils.hasProperty(item, key)) continue;

        const lastSplit = key.lastIndexOf(".");
        const metadata = item.getPropertyMetadata(key.substring(29, lastSplit));
        const value = metadata[key.substring(lastSplit + 1)] ?? false;
        if (value === data[key]) delete data[key];
    }
}
