export default class TdeTremorSenseDetectionMode extends DetectionModeTremor {
    /** @override */
    static getDetectionFilter() {
        this._detectionFilter ??= OutlineOverlayFilter.create({
            thickness: [1, 1],
            outlineColor: [0.7, 0.7, 0.7, 1],
            knockout: true,
            wave: true,
        });

        return this._detectionFilter;
    }
}
