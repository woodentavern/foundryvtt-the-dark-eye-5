import { ActorLogEntry } from "../actors/logEntry.js";
import promptReplace from "../ui/replaceDialog.js";
import { hasVariants, promptVariant } from "../ui/variantPicker.js";
import * as util from "../util.js";

/**
 * Map of preparation steps that specify when an item is prepared.
 */
export const PreparationPriority = {
    /** Earliest step for modifying independent base data. */
    BEFORE_ATTRIBUTES: 0,

    /** After attributes are prepared, but before derived values are calculated. */
    AFTER_ATTRIBUTES: 1,

    /** After derived value calculation, but before they are modified. */
    AFTER_DERIVED: 2,

    /** After user value modifications, but before derivatives of those. */
    AFTER_MODIFIED: 3,

    /** After preparation is complete, but before dependent items. */
    AFTER_PREPARATION_EARLY: 4,

    /** Latest step for modifying anything that depends on other items. */
    AFTER_PREPARATION_LATE: 5,
};

/**
 * The default item class for all TDE items.
 */
export default class TdeItem extends Item {
    /**
     * The path to the default icon for this item type.
     * @type {string}
     */
    static defaultIconPath = "icons/svg/mystery-man-black.svg";

    /**
     * Value indicating when this item is prepared within an actor preparation.
     * @see PreparationPriority
     * @type {number}
     */
    preparationPriority = PreparationPriority.AFTER_PREPARATION_EARLY;

    /**
     * Creates a fully typed item from the given data.
     * @param {object} data The data that the item is created with.
     * @param {object} options The options of the creation.
     * @param {boolean} typed Flag indicating that the type was already resolved.
     */
    constructor(data, options, typed = false) {
        if (!typed) {
            const cls = TdeItem.resolveClass(data.type, data.system?.cid, data.system?.group);
            return new cls(data, options, true);
        }
        super(data, options);
        this.buildReferences();
    }

    /**
     * Shortcut to the compendium ID of this item.
     * @returns {string?} The CID of the item or null.
     */
    get cid() {
        return this.system.cid;
    }

    /**
     * Returns the ID of the compendium that this item belongs to (if any) or was generated from (for instanced items).
     * @returns {string?} The ID of the item's source compendium.
     */
    get compendiumId() {
        if (this.compendium) return this.compendium.collection;

        const source = this._stats.compendiumSource;
        if (!source || !source.startsWith("Compendium.")) return null;
        return foundry.utils.parseUuid(source).collection?.collection;
    }

    /**
     * Retrieves the item's source. If none is found, this is the item itself.
     * @returns {TdeItem | object} The source item or its compendium index entry.
     */
    get sourceItem() {
        const source = this._stats.compendiumSource;
        if (source) {
            const sourceItem = fromUuidSync(source);
            if (sourceItem?.system?.cid && this.cid.startsWith(sourceItem.system.cid)) return sourceItem;
        }

        return this;
    }

    /**
     * Shortcut to access the unmodified system data of the item.
     * @returns {object} The item's system data without modifications.
     */
    get baseData() {
        return this._source.system;
    }

    /**
     * Checks if this item can be used for a skill check.
     * @returns {boolean} True if the item can be rolled as a skill check, false otherwise.
     */
    get isRollable() {
        return "check" in this.system;
    }

    /**
     * Determines whether an item is inherent to the owner, meaning it cannot be removed or discarded.
     * This flag also ensures that an inherent item will not be displayed in the inventory.
     * @returns {boolean} If true, this item cannot be removed or discarded.
     */
    get isNatural() {
        return false;
    }

    /**
     * Calculates the adventure points required to activate this item.
     * @returns {number} The item's activation cost.
     */
    get activationCost() {
        return this.system.improvement.column ? TDE5E.improvementTable[this.system.improvement.column][0] : 0;
    }

    /**
     * Checks if this item can be used to modify a skill check of the given item.
     *  The result should not depend on any temporary states of the target. For
     *  those cases, return true here and return null in {@link modifyRoll}.
     * @param {RollableItem} item The item that is being rolled.
     * @returns {boolean} True if the item can modify skill checks, false otherwise.
     */
    // eslint-disable-next-line no-unused-vars -- Keep parameter for documentation purposes.
    canModifyRoll(item) {
        return false;
    }

    /**
     * Returns the full name of this item.
     * @returns {string} The prepared display name or the original name.
     */
    get displayName() {
        if (!this._displayName) this._displayName = this._createDisplayName();
        return this._displayName;
    }

    /**
     * Creates the name for this item including translations, variants and levels.
     * @returns {string} The full name of the item.
     */
    _createDisplayName() {
        const levels = this.system.purchase?.levels;
        if (this.compendium) {
            let name = (this.system.wikiName) ? this.system.wikiName : this.name;
            if (levels > 1) name += ` I-${util.romanize(levels)}`;
            return name;
        }

        let { name } = this;
        const { level, selected } = this.system.variant ?? {};
        if (selected) {
            name += " (";
            if (selected.target) {
                if (selected.target.startsWith("tde5e.")) name += game.i18n.localize(selected.target);
                else name += this.actor.items.get(selected.target)?.name ?? "";
            }
            if (selected.name) name += (selected.target ? " - " : "") + selected.name;
            name += ")";
        }

        if (level && levels > 1) name += ` ${util.romanize(level)}`;
        return name;
    }

    /**
     * Translates the name of this item's type or group.
     * @returns {string} The localized item category.
     */
    get localizedGroupName() {
        return util.localizeType(this.type, this.system.group);
    }

    /**
     * Checks whether the item has a variant.
     * This does not inform whether the item supports variants but merely checks whether one is present.
     * @returns {boolean} True if a variant was set for this item, false otherwise.
     */
    get hasVariant() {
        return !!this.system.variant?.selected;
    }

    /**
     * Checks whether the item has charges.
     * @returns {boolean} True if the flags contain charge information, false otherwise.
     */
    get hasCharges() {
        return !!this.flags.tde5e?.charges?.max;
    }

    /**
     * Creates an object containing information on how to handle the item's quantity, e.g. with charges or an amount.
     * @returns {object?} An object containing information about the item's quantity or null.
     */
    get quantityInformation() {
        if (!this.hasCharges) return null;
        const chargeInfo = foundry.utils.deepClone(this.getFlag("tde5e", "charges"));
        chargeInfo.path = "flags.tde5e.charges.value";
        return chargeInfo;
    }

    /**
     * Checks whether the item is locked, limiting the properties that can be edited.
     * @returns {boolean} True if the item was locked by a GM, false otherwise.
     */
    get locked() {
        return this.flags.tde5e?.locked ?? false;
    }

    /**
     * Retrieves documents that directly reference this item or its index.
     * @returns {string[]} An array of child document UUIDs.
     */
    get references() {
        if (this.compendium && !this.isEmbedded) {
            const indexEntry = this.compendium.index.get(this.id);
            if (indexEntry) {
                indexEntry.references ??= new Set();
                return indexEntry.references;
            }
        }

        this._references ??= new Set();
        return this._references;
    }

    /**
     * Adds references to this item within its parent's children.
     */
    buildReferences() {
        if (!TDE5E.compendiumsReady || !this.system.parents.size) return;

        const { sourceItem } = this;
        if (!sourceItem._id) return;
        const isSource = sourceItem === this;

        for (const parent of this.system.parents) {
            let { uuid } = sourceItem;
            if (!isSource) {
                // Only add references that aren't included in the parent.
                const { parents } = sourceItem.system;
                if (parents && ((Array.isArray(parents) && parents.includes(parent)) || parents.has(parent))) continue;
                uuid = this.uuid; // Local reference, don't use source item.
            }

            // Defer world items until collection is ready.
            if (!game._documentsReady && parent.startsWith("Item.")) setTimeout(() => this._addReference(parent, uuid));
            else this._addReference(parent, uuid);
        }
    }

    /**
     * Adds the given reference to the resolved parent.
     * @param {string} parent The UUID of the parent to add the reference to.
     * @param {string} reference The reference UUID to add.
     */
    _addReference(parent, reference) {
        let parentEntry = fromUuidSync(parent, { strict: false });
        if (!parentEntry) return;
        if (parentEntry instanceof foundry.abstract.Document && parentEntry.collection?.index) {
            parentEntry = parentEntry.collection.index.get(parentEntry.id);
        }

        parentEntry.references ??= new Set();
        parentEntry.references.add(reference);
    }

    /**
     * Refreshes child references of this item's parents by removing old and adding new entries.
     * @param {string[]} oldParents The array of parent UUIDs before the update.
     */
    rebuildReferences(oldParents) {
        this.removeReferences(oldParents);
        this.buildReferences();
    }

    /**
     * Removes references to this item from its parents.
     * @param {string[]?} oldParents The parent UUIDs to remove. Defaults to the current parents,
     */
    removeReferences(oldParents = this.system.parents) {
        for (const oldParent of oldParents) {
            const parentEntry = fromUuidSync(oldParent, { strict: false });
            parentEntry?.references?.delete(this.uuid);
        }
    }

    /**
     * Retrieves documents that reference this item as their parent.
     * Unlike {@link references}, this also includes the references of the item's source.
     * @returns {string[]} An array of child document UUIDs.
     */
    get children() {
        const references = Array.from(this.references);

        const { sourceItem } = this;
        if (sourceItem !== this) {
            const globalReferences = sourceItem.children ?? sourceItem.references;
            if (globalReferences && (globalReferences.length || globalReferences.size)) {
                references.push(...globalReferences);
            }
        }

        return references;
    }

    /**
     * Retrieves metadata information for the property with the given path. The metadata is assembled with global
     *  system values, type specific system values and local metadata flags for the property path. Existing values
     *  are overridden based on highest specifity (e.g. flags will override global and type values).
     * The result is cached until this item is updated.
     * @param {string} path The path of the property within this item.
     * @returns {object} An object containing merged metadata information. If there is no metadata or the path does
     *  not exist, an empty object is returned.
     */
    getPropertyMetadata(path) {
        if (!path) return {};

        // Merge model and item metadata.
        let systemMetadata;
        if (path.startsWith("system.")) systemMetadata = this.system.schema.getField(path.substring(7));
        else if (path.startsWith("flags.")) systemMetadata = TDE5E.flagMetadata[path];
        else systemMetadata = this.schema.getField(path);
        if (!systemMetadata) return { label: path };
        const localMetadata = foundry.utils.getProperty(this, `flags.tde5e.propertyMetadata.${path}`) ?? {};
        return {
            label: systemMetadata.label ? game.i18n.localize(systemMetadata.label) : path,
            hint: systemMetadata.hint ? game.i18n.localize(systemMetadata.hint) : "",
            alwaysEditable: systemMetadata.alwaysEditable ?? false,
            hidden: localMetadata.hidden ?? systemMetadata.hidden ?? false,
            locked: localMetadata.locked ?? systemMetadata.locked ?? false,
            noMigration: localMetadata.noMigration ?? systemMetadata.noMigration ?? false,
        };
    }

    /**
     * Determines if a log entry should be created for a property change.
     * @param {string} path The path of the changed property.
     * @param {*} value The old value of the changed property.
     * @returns {boolean} True if the change should have a log entry, false otherwise.
     */
    trackProperty(path, value) {
        if (value === undefined) return false;

        let metadata;
        if (path.startsWith("system.")) metadata = this.system.schema.getField(path.substring(7));
        else if (path.startsWith("flags.tde5e.")) metadata = TDE5E.flagMetadata[path];

        return metadata ? !metadata.skipChangelog : false;
    }

    /** @override */
    prepareBaseData() {
        super.prepareBaseData();
        if (!TDE5E.hasAssets && this._source.img?.startsWith("modules/tde5e-assets/")) this.img = "";
    }

    /**
     * Validates references to source and other dependencies.
     * @returns {boolean} True, iff references are pointing to valid objects. False otherwise.
     */
    hasValidReferences() {
        const uuid = this._stats.compendiumSource;
        if (!uuid) return true;
        const uuidParts = foundry.utils.parseUuid(uuid);
        if (!uuidParts.collection?.index) return false;
        return uuidParts.collection.index.has(uuidParts.id);
    }

    /**
     * Prepares any data related to the preparation of the parent actor. This
     *  can be used to modify the actor or other embedded items.
     */
    prepareActorData() {}

    /**
     * Applies a modifier to the given property of the given object and adds a
     *  descriptive entry in the parent's modifier list.
     * @param {string} name The name of the modifier.
     * @param {object} parent The parent of the value to modify.
     * @param {string} propertyName The name of the property within the parent to modify.
     * @param {number} value The value to add to the property. This is not required to be a number,
     *  but must support the + operator.
     */
    modify(name, parent, propertyName, value) {
        if (!value) return;
        parent.modifiers ??= [];
        parent[propertyName] ??= 0;
        parent[propertyName] += value;
        parent.modifiers.push([name, value]);
    }

    /**
     * Gets the path to the template that should be used to display the properties of this item.
     * @returns {string} The path to the item's sheet content template.
     */
    get sheetContentTemplate() {
        return `systems/tde5e/templates/items/content/${this.type}.hbs`;
    }

    /**
     * Gets the path to the template that should be used to display variants of this item.
     * @returns {string} The path to the item's sheet variant template.
     */
    get sheetVariantTemplate() {
        return `systems/tde5e/templates/items/variants/${this.type}Variant.hbs`;
    }

    /**
     * Gets the header class representing this item's base color.
     * @returns {string} A CSS class representing the item's color.
     */
    get sheetHeaderClass() {
        return "bg-brown";
    }

    /**
     * Creates an object that contains all relevant data of this item, in a ui-friendly form.
     * This data can then be processed by the sheet to display this item's information to the user.
     * @returns {object} An object containing all relevant data to display it the given context in the ui layer.
     */
    getSheetData() {
        return {
            ...this,
            _source: this._source,
            _id: this.id,
            id: this.id,
            uuid: this.uuid,
            link: this.createLink(),
            symbol: `<a class="content-link nobg" data-link data-uuid="${this.uuid}">
            <img class="symbol-img" src="${this.constructor.defaultIconPath}"></a>`,
            canRoll: this.isRollable,
            canUse: this.isLightSource ?? false,
            canDelete: game.user.isGM || !this.getFlag("tde5e", "preventDelete"),
            canTransfer: game.user.isGM || !this.getFlag("tde5e", "preventTransfer"),
            hasValidReferences: this.hasValidReferences(),
        };
    }

    /**
     * Creates an HTML link to open the sheet of this item.
     * @returns {string} The HTML link to this item.
     */
    createLink() {
        return `<a class="content-link nobg" data-link draggable="true" data-uuid=${this.uuid} ${
            this.compendium ? `data-pack=${this.compendium.collection}` : ""
        }>${this.displayName}</a>`;
    }

    /**
     * Posts a link to this item in the chat. If the players do not have sufficient permissions to view it, a prompt
     *  will ask the user to change the permissions to limited access.
     * @returns {Promise} A promise representing the message creation.
     */
    async share() {
        // Hidden compendium items cannot be shared
        const ownershipLevels = CONST.DOCUMENT_OWNERSHIP_LEVELS;
        if (this.compendium && ownershipLevels[this.compendium.ownership.PLAYER] < ownershipLevels.LIMITED) {
            return util.error(game.i18n.localize("tde5e.sheetHeader.notVisibleCompendium"));
        }

        let content = `<h2 class="${this.sheetHeaderClass}">${this.createLink()}</h2>`;
        if (this.img !== this.constructor.defaultIconPath) content += `<img src="${this.img}"/>`;
        // Change permissions for none-compendium items
        if (!this.compendium
            && game.user.isGM
            && this.ownership.default < ownershipLevels.LIMITED) {
            const changePermissions = await Dialog.confirm({
                title: game.i18n.localize("tde5e.sheetHeader.sharePermissionsTitle"),
                content: game.i18n.localize("tde5e.sheetHeader.sharePermissions"),
            });
            if (changePermissions) await this.update({ "ownership.default": ownershipLevels.LIMITED });
        }

        return ChatMessage.create({
            user: game.user.id,
            type: CONST.CHAT_MESSAGE_TYPES.OOC,
            content,
        });
    }

    /**
     * Creation method that is called before the item is created. This method may be used to override data that will
     *  be merged during the creation.
     * @param {object} data A data object that can be used to override values on creation.
     * @param {object} options The options of the item creation.
     * @returns {object} The final data that the item will be created with.
     * @override
     */
    async _preCreate(data, options, user) {
        if (data.system?.changelog?.length) this.updateSource({ "system.changelog": [] });
        if (!data.img) this.updateSource({ img: null });
        return super._preCreate(data, options, user);
    }

    /**
     * Extends FoundryVTT's method to track changes.
     * @inheritdoc
     */
    async _preUpdate(changes, options, user) {
        if (!options.skipChangelog) ActorLogEntry.prepareUpdate(this, changes);
        if (changes.system?.parents) options.rebuildReferences = this._source.system.parents;
        return super._preUpdate(changes, options, user);
    }

    /**
     * Validates properties before updating the item.
     * @param {object} data The data of the update.
     * @param {object} options The options of the update.
     * @override
     */
    async update(data, options = {}) {
        if ("name" in data && !data.name) data.name = "-";

        // Skip preprocessing during migration.
        if (options.migration) return super.update(data, options);

        // Make sure we are only considering changes.
        if (options.diff !== false) data = foundry.utils.diffObject(this._source, foundry.utils.expandObject(data));

        const props = Object.keys(data);
        if (!props.length) return Promise.resolve();

        if (this.cid) {
            // Prevent migration of manually changed properties.
            const flatData = foundry.utils.flattenObject(data);
            for (const prop of Object.keys(flatData)) {
                if (prop.startsWith("flags.")) continue; // Flags should never be migrated, skip them here.
                const metadata = this.getPropertyMetadata(prop);
                if (!metadata.noMigration) {
                    foundry.utils.setProperty(data, `flags.tde5e.propertyMetadata.${prop}.noMigration`, true);
                }
            }
        }

        return super.update(data, options);
    }

    /**
     * Migrates the item to the current system version.
     * @param {object} data The raw database object before any validation.
     * @returns {object} The update data to apply.
     */
    // eslint-disable-next-line no-unused-vars -- Keep for documentation purposes.
    migrate(data) {
        let update = {};

        // Migrate protected properties to new metadata format.
        const protectedProperties = this.getFlag("tde5e", "protectedProperties");
        if (protectedProperties) {
            update = protectedProperties.reduce((u, property) => {
                // Replace data with system prefix for v9 items.
                if (property.startsWith("data.")) property = `system.${property.substring(5)}`;

                u[`flags.tde5e.propertyMetadata.${property}.noMigration`] = true;
                return u;
            }, {});
            update["flags.tde5e.-=protectedProperties"] = null;
            return update;
        }

        const sourceId = this._stats.compendiumSource;
        if (sourceId?.startsWith("Compendium.") && !sourceId.includes(".Item.")) {
            const sourceParts = sourceId.split(".");
            sourceParts.splice(-1, 0, "Item");
            update["_stats.compendiumSource"] = sourceParts.join(".");
        }

        return update;
    }

    /**
     * Extends Foundry's method to reset cached data properties.
     * @override
     */
    _onUpdate(data, options, userId) {
        super._onUpdate(data, options, userId);
        if (!this.isEmbedded) this._apValue = null;
        if (("name" in data) || data.system?.variant) this._displayName = null;
        if (options.rebuildReferences) this.rebuildReferences(options.rebuildReferences);
    }

    /**
     * Extends Foundry's method to clean up references.
     * @override
     */
    _onDelete() {
        this.removeReferences();
    }

    /**
     * Displays a confirmation dialogue for deleting this item.
     */
    deleteConfirm() {
        const type = game.i18n.localize(this.constructor.metadata.label);
        Dialog.confirm({
            title: `${game.i18n.format("DOCUMENT.Delete", { type })}: ${this.name}`,
            content: `<h4>${game.i18n.localize("AreYouSure")}</h4>
                <p>${game.i18n.format("SIDEBAR.DeleteWarning", { type })}</p>`,
            yes: () => {
                this.sheet?.close({ submit: false });
                this.delete();
            },
        });
    }

    /**
     * Performs validation and variant selection before items are created.
     * @param {object[]} data The data of the items to create.
     * @param {object} context Additional options for the creation.
     * @returns {Promise} A promise representing the item creation.
     */
    static async createDocuments(data = [], context = {}) {
        if (!(context.parent && context.parent instanceof Actor)) {
            data.forEach(d => foundry.utils.setProperty(d, "flags.tde5e.locked", true));
            if (context.pack?.startsWith("tde5e.")) {
                // Generate default CID, ID and image for new system items.
                context.keepId = true;
                for (const d of data) {
                    d.system ??= { cid: util.generateCid(d.name) };
                    d._id = util.generateId(d.system.cid);
                    d.img = TdeItem.resolveClass(d.type, d.system.cid, d.system.group).defaultIconPath;
                }
            }
            return super.createDocuments(data, context);
        }

        if (context.skipCreationCheck) return super.createDocuments(data, context);

        const allowedItems = [];
        for (const item of data) {
            const cls = this.resolveClass(item.type, item.system.cid, item.system.group);
            if (await cls.allowCreation(item, context.parent, !!context.skipVariants)) allowedItems.push(item);
        }

        if (allowedItems.length) return super.createDocuments(allowedItems, context);
        return Promise.resolve();
    }

    /**
     * Checks whether an item with the given data may be embedded into the given
     *  actor. This method may also manipulate the data to adjust the creation.
     * @param {object} data The data of the new item.
     * @param {TdeActor} actor The actor that the item should be created for.
     * @param {boolean} skipVariants Indicates whether the caller will handle variant selection.
     * @returns {Promise.<boolean>} True if the item can be created, false otherwise.
     */
    static async allowCreation(data, actor, skipVariants) {
        if (hasVariants(data) && !skipVariants) {
            // Make sure that we don't already have the maximum amount of purchaseable variants.
            const maxCount = data.system.purchase?.maxTimeBuyable;
            if (maxCount) {
                const count = actor.getItems(data.type, data.system.cid).length;
                if (count >= maxCount) {
                    util.error(`Can't add item ${data.name} because you have too many of its type.`);
                    return false;
                }
            }

            await promptVariant(data, actor);
        }

        if (!data.system.cid) return true;

        const existing = actor.uniqueItems.get(data.system.cid);
        const allow = existing ? await promptReplace(existing) : true;

        if (!allow) util.error(`Can't add item ${data.name} because it already exists.`);
        return allow;
    }

    /**
     * Creates an HTML string containing links to the given compendium items.
     * @param {string[]} itemIds An array of item IDs to link.
     * @returns {string} The links to the compendium items, separated by a comma.
     */
    static createLinkedItemList(itemIds) {
        if (!Array.isArray(itemIds) || itemIds.length === 0) return game.i18n.localize("tde5e.general.none");
        return itemIds.map(id => this.createCompendiumLink(id)).join(", ");
    }

    /**
     * Creates an HTML link to the compendium entry of the given item without downloading the entire document.
     * @param {string} id The fully qualified ID of the item to link, including the compendium and the indexed ID.
     * @param {boolean} includeIcon Flag indicating whether a compendium icon should be added to the link.
     * @returns {string} The HTML link to the compendium item.
     */
    static createCompendiumLink(id, includeIcon = false) {
        if (!id) return "";
        const ref = util.lookupCompendiumEntry(id);
        let icon = "";
        if (includeIcon) {
            if (!ref.pack) icon = "<i class='fas fa-atlas'></i>";
            else {
                icon = ref.pack.collection.startsWith("world.")
                    ? "<i class='fas fa-atlas'></i>"
                    : "<i class='fas fa-book'></i>";
            }
        }
        return `<a class="content-link${ref.entry ? "" : " broken"}" draggable="true" data-link`
            + ` data-pack="${ref.pack?.collection}" data-uuid="${ref.entry?.uuid ?? id}">`
            + `${icon} ${ref.entry?.name ?? id}</a>`;
    }

    /**
     * Adds listeners to the given HTML to roll or activate items.
     * @param {jQuery.Element} html The HTML element to add the listener on.
     */
    static addActivationListener(html) {
        html.on("click", "a.usable", async function use(event) {
            event.preventDefault();

            let item;
            if (this.name) {
                item = await fromUuid(this.name);
                if (!item) {
                    util.error(`Item ${this.name} does not exist.`);
                    return Promise.resolve();
                }
            }

            if (this.classList.contains("roll")) {
                // Roll formula.
                if (this.dataset.formula) {
                    const roll = new Roll(this.dataset.formula);
                    if (this.dataset.speaker) roll.toMessage({ speaker: { alias: this.dataset.speaker } });
                    else roll.toMessage();
                    return Promise.resolve();
                }

                // Roll item.
                if (!item?.isRollable) {
                    util.error(`Item ${this.name} can not be rolled.`);
                    return Promise.resolve();
                }
                return item.roll();
            }

            if (this.classList.contains("light")) {
                // Activate light source.
                if (!item?.isLightSource) {
                    util.error(`Item ${this.name} can not be activated.`);
                    return Promise.resolve();
                }
                return item.activate();
            }

            return Promise.resolve();
        });
    }

    /**
     * Resolves the correct runtime class for an item with the given type and ID.
     * @param {string} type The type of the item.
     * @param {string} cid The compendium ID of the item.
     * @param {string?} group The group of the item. Can be left empty to ignore the group.
     * @returns {object} The class associated with the data.
     */
    static resolveClass(type, cid, group) {
        const hasVariant = cid?.includes(":") ?? false;

        const itemClassMap = TDE5E.itemTypeClasses[group]
            ?? TDE5E.itemTypeClasses[type]
            ?? { default: CONFIG.Item.documentClass };

        return (hasVariant
            ? itemClassMap[cid] ?? itemClassMap[cid.split(":")[0]]
            : itemClassMap[cid])
            ?? itemClassMap.default;
    }
}
