import TdeItem from "./item.js";
import { info } from "../util.js";
import promptReplace from "../ui/replaceDialog.js";
import { promptVariant } from "../ui/variantPicker.js";
import { findIndexByPackId } from "../compendium.js";

class CreatureType extends TdeItem {
    /** @override */
    static async allowCreation(data, actor) {
        if (actor.type !== "creature") return false;
        const currentType = actor.itemTypes[data.type];
        if (currentType.length > 0 && !await promptReplace(currentType[0], data)) return false;
        if (data.system.variant?.available?.length && !await promptVariant(data, actor)) return false;
        await CreatureType.matchImmunities(data, actor);
        return true;
    }

    /**
     * Removes all items that the creatureType gives immunities to.
     * This ensures that a create cannot receive states or conditions it should not have.
     * @param {object} data The data that the item is being created with.
     * @param {TdeActor} actor The actor that the item is being created for.
     */
    static async matchImmunities(data, actor) {
        const conditionIdDelete = this.getImmunityIds(
            actor,
            "condition",
            data.system.conditionImmunities.all,
            data.system.conditionImmunities.values,
        );

        const stateIdDelete = this.getImmunityIds(
            actor,
            "state",
            data.system.stateImmunities.all,
            data.system.stateImmunities.values,
        );

        const deleteIds = [].concat(conditionIdDelete, stateIdDelete);

        if (deleteIds.length) {
            actor.deleteEmbeddedDocuments("Item", deleteIds);
            info(`Removed ${deleteIds.length} states and conditions due to immunities of creatureType ${this.name}.`);
        }
    }

    /**
     * Gets a list of ids that the respective immunity would need to delete.
     * @param {CreatureActor} actor The owning actor.
     * @param {string} type  the type, either condition or state.
     * @param {boolean} useAll True, if immunite is against all item of the type.
     * @param {string[]} items A list of ids that shoudl either be added or ignored depending on <i>useAll</i>.
     * @returns {string[]} A list of ids specific to the actor.
     */
    static getImmunityIds(actor, type, useAll, items) {
        const uuidToItemId = uuid => actor.getItems(type, findIndexByPackId(uuid)?.system.cid)[0]?.id;

        const baseIdObject = ((useAll) ? actor.itemTypes[type].map(item => item.id) : [])
            .reduce((sum, current) => { if (current !== null) sum[current] = 0; return sum; }, {});

        const itemIds = items
            .map(state => uuidToItemId(state))
            .filter(itemId => !!itemId);

        if (useAll) itemIds.forEach(id => delete baseIdObject[id]);
        else itemIds.forEach(id => { baseIdObject[id] = 0; });

        return Object.keys(baseIdObject);
    }
}

export default {
    default: CreatureType,
};
