import TdeItemModel from "./abstract/itemModel.js";

/**
 * Data model for items with the status type.
 */
export default class StateData extends TdeItemModel {
    /** @inheritdoc */
    static defineSchema() {
        const { fields } = foundry.data;

        const active = new fields.BooleanField();
        active.noMigration = true;

        return Object.assign(super.defineSchema(), {
            isDefault: new fields.BooleanField({ label: "tde5e.item.config.isDefault" }),
            active,
        });
    }

    /** @inheritdoc */
    static migrateData(source) {
        // Fix states broken by missing Babele mapping.
        if (source.hasOwnProperty("description") && typeof (source.description) === "object") {
            source.description = source.description.value;
        }

        return super.migrateData(source);
    }
}
