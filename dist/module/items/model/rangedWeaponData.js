import MeleeWeaponData from "./meleeWeaponData.js";

/**
 * Data model for items with the ranged weapon type.
 */
export default class RangedWeaponData extends MeleeWeaponData {
    /** @inheritdoc */
    static defineSchema() {
        const { fields } = foundry.data;

        const meleeWeaponSchema = super.defineSchema();
        meleeWeaponSchema.group.initial = "Compendium.tde5e.skills.Item.SC02156587103424";

        return Object.assign(meleeWeaponSchema, {
            range: new fields.ArrayField(
                new fields.NumberField({
                    integer: true,
                    positive: true,
                    initial: 10,
                    nullable: false,
                }),
                { label: "tde5e.item.equipment.range", initial: [10, 20, 50], validate: value => value.length === 3 },
            ),
            reloadTime: new fields.NumberField({
                label: "tde5e.item.equipment.reloadTime",
                integer: true,
                min: 0,
                initial: 1,
                nullable: false,
            }),
            ammunitionType: new fields.StringField({
                label: "tde5e.types.ammunition.name",
                choices: ["arrows", "balls", "bolts", "darts", "self", "stones"],
                initial: "arrows",
            }),
        });
    }
}
