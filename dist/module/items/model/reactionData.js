import ActionData from "./actionData.js";
import LocalizableStringField from "./partial/localizableStringField.js";
import StringSetField from "./partial/stringSetField.js";

/**
 * Data model for items with the reaction type.
 */
export default class ReactionData extends ActionData {
    /** @inheritdoc */
    static defineSchema() {
        const { fields } = foundry.data;
        return Object.assign(super.defineSchema(), {
            target: new fields.SchemaField({
                other: new fields.BooleanField({ label: "tde5e.item.maneuver.targetOther" }),
                category: new StringSetField(new LocalizableStringField("tde5e.item.maneuver.reactionTargets", {
                    choices: ["damage", "meleeAttack", "movement", "projectile", "status", "zone"],
                }), { label: "tde5e.item.maneuver.targetCategory" }),

            }, { label: "tde5e.item.maneuver.target" }),
        });
    }

    /** @inheritdoc */
    static migrateData(source) {
        if (source.hasOwnProperty("target") && Array.isArray(source.target)) {
            const target = { category: source.target };
            source.target = target;
        }

        if (source.hasOwnProperty("targetOther") && !source.target?.hasOwnProperty("other")) {
            source.target ??= {};
            source.target.other = source.targetOther;
            delete source.targetOther;
        }

        return super.migrateData(source);
    }
}
