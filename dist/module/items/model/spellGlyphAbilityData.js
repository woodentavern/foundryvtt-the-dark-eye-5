import SpecialAbilityModel from "./abstract/specialAbilityModel.js";
import CastableCostField from "./partial/castableCostField.js";
import LocalizableStringField from "./partial/localizableStringField.js";

/**
 * Data model for items with the spell glyph type.
 */
export default class SpellGlyphAbilityData extends SpecialAbilityModel {
    /** @inheritdoc */
    static defineSchema() {
        return Object.assign(super.defineSchema(), {
            cost: new CastableCostField(),
            property: new LocalizableStringField("tde5e.properties", { label: "tde5e.item.castable.property" }),
        });
    }

    /** @inheritdoc */
    static migrateData(source) {
        if (source.hasOwnProperty("circleMagic")) {
            delete source.circleMagic;
        }

        return super.migrateData(source);
    }
}
