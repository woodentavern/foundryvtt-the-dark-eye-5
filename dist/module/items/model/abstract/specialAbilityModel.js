import PurchaseField from "../partial/purchaseField.js";
import VariantModel, { migrateVariants } from "../partial/variantModel.js";
import TdeItemModel from "./itemModel.js";

/**
 * Data model for special ability items.
 */
export default class SpecialAbilityModel extends TdeItemModel {
    /** @inheritdoc */
    static defineSchema() {
        const { fields } = foundry.data;

        const purchase = new PurchaseField({
            maxTimeBuyable: new fields.NumberField({
                integer: true,
                positive: true,
            }),
            levels: new fields.NumberField({
                label: "tde5e.item.specialAbility.levels",
                integer: true,
                positive: true,
                initial: 1,
                nullable: false,
            }),
        });

        return Object.assign(super.defineSchema(), {
            group: new fields.StringField({ label: "tde5e.item.sheet.group" }),
            variant: new fields.EmbeddedDataField(VariantModel),
            purchase,
            requirementText: new fields.StringField({ label: "tde5e.item.sheet.requirementText" }),
        });
    }

    /** @inheritdoc */
    static migrateData(source) {
        migrateVariants(source);
        return super.migrateData(source);
    }

    /** @inheritdoc */
    get adventurePointValue() {
        return this.getImprovement("system.variant.level").adventurePointValue;
    }
}
