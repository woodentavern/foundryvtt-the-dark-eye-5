import PurchaseField from "../partial/purchaseField.js";
import TdeItemModel from "./itemModel.js";

/**
 * Data model for language items.
 */
export default class LanguageModel extends TdeItemModel {
    /** @inheritdoc */
    static defineSchema() {
        const { fields } = foundry.data;
        return Object.assign(super.defineSchema(), {
            isExtinct: new fields.BooleanField({ label: "tde5e.item.creatureType.isExtinct" }),
            continent: new fields.StringField({ label: "tde5e.item.language.continent" }),
            purchase: new PurchaseField(),
        });
    }
}
