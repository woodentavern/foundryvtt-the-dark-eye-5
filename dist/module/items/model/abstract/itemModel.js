import { ActorLogEntry } from "../../../actors/logEntry.js";
import { FieldImprovement } from "../partial/fieldImprovement.js";
import UuidArrayField from "../partial/uuidArrayField.js";

/**
 * Common data model for system items.
 */
export default class TdeItemModel extends foundry.abstract.DataModel {
    /** @inheritdoc */
    static defineSchema() {
        const { fields } = foundry.data;

        const cid = new fields.StringField({ readonly: true });
        cid.noMigration = true;

        const notes = new fields.StringField({ label: "tde5e.actor.bio.notes", required: true });
        notes.noMigration = true;
        notes.alwaysEditable = true;

        const gmNotes = new fields.StringField({ label: "tde5e.actor.bio.gmNotes", required: true });
        gmNotes.noMigration = true;
        gmNotes.alwaysEditable = true;
        gmNotes.hidden = true;
        gmNotes.skipChangelog = true;

        const changelog = new fields.ArrayField(new fields.EmbeddedDataField(ActorLogEntry));
        changelog.skipChangelog = true;
        changelog.noMigration = true;

        return {
            sources: new fields.SetField(new fields.StringField({ blank: false })),
            cid,
            notes,
            gmNotes,
            flavor: new fields.HTMLField({ label: "tde5e.item.sheet.flavor" }),
            rule: new fields.HTMLField({ label: "tde5e.item.sheet.rule" }),
            changelog,
            parents: new UuidArrayField("tde5e.item.sheet.related"),
        };
    }

    /** @inheritdoc */
    static migrateData(source) {
        if (source.hasOwnProperty("flavour") && !source.hasOwnProperty("flavor")) {
            source.flavor = source.flavour;
            delete source.flavour;
        }

        if (!source.hasOwnProperty("rule")) {
            if (source.hasOwnProperty("effect")) {
                source.rule = source.effect;
                delete source.effect;
            } else if (source.hasOwnProperty("description")) {
                source.rule = source.description;
                delete source.description;
            } else if (source.hasOwnProperty("rules")) {
                source.rule = source.rules;
                delete source.rules;
            }
        }

        return super.migrateData(source);
    }

    /**
     * Retrieves an instance for calculating improvement related information for the given property.
     * @param {string} path The path of the property to improve.
     * @returns {FieldImprovement?} An improvement instance for the property or null if it isn't improvable.
     */
    getImprovement(path) {
        if (!path.startsWith("system.")) return null;
        return FieldImprovement.create(this, path.substring(7));
    }

    /**
     * Gets the adventure point value of this item in its current state.
     * @returns {number} The amount of adventure points that this item is worth.
     */
    get adventurePointValue() {
        return this.purchase?.cost?.[0] ?? 0;
    }
}
