import UuidArrayField from "../partial/uuidArrayField.js";
import TdeItemModel from "./itemModel.js";

/**
 * Data model for storing common advantages and disadvantages.
 */
export default class CommonVantageModel extends TdeItemModel {
    /** @inheritdoc */
    static defineSchema() {
        return Object.assign(super.defineSchema(), {
            commonAdvantages: new UuidArrayField("tde5e.item.race.commonAdvantages"),
            commonDisadvantages: new UuidArrayField("tde5e.item.race.commonDisadvantages"),
            uncommonAdvantages: new UuidArrayField("tde5e.item.race.uncommonAdvantages"),
            uncommonDisadvantages: new UuidArrayField("tde5e.item.race.uncommonDisadvantages"),
        });
    }
}
