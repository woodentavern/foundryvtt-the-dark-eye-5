import TdeItemModel from "./itemModel.js";

/**
 * Data model for skills with a value and improvement column.
 */
export default class ImprovableSkillModel extends TdeItemModel {
    /** @inheritdoc */
    static defineSchema() {
        const { fields } = foundry.data;

        const value = new fields.NumberField({
            label: "tde5e.item.skill.skillValue",
            integer: true,
            min: 0,
            initial: 0,
            nullable: false,
        });
        value.noMigration = true;
        value.improvement = { type: "value", column: "improvement.column" };

        const column = new fields.StringField({
            label: "tde5e.item.skill.improvementCost",
            choices: ["A", "B", "C", "D", "E"],
            initial: "A",
        });
        column.improvement = { type: "column", value: "improvement.value" };

        return Object.assign(super.defineSchema(), {
            improvement: new fields.SchemaField({
                value,
                column,
            }),
        });
    }

    /** @inheritdoc */
    static migrateData(source) {
        if (source.hasOwnProperty("improvementCost") && !source.improvement?.hasOwnProperty("column")) {
            source.improvement ??= {};
            source.improvement.column = source.improvementCost;
            source.improvement.value = source.value;
            delete source.improvementCost;
            delete source.value;
        }

        return super.migrateData(source);
    }

    /**
     * The adventure point cost to activate this skill.
     * @returns {number} The amount of adventure points required for this skill.
     */
    get activationCost() {
        return TDE5E.improvementTable[this._source.improvement.column][0];
    }

    /** @inheritdoc */
    get adventurePointValue() {
        return this.activationCost + this.getImprovement("system.improvement.value").adventurePointValue;
    }
}
