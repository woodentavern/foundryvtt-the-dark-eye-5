import TdeItemModel from "./itemModel.js";

/**
 * Data model for inventory items.
 */
export default class InventoryModel extends TdeItemModel {
    /** @inheritdoc */
    static defineSchema() {
        const { fields } = foundry.data;

        const container = new fields.StringField({
            blank: false,
            initial: "DEFAULT",
            required: true,
        });
        container.noMigration = true;
        container.skipChangelog = true;

        const quantity = new fields.NumberField({
            label: "tde5e.inventory.quantity",
            min: 0,
            initial: 1,
            nullable: false,
        });
        quantity.noMigration = true;
        quantity.alwaysEditable = true;

        const price = new fields.NumberField({
            label: "tde5e.item.equipment.price",
            integer: true,
            min: 0,
            initial: 0,
            nullable: false,
        });
        price.noMigration = true;

        return Object.assign(super.defineSchema(), {
            container,
            quantity,
            weight: new fields.NumberField({
                label: "tde5e.inventory.weight",
                integer: true,
                min: 0,
                initial: 0,
                nullable: false,
            }),
            price,
            rarity: new fields.StringField({
                label: "tde5e.item.equipment.rarity",
                choices: ["common", "fate", "rare", "superRare", "ubiquitous", "uncommon", "unique"],
                initial: "common",
            }),
            traits: new fields.ArrayField(
                new fields.StringField({ blank: false }),
                { label: "tde5e.item.creatureType.traits", initial: ["profane"] },
            ),
            region: new fields.SchemaField({
                codes: new fields.ArrayField(new fields.StringField({ blank: false }), { initial: ["common"] }),
                note: new fields.StringField(),
            }, { label: "tde5e.region.name" }),
        });
    }
}
