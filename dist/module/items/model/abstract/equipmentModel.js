import VariantModel, { migrateVariants } from "../partial/variantModel.js";
import InventoryModel from "./inventoryModel.js";

/**
 * Data model for equippable items.
 */
export default class EquipmentModel extends InventoryModel {
    /** @inheritdoc */
    static defineSchema() {
        const { fields } = foundry.data;

        const prefix = new fields.StringField({
            label: "tde5e.item.equipment.prefix",
            nullable: true,
            choices: [
                "agile", "balanced", "blunt", "crooked", "flexible", "fragile", "heavy", "light", "long", "precise",
                "reinforced", "relentless", "rigid", "short", "sluggish", "solid", "thin", "uneven", "unstable",
                "unwieldy",
            ],
            initial: null,
        });
        prefix.noMigration = true;

        const wear = new fields.NumberField({
            label: "tde5e.item.equipment.wear",
            integer: true,
            min: 0,
            max: 4,
            initial: 0,
            nullable: false,
        });
        wear.alwaysEditable = true;

        return Object.assign(super.defineSchema(), {
            isUnbreakable: new fields.BooleanField({ label: "tde5e.item.equipment.isUnbreakable" }),
            adrenaline: new fields.NumberField({
                label: "tde5e.actor.resources.adrenaline",
                integer: true,
                min: 0,
                initial: 0,
                nullable: false,
            }),
            prefix,
            wear,
            breakingPointRating: new fields.NumberField({
                label: "tde5e.item.equipment.breakingPointRating",
                integer: true,
                positive: true,
                max: 20,
                initial: 10,
                nullable: false,
            }),
            variant: new fields.EmbeddedDataField(VariantModel),
        });
    }

    /** @inheritdoc */
    static migrateData(source) {
        if (source.hasOwnProperty("prefix") && source.prefix === "none") source.prefix = null;
        if (source.hasOwnProperty("breakingPointRating") && source.breakingPointRating <= 0) {
            source.breakingPointRating = 1;
        }

        // Rename damageRange to range.
        if (source.damage?.hasOwnProperty("damageRange") && !source.damage.hasOwnProperty("range")) {
            source.damage.range = source.damage.damageRange;
            delete source.damage.damageRange;
        }

        // Rename damageFlat to flat.
        if (source.damage?.hasOwnProperty("damageFlat") && !source.damage.hasOwnProperty("flat")) {
            source.damage.flat = source.damage.damageFlat;
            delete source.damage.damageFlat;
        }

        // Move up modifiers.
        if (source.hasOwnProperty("modifiers")) {
            for (const [mod, val] of Object.entries(source.modifiers)) {
                source[mod] ??= val;
            }
            delete source.modifiers;
        }

        migrateVariants(source);
        return super.migrateData(source);
    }
}
