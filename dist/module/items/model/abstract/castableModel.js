import CheckField from "../partial/checkField.js";
import CastableCostField from "../partial/castableCostField.js";
import { CastableEnhancementImprovement } from "../partial/fieldImprovement.js";
import ImprovableSkillModel from "./improvableSkillModel.js";
import BlacklistUuidField from "../partial/blacklistUuidField.js";

/**
 * Data model for castable items such as spells or chants.
 */
export default class CastableModel extends ImprovableSkillModel {
    /** @inheritdoc */
    static defineSchema() {
        const { fields } = foundry.data;

        const enhancementOwned = new fields.BooleanField({ required: false });
        enhancementOwned.improvement = { type: "enhancement" };

        const enhancements = new fields.ArrayField(
            new fields.SchemaField({
                name: new fields.StringField(),
                skillPoints: new fields.NumberField({
                    integer: true,
                    positive: true,
                    required: true,
                    nullable: false,
                    initial: 6,
                }),
                effect: new fields.HTMLField({ required: false, initial: "" }),
                owned: enhancementOwned,
            }),
            { label: "tde5e.item.castable.enhancement" },
        );
        enhancements.alwaysEditable = true;
        enhancements.improvement = { type: "array", fields: ["owned"] };

        return Object.assign(super.defineSchema(), {
            group: new fields.StringField({ label: "tde5e.item.sheet.group", initial: "incantation" }),
            check: new CheckField({
                resistedBySpirit: new fields.BooleanField({ label: "tde5e.item.castable.resistedBySpirit" }),
                resistedByToughness: new fields.BooleanField({ label: "tde5e.item.castable.resistedByToughness" }),
                resistanceIsHalfed: new fields.BooleanField({ label: "tde5e.item.castable.resistanceIsHalfed" }),
            }),
            cost: new CastableCostField({
                textLong: new fields.StringField({ label: "tde5e.item.castable.cost" }),
            }),
            modifications: new fields.SchemaField({
                canModCastingTime: new fields.BooleanField({ initial: true }),
                canModCost: new fields.BooleanField({ initial: true }),
                canModRange: new fields.BooleanField({ initial: true }),
            }, { label: "tde5e.item.castable.modifications" }),
            castingTime: new fields.StringField(),
            castingTimeLong: new fields.StringField({ label: "tde5e.item.castable.castingTime" }),
            range: new fields.StringField({ label: "tde5e.item.equipment.range" }),
            rangeLong: new fields.StringField({ label: "tde5e.item.equipment.range" }),
            duration: new fields.StringField({ label: "tde5e.item.castable.duration" }),
            durationLong: new fields.StringField({ label: "tde5e.item.castable.duration" }),
            targetCategory: new fields.StringField({ label: "tde5e.item.castable.targetCategory" }),
            requirementText: new fields.StringField({ label: "tde5e.item.sheet.requirementText" }),
            traditions: new BlacklistUuidField("tde5e.item.castable.traditions"),
            enhancements,
        });
    }

    /** @inheritdoc */
    static migrateData(source) {
        // Change enhancements from object to array.
        if (source.hasOwnProperty("enhancement") && !source.hasOwnProperty("enhancements")) {
            source.enhancement.level1.skillPoints = 6;
            source.enhancement.level2.skillPoints = 11;
            source.enhancement.level3.skillPoints = 14;
            const enhancements = [
                source.enhancement.level1,
                source.enhancement.level2,
                source.enhancement.level3,
            ];
            source.enhancements = enhancements.filter(e => e?.name);
            delete source.enhancement;
        }

        if (source.hasOwnProperty("traditions") && Array.isArray(source.traditions)) {
            source.traditions = { all: false, values: source.traditions };
        }

        if (source.hasOwnProperty("allTraditions") && !source.traditions?.hasOwnProperty("all")) {
            source.traditions ??= {};
            source.traditions.all = source.allTraditions;
        }

        if (source.modifications.hasOwnProperty("canModDuration")) {
            delete source.modifications.canModDuration;
        }

        return super.migrateData(source);
    }

    /** @inheritdoc */
    getImprovement(path) {
        return path.startsWith("system.enhancements[")
            ? CastableEnhancementImprovement.create(this, path.substring(7))
            : super.getImprovement(path);
    }

    /** @inheritdoc */
    get adventurePointValue() {
        return super.adventurePointValue + this.getImprovement("system.enhancements").adventurePointValue;
    }
}
