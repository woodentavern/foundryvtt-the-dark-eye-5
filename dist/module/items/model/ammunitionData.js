import InventoryModel from "./abstract/inventoryModel.js";

/**
 * Data model for items with the ammunition type.
 */
export default class AmmunitionData extends InventoryModel {
    /** @inheritdoc */
    static defineSchema() {
        const { fields } = foundry.data;

        const isSpecial = new fields.BooleanField();
        isSpecial.noMigration = true;

        return Object.assign(super.defineSchema(), {
            group: new fields.StringField({
                label: "tde5e.item.sheet.group",
                choices: ["arrows", "balls", "bolts", "darts", "self", "stones"],
                initial: "arrows",
            }),
            isSpecial,
        });
    }
}
