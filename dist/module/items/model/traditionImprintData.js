import TdeItemModel from "./abstract/itemModel.js";
import LocalizableStringField from "./partial/localizableStringField.js";

/**
 * Data model for items with the tradition imprint type.
 */
export default class TraditionImprintData extends TdeItemModel {
    /** @inheritdoc */
    static defineSchema() {
        const { fields } = foundry.data;
        return Object.assign(super.defineSchema(), {
            group: new LocalizableStringField("tde5e.types.traditionImprint", {
                label: "tde5e.combat.group",
                choices: ["magical", "blessed"],
                initial: "magical",
            }),
            requirementText: new fields.StringField({ label: "tde5e.item.sheet.requirementText" }),
        });
    }

    /** @inheritdoc */
    static migrateData(source) {
        if (source.hasOwnProperty("group") && source.group.endsWith("TraditionImprint")) {
            source.group = source.group.substring(0, 7);
        }

        return super.migrateData(source);
    }
}
