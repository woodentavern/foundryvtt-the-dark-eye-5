import TdeItemModel from "./abstract/itemModel.js";
import LocalizableStringField from "./partial/localizableStringField.js";
import PurchaseField from "./partial/purchaseField.js";
import StringSetField from "./partial/stringSetField.js";
import UuidArrayField from "./partial/uuidArrayField.js";

/**
 * Data model for items with the tradition type.
 */
export default class TraditionData extends TdeItemModel {
    /** @inheritdoc */
    static defineSchema() {
        const { fields } = foundry.data;
        return Object.assign(super.defineSchema(), {
            shortName: new fields.StringField({ label: "tde5e.item.tradition.shortName" }),
            group: new fields.StringField({
                label: "tde5e.item.sheet.group",
                choices: ["magical", "blessed"],
                initial: "magical",
            }),
            purchase: new PurchaseField(),
            primaryAttribute: new fields.StringField({
                label: "tde5e.item.tradition.primaryAttribute",
                blank: true,
                choices: [
                    "agility", "charisma", "constitution", "courage", "dexterity", "intuition", "sagacity", "strength",
                ],
            }),
            learnableTypes: new fields.SchemaField({
                cantrips: new fields.BooleanField(),
                spells: new fields.BooleanField(),
                rituals: new fields.BooleanField(),
            }, { label: "tde5e.item.tradition.learnableTypes" }),
            allowMultipleTraditions: new fields.BooleanField({ label: "tde5e.item.tradition.allowMultipleTraditions" }),
            favoredSkills: new UuidArrayField("tde5e.item.tradition.favoredSkills"),
            aspects: new StringSetField(new LocalizableStringField("tde5e.aspects", {
                choices: [
                    "agriculture", "antiMagic", "change", "cold", "commerce", "courage", "craft", "death", "dream",
                    "ecstasy", "education", "fate", "fire", "freedom", "friendship", "general", "goodGold", "goodfight",
                    "harmony", "healing", "helpfulness", "home", "insight", "knowledge", "loyalty", "lust", "maelstrom",
                    "magic", "nature", "order", "rush", "shadow", "shield", "storm", "strength", "theHunt", "travel",
                    "unendingDeep", "wave", "wind",
                ],
            }), { label: "tde5e.item.tradition.aspects" }),
            requirementText: new fields.StringField({ label: "tde5e.item.sheet.requirementText" }),
        });
    }

    /** @inheritdoc */
    static migrateData(source) {
        if (source.hasOwnProperty("canLearnCantrips") && !source.learnableTypes?.hasOwnProperty("cantrips")) {
            source.learnableTypes ??= {};
            source.learnableTypes.cantrips = source.canLearnCantrips;
            delete source.canLearnCantrips;
        }

        if (source.hasOwnProperty("canLearnSpells") && !source.learnableTypes?.hasOwnProperty("spells")) {
            source.learnableTypes ??= {};
            source.learnableTypes.spells = source.canLearnSpells;
            delete source.canLearnSpells;
        }

        if (source.hasOwnProperty("canLearnRituals") && !source.learnableTypes?.hasOwnProperty("rituals")) {
            source.learnableTypes ??= {};
            source.learnableTypes.rituals = source.canLearnRituals;
            delete source.canLearnRituals;
        }

        return super.migrateData(source);
    }
}
