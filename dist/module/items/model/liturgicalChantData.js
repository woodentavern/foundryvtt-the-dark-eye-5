import CastableModel from "./abstract/castableModel.js";
import LocalizableStringField from "./partial/localizableStringField.js";
import StringSetField from "./partial/stringSetField.js";

/**
 * Data model for items with the chant type.
 */
export default class LiturgicalChantData extends CastableModel {
    /** @inheritdoc */
    static defineSchema() {
        const castableModel = super.defineSchema();
        castableModel.group.initial = "liturgicalChant";
        castableModel.improvement.fields.value.label = "tde5e.item.castable.liturgyValue";

        return Object.assign(castableModel, {
            aspects: new StringSetField(
                new LocalizableStringField("tde5e.aspects"),
                { label: "tde5e.item.tradition.aspects" },
            ),
        });
    }
}
