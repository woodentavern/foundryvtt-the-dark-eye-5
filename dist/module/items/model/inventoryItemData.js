import InventoryModel from "./abstract/inventoryModel.js";

/**
 * Data model for items with the inventory type.
 */
export default class InventoryItemData extends InventoryModel {
    /** @inheritdoc */
    static defineSchema() {
        const { fields } = foundry.data;
        return Object.assign(super.defineSchema(), {
            light: new fields.SchemaField({
                level: new fields.NumberField({
                    integer: true,
                    min: 0,
                    max: 4,
                    initial: 0,
                    nullable: false,
                }),
                falloff: new fields.NumberField({ min: 0, initial: 0, nullable: false }),
            }, { label: "tde5e.item.light.level" }),
        });
    }

    /** @inheritdoc */
    static migrateData(source) {
        // Ensure that only one rule property exists.
        if (source.effect && !source.description) {
            delete source.description;
        } else if (source.description && !source.effect) {
            delete source.effect;
        }

        return super.migrateData(source);
    }
}
