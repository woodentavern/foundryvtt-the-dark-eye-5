import CastableModel from "./abstract/castableModel.js";
import LocalizableStringField from "./partial/localizableStringField.js";
import UuidArrayField from "./partial/uuidArrayField.js";

/**
 * Data model for items with the spell type.
 */
export default class SpellData extends CastableModel {
    /** @inheritdoc */
    static defineSchema() {
        const { fields } = foundry.data;

        const castableModel = super.defineSchema();
        castableModel.improvement.fields.value.label = "tde5e.item.castable.spellValue";

        return Object.assign(castableModel, {
            checkedSkill: new UuidArrayField("tde5e.item.castable.checkedSkill", { nullable: true, initial: null }),
            property: new LocalizableStringField("tde5e.properties", { label: "tde5e.item.castable.property" }),
            subTraditions: new fields.StringField({ label: "tde5e.item.castable.subTraditions" }),
        });
    }
}
