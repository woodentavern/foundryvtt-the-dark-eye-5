import ImprovableSkillModel from "./abstract/improvableSkillModel.js";
import CheckField from "./partial/checkField.js";

/**
 * Data model for items with the combat technique type.
 */
export default class CombatTechniqueData extends ImprovableSkillModel {
    /** @inheritdoc */
    static defineSchema() {
        const { fields } = foundry.data;

        const isDefault = new fields.BooleanField({ label: "tde5e.item.config.isDefault" });
        isDefault.noMigration = true;
        isDefault.hidden = true;

        return Object.assign(super.defineSchema(), {
            isDefault,
            check: new CheckField(),
            applications: new fields.StringField({ label: "tde5e.item.skill.applications" }),
        });
    }
}
