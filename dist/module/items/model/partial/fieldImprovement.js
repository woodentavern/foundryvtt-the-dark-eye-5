import { extractIndex } from "../../../formDataParsing.js";
import { findParentActor } from "../../../util.js";

/**
 * Data field extension for managing improvement related information.
 */
export class FieldImprovement {
    /**
     * The model to read current values from.
     * @type {DataModel}
     */
    model;

    /**
     * The path of the improved property.
     * @type {string}
     */
    path;

    /**
     * The column used by this improvement.
     * @type {string}
     */
    column;

    /**
     * Creates an improvement instance for a specific property.
     * @param {DataModel} model The data model containing the property.
     * @param {string} path The path of the property to improve.
     * @param {string} column The improvement column.
     */
    constructor(model, path, column) {
        this.model = model;
        this.path = path;
        this.column = column;
    }

    /**
     * Resolves the value of the field from the data model.
     * @returns {number} The value of the field.
     */
    get currentValue() {
        return foundry.utils.getProperty(this.model._source, this.path);
    }

    /**
     * Calculates the maximum value that the field can be improved to.
     * @returns {number} The maximum value of the field.
     */
    get maximumValue() {
        return this.levelCosts.length;
    }

    /**
     * Determines the default value that the parent field should be improved to if no explicit value was given.
     * @returns {number} The default target value for the improvement.
     */
    get defaultValue() {
        return this.currentValue + 1;
    }

    /**
     * Resolves the actual cost column for improving the field.
     * @returns {number[]} An array containing the improvement costs per level.
     */
    get levelCosts() {
        return TDE5E.improvementTable[this.column].slice(1);
    }

    /**
     * Calculates the overall value of the parent field.
     * @returns {number} The adventure point value of the field.
     */
    get adventurePointValue() {
        return this.levelCosts
            .slice(0, this.currentValue)
            .reduce((sum, level) => sum + level, 0);
    }

    /**
     * Indicates whether the field's value meets all requirements for an improvement.
     * @param {number=} targetValue The target value of the improvement. Defaults to the current value + 1.
     * @returns {boolean|string} True if the field meets improvement requirements, a localized string that explains
     *  which requirement hasn't been met, false if the field is not improvable.
     */
    // eslint-disable-next-line no-unused-vars -- Keep as documentation.
    canImprove(targetValue) {
        return true;
    }

    /**
     * Calculates the adventure point cost to improve the parent field from its current to the given target value.
     * @param {number=} targetValue An optional value to improve the field to. Defaults to the current value + 1.
     * @returns {number} The cost to improve the field from its current to the target value.
     */
    calculateImprovementCost(targetValue = this.defaultValue) {
        let { currentValue } = this;
        if (currentValue === targetValue) return 0;

        const reverse = targetValue < currentValue;
        if (reverse) {
            const tmp = currentValue;
            currentValue = targetValue;
            targetValue = tmp;
        }

        const cost = this.levelCosts
            .slice(currentValue, targetValue)
            .reduce((sum, level) => sum + level, 0);
        return reverse ? -cost : cost;
    }

    /**
     * Applies an improvement to the given value by updating the parent document.
     * @param {number=} targetValue An optional value to improve the field to. Defaults to the current value + 1.
     * @returns {Promise} A promise representing the document update.
     */
    apply(targetValue = this.defaultValue) {
        return this.model.parent.update({ [`system.${this.path}`]: targetValue });
    }

    /**
     * Creates a new @see FieldImprovement from the schema field data.
     * @param {DataModel} model The model to create the improvement for.
     * @param {string} path The path to the field within the model's schema.
     * @returns {FieldImprovement?} An instance for improving the field or null if it isn't improvable.
     */
    static create(model, path) {
        const options = model.schema.getField(path)?.improvement;
        if (!options) return null;

        switch (options.type) {
        case "value": return new CheckValueImprovement(model, path, options.column);
        case "column": return new ColumnImprovement(model, path, options.value);
        case "level": return new LevelImprovement(model, path, options.column);
        case "cost": return new CostImprovement(model, path, options.value);
        case "attribute": return new AttributeImprovement(model, path, options.column);
        case "array": return new ArrayImprovementDelegate(model, path, options.fields);
        case "enhancement": return CastableEnhancementImprovement.create(model, path);
        default: return new FieldImprovement(model, path, options.column);
        }
    }
}

/**
 * Improvement extension for fields that represent the value of a skill check.
 */
export class CheckValueImprovement extends FieldImprovement {
    /** @inheritdoc */
    get maximumValue() {
        const actor = findParentActor(this.model);
        if (!actor) return super.maximumValue;

        return this.model.check?.attributes
            .map(attr => actor._source.system.attributes[attr].base)
            .sort()[1] ?? 0;
    }

    /**
     * Resolves the cost column from another field within the model.
     * @inheritdoc
     */
    get levelCosts() {
        const refColumn = foundry.utils.getProperty(this.model._source, this.column);
        return TDE5E.improvementTable[refColumn].slice(1);
    }

    /** @inheritdoc */
    canImprove(targetValue = this.defaultValue) {
        const actor = findParentActor(this.model);
        if (!actor) return true;

        const medianAttribute = this.model.check?.attributes
            .map(attr => actor._source.system.attributes[attr].base)
            .sort()[1] ?? 0;

        if (targetValue <= medianAttribute) return true;
        return game.i18n.format("tde5e.actor.changelog.prerequisite.attr", { value: targetValue });
    }
}

/**
 * Improvement extension for changing a column referenced by a different field.
 */
export class ColumnImprovement extends FieldImprovement {
    /** @inheritdoc */
    get defaultValue() {
        return String.fromCharCode(this.currentValue.charCodeAt(0)) + 1;
    }

    /** @inheritdoc */
    get levelCosts() {
        return TDE5E.improvementTable[this.currentValue];
    }

    /**
     * Disables adventure point calculations for the column itself.
     * @returns {number} 0.
     */
    get adventurePointValue() {
        return 0;
    }

    /**
     * Disables improvements for the column itself.
     * @returns {boolean} False.
     */
    canImprove() {
        return false;
    }

    /**
     * Calculates the adventure point cost to improve the parent field from its current to the given target value.
     * @param {number=} targetValue An optional value to improve the field to. Defaults to the current value + 1.
     * @returns {number} The cost to improve the field from its current to the target value.
     */
    calculateImprovementCost(targetValue = this.defaultValue) {
        if (this.currentValue === targetValue) return 0;

        let value = foundry.utils.getProperty(this.model._source, this.column);
        if (!value) return 0;

        value++;
        const apValue = this.levelCosts.slice(0, value).reduce((sum, level) => sum + level, 0);
        const newColumn = TDE5E.improvementTable[targetValue];
        const newApValue = newColumn.slice(0, value).reduce((sum, level) => sum + level, 0);
        return newApValue - apValue;
    }
}

/**
 * Improvement extension for fields that represent the level of a purchased ability.
 */
export class LevelImprovement extends FieldImprovement {
    /** @inheritdoc */
    get maximumValue() {
        return this.model._source.purchase?.levels ?? 1;
    }

    /**
     * Resolves the cost column directly from another field within the model.
     * @inheritdoc
     */
    get levelCosts() {
        return foundry.utils.getProperty(this.model._source, this.column);
    }

    /** @inheritdoc */
    canImprove(targetValue = this.defaultValue) {
        return targetValue <= this.maximumValue;
    }
}

/**
 * Improvement extension for changing a cost referenced by a different field.
 */
export class CostImprovement extends FieldImprovement {
    /** @inheritdoc */
    get defaultValue() {
        return this.currentValue;
    }

    /** @inheritdoc */
    get levelCosts() {
        return this.currentValue;
    }

    /**
     * Disables adventure point calculations for the cost itself.
     * @returns {number} 0.
     */
    get adventurePointValue() {
        return 0;
    }

    /**
     * Disables improvements for the cost itself.
     * @returns {boolean} False.
     */
    canImprove() {
        return false;
    }

    /**
     * Calculates the adventure point cost to improve the parent field from its current to the given target value.
     * @param {number=} targetValue An optional value to improve the field to. Defaults to the current value + 1.
     * @returns {number} The cost to improve the field from its current to the target value.
     */
    calculateImprovementCost(targetValue = this.defaultValue) {
        const { currentValue } = this;
        if (currentValue.length === targetValue.length
            && currentValue.every((value, index) => value === targetValue[index])) {
            return 0;
        }

        const value = foundry.utils.getProperty(this.model._source, this.column) ?? 1;
        if (!value) return 0;

        const apValue = this.levelCosts.slice(0, value).reduce((sum, level) => sum + level, 0);
        const newApValue = targetValue.slice(0, value).reduce((sum, level) => sum + level, 0);
        return newApValue - apValue;
    }
}

/**
 * Improvement extension for fields that represent an actor's attribute.
 */
export class AttributeImprovement extends FieldImprovement {
    /** @inheritdoc */
    canImprove(targetValue = this.defaultValue) {
        const actor = findParentActor(this.model);
        if (actor?.type !== "player") return true;
        return targetValue > 18 ? game.i18n.localize("tde5e.actor.changelog.prerequisite.maxAttr") : true;
    }
}

/**
 * Improvement extension for delegating cost calculation to one or multiple improvement implementations within the
 *  items of the array.
 */
export class ArrayImprovementDelegate extends FieldImprovement {
    /**
     * The improvable fields contained in the array.
     * @type {CastableEnhancementImprovement[]}
     */
    improvements;

    /**
     * Creates a delegate for calculating improvement costs on arrays with improvable children.
     * @param {DataModel} model The data model containing the array.
     * @param {string} path The path of the array within the model.
     * @param {string[]} fields A list of improvable fields within the array.
     */
    constructor(model, path, fields) {
        super(model, path, null);
        this.improvements = fields.map(field => new CastableEnhancementImprovement(model, path, 0, field));
    }

    /** @inheritdoc */
    get defaultValue() {
        return this.currentValue.map(
            (_, index) => this.improvements.reduce((defaults, improvement) => {
                improvement.index = index;
                foundry.utils.setProperty(defaults, improvement.localPath, improvement.defaultValue);
                return defaults;
            }, {}),
        );
    }

    /** @inheritdoc */
    get adventurePointValue() {
        return this.currentValue.reduce((apValue, _, index) => apValue + this._calculateApValue(index), 0);
    }

    /**
     * Calculates the adventure point value for a single array entry identified by the given index.
     * @param {number} index The entry index to calculate the value for.
     * @returns {number} The adventure point value of the entry.
     */
    _calculateApValue(index) {
        return this.improvements.reduce((apValue, improvement) => {
            improvement.index = index;
            return apValue + improvement.adventurePointValue;
        }, 0);
    }

    /** @inheritdoc */
    canImprove() {
        const { currentValue } = this;
        for (let i = 0; i < currentValue.length; i++) {
            for (const improvement of this.improvements) {
                improvement.index = i;
                const improvable = improvement.canImprove();
                if (improvable === false || typeof (improvable) === "string") return improvable;
            }
        }

        return true;
    }

    /** @inheritdoc */
    calculateImprovementCost(targetValue = this.defaultValue) {
        const { currentValue } = this;
        let improvementCost = targetValue.reduce(
            (cost, entry, index) => cost + this.improvements.reduce((apCost, improvement) => {
                improvement.index = index;
                const target = foundry.utils.getProperty(entry, improvement.localPath);
                return apCost + improvement.calculateImprovementCost(target);
            }, 0),
            0,
        );

        if (targetValue.length < currentValue.length) {
            // If entries were removed, subtract their adventure point value.
            improvementCost -= currentValue
                .slice(targetValue.length)
                .reduce((cost, _, index) => cost + this._calculateApValue(index), 0);
        }

        return improvementCost;
    }
}

/**
 * Improvement extension for fields that represent ownership of an enhancement.
 */
export class CastableEnhancementImprovement extends FieldImprovement {
    /**
     * The index of the improved item within its array.
     * @type {number}
     */
    index;

    /**
     * The path of the improved property within its array item.
     * @type {string}
     */
    localPath;

    /**
     * Creates an improvement instance for an array item.
     * @param {DataModel} model The data model containing the property.
     * @param {string} path The path of the property to improve.
     * @param {number} index The index of the array item containing the property.
     * @param {string} localPath The path of the property within its array item.
     */
    constructor(model, path, index, localPath) {
        super(model, path, null);
        this.index = index;
        this.localPath = localPath;
    }

    /** @inheritdoc */
    get currentValue() {
        const item = super.currentValue[this.index];
        return (this.localPath && item) ? foundry.utils.getProperty(item, this.localPath) : item;
    }

    /** @inheritdoc */
    get defaultValue() {
        return true;
    }

    /** @inheritdoc */
    get adventurePointValue() {
        return this.currentValue ? this.model.activationCost * (this.index + 1) : 0;
    }

    /** @inheritdoc */
    canImprove() {
        if (this.currentValue) return false;

        const enhancement = this.model._source.enhancements[this.index];
        return enhancement.skillPoints > this.model._source.improvement.value
            ? game.i18n.format("tde5e.actor.changelog.prerequisite.sr", { value: enhancement.skillPoints })
            : true;
    }

    /** @inheritdoc */
    calculateImprovementCost(targetValue = this.defaultValue) {
        const currentValue = this.currentValue ?? false;
        if (currentValue === targetValue) return 0;

        const enhancementCost = this.model.activationCost * (this.index + 1);
        return currentValue && !targetValue ? -enhancementCost : enhancementCost;
    }

    /** @inheritdoc */
    apply(targetValue = this.defaultValue) {
        const array = foundry.utils.deepClone(super.currentValue);
        if (this.localPath) {
            const item = array[this.index];
            foundry.utils.setProperty(item, this.localPath, targetValue);
        } else {
            array[this.index] = targetValue;
        }

        return super.apply(array);
    }

    /**
     * Creates a new @see CastableEnhancementImprovement by extracting array syntax from the given path.
     * @param {DataModel} model The model to create the improvement for.
     * @param {string} path The path to field within the model's schema.
     * @returns {CastableEnhancementImprovement?} An improvement instance for the array field or null.
     */
    static create(model, path) {
        const indexInfo = extractIndex(path);
        if (!indexInfo) return null;
        const [prefix, index, suffix] = indexInfo;

        let element = model.schema.getField(prefix)?.element;
        if (suffix) element = element?.getField(suffix);
        const improvement = element?.improvement;
        return improvement ? new CastableEnhancementImprovement(model, prefix, index, suffix) : null;
    }
}
