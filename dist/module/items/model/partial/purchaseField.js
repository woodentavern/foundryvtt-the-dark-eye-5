/**
 * Data field representing the purchase information for an item.
 */
export default class PurchaseField extends foundry.data.fields.SchemaField {
    /**
     * Creates a new instance of a purchase data field.
     * @param {object?} additionalFields Optional fields to merge with the predefined ones.
     */
    constructor(additionalFields = {}) {
        const { fields } = foundry.data;

        additionalFields.cost = new fields.ArrayField(
            new fields.NumberField({ integer: true, initial: 0, nullable: false }),
            { initial: [0] },
        );
        additionalFields.cost.improvement = { type: "cost", value: "variant.level" };
        additionalFields.description = new fields.StringField();

        super(additionalFields, { label: "tde5e.item.sheet.purchase", readonly: true });
    }

    /** @inheritdoc */
    initialize(value) {
        const purchase = super.initialize(value);
        if (purchase && !purchase.description) {
            purchase.description = `${purchase.cost[0]} ${game.i18n.localize("tde5e.actor.adventurePoints.name")}`;
        }
        return purchase;
    }
}
