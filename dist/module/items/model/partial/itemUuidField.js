/**
 * Data field representing a reference to another item.
 */
export default class ItemUuidField extends foundry.data.fields.DocumentUUIDField {
    /** @inheritdoc */
    initialize(value) {
        if (!value) return null;
        return () => fromUuidSync(value, { strict: false });
    }

    /**
     * Converts the given value into a human readable string.
     * @param {object} value The value to convert.
     * @returns {string} A human readable string containing the item's name.
     */
    valueToString(value) {
        if (!value) return "";
        if (typeof (value) === "object") return value.name;
        return fromUuidSync(value, { strict: false })?.name ?? value;
    }
}
