import LocalizableStringField from "./localizableStringField.js";
import StringArrayField from "./stringArrayField.js";

/**
 * Data field representing a rollable attribute check.
 */
export default class CheckField extends foundry.data.fields.SchemaField {
    /**
     * Creates an instance of a skill check field.
     * @param {object?} additionalFields Optional fields to merge with the predefined ones.
     */
    constructor(additionalFields = {}) {
        additionalFields.attributes = new StringArrayField(
            new LocalizableStringField("tde5e.actor.attributesShort", {
                blank: false,
                choices: [
                    "courage",
                    "sagacity",
                    "charisma",
                    "intuition",
                    "dexterity",
                    "agility",
                    "strength",
                    "constitution",
                ],
            }),
            {
                label: "tde5e.actor.attributes.name",
                initial: ["courage", "courage", "courage"],
                validate: value => value.length === 3,
                separator: "/",
            },
        );

        super(additionalFields, { label: "tde5e.item.skill.check" });
    }

    /** @inheritdoc */
    validate(value, options = {}) {
        return super.validate(value, options) && value.attributes.length === 3;
    }
}
