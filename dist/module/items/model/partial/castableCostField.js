/**
 * Data field representing the cost of a castable item.
 */
export default class CastableCostField extends foundry.data.fields.SchemaField {
    /**
     * Creates an instance of a casting cost field.
     * @param {object?} additionalFields Optional fields to merge with the predefined ones.
     */
    constructor(additionalFields = {}) {
        const { fields } = foundry.data;

        Object.assign(additionalFields, {
            text: new fields.StringField({ label: "tde5e.item.castable.costShort" }),
            constant: new fields.NumberField({
                integer: true,
                min: 0,
                initial: 8,
                nullable: false,
            }),
            recurring: new fields.SchemaField({
                interval: new fields.NumberField({
                    integer: true,
                    min: 0,
                    initial: 0,
                    nullable: false,
                }),
                unit: new fields.StringField({ blank: false }),
            }, { nullable: true, initial: null }),
        });

        super(additionalFields, { label: "tde5e.item.castable.cost" });
    }
}
