/**
 * Data model for storing variant related data.
 */
export default class VariantModel extends foundry.abstract.DataModel {
    /** @inheritdoc */
    static defineSchema() {
        const { fields } = foundry.data;

        const level = new fields.NumberField({
            label: "tde5e.variantPicker.level",
            integer: true,
            positive: true,
            initial: 1,
            nullable: false,
        });
        level.noMigration = true;
        level.improvement = { type: "level", column: "purchase.cost" };

        const selected = new fields.ObjectField({ nullable: true, initial: null, required: false });
        selected.noMigration = true;

        const available = new fields.ArrayField(
            new fields.ObjectField(),
            {
                label: "tde5e.values.values",
                required: false,
                nullable: true,
                initial: null,
            },
        );
        available.noMigration = true;

        const input = new fields.BooleanField({
            label: "tde5e.item.specialAbility.variantInput",
            nullable: true,
            required: false,
        });
        input.noMigration = true;

        const categories = new fields.ArrayField(new fields.SchemaField({
            scope: new fields.StringField({ choices: ["item", "translation"] }),
            type: new fields.StringField(),
            groups: new fields.ArrayField(new fields.StringField()),
        }), { required: false, nullable: true, initial: null });
        categories.noMigration = true;

        return {
            level, selected, available, input, categories,
        };
    }
}

/**
 * Migrates variant related data. This is defined outside of the model because it requires the parent's data.
 * @param {object} source The source data to migrate.
 */
export function migrateVariants(source) {
    if (source.variant?.hasOwnProperty("vid")) {
        source.variant = { selected: source.variant };
    }

    if (source.hasOwnProperty("level") && !source.variant?.hasOwnProperty("level")) {
        source.variant ??= {};
        source.variant.level = source.level;
        delete source.level;
    }

    if (source.hasOwnProperty("variantCategories") && !source.variant?.hasOwnProperty("categories")) {
        source.variant ??= {};
        source.variant.categories = source.variantCategories;
        delete source.variantCategories;
    }

    if (source.hasOwnProperty("variantInput") && !source.variant?.hasOwnProperty("input")) {
        source.variant ??= {};
        source.variant.input = source.variantInput;
        delete source.variantInput;
    }

    if (source.hasOwnProperty("variants") && !source.variant?.hasOwnProperty("available")) {
        source.variant ??= {};
        source.variant.available = source.variants;
        delete source.variants;
    }
}
