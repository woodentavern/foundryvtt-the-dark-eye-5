import UuidArrayField from "./uuidArrayField.js";

/**
 * Data field representing a UUID black- or whitelist.
 */
export default class BlacklistUuidField extends foundry.data.fields.SchemaField {
    /**
     * Creates an instance of a black-/whitelist field.
     * @param {string} label The localization label of the field.
     */
    constructor(label = "") {
        const { fields } = foundry.data;
        super({
            all: new fields.BooleanField(),
            values: new UuidArrayField(),
        }, { label });
    }

    /** @inheritdoc */
    initialize(value) {
        // TODO This hack should be moved to a proper data model.
        const obj = super.initialize(value);
        obj.hasUuid = hasUuid;
        obj.hasCid = hasCid;
        return obj;
    }

    /**
     * Converts the given value into a human readable string.
     * @param {object} value The value to convert.
     * @returns {string} A human readable string containing the concatenated string values.
     */
    valueToString(value) {
        if (!value) return "";
        if (typeof (value) !== "object") return "";
        if (!value.all && value.values.length === 0) return "";
        if (value.all && value.values.length === 0) return game.i18n.localize("tde5e.general.all");
        const valueNames = value.values
            .map(uuid => fromUuidSync(uuid, { strict: false })?.name ?? uuid)
            .join(", ");
        return value.all ? game.i18n.format("tde5e.general.allExcept", { values: valueNames }) : valueNames;
    }
}

/**
 * Checks if the given UUID is excluded by this black- or whitelist.
 * @param {string} uuid The UUID to check.
 * @returns {boolean} True if the UUID is included, false otherwise.
 */
function hasUuid(uuid) {
    const exists = this.values.has(uuid);
    return this.all ? !exists : exists;
}

/**
 * Checks if any document with the given CID is excluded by this black- or whitelist.
 * @param {string} cid The CID to check.
 * @returns {boolean} True if any document with the CID is included, false otherwise.
 */
function hasCid(cid) {
    const exists = this.values.some(uuid => fromUuidSync(uuid, { strict: false })?.system.cid === cid);
    return this.all ? !exists : exists;
}
