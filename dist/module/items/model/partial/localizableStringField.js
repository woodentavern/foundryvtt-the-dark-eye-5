/**
 * Data field representing a localizable string.
 */
export default class LocalizableStringField extends foundry.data.fields.StringField {
    /**
     * The path to prepend to the value when localizing it.
     * @type {string}
     */
    translationPath;

    /**
     * Creates a new instance of a localizable text field.
     * @param {string} translationPath The prefix for the localization value.
     * @param {DataFieldOptions?} options Optional settings for the field's behavior.
     */
    constructor(translationPath, options = {}) {
        super(options);
        this.translationPath = translationPath;
    }

    /**
     * Converts the given value into a human readable string.
     * @param {object} value The value to convert.
     * @returns {string} A human readable string containing the localized value.
     */
    valueToString(value) {
        return value ? game.i18n.localize(`${this.translationPath}.${value}`) : "";
    }
}
