/**
 * Data field representing a set of joinable strings.
 */
export default class StringSetField extends foundry.data.fields.SetField {
    /**
     * Creates a new instance of a string concatenation field.
     * @param {DataField} element The data field stored within the array.
     * @param {DataFieldOptions} options Optional settings for the field's behavior.
     */
    constructor(element, options = {}) {
        super(element, options);
    }

    /**
     * Converts the given value into a human readable string.
     * @param {object} value The value to convert.
     * @returns {string} A human readable string containing the concatenated string values.
     */
    valueToString(value) {
        if (!value) return "";
        if (!Array.isArray(value)) value = [value];
        return value.map(v => this.element.valueToString(v)).join(", ");
    }
}
