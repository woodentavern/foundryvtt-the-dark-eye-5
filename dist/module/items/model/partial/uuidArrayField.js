import StringSetField from "./stringSetField.js";

/**
 * Data field representing an array of @see ItemUuidField instances.
 */
export default class UuidArrayField extends StringSetField {
    /**
     * Creates a new instance of a UUID reference list.
     * @param {string?} label Optional label for the array.
     * @param {object?} options Other options of the array.
     */
    constructor(label = "", options = {}) {
        options.label = label;
        super(new foundry.data.fields.DocumentUUIDField(), options);
    }

    /**
     * Converts the given value into a human readable string.
     * @param {object} value The value to convert.
     * @returns {string} A human readable string containing the concatenated string values.
     */
    valueToString(value) {
        if (!value) return "";
        if (!Array.isArray(value)) value = [value];
        return value.map(v => fromUuidSync(v, { strict: false })?.name ?? v).join(", ");
    }
}
