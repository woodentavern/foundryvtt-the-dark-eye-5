/**
 * Data field representing a reference to a boolean.
 * A translation for all three states of the boolean can be set.
 */
export default class LocalizableBooleanField extends foundry.data.fields.BooleanField {
    /** Object mapping possible values to translation keys  */
    translation;

    constructor(translation = {}, options = {}, context = {}) {
        super(options, context);
        this.translation = {
            yes: translation.yes ?? "",
            no: translation.no ?? "",
            none: translation.none ?? "",
        };
    }

    /**
     * Converts the given value into a human readable string.
     * @param {object} value The value to convert.
     * @returns {string} A human readable string containing the item's name.
     */
    valueToString(value) {
        if (typeof value !== "boolean") return game.i18n.localize(this.translation.none);
        return game.i18n.localize(value
            ? this.translation.yes
            : this.translation.no);
    }
}
