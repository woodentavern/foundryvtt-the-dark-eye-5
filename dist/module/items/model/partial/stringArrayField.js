/**
 * Data field representing an array of joinable strings.
 */
export default class StringArrayField extends foundry.data.fields.ArrayField {
    /**
     * The separator to insert when joining the strings.
     * @type {string}
     */
    separator;

    /**
     * Creates a new instance of a string concatenation field.
     * @param {DataField} element The data field stored within the array.
     * @param {DataFieldOptions} options Optional settings for the field's behavior.
     */
    constructor(element, options = {}) {
        super(element, options);
        this.separator = options.separator ?? ", ";
    }

    /**
     * Converts the given value into a human readable string.
     * @param {object} value The value to convert.
     * @returns {string} A human readable string containing the concatenated string values.
     */
    valueToString(value) {
        if (!value) return "";
        if (Array.isArray(value)) value.map(v => this.element.valueToString(v)).join(this.separator);
        return this.element.valueToString(value);
    }
}
