/**
 * Data model for storing damage values.
 */
export default class DamageModel extends foundry.abstract.DataModel {
    /** @inheritdoc */
    static defineSchema() {
        const { fields } = foundry.data;

        return {
            diceNumber: new fields.NumberField({
                integer: true,
                min: 0,
                initial: 1,
                nullable: false,
            }),
            diceSides: new fields.NumberField({
                label: "tde5e.item.equipment.damage",
                integer: true,
                positive: true,
                initial: 6,
                nullable: false,
            }),
            flat: new fields.NumberField({ integer: true, initial: 0, nullable: false }),
            range: new fields.ArrayField(
                new fields.NumberField({
                    integer: true,
                    positive: true,
                    max: 20,
                    initial: 10,
                    nullable: false,
                }),
                {
                    label: "tde5e.item.equipment.damageRange",
                    initial: [1, 20],
                    validate: value => value.length === 2 && value[1] >= value[0],
                },
            ),
            types: new fields.SetField(
                new fields.StringField({
                    blank: false,
                    choices: [
                        "acid",
                        "air",
                        "blunt",
                        "direct",
                        "energy",
                        "fire",
                        "humus",
                        "ice",
                        "ore",
                        "other",
                        "pierce",
                        "slash",
                        "water",
                    ],
                }),
                { label: "tde5e.item.equipment.damageType", initial: ["pierce"] },
            ),
        };
    }

    /** @inheritdoc */
    static migrateData(source) {
        if (source.hasOwnProperty("range") && source.range[0] <= 0) source.range[0] = 1;
        return super.migrateData(source);
    }

    /**
     * @returns {string} A shorter localizable label.
     */
    get labelShort() {
        return "tde5e.item.equipment.damageShort";
    }

    /**
     * @returns {string} A human readable string containing the damage formula.
     */
    toString() {
        let damage = this.diceNumber + game.i18n.localize("tde5e.diceShort") + this.diceSides;
        if (this.flat) damage += this.flat.signedString();
        return damage;
    }

    /**
     * Converts the given value into a human readable string using the model.
     * @param {object} value The value to convert.
     * @returns {string} A human readable string containing the damage formula.
     */
    valueToString(value) {
        if (!value) return "";
        if (value instanceof DamageModel) return value.toString();
        return this.toString.apply(value);
    }
}
