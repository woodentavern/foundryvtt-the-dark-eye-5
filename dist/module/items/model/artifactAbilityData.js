import SpecialAbilityModel from "./abstract/specialAbilityModel.js";
import CastableCostField from "./partial/castableCostField.js";
import ItemUuidField from "./partial/itemUuidField.js";
import LocalizableStringField from "./partial/localizableStringField.js";
import StringSetField from "./partial/stringSetField.js";

/**
 * Data model for items with the artifact ability type.
 */
export default class ArtifactAbilityData extends SpecialAbilityModel {
    /** @inheritdoc */
    static defineSchema() {
        const { fields } = foundry.data;

        return Object.assign(super.defineSchema(), {
            tradition: new ItemUuidField({ label: "tde5e.types.tradition.name" }),
            cost: new CastableCostField(),
            property: new LocalizableStringField(
                "tde5e.properties",
                {
                    label: "tde5e.item.castable.property",
                    nullable: true,
                    blank: false,
                    initial: null,
                },
            ),
            brewType: new fields.StringField({
                label: "tde5e.item.specialAbility.brewType",
                nullable: true,
                blank: false,
                initial: null,
            }),
            aspects: new StringSetField(
                new LocalizableStringField("tde5e.aspects"),
                { label: "tde5e.item.tradition.aspects" },
            ),
            binding: new fields.SchemaField({
                cost: new fields.StringField({ label: "tde5e.item.specialAbility.bindingCost" }),
                volume: new fields.StringField({ label: "tde5e.item.specialAbility.volume" }),
            }),
        });
    }

    /** @inheritdoc */
    static migrateData(source) {
        if (source.hasOwnProperty("property") && source.property?.length === 0) {
            source.property = null;
        }

        if (source.hasOwnProperty("brewType") && source.brewType?.length === 0) {
            source.brewType = null;
        }

        return super.migrateData(source);
    }
}
