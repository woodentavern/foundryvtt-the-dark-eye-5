import TdeItemModel from "./abstract/itemModel.js";
import BlacklistUuidField from "./partial/blacklistUuidField.js";
import LocalizableStringField from "./partial/localizableStringField.js";

/**
 * Data model for cantrips.
 */
export default class CantripData extends TdeItemModel {
    /** @inheritdoc */
    static defineSchema() {
        const { fields } = foundry.data;

        return Object.assign(super.defineSchema(), {
            range: new fields.StringField({ label: "tde5e.item.equipment.range" }),
            rangeLong: new fields.StringField({ label: "tde5e.item.equipment.range" }),
            duration: new fields.StringField({ label: "tde5e.item.castable.duration" }),
            durationLong: new fields.StringField({ label: "tde5e.item.castable.duration" }),
            targetCategory: new fields.StringField({ label: "tde5e.item.castable.targetCategory" }),
            requirementText: new fields.StringField({ label: "tde5e.item.sheet.requirementText" }),
            traditions: new BlacklistUuidField("tde5e.item.castable.traditions"),
            property: new LocalizableStringField("tde5e.properties", { label: "tde5e.item.castable.property" }),
        });
    }

    /**
     * @returns {string} The shortened casting time of cantrips in the spell format.
     */
    get castingTime() {
        return `1 ${game.i18n.localize("tde5e.item.castable.actionShort")}`;
    }

    /**
     * @returns {string} The casting time of cantrips in the spell format.
     */
    get castingTimeLong() {
        return `1 ${game.i18n.localize("tde5e.item.castable.action")}`;
    }

    /**
     * @returns {object} The casting cost of cantrips in the spell format.
     */
    get cost() {
        return {
            text: `1 ${game.i18n.localize("tde5e.item.castable.costShort")}`,
            constant: 1,
            recurring: null,
        };
    }

    /** @inheritdoc */
    get adventurePointValue() {
        return 1;
    }
}
