import SpecialAbilityModel from "./abstract/specialAbilityModel.js";
import CastableCostField from "./partial/castableCostField.js";
import LocalizableStringField from "./partial/localizableStringField.js";
import UuidArrayField from "./partial/uuidArrayField.js";

/**
 * Data model for items with the magic ability type.
 */
export default class MagicAbilityData extends SpecialAbilityModel {
    /** @inheritdoc */
    static defineSchema() {
        return Object.assign(super.defineSchema(), {
            cost: new CastableCostField(),
            property: new LocalizableStringField("tde5e.properties", { label: "tde5e.item.castable.property" }),
            styleAbilities: new UuidArrayField("tde5e.item.specialAbility.styleAbilities"),
        });
    }

    /** @inheritdoc */
    static migrateData(source) {
        // Move "arcane" properties to root.
        if (source.hasOwnProperty("arcane") && !source.hasOwnProperty("property")) {
            source.property = source.arcane.property;
            source.cost = source.arcane.cost;
            delete source.arcane;
        }

        return super.migrateData(source);
    }
}
