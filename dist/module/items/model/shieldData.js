import EquipmentModel from "./abstract/equipmentModel.js";
import DamageModel from "./partial/damageModel.js";

/**
 * Data model for items with the shield type.
 */
export default class ShieldData extends EquipmentModel {
    /** @inheritdoc */
    static defineSchema() {
        const { fields } = foundry.data;
        return Object.assign(super.defineSchema(), {
            damage: new fields.EmbeddedDataField(DamageModel, { label: "tde5e.item.equipment.damage" }),
            diameter: new fields.NumberField({
                label: "tde5e.item.equipment.diameter",
                integer: true,
                positive: true,
                initial: 10,
                nullable: false,
            }),
        });
    }

    /** @inheritdoc */
    static migrateData(source) {
        if (source.hasOwnProperty("resources") && !source.hasOwnProperty("structurePoints")) {
            source.structurePoints = source.resources.structurePoints;
            delete source.resources;
        }

        if (source.hasOwnProperty("structurePoints")) {
            const slope = 6 / 42;
            if (source.structurePoints.hasOwnProperty("max") && !source.hasOwnProperty("breakingPointRating")) {
                source.breakingPointRating = Math.round(6 + slope * (source.structurePoints.max - 16));
            }

            if (source.structurePoints.hasOwnProperty("value") && !source.hasOwnProperty("wear")) {
                source.wear = Math.floor(4 - 4 * (source.structurePoints.value / (source.structurePoints.max ?? 58)));
            }
        }

        return super.migrateData(source);
    }
}
