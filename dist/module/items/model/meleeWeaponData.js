import EquipmentModel from "./abstract/equipmentModel.js";
import ItemUuidField from "./partial/itemUuidField.js";
import DamageModel from "./partial/damageModel.js";

/**
 * Data model for items with the melee weapon type.
 */
export default class MeleeWeaponData extends EquipmentModel {
    /** @inheritdoc */
    static defineSchema() {
        const { fields } = foundry.data;
        return Object.assign(super.defineSchema(), {
            group: new ItemUuidField({
                label: "TYPES.Item.combatTechnique",
                initial: "Compendium.tde5e.skills.Item.SC43947299610730",
            }),
            damage: new fields.EmbeddedDataField(DamageModel, { label: "tde5e.item.equipment.damage" }),
            length: new fields.NumberField({
                label: "tde5e.item.equipment.length",
                integer: true,
                positive: true,
                initial: 80,
                nullable: false,
            }),
            hands: new fields.NumberField({
                label: "tde5e.item.equipment.hands.name",
                min: 1,
                step: 0.5,
                initial: 1,
                nullable: false,
            }),
            application: new fields.StringField({
                label: "tde5e.item.equipment.application",
            }),
        });
    }

    /** @inheritdoc */
    static migrateData(source) {
        // Enforce positive length.
        if (source.hasOwnProperty("length") && source.length <= 0) {
            source.length = 1;
        }

        // Convert handedness.
        if (!source.hasOwnProperty("hands")) {
            if (source.hasOwnProperty("isOneHanded")) {
                source.hands = 1;
                delete source.isOneHanded;
            }
            if (source.hasOwnProperty("isTwoHanded")) {
                source.hands = 2;
                delete source.isTwoHanded;
            }
        }

        return super.migrateData(source);
    }
}
