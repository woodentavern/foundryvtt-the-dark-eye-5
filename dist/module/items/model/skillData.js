import ImprovableSkillModel from "./abstract/improvableSkillModel.js";
import CheckField from "./partial/checkField.js";

/**
 * Data model for items with the skill type.
 */
export default class SkillData extends ImprovableSkillModel {
    /** @inheritdoc */
    static defineSchema() {
        const { fields } = foundry.data;

        const isDefault = new fields.BooleanField({ label: "tde5e.item.config.isDefault" });
        isDefault.noMigration = true;
        isDefault.hidden = true;

        return Object.assign(super.defineSchema(), {
            group: new fields.StringField({ label: "tde5e.item.sheet.group" }),
            isDefault,
            check: new CheckField({
                affectedByEncumbrance: new fields.BooleanField({
                    label: "tde5e.item.skill.affectedByEncumbrance",
                    initial: null,
                    nullable: true,
                }),
                hideValue: new fields.BooleanField({ label: "tde5e.item.skill.hideCheck", initial: true }),
                hideResult: new fields.BooleanField(),
            }),
            applications: new fields.StringField({ label: "tde5e.item.skill.applications" }),
        });
    }

    /** @inheritdoc */
    static migrateData(source) {
        // Change applications and uses from array to string.
        if (source.hasOwnProperty("applications") && Array.isArray(source.applications)) {
            source.applications = source.applications.join(", ");
        }
        if (source.hasOwnProperty("uses") && Array.isArray(source.uses)) {
            source.uses = source.uses.join(", ");
        }

        if (source.hasOwnProperty("description") && !source.hasOwnProperty("details")) {
            source.details = source.description;
            delete source.description;
        }
        if (source.hasOwnProperty("details")) {
            delete source.details;
        }

        return super.migrateData(source);
    }

    /** @inheritdoc */
    get activationCost() {
        return 0;
    }
}
