import SpecialAbilityModel from "./abstract/specialAbilityModel.js";
import CastableCostField from "./partial/castableCostField.js";
import UuidArrayField from "./partial/uuidArrayField.js";

/**
 * Data model for items with the karma ability type.
 */
export default class KarmaAbilityData extends SpecialAbilityModel {
    /** @inheritdoc */
    static defineSchema() {
        const { fields } = foundry.data;

        return Object.assign(super.defineSchema(), {
            cost: new CastableCostField(),
            aspect: new fields.StringField({ label: "tde5e.item.tradition.aspects" }),
            styleAbilities: new UuidArrayField("tde5e.item.specialAbility.styleAbilities"),
        });
    }

    /** @inheritdoc */
    static migrateData(source) {
        // Move "blessed" properties to root.
        if (source.hasOwnProperty("blessed") && !source.hasOwnProperty("aspect")) {
            source.aspect = source.blessed.aspect;
            source.cost = source.blessed.cost;
            delete source.blessed;
        }

        return super.migrateData(source);
    }
}
