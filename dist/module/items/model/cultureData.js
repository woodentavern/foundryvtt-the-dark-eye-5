import CommonVantageModel from "./abstract/commonVantageModel.js";
import ItemUuidField from "./partial/itemUuidField.js";
import UuidArrayField from "./partial/uuidArrayField.js";

/**
 * Data model for items with the culture type.
 */
export default class CultureData extends CommonVantageModel {
    /** @inheritdoc */
    static defineSchema() {
        const { fields } = foundry.data;

        const culturalPackageSkills = new fields.ArrayField(new fields.SchemaField({
            skill: new ItemUuidField(),
            value: new fields.NumberField({ integer: true, initial: 0, nullable: false }),
        }), { label: "tde5e.item.culture.culturalPackageSkills" });
        culturalPackageSkills.locked = true;

        return Object.assign(super.defineSchema(), {
            areaKnowledge: new fields.StringField(),
            areaKnowledgeLong: new fields.StringField({ label: "tde5e.item.skill.areaKnowledge" }),
            commonNames: new fields.StringField({ label: "tde5e.item.culture.commonNames" }),
            socialStatus: new fields.ArrayField(new fields.StringField({
                choices: ["notFree", "free", "lesserNoble", "noble", "aristocracy"],
            }), { label: "tde5e.actor.bio.socialStatus", initial: ["free"] }),
            commonMundaneProfessions: new fields.StringField({ label: "tde5e.item.culture.commonMundaneProfessions" }),
            commonMagicalProfessions: new fields.StringField({ label: "tde5e.item.culture.commonMagicalProfessions" }),
            commonBlessedProfessions: new fields.StringField({ label: "tde5e.item.culture.commonBlessedProfessions" }),
            languagesSpoken: new UuidArrayField("tde5e.types.languageSpoken"),
            commonSkills: new UuidArrayField("tde5e.item.culture.commonSkills"),
            uncommonSkills: new UuidArrayField("tde5e.item.culture.uncommonSkills"),
            culturalPackageSkills,
        });
    }
}
