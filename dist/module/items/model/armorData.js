import EquipmentModel from "./abstract/equipmentModel.js";

/**
 * Data model for items with the armor type.
 */
export default class ArmorData extends EquipmentModel {
    /** @inheritdoc */
    static defineSchema() {
        const { fields } = foundry.data;

        const protection = new fields.SchemaField({
            blunt: new fields.NumberField({
                integer: true,
                min: 0,
                initial: 0,
                nullable: false,
            }),
            pierce: new fields.NumberField({
                integer: true,
                min: 0,
                initial: 0,
                nullable: false,
            }),
            slash: new fields.NumberField({
                integer: true,
                min: 0,
                initial: 0,
                nullable: false,
            }),
            mod: new fields.NumberField({
                integer: true,
                initial: 0,
                nullable: false,
            }),
        }, { label: "tde5e.item.equipment.protection" });
        protection.valueToString = function valueToString(value) {
            return `${value.pierce}/${value.slash}/${value.blunt}`;
        };

        const isSelected = new fields.BooleanField();
        isSelected.noMigration = true;
        isSelected.skipChangelog = true;

        return Object.assign(super.defineSchema(), {
            group: new fields.StringField({
                label: "tde5e.item.sheet.group",
                blank: false,
                choices: ["chainOrScale", "fabric", "leather", "plate"],
                initial: "fabric",
            }),
            protection,
            encumbrance: new fields.NumberField({
                label: "tde5e.item.equipment.encumbrance",
                integer: true,
                min: 0,
                initial: 0,
                nullable: false,
            }),
            isSelected,
        });
    }
}
