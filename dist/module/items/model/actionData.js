import PurchaseField from "./partial/purchaseField.js";
import UuidArrayField from "./partial/uuidArrayField.js";
import TdeItemModel from "./abstract/itemModel.js";
import LocalizableBooleanField from "./partial/localizableBooleanField.js";

/**
 * Data model for items with the action type.
 */
export default class ActionData extends TdeItemModel {
    /** @inheritdoc */
    static defineSchema() {
        const { fields } = foundry.data;
        return Object.assign(super.defineSchema(), {
            prerequisites: new fields.SchemaField({
                combatStyle: new UuidArrayField("tde5e.types.combatAbility.combatStyle"),
                equipment: new UuidArrayField(),
                quality: new fields.NumberField({
                    label: "tde5e.item.prerequisites.quality",
                    integer: true,
                    min: 0,
                    initial: 1,
                    nullable: false,
                }),
            }, { label: "tde5e.item.prerequisites.name" }),
            ruleShort: new fields.StringField({ label: "tde5e.item.sheet.effect" }),
            isAttack: new LocalizableBooleanField({
                yes: "tde5e.item.maneuver.isAttack",
                no: "tde5e.item.maneuver.type.tactical",
            }, {
                label: "tde5e.item.maneuver.isAttack",
            }),
            isRanged: new LocalizableBooleanField({
                yes: "tde5e.item.maneuver.reach.ranged",
                no: "tde5e.item.maneuver.reach.melee",
                none: "tde5e.general.all",
            }, {
                label: "tde5e.item.maneuver.isRanged",
                nullable: true,
                initial: null,
            }),
            cost: new fields.SchemaField({
                resources: new fields.SchemaField({
                    actionPoints: new fields.NumberField({
                        label: "tde5e.combat.actionPoints",
                        integer: true,
                        initial: 1,
                        nullable: false,
                    }),
                    adrenaline: new fields.NumberField({
                        integer: true,
                        initial: 0,
                        nullable: false,
                    }),
                    style: new fields.NumberField({
                        integer: true,
                        initial: 0,
                        nullable: false,
                    }),
                }),
                description: new fields.StringField(),
            }, { label: "tde5e.item.castable.cost" }),
            requirementText: new fields.StringField({ label: "tde5e.item.sheet.requirementText" }),
            purchase: new PurchaseField(),
            quality: new fields.SchemaField({
                limited: new fields.StringField(),
                disadvantaged: new fields.StringField(),
                advantaged: new fields.StringField(),
                overreached: new fields.StringField(),
            }, { label: "tde5e.item.maneuver.quality" }),
            empowerments: new UuidArrayField("tde5e.item.groups.empowerments"),
        });
    }

    /** @inheritdoc */
    static migrateData(source) {
        if (source.hasOwnProperty("group") && !source.hasOwnProperty("isRanged")) {
            switch (source.group) {
            case "meleeAttack":
                source.isAttack = true;
                source.isRanged = false;
                break;
            case "rangedAttack":
                source.isAttack = true;
                source.isRanged = true;
                break;
            default:
                source.isAttack = false;
                source.isRanged = null;
            }

            delete source.group;
        }

        if (source.hasOwnProperty("effect") && !source.hasOwnProperty("ruleShort")) {
            source.ruleShort = source.effect;
            delete source.effect;
        }

        return super.migrateData(source);
    }
}
