import TdeItemModel from "./abstract/itemModel.js";
import BlacklistUuidField from "./partial/blacklistUuidField.js";
import LocalizableStringField from "./partial/localizableStringField.js";

/**
 * Data model for items with the creature definition type.
 */
export default class CreatureTypeData extends TdeItemModel {
    /** @inheritdoc */
    static defineSchema() {
        const { fields } = foundry.data;
        return Object.assign(super.defineSchema(), {
            group: new fields.StringField({
                label: "tde5e.item.creatureType.category",
                choices: [
                    "animal", "beast", "chimera", "creature", "cultural", "daimonid", "dragon", "elemental", "fairy",
                    "ghost", "golem", "higherDemon", "hornedDemon", "humanoid", "living", "lowerDemon", "nonHumanoid",
                    "notLiving", "other", "plant", "supernatural", "undead",
                ],
                initial: "beast",
            }),
            calling: new fields.SchemaField({
                type: new fields.StringField({
                    nullable: true,
                    choices: ["creation", "hunting", "summon"],
                    initial: "hunting",
                }),
                value: new fields.NumberField({
                    integer: true,
                    min: 0,
                    initial: 0,
                    nullable: false,
                }),
            }, { label: "tde5e.callings.name" }),
            ignoresPainFromLowHealth: new fields.BooleanField({
                label: "tde5e.item.creatureType.ignoresPainFromLowHealth",
            }),
            conditionImmunities: new BlacklistUuidField("tde5e.item.creatureType.conditionImmunities"),
            stateImmunities: new BlacklistUuidField("tde5e.item.creatureType.stateImmunities"),
            magicalImmunities: new fields.SchemaField({
                all: new fields.BooleanField(),
                values: new fields.SetField(new LocalizableStringField("tde5e.properties")),
            }, { label: "tde5e.item.creatureType.magicalImmunities" }),
        });
    }

    /** @inheritdoc */
    static migrateData(source) {
        if (source.hasOwnProperty("huntingDifficulty") && !source.hasOwnProperty("calling")) {
            for (const callingType of ["hunting", "summon", "creation"]) {
                const key = `${callingType}Difficulty`;
                const calling = source[key];
                if (calling) {
                    delete source[key];
                    if (calling.hasValue) {
                        source.calling = { type: callingType, value: calling.value };
                        break;
                    }
                }
            }
        }

        if (source.hasOwnProperty("calling") && source.calling.type === "-") {
            source.calling.type = null;
        }

        return super.migrateData(source);
    }
}
