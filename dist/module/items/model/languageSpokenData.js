import LanguageModel from "./abstract/languageModel.js";
import VariantModel, { migrateVariants } from "./partial/variantModel.js";

/**
 * Data model for items with the spoken language type.
 */
export default class LanguageSpokenData extends LanguageModel {
    /** @inheritdoc */
    static defineSchema() {
        const { fields } = foundry.data;

        const languageModel = super.defineSchema();
        languageModel.purchase.fields.levels = new fields.NumberField({
            label: "tde5e.item.specialAbility.levels",
            integer: true,
            positive: true,
            initial: 1,
            nullable: false,
        });

        return Object.assign(languageModel, {
            variant: new fields.EmbeddedDataField(VariantModel),
        });
    }

    /** @inheritdoc */
    static migrateData(source) {
        migrateVariants(source);
        return super.migrateData(source);
    }

    /** @inheritdoc */
    get adventurePointValue() {
        return this.getImprovement("system.variant.level").adventurePointValue;
    }
}
