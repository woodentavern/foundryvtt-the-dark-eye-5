import SpecialAbilityModel from "./abstract/specialAbilityModel.js";

/**
 * Data model for items with the advantage or disadvantage type.
 */
export default class VantageData extends SpecialAbilityModel {
    /** @inheritdoc */
    static defineSchema() {
        const { fields } = foundry.data;
        return Object.assign(super.defineSchema(), {
            onlyWhenCommonOrSuggested: new fields.BooleanField({ label: "tde5e.item.vantage.onlyWhenSuggested" }),
            requirementText: new fields.StringField({ label: "tde5e.item.sheet.requirementText" }),
        });
    }

    /** @inheritdoc */
    static migrateData(source) {
        // Ensure that only one rule property exists.
        if (source.effect && !source.rules) {
            delete source.rules;
        } else if (source.rules && !source.effect) {
            delete source.effect;
        }

        return super.migrateData(source);
    }
}
