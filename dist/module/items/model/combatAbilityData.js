import SpecialAbilityModel from "./abstract/specialAbilityModel.js";
import BlacklistUuidField from "./partial/blacklistUuidField.js";

/**
 * Data model for items with the combat ability type.
 */
export default class CombatAbilityData extends SpecialAbilityModel {
    /** @inheritdoc */
    static defineSchema() {
        const { fields } = foundry.data;

        const abilityData = super.defineSchema();
        abilityData.group.initial = "passive";

        return Object.assign(abilityData, {
            combat: new fields.SchemaField({
                combatTechniques: new BlacklistUuidField("tde5e.types.combatTechnique"),
                armorGroups: new fields.ArrayField(new fields.StringField({
                    blank: false,
                    choices: ["chainOrScale", "fabric", "leather", "plate"],
                }), { label: "tde5e.types.armor.name" }),
            }, { label: "tde5e.itemSheet.combat" }),
        });
    }

    /** @inheritdoc */
    static migrateData(source) {
        if (source.combat?.hasOwnProperty("techniques")) {
            source.combat.combatTechniques = { all: false, values: source.combat.techniques };
            delete source.combat.techniques;
        }

        if (source.combat?.hasOwnProperty("combatTechniques") && Array.isArray(source.combat.combatTechniques)) {
            source.combat.combatTechniques = { all: false, values: source.combat.combatTechniques };
        }

        if (source.combat?.hasOwnProperty("allCombatTechniques")) {
            source.combat.combatTechniques ??= {};
            source.combat.combatTechniques.all = source.combat.allCombatTechniques;
        }

        return super.migrateData(source);
    }
}
