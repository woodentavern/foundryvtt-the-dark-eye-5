import TdeItemModel from "./abstract/itemModel.js";

/**
 * Data model for items with the condition type.
 */
export default class ConditionData extends TdeItemModel {
    /** @inheritdoc */
    static defineSchema() {
        const { fields } = foundry.data;

        const value = new fields.NumberField({
            integer: true,
            min: 0,
            initial: 0,
            nullable: false,
        });
        value.noMigration = true;

        return Object.assign(super.defineSchema(), {
            isDefault: new fields.BooleanField({ label: "tde5e.item.config.isDefault" }),
            value,
            maxLevel: new fields.NumberField({
                label: "tde5e.item.condition.maxLevel",
                integer: true,
                positive: true,
                initial: 4,
                nullable: false,
            }),
            levelEffects: new fields.ArrayField(
                new fields.StringField(),
                { label: "tde5e.item.condition.levelEffects" },
            ),
            levelEffectsShort: new fields.ArrayField(
                new fields.StringField(),
                { label: "tde5e.item.condition.levelEffectsShort" },
            ),
            duration: new fields.StringField({ label: "tde5e.item.castable.duration" }),
            removal: new fields.StringField({ label: "tde5e.item.condition.removal" }),
        });
    }
}
