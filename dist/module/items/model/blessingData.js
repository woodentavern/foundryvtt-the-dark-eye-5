import CantripData from "./cantripData.js";
import LocalizableStringField from "./partial/localizableStringField.js";

/**
 * Data model for cantrips.
 */
export default class BlessingData extends CantripData {
    /** @inheritdoc */
    static defineSchema() {
        const cantripData = super.defineSchema();
        delete cantripData.property;
        cantripData.aspect = new LocalizableStringField("tde5e.aspects", { label: "tde5e.item.tradition.aspects" });
        return cantripData;
    }
}
