import LanguageModel from "./abstract/languageModel.js";
import UuidArrayField from "./partial/uuidArrayField.js";

/**
 * Data model for items with the written language type.
 */
export default class LanguageWrittenData extends LanguageModel {
    /** @inheritdoc */
    static defineSchema() {
        return Object.assign(super.defineSchema(), {
            languages: new UuidArrayField("tde5e.types.languageSpoken"),
        });
    }
}
