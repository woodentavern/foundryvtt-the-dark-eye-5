import CommonVantageModel from "./abstract/commonVantageModel.js";
import PurchaseField from "./partial/purchaseField.js";
import UuidArrayField from "./partial/uuidArrayField.js";
import VariantModel, { migrateVariants } from "./partial/variantModel.js";

/**
 * Data model for items with the race type.
 */
export default class RaceData extends CommonVantageModel {
    /** @inheritdoc */
    static defineSchema() {
        const { fields } = foundry.data;

        const attributeAdjustments = new fields.ArrayField(new fields.SchemaField({
            attribute: new fields.StringField({
                choices: [
                    "agility", "any", "charisma", "constitution", "courage", "dexterity", "intuition",
                    "sagacity", "strength",
                ],
                initial: "agility",
            }),
            of: new fields.ArrayField(new fields.StringField({
                choices: [
                    "agility", "charisma", "constitution", "courage", "dexterity", "intuition",
                    "sagacity", "strength",
                ],
            }), { nullable: true, initial: null }),
            value: new fields.NumberField({
                integer: true,
                required: true,
                initial: 0,
                nullable: false,
            }),
        }, { label: "tde5e.item.race.attributeAdjustments" }));
        attributeAdjustments.noMigration = true;
        attributeAdjustments.locked = true;

        return Object.assign(super.defineSchema(), {
            variant: new fields.EmbeddedDataField(VariantModel),
            attributeAdjustments,
            modifiers: new fields.SchemaField({
                lifePoints: new fields.NumberField({ integer: true, initial: 0, nullable: false }),
                movement: new fields.NumberField({ integer: true, initial: 0, nullable: false }),
                spirit: new fields.NumberField({ integer: true, initial: 0, nullable: false }),
                toughness: new fields.NumberField({ integer: true, initial: 0, nullable: false }),
            }, { label: "tde5e.item.race.modifiers" }),
            automaticAdvantages: new UuidArrayField("tde5e.item.race.automaticAdvantages"),
            automaticDisadvantages: new UuidArrayField("tde5e.item.race.automaticDisadvantages"),
            stronglyRecommendedAdvantages: new UuidArrayField(),
            stronglyRecommendedDisadvantages: new UuidArrayField(),
            hairColors: new fields.ArrayField(new fields.StringField(), { label: "tde5e.item.race.hairColors" }),
            eyeColors: new fields.ArrayField(new fields.StringField(), { label: "tde5e.item.race.eyeColors" }),
            weight: new fields.StringField({ label: "tde5e.actor.bio.weight" }),
            size: new fields.StringField({ label: "tde5e.actor.bio.height" }),
            purchase: new PurchaseField(),
        });
    }

    /** @inheritdoc */
    static migrateData(source) {
        if (source.hasOwnProperty("weight") && Array.isArray(source.weight)) {
            const weight = source.weight[0];
            source.weight = weight ? `${weight.modDiceNumber}d${weight.modDiceSides}+${weight.basis}` : "";
        }
        if (source.hasOwnProperty("size") && Array.isArray(source.size)) {
            const size = source.size[0];
            source.size = size ? `${size.modDiceNumber}d${size.modDiceSides}+${size.basis}` : "";
        }

        if (source.hasOwnProperty("attributeAdjustments") && !Array.isArray(source.attributeAdjustments)) {
            source.attributeAdjustments = [];
        }

        migrateVariants(source);
        return super.migrateData(source);
    }
}
