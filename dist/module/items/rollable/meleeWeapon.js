import WeaponItem from "./weaponItem.js";
import { Shield } from "./shield.js";

export class MeleeWeapon extends WeaponItem {
    /** @override */
    static defaultIconPath = "systems/tde5e/assets/defaultItems/melee_weapon.svg";

    /**
     * @override
     */
    get localizedGroupName() {
        return game.i18n.localize(`tde5e.types.${this.type}`);
    }

    /** @override */
    get sheetVariantTemplate() {
        return "systems/tde5e/templates/items/variants/weaponVariant.hbs";
    }

    /** @override */
    getSheetData() {
        const data = super.getSheetData();
        data.equipmentType = "melee";
        return data;
    }

    /**
     * Checks whether the provided weapon or shield can we used as an offhand
     * in conjunction with this weapon.
     * @param {MeleeWeapon | RangedWeapon | Shield} offhandWeaponItem  The offhand weaponItem.
     * @param {boolean} canUseShields True if the actor can dual wield, fals otherwise.
     * @param {boolean} canDualWield True if the actor can use shields, false otherwise.
     * @returns {boolean} True, if the given weapon can work as offhand, false otherwise.
     */
    canHaveOffhand(offhandWeaponItem, canUseShields, canDualWield) {
        if (!offhandWeaponItem) return true;

        if (offhandWeaponItem.cid === "SCHIMMERNDER_SCHILD") return true;

        if (offhandWeaponItem instanceof Shield
            && canUseShields
            && this.allowsShield) { return true; }

        if (offhandWeaponItem instanceof MeleeWeapon
            && canDualWield
            && this.allowsOffhandWeapon
            && offhandWeaponItem.allowsOffhandWeapon) { return true; }

        return false;
    }

    /**
     * Determines whether this weapon can be used together with a shield.
     * @returns {boolean} True, if this weapon can be used with a shield, false otherwise.
     */
    get allowsShield() {
        return this.combatTechnique?.allowsShield ?? false;
    }

    /**
     * Determines whether this weapon can be used together with a second melee weapon.
     * Whether the other weapon is invalid (e.g. a two handed weapon or ranged weapon) is
     *  irrelevant here and should be checked on the other item as well.
     * @returns {boolean} True, if this weapon can be used with a shield, false otherwise.
     */
    get allowsOffhandWeapon() {
        return this.system.hands < 2;
    }
}

class Weaponless extends MeleeWeapon {
    /** @override */
    get canHavePrefix() {
        return false;
    }

    /** @override */
    get isNatural() {
        // Ensure multiple Weaponless items can be removed but one cannot remove the last one
        const fists = this.actor.getItems(TDE5E.itemTypes.MeleeWeapon, "WAFFENLOS");
        return fists.length < 2;
    }

    /** @override */
    prepareBonusDamage() {
        super.prepareBonusDamage();
        const werebear = this.actor?.uniqueItems?.get("WERBARGESTALT");
        if (werebear) this.bonusDamage += werebear.unarmedDamageBonus;
    }
}

export default {
    default: MeleeWeapon,
    WAFFENLOS: Weaponless,
};
