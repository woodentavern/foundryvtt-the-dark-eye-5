import TdeItem from "../item.js";
import TdeSkillCheck from "../../ui/rolls/skillCheck.js";
import { error } from "../../util.js";

/**
 * Extends the default item with self-rolling capabilities.
 */
export default class RollableItem extends TdeItem {
    /** @inheritdoc */
    get isRollable() {
        return true;
    }

    /**
     * Checks if this item can be used as a skill for the combat group check.
     * @returns {boolean} True if this skill can be used as combat check, false otherwise.
     */
    get isCombatSkill() {
        return false;
    }

    /**
     * Checks if this skill can be rolled with the give loadout.
     * @param {Loadout} loadout The loadout to check.
     * @returns {boolean} True if the skill can be used with the loadout, false otherwise.
     */
    // eslint-disable-next-line no-unused-vars -- Keep for documentation purposes.
    canUseLoadout(loadout) {
        return true;
    }

    /**
     * Attempts to roll a check for this item, if it allows doing so.
     * @param {string?} rollMode The roll mode to use for the chat message.
     * @param {number=} modifier An optional roll modifier. Defaults to 0.
     * @returns {Promise} A promise representing the message creation.
     */
    async roll(rollMode, modifier = 0) {
        const check = this.system?.check;
        if (!check || !this.actor) {
            error(`The ability ${this.name} cannot be rolled.`);
            return Promise.resolve();
        }

        const roll = new TdeSkillCheck(this, modifier);
        return roll.toMessage({ speaker: { alias: roll.data.actor.name } }, { rollMode });
    }

    /** @override */
    modifyRoll() {
        return null;
    }
}
