import CastableItem from "./castableItem.js";
import * as util from "../../util.js";

export class Liturgy extends CastableItem {
    /** @override */
    get isCombatSkill() {
        return this.system.check && this.system.group !== "ceremony";
    }

    /** @inheritdoc */
    getSheetData() {
        const data = super.getSheetData();
        data.primaryAttribute = game.i18n.localize(`tde5e.actor.attributes.${this.system.primaryAttribute}`);
        data.property = Array.from(this.system.aspects).map(a => game.i18n.localize(`tde5e.aspects.${a}`));
        return data;
    }

    /** @inheritdoc */
    get isRollable() {
        return util.hasValue(this.system.check);
    }
}

export default {
    default: Liturgy,
};
