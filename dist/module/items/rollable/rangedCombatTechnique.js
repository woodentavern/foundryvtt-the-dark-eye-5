import CombatTechnique from "./combatTechnique.js";
import { QualityModification } from "../../ui/rolls/rollModification.js";

export class RangedCombatTechnique extends CombatTechnique {
    /** @inheritdoc */
    get allowsShield() {
        return false;
    }

    /** @inheritdoc */
    modifyRoll() {
        return super.modifyRoll().concat([
            new QualityModification(
                { id: this.id, displayName: game.i18n.localize("tde5e.combat.modifiers.embattled.name") },
                -3,
                {
                    select: true,
                    values: {
                        "-3": "tde5e.combat.modifiers.crowded.single",
                        "-4": "tde5e.combat.modifiers.crowded.double",
                        "-5": "tde5e.combat.modifiers.crowded.triple",
                    },
                    active: false,
                },
            ),
            new QualityModification(
                { id: this.id, displayName: game.i18n.localize("tde5e.combat.modifiers.wind.name") },
                -1,
                {
                    select: true,
                    values: {
                        "-1": "tde5e.combat.modifiers.wind.storm",
                        "-2": "tde5e.combat.modifiers.wind.hurricane",
                    },
                    active: false,
                },
            ),
            new QualityModification(
                { id: this.id, displayName: game.i18n.localize("tde5e.combat.modifiers.stability.name") },
                -1,
                {
                    select: true,
                    values: {
                        "-1": "tde5e.combat.modifiers.stability.unsteady",
                        0: "tde5e.combat.modifiers.stability.mediumWater",
                        "-2": "tde5e.combat.modifiers.stability.highWater",
                        "-8": "tde5e.combat.modifiers.stability.underwater",
                    },
                    active: false,
                },
            ),
            new QualityModification(
                { id: this.id, displayName: game.i18n.localize("tde5e.combat.modifiers.size.name") },
                0,
                {
                    select: true,
                    values: {
                        "-2": "tde5e.combat.modifiers.size.tiny",
                        "-1": "tde5e.combat.modifiers.size.small",
                        0: "tde5e.combat.modifiers.size.medium",
                        1: "tde5e.combat.modifiers.size.large",
                        2: "tde5e.combat.modifiers.size.huge",
                    },
                },
            ),
            new QualityModification(
                { id: this.id, displayName: game.i18n.localize("tde5e.combat.modifiers.distance.name") },
                0,
                {
                    select: true,
                    values: {
                        0: "tde5e.combat.modifiers.distance.short",
                        "-1": "tde5e.combat.modifiers.distance.medium",
                        "-2": "tde5e.combat.modifiers.distance.long",
                    },
                },
            ),
            new QualityModification(
                { id: this.id, displayName: game.i18n.localize("tde5e.combat.modifiers.crowded.name") },
                -1,
                {
                    select: true,
                    values: {
                        "-1": "tde5e.combat.modifiers.crowded.single",
                        "-2": "tde5e.combat.modifiers.crowded.double",
                        "-3": "tde5e.combat.modifiers.crowded.triple",
                    },
                    active: false,
                },
            ),
            new QualityModification(
                { id: this.id, displayName: game.i18n.localize("tde5e.combat.modifiers.movement.name") },
                0,
                {
                    select: true,
                    values: {
                        1: "tde5e.combat.modifiers.movement.none",
                        0: "tde5e.combat.modifiers.movement.slow",
                        "-1": "tde5e.combat.modifiers.movement.fast",
                        "-2": "tde5e.combat.modifiers.movement.dodging",
                    },
                },
            ),
            new QualityModification(
                { id: this.id, displayName: game.i18n.localize("tde5e.combat.modifiers.position.name") },
                2,
                {
                    select: true,
                    values: {
                        2: "tde5e.combat.modifiers.position.elevated",
                        1: "tde5e.combat.modifiers.position.riding",
                    },
                    active: false,
                },
            ),
        ]);
    }
}

export default {
    SCHUSSWAFFEN: RangedCombatTechnique,
    SCHLEUDERWAFFEN: RangedCombatTechnique,
};
