import RollableItem from "./rollableItem.js";

class Skill extends RollableItem {
    /** @override */
    get sheetHeaderClass() {
        switch (this.system.group) {
        case "physical": return "bg-teal";
        case "social": return "bg-red";
        case "nature": return "bg-green";
        case "knowledge": return "bg-blue";
        default: return "bg-brown";
        }
    }

    /** @inheritdoc */
    getSheetData() {
        const data = super.getSheetData();
        [data.encumberance, data.encumbranceClass] = this.encumbranceData;
        data.attributeText = this.system.check.attributes
            .map(at => game.i18n.localize(`tde5e.actor.attributesShort.${at}`))
            .join("/");
        return data;
    }

    /**
     * @returns {CommonSpecialAbility[]} The acquired specializations for this skill.
     */
    get specializations() {
        return this.actor.getItems(TDE5E.itemTypes.CommonAbility, "FOKUSGEBIET")
            .filter(spec => spec.system.variant.selected?.target === this.id);
    }

    /**
     * Transformes the given boolean into the encumbrance text for the ui.
     * @returns {string[2]} The text and icon representation of the value.
     */
    get encumbranceData() {
        switch (this.system.check.affectedByEncumbrance) {
        case true: return ["tde5e.choices.yes", "fa-check"];
        case false: return ["tde5e.choices.no", ""];
        default: return ["tde5e.choices.maybe", "fa-question"];
        }
    }
}

/**
 * Overrides for specific compendium entries to use a custom class.
 */
export default {
    default: Skill,
};
