import { AttributeModification } from "../../ui/rolls/rollModification.js";
import RollableItem from "./rollableItem.js";

/**
 * Base class for spells and chants.
 */
export default class CastableItem extends RollableItem {
    /** @inheritdoc */
    getSheetData() {
        const data = super.getSheetData();

        if (this.isRollable) {
            data.attributes = this.system.check.attributes
                .map(a => game.i18n.localize(`tde5e.actor.attributesShort.${a}`))
                .join("/");

            const { resistedBySpirit, resistedByToughness, resistanceIsHalfed } = this.system.check;
            data.attributes += this._applyModBy("spirit", resistedBySpirit, resistanceIsHalfed);
            data.attributes += this._applyModBy("toughness", resistedByToughness, resistanceIsHalfed);
        }

        const { modifications } = this.system;
        data.duration = this.system.duration;
        if (modifications) {
            data.range = this._applyNotModifiable(this.system.range, modifications.canModRange);
            data.cost = this._applyNotModifiable(this.system.cost.text, modifications.canModCost);
            data.spellTime = this._applyNotModifiable(this.system.castingTime, modifications.canModCastingTime);
        }

        return data;
    }

    /** @inheritdoc */
    async _preUpdate(data, options, user) {
        // On group change delete enhancements or add them.
        const group = foundry.utils.getProperty(data, "system.group");
        if (group) {
            const enhancementCount = this.system.enhancements?.length ?? 0;
            const hasEnhancements = ["incantation", "ritual", "liturgicalChant", "ceremony"].includes(group);

            if (hasEnhancements === !enhancementCount) {
                const defaultEnhancements = hasEnhancements ? game.template.Item.spell.enhancements : [];
                foundry.utils.setProperty(data, "system.enhancements", defaultEnhancements);
            }
        }

        return super._preUpdate(data, options, user);
    }

    /**
     * Append a lock icon to the given value if it can be modified.
     * @param {string} text The value to append the icon to.
     * @param {bool} canModify True if the associated element can be modfied, false otherwise.
     * @returns {string} The modified text.
     */
    _applyNotModifiable(text, canModify) {
        if (!text) return "-";
        if (canModify) return text;
        return `${text} <i class="nomod-icon fas fa-lock"></i>`;
    }

    /**
     * Return the modifier text behind the key as part of teh modifiedBy rules for spells.
     * @param {string} type  either toughness or spirit.
     * @param {bool} canModify True if the modifier should be returned, false otherwise.
     * @param {bool} isHalved True if the modification is halved, false otherwise.
     * @returns {string} A modifier html element.
     */
    _applyModBy(type, canModify, isHalved) {
        if (!canModify) return "";
        const halved = (isHalved) ? "/2" : "";
        const text = game.i18n.localize(`tde5e.actor.derived.${type}Short`);
        return `<span class="mod-by">(-${text}${halved})</span>`;
    }

    /** @inheritdoc */
    canModifyRoll(item) {
        return item.id === this.id;
    }

    /** @inheritdoc */
    modifyRoll() {
        const modifiers = [];
        const modData = this.system.modifications;
        if (modData.canModCost) {
            modifiers.push(new AttributeModification(
                { id: this.id, displayName: game.i18n.localize("tde5e.item.castable.cost") },
                -1,
                {
                    select: true,
                    values: {
                        1: "tde5e.item.castable.modifiers.cost.increase",
                        "-1": "tde5e.item.castable.modifiers.cost.decrease",
                    },
                    active: false,
                    deferred: true,
                },
            ));
        }

        if (modData.canModCastingTime) {
            modifiers.push(new AttributeModification(
                { id: this.id, displayName: game.i18n.localize("tde5e.item.castable.castingTime") },
                -1,
                {
                    select: true,
                    values: {
                        1: "tde5e.item.castable.modifiers.cost.increase",
                        "-1": "tde5e.item.castable.modifiers.cost.decrease",
                    },
                    active: false,
                    deferred: true,
                },
            ));
        }

        if (modData.canModRange) {
            modifiers.push(new AttributeModification(
                { id: this.id, displayName: game.i18n.localize("tde5e.item.equipment.range") },
                -1,
                {
                    select: true,
                    values: {
                        1: "tde5e.item.castable.modifiers.range.increase",
                        "-1": "tde5e.item.castable.modifiers.range.decrease",
                    },
                    active: false,
                    deferred: true,
                },
            ));
        }

        if (this.system.group !== "ritual" && this.system.group !== "ceremony") {
            modifiers.push(new AttributeModification(
                { id: this.id, displayName: game.i18n.localize("tde5e.item.castable.modifiers.freeCasting.name") },
                -2,
                {
                    select: true,
                    values: {
                        "-2": "tde5e.item.castable.modifiers.freeCasting.gesture",
                        "-2.0": "tde5e.item.castable.modifiers.freeCasting.incantation",
                        "-4": "tde5e.item.castable.modifiers.freeCasting.both",
                    },
                    active: false,
                    deferred: true,
                },
            ));
        }

        const checkData = this.system.check;
        if (checkData?.resistedBySpirit) {
            modifiers.push(new AttributeModification(
                { id: this.id, displayName: game.i18n.localize("tde5e.actor.derived.spirit") },
                -1,
                {
                    select: true,
                    values: checkData.resistanceIsHalfed ? {
                        "-1": "1", "-1.0": "2", "-2": "3", "-2.0": "4", "-3": "5",
                    } : {
                        "-1": "1", "-2": "2", "-3": "3", "-4": "4", "-5": "5",
                    },
                    deferred: true,
                },
            ));
        } else if (checkData?.resistedByToughness) {
            modifiers.push(new AttributeModification(
                { id: this.id, displayName: game.i18n.localize("tde5e.actor.derived.toughness") },
                -1,
                {
                    select: true,
                    values: checkData.resistanceIsHalfed ? {
                        "-1": "1", "-1.0": "2", "-2": "3", "-2.0": "4", "-3": "5",
                    } : {
                        "-1": "1", "-2": "2", "-3": "3", "-4": "4", "-5": "5",
                    },
                    deferred: true,
                },
            ));
        }

        return modifiers;
    }
}
