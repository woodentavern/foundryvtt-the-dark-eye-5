import { hasValue, toLocaleFixed } from "../../util.js";
import PhysicalItem from "./physicalItem.js";

export class InventoryItem extends PhysicalItem {
    /** @override */
    static defaultIconPath = "systems/tde5e/assets/defaultItems/inventory_item.svg";

    /** @override */
    get isRollable() {
        return false;
    }

    /**
     * Checks if this item can be used as a light source.
     * @returns {boolean} True if the item can emit light, false otherwise.
     */
    get isLightSource() {
        return this.system.light.level > 0;
    }

    /** @override */
    get quantityInformation() {
        const info = super.quantityInformation;
        if (info) return info; // When there are charges, return those instead.
        return {
            path: "system.quantity",
            icon: "fas fa-box-open",
            value: this.system.quantity,
            max: 999,
            recharge: { type: "manual" },
        };
    }

    /** @inheritdoc */
    getSheetData() {
        const data = super.getSheetData();
        data.silverPriceText = (this.system.price / 100).toFixed(2);

        if (this.getFlag("tde5e", "trade")) {
            data.buyPriceText = this.buyPrice ? toLocaleFixed(this.buyPrice / 100) : "";
            data.sellPriceText = this.sellPrice ? toLocaleFixed(this.sellPrice / 100) : "";
        }

        return data;
    }

    /**
     * Toggles this item as a light source of all connected tokens.
     * @returns {Promise} A promise representing the token updates.
     */
    async activate() {
        if (!this.isLightSource) return;
        const lightInfo = this.system.light;

        // Loop over relevant tokens.
        const tokens = this.actor?.getActiveTokens(true, true) ?? [];
        for (const token of tokens) {
            if (token.getFlag("tde5e", "activeLightSource") === this.uuid) {
                // When this light source is already active, disable it.
                await token.update({
                    light: { dim: 0, bright: 0 },
                    "flags.tde5e.activeLightSource": null,
                });
            } else {
                // Otherwise, override with item's light settings.
                await token.update({
                    light: {
                        bright: lightInfo.falloff,
                        dim: lightInfo.level * lightInfo.falloff,
                        color: "#402d26",
                        alpha: lightInfo.level * 0.25,
                    },
                    "flags.tde5e.activeLightSource": this.uuid,
                });
            }
        }
    }

    /** @override */
    prepareBaseData() {
        super.prepareBaseData();

        if (!this.img || this.img === "icons/svg/mystery-man.svg") {
            this.img = "systems/tde5e/assets/defaultItems/inventory_item.svg";
        }

        const tradeData = this.getFlag("tde5e", "trade");
        if (tradeData) {
            const basePrice = this.system.price * this.system.quantity;
            const percentPerQl = this.actor?.uniqueItems.has("VERHANDLUNGSGESCHICK") ? 0.15 : 0.1;

            if (hasValue(tradeData.buyQl) || hasValue(tradeData.buyZone)) {
                const zoneFactor = this._calculateZonePrice(tradeData.sourceZone, tradeData.buyZone);
                this.buyPrice = basePrice * (1 + zoneFactor - (tradeData.buyQl ?? 0) * percentPerQl);
            } else {
                this.buyPrice = null;
            }

            if (hasValue(tradeData.sellQl) || hasValue(tradeData.sellZone)) {
                const zoneFactor = this._calculateZonePrice(
                    tradeData.sourceZone || tradeData.buyZone,
                    tradeData.sellZone || tradeData.buyZone,
                );
                this.sellPrice = basePrice * (1 + zoneFactor + (tradeData.sellQl ?? 0) * percentPerQl);
                this.sellProfit = this.sellPrice - (this.buyPrice ?? basePrice);
            } else {
                this.sellPrice = null;
                this.sellProfit = null;
            }
        }
    }

    /**
     * Determines the price increase factor for trading items between the two given zones.
     * @param {string} sourceZone The abbreviation of the zone that the item is from.
     * @param {string} targetZone The abbreviation of the current zone.
     * @returns {number} The factor for the item's price increase in the target zone.
     */
    _calculateZonePrice(sourceZone, targetZone) {
        if (!sourceZone || !targetZone) return 0;

        const row = TDE5E.tradeZoneFactors[sourceZone];
        const targetZoneIndex = Object.keys(TDE5E.tradeZoneFactors).indexOf(targetZone);
        if (!row || targetZoneIndex < 0) return 0;

        return row[targetZoneIndex];
    }
}

export default {
    default: InventoryItem,
};
