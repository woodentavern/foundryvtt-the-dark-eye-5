import { QualityModification } from "../../ui/rolls/rollModification.js";
import RollableItem from "./rollableItem.js";

export default class CombatTechnique extends RollableItem {
    /** @inheritdoc */
    getSheetData() {
        const data = super.getSheetData();
        data.attributeText = this.system.check.attributes
            .map(at => game.i18n.localize(`tde5e.actor.attributesShort.${at}`))
            .join("/");
        data.encumbranceClass = "fa-check";
        data.improvement = this.system.improvement;
        return data;
    }

    /**
     * Extends Foundry's method to update the combatant's equipment overview
     *  when the loadout changes.
     * @override
     */
    _onUpdate(data, options, userId) {
        super._onUpdate(data, options, userId);
        if (foundry.utils.hasProperty(data, "flags.tde5e.loadout") && game.combat?.round) {
            const combatants = game.combat.combatants.filter(turn => turn.actor === this.parent);
            if (!combatants.length) return;

            // TODO: Replace this logic for loadouts
            // Use JQuery to update the tooltip without rendering the entire tracker.
            const tracker = $("#combat-tracker");
            for (const combatant of combatants) {
                combatant._prepareEquipmentDetails();
                tracker.find(`li.combatant[data-combatant-id="${combatant.id}"] > .token-selection`)
                    .prop("title", combatant.equipmentDetails);
            }
        }
    }

    /** @override */
    get isCombatSkill() {
        return this.system.improvement.value > 0;
    }

    /**
     * Determines whether this combat technique represents ranged weapons
     * @returns {bool} True if this compat technique does ranged combat, false otherwise.
     */
    get isRanged() {
        return this.system.group === "ranged";
    }

    /** @inheritdoc */
    canUseLoadout(loadout) {
        return loadout.combatTechnique?.cid === this.cid;
    }

    /** @inheritdoc */
    canModifyRoll(item) {
        return item.id === this.id;
    }

    /** @inheritdoc */
    modifyRoll() {
        return [
            new QualityModification(
                { id: this.id, displayName: game.i18n.localize("tde5e.combat.modifiers.visibility.name") },
                -1,
                {
                    select: true,
                    values: {
                        "-1": "tde5e.combat.modifiers.visibility.obstructed",
                        "-2": "tde5e.combat.modifiers.visibility.silhouette",
                        "-3": "tde5e.combat.modifiers.visibility.shadowy",
                        "-4": "tde5e.combat.modifiers.visibility.invisible",
                    },
                    active: false,
                },
            ),
            new QualityModification(
                { id: this.id, displayName: game.i18n.localize("tde5e.combat.modifiers.multiHit.name") },
                -2,
                {
                    select: true,
                    values: {
                        "-2": "tde5e.combat.modifiers.multiHit.second",
                        "-4": "tde5e.combat.modifiers.multiHit.third",
                        "-6": "tde5e.combat.modifiers.multiHit.fourth",
                    },
                    active: false,
                },
            ),
        ];
    }

    /**
     * Checks if this combat technique can be used to perform the given combat application.
     * @param {Maneuver} item The action/reaction to check.
     * @param {Loadout?} loadout The current loadout of the combatant. If omitted, loadout restrictions aren't checked.
     * @returns {boolean} True if the combat application can be used, false otherwise.
     */
    canUseManeuver(item, loadout) {
        if (this.actor?.type === "creature") return true; // Creatures can use all maneuvers.

        // Check the active combat style if the maneuver requires one.
        const styleUuids = item.system.prerequisites?.combatStyle;
        if (loadout && styleUuids?.length) {
            if (!styleUuids.some(uuid => this.actor.getLocalItem(uuid) === loadout.combatStyle)) return false;
        }

        if (item.type === "action" || item.type === "reaction") {
            if (item.system.isRanged === null) return true;
            return item.system.isRanged === this.isRanged;
        }

        return true;
    }
}
