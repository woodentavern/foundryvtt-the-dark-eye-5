import WeaponItem from "./weaponItem.js";
import { MeleeWeapon } from "./meleeWeapon.js";
import { Shield } from "./shield.js";

export class RangedWeapon extends WeaponItem {
    /** @override */
    static defaultIconPath = "systems/tde5e/assets/defaultItems/ranged_weapon.svg";

    /** @override */
    get sheetVariantTemplate() {
        return "systems/tde5e/templates/items/variants/weaponVariant.hbs";
    }

    /** @inheritdoc */
    getSheetData() {
        const data = super.getSheetData();
        data.equipmentType = "ranged";
        data.range = this.system.range.join("/");
        data.baseRange = this._source.system.range.join("/");
        data.reloadTime = this.system.reloadTime;
        return data;
    }

    /**
     * @override
     */
    get localizedGroupName() {
        return game.i18n.localize(`tde5e.types.${this.type}`);
    }

    /**
     * Checks whether the provided weapon or shield can we used as an offhand in conjunction with this weapon.
     * @param {MeleeWeapon | RangedWeapon | Shield} offhandWeaponItem  The offhand weaponItem.
     * @param {boolean} canUseShields True if the actor can dual wield, fals otherwise.
     * @param {boolean} canDualWield True if the actor can use shields, false otherwise.
     * @returns {boolean} True if the current equipment allows an offhand item, false otherwise.
     */
    canHaveOffhand(offhandWeaponItem, canUseShields, canDualWield) {
        if (!offhandWeaponItem) return true;

        if (offhandWeaponItem instanceof Shield && canUseShields && this.allowsShield) return true;

        if (offhandWeaponItem instanceof MeleeWeapon
            && canDualWield
            && this.allowsOffhandWeapon
            && offhandWeaponItem.allowsOffhandWeapon) return true;

        return false;
    }

    /**
     * Determines whether this weapon can be used together with a shield.
     * @returns {boolean} True, if this weapon can be used with a shield, false otherwise.
     */
    get allowsShield() {
        return false;
    }

    /**
     * Determines whether this weapon can be used together with a second melee weapon.
     * Whether the other weapon is invalid (e.g. a two handed weapon or ranged weapon) is
     *  irrelevant here and should be checked on the other item as well.
     * @returns {boolean} True, if this weapon can be used with a shield, false otherwise.
     */
    get allowsOffhandWeapon() {
        return false;
    }
}

export default {
    default: RangedWeapon,
};
