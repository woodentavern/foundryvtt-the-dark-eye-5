import PhysicalItem from "./physicalItem.js";

/**
 * Enriches the physical item with capabilities to determine damage and
 *  calculate bonus damage. Can also display its combat related properties.
 */
export default class WeaponItem extends PhysicalItem {
    /**
     * Gets all tradition UUIDs that this weapon can be used for.
     * @returns {string[]} A list of allowed tradition UUIDs.
     */
    get traditions() {
        return (this.system.belongsMagicalTradition ?? []).concat(this.system.belongsBlessedTradition ?? []);
    }

    /**
     * @returns {object?} The effects of this weapon's current wear.
     */
    get wearEffect() {
        if (!this.canBreak || this.system.wear <= 0) return null;
        return TDE5E.wearWeaponEffects[this.system.wear];
    }

    /** @inheritdoc */
    async roll(rollMode) {
        const diceShort = game.i18n.localize("tde5e.diceShort");
        const damageText = this.damageText.replace(diceShort, "d");
        return new Roll(damageText).toMessage(null, { rollMode });
    }

    /** @inheritdoc */
    getSheetData() {
        const data = super.getSheetData();
        data.damage = this.damageText;
        data.adrenalineMod = this.system.adrenaline;
        data.damageTypes = this.damageTypesIcons;
        data.canBreak = this.canBreak;
        data.isNatural = this.isNatural;
        data.damageTypesTooltip = data.damageTypes.map(d => d.name).join(", ");
        data.breakingPointRating = this.system.breakingPointRating;

        if (this.system.group) {
            data.combatTechnique = this.combatTechnique?.name;
            data.combatTechniqueLink = this.combatTechnique?.createLink()
                ?? game.i18n.localize("tde5e.general.notFound");
            data.attributeText = this.combatTechnique?.system.check.attributes
                .map(at => game.i18n.localize(`tde5e.actor.attributesShort.${at}`))
                .join("/");
        }

        return data;
    }

    /**
     * @override
     */
    prepareBaseData() {
        super.prepareBaseData();

        const { prefix, wearEffect } = this;
        this.system.adrenaline = (this._source.system.adrenaline ?? 0)
            + (wearEffect?.adrenaline ?? 0)
            + (prefix.adrenaline ?? 0);
        this.system.breakingPointRating = (this._source.system.breakingPointRating)
            + (wearEffect?.breakingPointRating ?? 0)
            + (prefix.breakingPointRating ?? 0);

        // Damage Text
        const diceShort = game.i18n.localize("tde5e.diceShort");
        const { damage } = this._source.system;
        this.baseDamageText = `${damage.diceNumber}${diceShort}${damage.diceSides}${damage.flat.signedString()}`;
        this.damageText = this.baseDamageText; // Unmodified damage without actor.

        if (this._source.system.group) this.combatTechnique = this.actor?.getLocalItem(this._source.system.group);
    }

    /** @override */
    prepareActorData() {
        // Bonus damage from attributes.
        this.prepareBonusDamage();

        if (this.canBreak && this.system.wear === 4) {
            this.damageText = "-";
        } else {
            const { prefix, wearEffect } = this;
            const { damage } = this.system;
            const flatDamage = (this.bonusDamage ?? 0) + damage.flat
                + (wearEffect?.damage ?? 0)
                + (prefix.damage ?? 0);
            this.damageText = damage.diceNumber.toString()
                + game.i18n.localize("tde5e.diceShort")
                + damage.diceSides.toString()
                + flatDamage.signedString();
        }
    }

    /**
     * Calculates and sets the bonus damage for the weapon.
     */
    prepareBonusDamage() {
        if (!this.actor || !this.combatTechnique) {
            this.bonusDamage = 0;
            return;
        }

        const damageBonusAttribute = this.combatTechnique.system.check.attributes
            .map(a => this.actor.system.attributes[a].value)
            .sort()[1];
        const thresholds = this.system.damage.range;
        this.bonusDamage = Math.min(0, damageBonusAttribute - thresholds[0])
            + Math.max(0, damageBonusAttribute - thresholds[1]);
    }

    /** @override */
    get canHavePrefix() {
        return true;
    }

    /**
     * Gets an array of objects that hold the icon, the order and the name of the damageTypes this weapon can apply.
     * @returns {Array.<object>}  An array of icon, name and order-number.
     */
    get damageTypesIcons() {
        if (!this.system.damage.types) return []; // Weapon is broken

        const types = [...this.system.damage.types].map(type => ({
            img: TDE5E.damageTypeIcons[type]?.icon,
            order: TDE5E.damageTypeIcons[type]?.order ?? 0,
            name: game.i18n.localize(`tde5e.damageTypes.${type}`),
        }));

        types.sort((t1, t2) => t1.order - t2.order);
        return types;
    }

    /** @inheritdoc */
    _preDelete(options, user) {
        super._preDelete(options, user);
        if (!this.actor) return;

        // Remove the weapons from its actor's loadouts.
        const loadouts = foundry.utils.deepClone(this.actor._source.system.loadouts);
        let update = false;
        for (const loadout of loadouts) {
            if (loadout.mainhand === this.id) {
                loadout.mainhand = null;
                loadout.offhand = null;
                update = true;
            } else if (loadout.offhand === this.id) {
                loadout.offhand = null;
                update = true;
            }
        }

        if (update) this.actor.update({ "system.loadouts": loadouts });
    }
}
