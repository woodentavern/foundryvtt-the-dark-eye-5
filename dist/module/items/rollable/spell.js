import CastableItem from "./castableItem.js";
import { hasValue } from "../../util.js";

export class Spell extends CastableItem {
    /** @override */
    get isCombatSkill() {
        return this.system.check && this.system.group !== "ritual";
    }

    /** @inheritdoc */
    getSheetData() {
        const data = super.getSheetData();
        data.property = game.i18n.localize(`tde5e.properties.${this._source.system.property}`);
        return data;
    }

    /** @override */
    get isRollable() {
        return hasValue(this._source.system.check);
    }
}

export default {
    default: Spell,
};
