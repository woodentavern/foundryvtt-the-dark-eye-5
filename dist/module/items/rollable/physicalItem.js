import RollableItem from "./rollableItem.js";
import * as util from "../../util.js";
import { promptVariant } from "../../ui/variantPicker.js";

/**
 * Extends the rollable item class to include information about inventory data such as weight.
 * Is used to describe items that are part of the equipment and physical in nature.
 */
export default class PhysicalItem extends RollableItem {
    // Items: MeleeWeapon, RangedWeapon, Armor, Shield, InventoryItem
    /** @inheritdoc */
    getSheetData() {
        const data = super.getSheetData();
        data.quantity = this.system.quantity ?? 1;
        data.totalWeight = this.totalWeight;
        return data;
    }

    /** @override */
    prepareBaseData() {
        super.prepareBaseData();

        // Total weight
        if (this.group === "WURFWAFFEN") this.totalWeight = this.system.weight * this.system.ammunitionAmount;
        else this.totalWeight = this.system.weight * (this.system.quantity ?? 1);
        this.totalWeight = Math.round(this.totalWeight); // Round to 1 ounce precision.
    }

    /** @override */
    migrate(data) {
        const update = super.migrate(data);

        const oldLootSheet = this.getFlag("tde5e", "lootSheet");
        if (oldLootSheet) {
            update["system.rarity"] = oldLootSheet.rarity?.value ?? "common";
            update["system.traits"] = [oldLootSheet.lootNature?.value ?? "profane"];
            update["system.flavor"] = `${data.system.flavor}${oldLootSheet.look?.value}`;
            update["flags.tde5e.-=lootSheet"] = null;
            util.debug(`Migrated old lootSheet data for item '${data.name}'`);
        }

        return update;
    }

    /** @override */
    static async allowCreation(data, actor) {
        const variants = data.system.variant?.available;
        if (variants?.length && !await promptVariant(data, actor, [], true)) return false;
        // TODO: Handle merge of items and increase quantity
        return true;
    }

    /**
     * Serializes and translates the given modifier object into a comma separated list of modifiers.
     * @param {object} prefix The object containing the modifiers.
     * @returns {string} The string representation of the modifiers.
     */
    _createModifierText(prefix) {
        return Object.entries(prefix)
            .map(([key, value]) => {
                const prop = key === "adrenaline"
                    ? "tde5e.actor.resources.adrenalineShort"
                    : `tde5e.item.equipment.${key}Short`;
                return `${game.i18n.localize(prop)} ${value.signedString()}`;
            })
            .join(", ");
    }

    /** @inheritdoc */
    _createDisplayName() {
        if (foundry.utils.getProperty(this, "flags.tde5e.propertyMetadata.name.noMigration")) return this.name;
        const variantName = this.system.variant?.selected?.name;
        if (variantName) return variantName;
        return super._createDisplayName();
    }

    /**
     * Determiens whether this item can accept prefixes
     * @returns {boolean} True if prefixes can be applied to this weapon, false otherwise.
     */
    get canHavePrefix() {
        return false;
    }

    /**
     * Determines whether this item can break, i.e. receive levels of wear.
     * @returns {boolean} If true, this weapon can receive levels of wear, false otherwise.
     */
    get canBreak() {
        return util.hasValue(this.system.wear) && !this.system.isUnbreakable;
    }

    /**
     * Retreives the modifier data of this weapon's prefix.
     * @returns {object} An object containing prefix modifiers.
     *  Note that this should be considered read-only.
     */
    get prefix() {
        return this.system.prefix ? TDE5E.prefixes[this.type][this.system.prefix] : {};
    }
}
