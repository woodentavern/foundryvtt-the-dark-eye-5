import PhysicalItem from "./physicalItem.js";

class Ammunition extends PhysicalItem {
    /** @inheritdoc */
    static defaultIconPath = "systems/tde5e/assets/defaultItems/ammunition.svg";

    /** @override */
    get isRollable() {
        return false;
    }

    /** @override */
    get quantityInformation() {
        const info = super.quantityInformation;
        if (info) return info; // When there are charges, return those instead.
        return {
            path: "system.quantity",
            icon: "fas fa-bow-arrow",
            value: this.system.quantity,
            max: 999,
            recharge: { type: "manual" },
        };
    }

    /** @inheritdoc */
    getSheetData() {
        const data = super.getSheetData();
        data.groupName = game.i18n.localize(`tde5e.types.ammunition.${this.system.group}`);
        return data;
    }
}

export default {
    default: Ammunition,
};
