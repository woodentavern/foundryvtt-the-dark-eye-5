import RollableItem from "./rollableItem.js";

class Maneuver extends RollableItem {
    /** @inheritdoc */
    static defaultIconPath = "systems/tde5e/assets/defaultItems/action.svg";

    /** @override */
    get isRollable() {
        return false;
    }
}

export default {
    default: Maneuver,
};
