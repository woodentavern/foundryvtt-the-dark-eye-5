import WeaponItem from "./weaponItem.js";

export class Shield extends WeaponItem {
    /** @override */
    static defaultIconPath = "systems/tde5e/assets/defaultItems/shield.svg";

    /**
     * @returns {object?} The effects of this shield's current wear.
     */
    get wearEffect() {
        if (this.system.wear <= 0) return null;
        return TDE5E.wearShieldEffects[this.system.wear];
    }

    /** @override */
    getSheetData() {
        const data = super.getSheetData();
        data.equipmentType = "shield";
        data.canBreak = true;
        return data;
    }
}

export default {
    default: Shield,
};
