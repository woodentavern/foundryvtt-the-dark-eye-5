import PhysicalItem from "./physicalItem.js";
import { PreparationPriority } from "../item.js";

export class Armor extends PhysicalItem {
    preparationPriority = PreparationPriority.AFTER_MODIFIED;

    /** @override */
    static defaultIconPath = "systems/tde5e/assets/defaultItems/armor.svg";

    /** @override */
    get isRollable() {
        return false;
    }

    /**
     * @returns {object?} The effects of this armor's current wear.
     */
    get wearEffect() {
        if (!this.canBreak || this.system.wear <= 0) return null;
        return TDE5E.wearArmorEffects[this.system.wear];
    }

    /** @inheritdoc */
    getSheetData() {
        const data = super.getSheetData();
        data.protection = this.protectionText;
        data.encumbrance = this.system.encumbrance;
        data.adrenalineMod = this.system.adrenaline;
        data.canBreak = this.canBreak;
        data.isNatural = this.isNatural;
        data.breakingPointRating = this.system.breakingPointRating;
        return data;
    }

    /** @inheritdoc */
    prepareBaseData() {
        super.prepareBaseData();

        const { prefix, wearEffect } = this;
        const { protection } = this.system;

        // Apply bonus & penalties
        if (this.canBreak && this.system.wear >= 4) {
            ["pierce", "slash", "blunt"].forEach(type => {
                protection[type] = 0;
            });
        } else {
            Object.assign(protection, this.system._source.protection); // Reset in case of duplicate preparation.
            if (prefix?.protection) {
                ["pierce", "slash", "blunt"].forEach(type => {
                    protection[type] = Math.max(0, protection[type] + prefix.protection);
                });
            }

            if (wearEffect?.protection) {
                protection.modifiers = [[game.i18n.localize("tde5e.item.equipment.wear"), wearEffect.protection]];
                ["pierce", "slash", "blunt"].forEach(type => {
                    protection[type] = Math.max(0, protection[type] + wearEffect.protection);
                });
            }
        }

        this.system.encumbrance = this._source.system.encumbrance
            + (prefix?.encumbrance ?? 0)
            + (wearEffect?.encumbrance ?? 0);
        this.system.adrenaline = this._source.system.adrenaline
            + (prefix?.adrenaline ?? 0);
        this.system.breakingPointRating = (this._source.system.breakingPointRating)
            + (prefix?.breakingPointRating ?? 0);
    }

    /** @inheritdoc */
    prepareActorData() {
        if (this.system.isSelected) {
            this.actor.uniqueItems.get("BELASTUNG")?.modifyValue(this.name, this.system.encumbrance);
        }

        const { protection } = this.system;
        if (protection.mod) {
            const userMod = this._source.system.protection.mod;
            if (userMod) {
                protection.modifiers ??= [];
                protection.modifiers.push([game.i18n.localize("tde5e.general.user"), userMod]);
                ["pierce", "slash", "blunt"].forEach(type => {
                    protection[type] = Math.max(0, protection[type] + protection.mod);
                });
            }
        }

        // Prepare properties derived from protection.
        this.totalProtection = protection.pierce + protection.slash + protection.blunt;
        this.protectionText = `${protection.pierce}/${protection.slash}/${protection.blunt}`;
    }

    /** @override */
    get canHavePrefix() {
        return true;
    }
}

class Unprotected extends Armor {
    /** @override */
    get canHavePrefix() {
        return false;
    }

    /** @override */
    get isNatural() {
        return true;
    }
}

export default {
    default: Armor,
    UNGERUSTET: Unprotected,
};
