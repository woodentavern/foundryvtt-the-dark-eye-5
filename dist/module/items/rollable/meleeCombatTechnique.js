import { QualityModification } from "../../ui/rolls/rollModification.js";
import CombatTechnique from "./combatTechnique.js";

class MeleeCombatTechnique extends CombatTechnique {
    /** @inheritdoc */
    modifyRoll() {
        return super.modifyRoll().concat([
            new QualityModification(
                { id: this.id, displayName: game.i18n.localize("tde5e.combat.modifiers.stability.name") },
                -1,
                {
                    select: true,
                    values: {
                        "-1": "tde5e.combat.modifiers.stability.unsteady",
                        "-1.0": "tde5e.combat.modifiers.stability.mediumWater",
                        "-2": "tde5e.combat.modifiers.stability.highWater",
                        "-3": "tde5e.combat.modifiers.stability.underwater",
                    },
                    active: false,
                },
            ),
            new QualityModification(
                { id: this.id, displayName: game.i18n.localize("tde5e.combat.modifiers.size.name") },
                -1,
                {
                    select: true,
                    values: {
                        "-1": "tde5e.combat.modifiers.size.small",
                        "-2": "tde5e.combat.modifiers.size.tiny",
                    },
                    active: false,
                },
            ),
            new QualityModification(
                { id: this.id, displayName: game.i18n.localize("tde5e.combat.modifiers.target.name") },
                1,
                {
                    select: true,
                    values: {
                        "-2": "tde5e.combat.modifiers.target.riding",
                        1: "tde5e.combat.modifiers.target.prone",
                        "1.0": "tde5e.combat.modifiers.target.bound",
                    },
                    active: false,
                },
            ),
            new QualityModification(
                { id: this.id, displayName: game.i18n.localize("tde5e.combat.modifiers.position.name") },
                1,
                {
                    select: true,
                    values: {
                        1: "tde5e.combat.modifiers.position.elevated",
                        2: "tde5e.combat.modifiers.position.riding",
                    },
                    active: false,
                },
            ),
        ]);
    }
}

class ImpactWeapons extends MeleeCombatTechnique {
    /** @override */
    get isCombatSkill() {
        return true; // Fists are always an option.
    }
}
export default {
    STANGENWAFFEN: MeleeCombatTechnique,
    KLINGENWAFFEN: MeleeCombatTechnique,
    SCHLAGWAFFEN: ImpactWeapons,
    GEISSELWAFFEN: MeleeCombatTechnique,
    WUCHTWAFFEN: MeleeCombatTechnique,
};
