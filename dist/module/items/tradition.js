import TdeItem, { PreparationPriority } from "./item.js";
import promptReplace from "../ui/replaceDialog.js";
import promptMultiSelect from "../ui/controls/multiSelectPicker.js";

class Tradition extends TdeItem {
    preparationPriority = PreparationPriority.AFTER_DERIVED;

    /** @override */
    getSheetData() {
        const data = super.getSheetData();
        data.primaryAttribute = this._source.system.primaryAttribute
            ? game.i18n.localize(`tde5e.actor.attributes.${this._source.system.primaryAttribute}`)
            : game.i18n.localize("tde5e.general.none");

        const shortName = (!this.system.shortName) ? this.name : this.system.shortName;
        data.traditionLink = `<a class="content-link" data-link data-uuid="${this.uuid}">${shortName}</a>`;

        return data;
    }

    /** @override */
    prepareActorData() {
        const modifiedResource = this.system.group === "blessed"
            ? this.actor.system.resources.karmaPoints
            : this.actor.system.resources.arcaneEnergy;
        const bonus = this.resourceBonus;

        if (!modifiedResource.modifiers) {
            this.actor.modify(this.name, modifiedResource, "base", bonus);
        } else {
            // Compare with unmodified base value so that only the best tradition is applied.
            const resourceValue = 20 + bonus + modifiedResource.bought;
            if (resourceValue > modifiedResource.base) {
                modifiedResource.modifiers.push([this.name, bonus]);
                modifiedResource.base = resourceValue;
            }
        }
    }

    /**
     * Gets the bonus to the resource (KP, AE) that the primaryAttribute
     *  of this tradition provides to the actor.
     * @returns {number} A zero-positive value.
     */
    get resourceBonus() {
        if (!this.actor) return 0;
        if (!this.system.primaryAttribute) return 0;
        return this.actor.system.attributes[this.system.primaryAttribute].base;
    }
}

class Animist extends Tradition {
    get resourceBonus() {
        return Math.round(super.resourceBonus / 2);
    }
}

class MagicalAlchemist extends Tradition {
    get resourceBonus() {
        return Math.round(super.resourceBonus / 2);
    }
}

class MagicalBard extends Tradition {
    get resourceBonus() {
        return Math.round(super.resourceBonus / 2);
    }
}

class MagicalDancer extends Tradition {
    get resourceBonus() {
        return Math.round(super.resourceBonus / 2);
    }
}

class TraditionTsa extends Tradition {
    /** @inheritdoc */
    static async allowCreation(data, actor) {
        // User needs to  pick 4 of these
        const selectionOptions = {
            selectionOptions: ["skills.HEILKUNDE_SEELE", "skills.HEILKUNDE_WUNDEN",
                "skills.HOLZBEARBEITUNG", "skills.MALEN_UND_ZEICHNEN",
                "skills.METALLBEARBEITUNG", "skills.PFLANZENKUNDE",
                "skills.STOFFBEARBEITUNG", "skills.TIERKUNDE"],
            maxSelectionCount: 4,
            minSelectionCount: 4,
        };

        const selectedSkills = await promptMultiSelect(selectionOptions);
        if (!selectedSkills) return false;

        // Add selected skills
        data.system.favoredSkills = data.system.favoredSkills.concat(selectedSkills);

        // Replace if exists
        const existing = actor.uniqueItems.get(data.system.cid);
        const allow = existing ? await promptReplace(existing) : true;

        // Finalize
        if (!allow) util.error(`Can't add item ${data.name} because it already exists.`, false, true);
        return allow;
    }
}

class TraditionAves extends Tradition {
    preparationPriority = PreparationPriority.AFTER_DERIVED;

    /** @override */
    prepareActorData() {
        super.prepareActorData();
        this.actor.uniqueItems.get("BETAUBUNG")?.modifyValue(this.name, -1);
    }
}

export default {
    default: Tradition,
    TRADITION_AVESKIRCHE: TraditionAves,
    TRADITION_ANIMISTEN: Animist,
    TRADITION_ZAUBERALCHIMISTEN: MagicalAlchemist,
    TRADITION_ZAUBERBARDE: MagicalBard,
    TRADITION_ZAUBERTANZER: MagicalDancer,
    TRADITION_TSAKIRCHE: TraditionTsa,
};
