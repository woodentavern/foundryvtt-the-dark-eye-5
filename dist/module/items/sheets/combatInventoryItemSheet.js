import TdeBaseItemSheet from "./baseItemSheet.js";

/**
 * Item sheet for weapons.
 */
export default class CombatInventoryItemSheet extends TdeBaseItemSheet {
    /** @inheritdoc */
    async getData() {
        const data = await super.getData();
        data.prefixes = Object.entries(TDE5E.prefixes[data.data.type]).reduce((map, [key, value]) => {
            const mod = this.item._createModifierText(value);
            map[key] = `${game.i18n.localize(`tde5e.item.equipment.prefixes.${key}`)}: ${mod}`;
            return map;
        }, {});
        return data;
    }
}
