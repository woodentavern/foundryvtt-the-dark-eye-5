/**
 * Form application that implements per-item configuration options.
 */
export default class TdeItemConfig extends FormApplication {
    /** @override */
    static get defaultOptions() {
        return foundry.utils.mergeObject(super.defaultOptions, {
            id: "actor-config",
            template: "systems/tde5e/templates/items/itemConfig.hbs",
            width: 420,
        });
    }

    /**
     * Prepares the display data of the dialog to include our internal flags.
     * @returns {object} The data required for rendering the dialog.
     * @override
     */
    getData() {
        return {
            data: this.object,
            isLootItem: this.object.isLootItem,
            isGlobal: !this.object.isEmbedded,
            chargeTypes: foundry.utils.getProperty(game.i18n.translations, "tde5e.item.config.chargeType"),
        };
    }

    /** @override */
    activateListeners(html) {
        super.activateListeners(html);

        html.find("select.charge-type").change(ev => {
            const { value } = ev.currentTarget;
            if (!value) return this.object.unsetFlag("tde5e", "charges").then(() => this.render());

            // Use existing values or set defaults.
            let chargeConfig = this.object.getFlag("tde5e", "charges");
            if (chargeConfig) chargeConfig.recharge.type = value;
            else {
                chargeConfig = {
                    value: 3,
                    max: 3,
                    recharge: {
                        type: value,
                        text: "",
                    },
                };
            }
            if (value !== "manual") {
                chargeConfig.recharge.interval ??= 24;
                if (value !== "afterUsageParallel") chargeConfig.recharge.amount ??= 1;
            }

            return this.object.setFlag("tde5e", "charges", chargeConfig).then(() => this.render());
        });
    }

    /**
     * Specifies the title of the dialog with the item's name.
     * @returns {string} The title of the configuration dialog.
     * @override
     */
    get title() {
        return `${this.object.name}: ${game.i18n.localize("tde5e.actor.config.title")}`;
    }

    /**
     * Updates the associated item with the form data settings.
     * @returns {Promise.<void>} A promise representing the update operation.
     * @override
     */
    async _updateObject(_event, formData) {
        return this.object.update(formData);
    }
}
