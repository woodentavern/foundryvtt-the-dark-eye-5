import * as util from "../../util.js";
import TdeItem from "../item.js";
import TdeItemConfig from "./itemConfig.js";
import GlobalContextMenu from "../../ui/globalContextMenu.js";
import { parseArrays, removeDefaultMetadata } from "../../formDataParsing.js";
import UnitControl from "../../ui/controls/unitControl.js";
import { migrateItem, migrateEmbeddedItemData } from "../../migration.js";
import AbilityTree from "../../ui/abilityTree.js";

/**
 * Base sheet for items. The sheet is rendered in read-only mode by default - editing can be activated using a button
 *  in the header. Properties marked as "alwaysEditable" in the metadata can always be edited if the user has the
 *  permission to do so.
 */
export default class TdeBaseItemSheet extends ItemSheet {
    /**
     * Indicates whether this sheet is rendered for a GM.
     * @type {boolean}
     */
    gmMode = game.user.isGM;

    /**
     * Stores a set of fieldset groups to check for duplicates.
     * @type {Set<string>}
     */
    groups = new Set();

    /**
     * Contains transient changes that weren't applied to the item yet.
     * @type {object}
     */
    temporaryData = null;

    /**
     * Precompiled Handlepars template for rendering GM controls without asynchronous code.
     * @type {Function}
     */
    static gmControls = null;

    /** @override */
    static get defaultOptions() {
        return foundry.utils.mergeObject(super.defaultOptions, {
            template: "systems/tde5e/templates/items/item.hbs",
            classes: ["sheet", "tde-item"],
            width: 600,
            closeOnSubmit: false,
            submitOnClose: true,
            submitOnChange: true,
            resizable: true,
            editing: false,
            focusNotes: false,
            baseApplication: "TdeBaseItemSheet",
        });
    }

    /**
     * Mapping for each rarity to a respective icon.
     */
    static rarityIcons = {
        ubiquitous: "",
        common: "<i class=\"rarity common fa-solid fa-star\"></i>",
        uncommon: "<i class=\"rarity uncommon fa-solid fa-star\"></i>".repeat(2),
        rare: "<i class=\"rarity rare fa-solid fa-star\"></i>".repeat(3),
        superRare: "<i class=\"rarity super-rare fa-solid fa-star\"></i>".repeat(4),
        unique: "<i class=\"rarity unique fa-solid fa-star\"></i>".repeat(5),
        fate: "<i class=\"rarity fate fa-solid fa-star\"></i>".repeat(6),
    };

    /**
     * Replaces the title of the window to add the item's location.
     * @override
     */
    get title() {
        if (this.item.compendium) return `${this.item.compendium.metadata.label} - ${this.item.name}`;
        if (this.item.actor) return `${this.item.actor.name} - ${this.item.name}`;
        return `${game.i18n.localize("DOCUMENT.Items")} - ${this.item.name}`;
    }

    /**
     * Gets an icon representing the item's location.
     * @returns {string} An HTML string containing the source icon.
     */
    getSourceIcon() {
        const { compendium } = this.item;
        let icon = "fa-suitcase";
        let title = "";
        if (compendium) {
            icon = compendium.collection.startsWith("world.") ? "fa-atlas" : "fa-book";
            title = compendium.metadata.label;
        } else if (this.item.actor) {
            icon = "fa-user";
            title = this.item.actor.name;
        }

        return `<i class="fas ${icon}" title="${title}"></i>`;
    }

    /**
     * Gets a list of sources for the item.
     * @returns {string} An HTML string containing the sources.
     */
    getSources() {
        const sources = [];
        const { compendiumId } = this.item;
        if (compendiumId) {
            // Display a link to the source compendium.
            const icon = compendiumId.startsWith("world.") ? "fa-atlas" : "fa-book";
            let compendiumLink = `<a class="compendium-link" data-pack="${compendiumId}">
                <i class="folder fas ${icon}"></i> ${this.item.localizedGroupName}</a>`;
            if (this.item.cid && !this.item.compendium) {
                compendiumLink += ` > ${TdeItem.createLinkedItemList([this.item._stats.compendiumSource])}`;
            }

            sources.push(compendiumLink);
        } else if (this.item._source.system.group) {
            // Display group if the source isn't valid.
            sources.push(this.item.localizedGroupName);
        }

        if (this.item._source.system.sources?.length) {
            const bookSources = this.item._source.system.sources
                .filter(Boolean)
                .map(source => game.i18n.localize(`tde5e.books.${source}`));
            sources.push(...bookSources);
        }

        return sources.join(", ");
    }

    /**
     * Data preparation method that is called before the sheet is rendered.
     * @inheritdoc
     */
    async getData() {
        this.options.isEditable = this.isEditable;
        if (!this.options.headerClass) this.options.headerClass = this.item.sheetHeaderClass;
        const uiData = {
            config: TDE5E,
            partials: [this.item.sheetContentTemplate],
            options: this.options,
            data: this.temporaryData ?? this.item._source,
            childReferences: this.options.editing ? [] : this.item.children,
            img: this.item.img,
            displayName: this.item.displayName,
            uuid: this.item.uuid,
            sources: this.getSources(),
            canEdit: this.canEdit.bind(this),
            canView: this.canView.bind(this),
            rarityIcons: TdeBaseItemSheet.rarityIcons,
        };

        // Add romanized levels
        if (uiData.data.system.purchase?.levels > 1) {
            uiData.levels = [];
            for (let i = 1; i <= uiData.data.system.purchase.levels; i++) {
                uiData.levels.push({ value: i, text: util.romanize(i) });
            }
        }

        if (uiData.img?.includes("defaultItems/")) uiData.img = null;
        if (uiData.data.system.variant?.available?.length) uiData.partials.push(this.item.sheetVariantTemplate);

        const localizedType = game.i18n.translations.tde5e.types?.[uiData.data.type];
        if (typeof (localizedType) === "object") {
            uiData.groups = foundry.utils.deepClone(localizedType);
            delete uiData.groups.name;
        }
        return uiData;
    }

    /**
     * Checks if the property with the given name can currently be edited. This considers the state of the sheet, the
     *  metadata of the property and the permissions of the user.
     * @param {string} name The name of the property.
     * @returns {boolean} True if the property can be edited, false otherwise.
     */
    canEdit(name) {
        if (!this.isEditable) return false;

        const metadata = this.item.getPropertyMetadata(name);
        if (!(this.options.editing || metadata.alwaysEditable)) return false;
        return game.user.isGM || !metadata.locked;
    }

    /**
     * Checks if the property with the given name is currently visible. This considers the metadata of the property and
     *  the permissions of the user.
     * @param {string} name The name of the property.
     * @returns {boolean} True if the property is visible, false otherwise.
     */
    canView(name) {
        const metadata = this.item.getPropertyMetadata(name);
        return game.user.isGM || !metadata.hidden;
    }

    /**
     * Extends Foundry's method to add the item specific templates and configurable extensions, such as the loot sheet
     *  or charge configuration options.
     * @override
     */
    async _renderInner(data) {
        // Make sure that partials are loaded.
        await loadTemplates(data.partials);
        if (this.gmMode && this.options.editing) {
            this.constructor.gmControls ??= await getTemplate(
                "systems/tde5e/templates/items/components/gmControls.hbs",
            );
        }

        // Render the content of the sheet.
        const html = await super._renderInner(data);

        // Hide & disable inputs based on state and metadata.
        this.groups.clear(); // Reset known groups before processing.
        html.find("fieldset").each((_, group) => this.processFieldset(group));
        return html;
    }

    /**
     * Supplements the given fieldset using the associated property metadata.
     * This adds a legend item if the property has a label and hides or locks the inputs based on the sheet states,
     *  user permissions and metadata flags.
     * @param {HTMLFieldSetElement} group The group of inputs to process.
     */
    processFieldset(group) {
        const name = group.dataset.group;
        if (!name) return; // Don't touch unidentified groups.

        const metadata = this.item.getPropertyMetadata(name);

        // Create label for the group.
        let legend;
        if (metadata.label && metadata.label !== name) {
            legend = document.createElement("legend");
            legend.innerHTML = `${(metadata.icon ? `<img src="${metadata.icon}">` : "")
                + metadata.label
                + (metadata.hint ? `<i class="fa-solid fa-circle-info hint" title="${metadata.hint}"></i>` : "")
            }:`;
            group.prepend(legend);
        }

        // Skip permission checks for hidden inputs.
        if (metadata.hidden && !this.gmMode) {
            group.style.display = "none";
            return;
        }

        // Evaluate permissions.
        if (this.options.editing) {
            // In editing mode, disable the group to disable child inputs.
            if (this.gmMode) {
                (legend ?? group.querySelector("legend") ?? group)
                    .insertAdjacentHTML("afterbegin", this.constructor.gmControls({
                        name,
                        metadata,
                        labelOnly: this.groups.has(name),
                    }));
                this.groups.add(name);
            } else if (metadata.locked) {
                group.disabled = true;
            }
        } else if (!this.isEditable || !metadata.alwaysEditable || (metadata.locked && !this.gmMode)) {
            // In read-only mode, replace the input entirely if the user doesn't have access.
            this.replaceInputs(group);
        }
    }

    /**
     * Replaces the enabled inputs within the given fieldset with their value.
     * @param {HTMLFieldSetElement} group The group of inputs to disable.
     */
    replaceInputs(group) {
        const inputs = group.querySelectorAll("input,textarea,select");
        for (const input of inputs) {
            if (input.disabled) continue; // Don't touch already disabled inputs.
            if (input.type === "radio") {
                // Hide the box and unchecked radio labels.
                (input.checked ? input : input.parentElement).style.display = "none";
                continue;
            }

            const valueSpan = document.createElement("span");
            if (input.tagName === "SELECT") {
                valueSpan.innerHTML = input.options[input.selectedIndex]?.text;
            } else if (input.type === "checkbox") {
                valueSpan.innerHTML = game.i18n.localize(`tde5e.choices.${input.checked ? "yes" : "no"}`);
            } else {
                valueSpan.innerHTML = input.value;
            }
            input.replaceWith(valueSpan);
        }

        const customInputs = group.querySelectorAll("item-tags,multi-select-ext");
        for (const input of customInputs) input.disabled ||= true;
    }

    /**
     * Extends Foundry's method to adjust the display of the title.
     * @override
     */
    async _renderOuter() {
        const html = await super._renderOuter();
        const title = html.find("h4.window-title");
        title.before(this.getSourceIcon());

        GlobalContextMenu.create(this, title.parent(), null, this._getHeaderContextOptions());
        return html;
    }

    /**
     * Extends the method to remove the sheet configuration button and add a
     *  delete button for the item.
     * @returns {object[]} The adjusted list of header buttons.
     * @override
     */
    _getHeaderButtons() {
        const buttons = super._getHeaderButtons().filter(button => button.class !== "configure-sheet");

        if (!this.item.compendium && (game.user.isGM || !this.item.getFlag("tde5e", "preventDelete"))) {
            buttons.unshift({
                label: game.i18n.localize("tde5e.general.remove"),
                class: "delete",
                icon: "fas fa-trash",
                onclick: () => this.object.deleteConfirm(),
            });
        }

        if (this.isEditable) {
            if (game.user.isGM || !this.item.locked) {
                buttons.unshift({
                    label: game.i18n.localize("tde5e.general.edit"),
                    class: "edit",
                    icon: "fas fa-edit",
                    onclick: this.toggleEdit.bind(this),
                });
            }

            if (game.user.isGM) {
                buttons.unshift({
                    label: game.i18n.localize("tde5e.actor.config.title"),
                    class: "item-config",
                    icon: "fas fa-cog",
                    onclick: this.configure.bind(this),
                });
            }
        }

        return buttons;
    }

    /**
     * Builds an array of options for the sheet header context menu.
     * @returns {Array[]} An array of context menu options.
     */
    _getHeaderContextOptions() {
        const entries = [
            {
                name: game.i18n.localize("tde5e.sheetHeader.copyId"),
                icon: "<i class='far fa-clipboard'></i>",
                callback: () => {
                    game.clipboard.copyPlainText(this.object.id);
                    ui.notifications.info(game.i18n.format("DOCUMENT.IdCopiedClipboard", {
                        label: game.i18n.localize(this.object.constructor.metadata.label),
                        type: "ID",
                        id: this.object.id,
                    }));
                },
            },
            {
                name: game.i18n.localize("tde5e.sheetHeader.copyUuid"),
                icon: "<i class='far fa-clipboard'></i>",
                callback: () => {
                    game.clipboard.copyPlainText(this.object.uuid);
                    ui.notifications.info(game.i18n.format("DOCUMENT.IdCopiedClipboard", {
                        label: game.i18n.localize(this.object.constructor.metadata.label),
                        type: "UUID",
                        id: this.object.uuid,
                    }));
                },
            },
            {
                name: game.i18n.localize("tde5e.sheetHeader.shareLink"),
                icon: "<i class='far fa-share'></i>",
                callback: () => this.item.share(),
            },
        ];

        if (TDE5E.debugMode) {
            entries.push({
                name: game.i18n.localize("tde5e.sheetHeader.copyItem"),
                icon: "<i class='far fa-copy'></i>",
                callback: () => {
                    game.clipboard.copyPlainText(JSON.stringify(this.object.toObject(), null, 2));
                    ui.notifications.info(game.i18n.format(
                        "tde5e.notification.copySourceToClipboard",
                        { name: this.object.name },
                    ));
                },
            });

            if (!this.object.compendium) {
                entries.push({
                    name: game.i18n.localize("tde5e.settings.forceMigration.name"),
                    icon: "<i class='far fa-file-arrow-up'></i>",
                    callback: async () => {
                        let itemData;
                        if (!this.object.actor) { // World item
                            itemData = game.data[this.object.collectionName].find(d => d._id === this.object.id);
                        } else { // Embedded item
                            const worldActorData = game.data.actors
                                ?.find(a => a._id === this.object.actor.id);
                            itemData = worldActorData.items
                                ?.find(i => i._id === this.object._id);
                        }
                        if (!itemData) {
                            util.error(`Unable to find source data for item ${this.object.id}`);
                            return;
                        }

                        await migrateItem(this.object, itemData);
                        await migrateEmbeddedItemData(this.object, this.actor);
                        ui.notifications.info(game.i18n.format(
                            "tde5e.notification.itemMigrated",
                            { name: this.object.name },
                        ));
                    },
                });
            }
        }

        return entries;
    }

    /**
     * Prevent Foundry from creating title interactions since we use a context menu for that purpose.
     * @override
     */
    _createDocumentIdLink() {}

    /**
     * Activate item sheet listeners.
     * @param {jQuery.Element} html The HTML element of the sheet.
     * @override
     */
    activateListeners(html) {
        super.activateListeners(html);
        html.find("a.compendium-link").click(event => {
            event.preventDefault();
            game.packs.get(event.currentTarget.dataset.pack).render(true);
        });

        if (this.options.editing) {
            html.find("button[name='reset']").click(event => this.toggleEdit(event, false));
            html.on("click", ".array-property a.add-element", event => this.addArrayElement(event));
            html.on("click", ".array-element a.remove-element", event => this.removeArrayElement(event));
            if (this.options.enableUnitControl) UnitControl.activate(html);
        } else {
            html.find("img.item-img").click(this.popoutImage.bind(this));
            html.find("a.skill-tree").click(this.openSkillTree.bind(this));
        }
    }

    /**
     * Extends Foundry's method to reset the editing state when the sheet is closed.
     * @override
     */
    async close(options = {}) {
        this.options.editing = false;
        this.refreshOptions();
        return super.close(options);
    }

    /**
     * Moves the item image from its corner into a popout spanning the entire window.
     * @param {jQuery.Event} event The JQuery event of the image click.
     */
    popoutImage(event) {
        const img = event.currentTarget;
        if (event.currentTarget.classList.contains("popout")) {
            // Image is already popped out, pop it back in.
            this.popinImage(event);
            return;
        }

        // Create placeholder element to maintain text wrapping.
        const bounds = img.getBoundingClientRect();
        const placeholder = document.createElement("div");
        placeholder.classList.add("popout-placeholder");
        placeholder.style.width = `${bounds.width}px`;
        placeholder.style.height = `${bounds.height}px`;
        img.before(placeholder);

        img.classList.add("popout");
        const aspectRatio = bounds.width / bounds.height;
        if (aspectRatio >= 2) img.style.maxWidth = "200%";
        else if (aspectRatio <= 0.5) img.style.maxHeight = "200%";
    }

    /**
     * Moves the item image back into its corner.
     * @param {jQuery.Event} event The JQuery event of the image click.
     */
    popinImage(event) {
        const img = event.currentTarget;
        const placeholder = img.previousElementSibling;
        if (placeholder.matches("div.popout-placeholder")) placeholder.remove();
        img.classList.remove("popout");
        img.style.maxWidth = img.style.maxHeight = "";
    }

    /**
     * Opens an @see AbilityTree with this item as its root node.
     */
    openSkillTree() {
        new AbilityTree(this.item).render(true);
    }

    /**
     * Opens an editable sheet for the current item.
     * @param {jQuery.Event} event The JQuery event that caused the toggle.
     * @param {boolean} submit Indicates whether changes should be submitted or discarded.
     * @returns {Promise} A promise representing the update and render operations.
     */
    async toggleEdit(event, submit = true) {
        this.options.editing = !this.options.editing;
        this.refreshOptions();
        if (submit) await super._onSubmit(event, { preventRender: true });
        return setTimeout(() => this.render()); // TODO this can still run too early, not sure why
    }

    /**
     * Adjusts sheet options based on the current editing state.
     */
    refreshOptions() {
        this.options.submitOnChange = !this.options.editing;
        this.options.submitOnClose = !this.options.editing;
        this.temporaryData = null;
    }

    /**
     * Adds a new element to the clicked button's parent fieldset.
     * The initial data of the element is determined by the item's data model.
     * @param {jQuery.Event} event The click event of the button.
     */
    addArrayElement(event) {
        const path = event.currentTarget.closest("fieldset").dataset.group;
        const field = path.startsWith("system.")
            ? this.item.system.schema.getField(path.substring(7))
            : this.item.schema.getField(path);
        if (!field?.element) return;

        this.createTemporaryData();
        const array = foundry.utils.getProperty(this.temporaryData, path);
        if (!Array.isArray(array)) return;

        array.push(field.element.getInitialValue({}));
        this.render();
    }

    /**
     * Removes the button's parent element from its array.
     * @param {jQuery.Event} event The click event of the button.
     */
    removeArrayElement(event) {
        event.currentTarget.closest(".array-element")?.remove();
        this.createTemporaryData();
    }

    /**
     * Creates temporary data from the item's source and the sheet's current content.
     */
    createTemporaryData() {
        this.temporaryData = this.item.toObject();
        foundry.utils.mergeObject(this.temporaryData, this._getSubmitData());
    }

    /**
     * Opens a new window to configure the item.
     */
    configure() {
        new TdeItemConfig(this.item, {
            top: this.position.top + 40,
            left: this.position.left + ((this.position.width - 400) / 2),
        }).render(true);
    }

    /**
     * Extends the default behavior to stop editing after submitting the form.
     * @param {jQuery.Event} event The JQuery submit event.
     * @param {object} options The submission options.
     * @returns {Promise} A promise representing the item update.
     */
    async _onSubmit(event, options = {}) {
        if (event.currentTarget?.matches("prose-mirror")) return Promise.resolve();
        if (this.options.editing && !options.preventRender) return this.toggleEdit(event);
        return super._onSubmit(event, options);
    }

    /**
     * Extends the default behavior to handle array inputs.
     * @override
     */
    _getSubmitData(updateData = {}) {
        const data = super._getSubmitData(updateData);
        if (data.img === "systems/tde5e/assets/ui/image-solid.svg") delete data.img;
        if (game.user.isGM) removeDefaultMetadata(data, this.object);
        parseArrays(data, this.item._source);
        return data;
    }

    /**
     * Extends the default behavior to add a changelog entry to the actor.
     * @override
     */
    async _updateObject(_event, formData) {
        this.temporaryData = null;
        return this.object.update(formData);
    }

    /**
     * Extends Foundry's edit listener to display an error if the user lacks the required permissions.
     * @param {jQuery.Event} event The event of the edit operation.
     * @override
     */
    _onEditImage(event) {
        if (game.user.can("FILES_BROWSE")) {
            super._onEditImage(event);
        } else {
            util.error("You lack the permissions to upload images.", false, true);
        }
    }
}
