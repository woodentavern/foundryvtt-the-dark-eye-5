import TdeItem from "./item.js";

/**
 * Base class for spoken and written languages.
 */
export class LanguageSpoken extends TdeItem {
}

export class LanguageWritten extends LanguageSpoken {
}
