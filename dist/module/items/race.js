import TdeItem, { PreparationPriority } from "./item.js";
import promptReplace from "../ui/replaceDialog.js";
import { promptVariant } from "../ui/variantPicker.js";
import { warn } from "../util.js";

class Race extends TdeItem {
    preparationPriority = PreparationPriority.AFTER_DERIVED;

    /** @override */
    prepareActorData() {
        const derivedData = this.actor.system.derived;
        const modifierData = this.system.modifiers;
        derivedData.movement.base = modifierData.movement ?? 8;
        this.actor.modify(this.name, derivedData.spirit, "base", modifierData.spirit);
        this.actor.modify(this.name, derivedData.toughness, "base", modifierData.toughness);
        this.actor.modify(this.name, this.actor.system.resources.lifePoints, "base", modifierData.lifePoints);
        this.actor.modify(this.name, this.actor.system.resources.adrenaline, "base", modifierData.lifePoints);
    }

    /**
     * Extends the parent method to delete automatic advantages when the race is removed.
     * @override
     */
    async _onDelete(options, userId) {
        super._onDelete(options, userId);
        if (game.user.id !== userId || !this.actor) return;

        await this._updateActorAttributes(!this.system.automaticAdvantages.length, true);

        if (this.system.automaticAdvantages.length) {
            // Delete automatic advantages.
            const autoAdvantageIds = this.system.automaticAdvantages
                .map(uuid => this.actor.getLocalItem(uuid))
                .filter(Boolean)
                .map(adv => adv.id);
            await this.actor.deleteEmbeddedDocuments("Item", autoAdvantageIds);
        }
    }

    /**
     * Extends the parent method to create automatic advantages when the race is created.
     * @override
     */
    async _onCreate(data, options, userId) {
        super._onCreate(data, options, userId);
        if (game.user.id !== userId || !this.actor) return;

        await this._updateActorAttributes(!this.system.automaticAdvantages.length);

        if (this.system.automaticAdvantages.size) {
            // Create automatic advantages.
            const advantages = await Promise.all(this.system.automaticAdvantages.map(advUuid => fromUuid(advUuid)));
            const advantageData = advantages
                .filter(Boolean)
                .map(adv => game.items.fromCompendium(adv, { clearFolder: true }));
            if (advantageData.length > 0) {
                this.actor.changelogMutex.wrap(() => this.actor.createEmbeddedDocuments("Item", advantageData));
            }
        }
    }

    /**
     * Applies the attribute changes of this race to its parent actor.
     * @param {boolean} render Determines whether the actor's sheet is rendered after the update.
     * @param {boolean} undo Indicates whether the attribute values are added (false) or subtracted (true).
     *  Defaults to false.
     * @returns {Promise} A promise representing the actor update.
     */
    _updateActorAttributes(render, undo = false) {
        if (!this.system.attributeAdjustments.length) return Promise.resolve();
        const attrUpdate = this.system.attributeAdjustments.reduce((update, attr) => {
            if (attr.attribute === "any") {
                warn(`Race ${this.uuid} has an 'any' attribute. Recreate it to select one.`);
            } else {
                const currentValue = this.actor.system.attributes[attr.attribute].base;
                const newValue = currentValue + (attr.value * (undo ? -1 : 1));
                update["system.attributes"][attr.attribute] = { base: newValue };
            }
            return update;
        }, { "system.attributes": {} });
        return this.actor.changelogMutex.wrap(() => this.actor.update(attrUpdate, { render }));
    }

    /** @override */
    static async allowCreation(data, actor) {
        if (actor.type === "creature") return false;
        const currentRaces = actor.itemTypes[data.type];
        if (currentRaces.length > 0 && !await promptReplace(currentRaces[0], data)) return false;

        // If there is a selectable attribute, add an additional input to the variant picker.
        const anyAttributeIndex = data.system.attributeAdjustments.findIndex(attr => attr.attribute === "any");
        if (anyAttributeIndex !== -1) {
            const anyAttribute = data.system.attributeAdjustments[anyAttributeIndex];
            const variant = {
                name: "attribute",
                title: `${game.i18n.format("tde5e.actor.attributes.name")} (${anyAttribute.value.signedString()})`,
                values: (anyAttribute.of ?? Object.keys(actor.system.attributes))
                    .map(attr => ({ value: attr, text: game.i18n.localize(`tde5e.actor.attributes.${attr}`) })),
                callback: (itemData, attribute) => itemData.system.attributeAdjustments
                    .splice(anyAttributeIndex, 1, { attribute, value: anyAttribute.value }),
            };
            if (!await promptVariant(data, actor, [variant])) return false;
        } else if (data.system.variant?.available?.length && !await promptVariant(data, actor)) {
            return false;
        }
        return true;
    }
}

export default {
    default: Race,
};
