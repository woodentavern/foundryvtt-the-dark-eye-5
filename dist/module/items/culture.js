import TdeItem from "./item.js";
import promptReplace from "../ui/replaceDialog.js";
import { promptVariant } from "../ui/variantPicker.js";

class Culture extends TdeItem {
    /**
     * Extends the parent method to undo skill changes when the culture is removed.
     * @override
     */
    async _onDelete(options, userId) {
        super._onDelete(options, userId);
        if (game.user.id === userId) {
            await this._updateActorSkills(true);
            await this._deleteHome();
        }
    }

    /**
     * Extends the parent method to apply skill changes when the culture is created.
     * @override
     */
    async _onCreate(data, options, userId) {
        super._onCreate(data, options, userId);
        if (game.user.id === userId) {
            await this._updateActorSkills();
            await this._createHome();
        }
    }

    /**
     * Applies the skill changes of this culture to its parent actor.
     * @param {boolean=} undo Indicates whether the skill values are added (false) or subtracted (true).
     *  Defaults to false.
     * @returns {Promise} A promise representing the skill updates.
     */
    _updateActorSkills(undo = false) {
        if (!this.actor || !this.system.culturalPackageSkills.length) return Promise.resolve();

        const skillUpdates = this.system.culturalPackageSkills.reduce((updates, skillInfo) => {
            const cid = skillInfo.skill?.system.cid;
            const skill = this.actor.uniqueItems.get(cid);
            if (skill) {
                const newValue = skill._source.system.improvement.value + (skillInfo.value * (undo ? -1 : 1));
                updates.push({ _id: skill.id, "system.improvement.value": newValue });
            }
            return updates;
        }, []);
        return this.actor.updateEmbeddedDocuments("Item", skillUpdates);
    }

    /**
     * Creates free items for the selected native language and home.
     * @returns {Promise} A promise representing the item creation.
     */
    async _createHome() {
        if (!this.actor) return;

        const creations = [];
        const language = await fromUuid(this.getFlag("tde5e", "nativeLanguage"));
        if (language && !this.actor.uniqueItems.has(language.system.cid)) {
            const itemData = game.items.fromCompendium(language);
            itemData.system.variant.level = 3;
            itemData.system.purchase.cost = [0, 0, 0];
            foundry.utils.setProperty(itemData, "flags.tde5e.propertyMetadata.system.purchase.cost.noMigration", true);
            creations.push(itemData);
        }

        const homeVariant = this.getFlag("tde5e", "home");
        if (homeVariant && !this.actor.uniqueItems.has(`ORTSKENNTNIS:${homeVariant}`)) {
            const placeKnowledge = await fromUuid("Compendium.tde5e.mundaneSpecialAbilities.Item.OR18262045412116");
            if (placeKnowledge) {
                const itemData = game.items.fromCompendium(placeKnowledge);
                itemData.system.cid += `:${homeVariant}`;
                itemData.system.variant.selected = { vid: "CUSTOM", name: homeVariant };
                itemData.system.purchase.cost = [0];
                foundry.utils.setProperty(
                    itemData,
                    "flags.tde5e.propertyMetadata.system.purchase.cost.noMigration",
                    true,
                );
                creations.push(itemData);
            }
        }

        if (creations.length) {
            await this.actor.changelogMutex.wrap(
                () => this.actor.createEmbeddedDocuments("Item", creations, { skipVariants: true, clearFolder: true }),
            );
        }
    }

    /**
     * Removes the items for the culture's native language and home.
     * @returns {Promise} A promise representing the item deletion.
     */
    async _deleteHome() {
        if (!this.actor) return;

        const deletions = [];
        const languageCid = fromUuidSync(this.getFlag("tde5e", "nativeLanguage"))?.system.cid;
        if (languageCid) {
            const language = this.actor.uniqueItems.get(languageCid);
            if (language) deletions.push(language.id);
        }

        const home = this.actor.uniqueItems.get(`ORTSKENNTNIS:${this.getFlag("tde5e", "home")}`);
        if (home) deletions.push(home.id);

        if (deletions.length) {
            await this.actor.changelogMutex.wrap(
                () => this.actor.deleteEmbeddedDocuments("Item", deletions, { skipCost: true }),
            );
        }
    }

    /** @override */
    static async allowCreation(data, actor) {
        // Prompt the user for a native language and home location.
        const nativeLanguage = {
            name: "flags.tde5e.language",
            title: game.i18n.localize("tde5e.types.languageSpoken"),
            values: data.system.languagesSpoken.map(langUuid => {
                const lang = fromUuidSync(langUuid);
                return lang ? { text: lang.name, value: langUuid } : { text: "-", value: "" };
            }),
            callback: (itemData, value) => foundry.utils.setProperty(itemData, "flags.tde5e.nativeLanguage", value),
        };
        const home = {
            name: "flags.tde5e.place",
            title: fromUuidSync("Compendium.tde5e.mundaneSpecialAbilities.Item.OR18262045412116")?.name,
            values: [],
            callback: (itemData, value) => foundry.utils.setProperty(itemData, "flags.tde5e.home", value),
        };
        if (!await promptVariant(data, actor, [nativeLanguage, home])) return false;

        const currentCultures = actor.itemTypes[data.type];
        if (currentCultures.length > 0 && !await promptReplace(currentCultures[0], data)) return false;
        return true;
    }
}

export default {
    default: Culture,
};
