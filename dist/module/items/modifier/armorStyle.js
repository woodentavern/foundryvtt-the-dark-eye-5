import combatSpecialAbility from "./combatSpecialAbility.js";

class ArmorStyle extends combatSpecialAbility.default {
    /** @override */
    isCombatRelevant(combatant) {
        return this.system.combat.armorGroups.includes(combatant.actor?.activeArmor?.system.group);
    }
}

class EncumbranceAdaption extends ArmorStyle {
    /** @override */
    prepareActorData() {
        const encumbrance = this.actor.uniqueItems.get("BELASTUNG");
        if (!encumbrance) return;

        // Only apply reduction if the value was already modified.
        const { value } = encumbrance;
        if (value <= 0 || !encumbrance.system.modifiers?.length) return;

        // Only modify plate, chain or scale armors.
        const armorGroup = this.actor.activeArmor?.system.group;
        if (!armorGroup || !["chainOrScale", "plate"].includes(armorGroup)) return;

        // Reduce modification to 1 in combat.
        const { inCombat } = this.actor;
        const modification = Math.clamp(inCombat ? 1 : 2, 0, inCombat ? value - 1 : value) * -1;
        if (modification < 0) encumbrance.modifyValue(this.name, modification);
    }
}

export default {
    default: ArmorStyle,
    BELASTUNGSGEWOHNUNG: EncumbranceAdaption,
};
