import ModifierItem from "./modifierItem.js";
import { PreparationPriority } from "../item.js";
import {
    AttributeModification,
    BotchRangeModification,
    InfoModification,
    SingleAttributeModification,
} from "../../ui/rolls/rollModification.js";

/**
 * Base class for advantages and disadvantages.
 */
class Vantage extends ModifierItem {
    /** @inheritdoc */
    get sheetVariantTemplate() {
        return "systems/tde5e/templates/items/variants/vantageVariant.hbs";
    }

    /** @inheritdoc */
    get sheetHeaderClass() {
        return "bg-red";
    }

    /** @inheritdoc */
    getSheetData() {
        return {
            hasValidReferences: this.hasValidReferences(),
            id: this.id,
            name: this.name,
            link: this.createLink(),
        };
    }
}

class Sluggish extends Vantage {
    /** @inheritdoc */
    prepareActorData() {
        this.actor.modifyWithItem(this, this.actor.system.derived.movement, "value", true);
    }
}

class LowArcaneEnergy extends Vantage {
    /** @inheritdoc */
    prepareActorData() {
        this.actor.modifyWithItem(this, this.actor.system.resources.arcaneEnergy, "max", true);
    }
}

class LowKarmaPoints extends Vantage {
    /** @inheritdoc */
    prepareActorData() {
        this.actor.modifyWithItem(this, this.actor.system.resources.karmaPoints, "max", true);
    }
}

class LowLifePoints extends Vantage {
    preparationPriority = PreparationPriority.AFTER_MODIFIED;

    /** @inheritdoc */
    prepareActorData() {
        this.actor.modifyWithItem(this, this.actor.system.resources.lifePoints, "max", true);
    }
}

class LowSpirit extends Vantage {
    /** @inheritdoc */
    prepareActorData() {
        this.actor.modifyWithItem(this, this.actor.system.derived.spirit, "value", true);
    }
}

class LowToughness extends Vantage {
    /** @inheritdoc */
    prepareActorData() {
        this.actor.modifyWithItem(this, this.actor.system.derived.toughness, "value", true);
    }
}

class Unlucky extends Vantage {
    /** @inheritdoc */
    prepareActorData() {
        this.actor.modifyWithItem(this, this.actor.system.resources.fatePoints, "max", true);
    }
}

class Fragile extends Vantage {
    /** @inheritdoc */
    prepareActorData() {
        const pain = this.actor.uniqueItems.get("SCHMERZ");
        if (pain && pain.value > 0) pain.modifyValue(this.name, 1);
    }
}

class MagicalConfinement extends Vantage {
    /** @override */
    canModifyRoll(item) {
        return item.type === "spell";
    }

    /** @inheritdoc */
    modifyRoll() {
        return new AttributeModification(this, -1);
    }
}
class FatBody extends Vantage {
    /** @inheritdoc */
    canModifyRoll(item) {
        return item.cid === "KORPERBEHERRSCHUNG"
            || item.cid === "TANZEN"
            || item.cid === "KLETTERN"
            || item.cid === "VERBERGEN";
    }

    /** @inheritdoc */
    modifyRoll() {
        return new AttributeModification(this, -1);
    }
}

class PredatorsScent extends Vantage {
    /** @inheritdoc */
    canModifyRoll(item) {
        return item.cid === "TIERKUNDE"
            || item.cid === "REITEN";
    }

    /** @inheritdoc */
    modifyRoll() {
        return null;
    }
}

class Glassy extends Vantage {
    /** @inheritdoc */
    prepareActorData() {
        this.actor.modify(this.displayName, this.actor.system.derived.woundThreshold, "value", -1);
    }
}

class CannotRememberNames extends Vantage {
    /** @inheritdoc */
    canModifyRoll(item) {
        return item.cid === "ETIKETTE";
    }

    /** @inheritdoc */
    modifyRoll() {
        return new SingleAttributeModification(this, -2, 0, { active: false });
    }
}

class Incompetence extends Vantage {
    /** @inheritdoc */
    canModifyRoll(item) {
        return item.id === this.system.variant.selected?.target;
    }

    /** @inheritdoc */
    modifyRoll() {
        return new InfoModification(this, game.i18n.localize("tde5e.check.modifications.rerollWorse"));
    }
}

class WildMagic extends Vantage {
    /** @inheritdoc */
    canModifyRoll(item) {
        return item.type === "spell";
    }

    /** @inheritdoc */
    modifyRoll() {
        return new BotchRangeModification(this, -1, { active: true });
    }
}

class MutilatedEar extends Vantage {
    /** @inheritdoc */
    canModifyRoll(item) {
        return item.system.cid === "SINNESSCHARFE";
    }

    /** @inheritdoc */
    modifyRoll() {
        return new AttributeModification(this, -1, { active: false });
    }
}

class MutilatedEye extends Vantage {
    /** @inheritdoc */
    canModifyRoll(item) {
        return item.type === "combatTechnique" && ["SCHLEUDERWAFFEN", "SCHUSSWAFFEN"].includes(item.system.cid);
    }

    /** @inheritdoc */
    modifyRoll() {
        return new AttributeModification(this, -2);
    }
}

class MutilatedLeg extends Vantage {
    /** @inheritdoc */
    prepareActorData() {
        const { movement } = this.actor.system.derived;
        this.actor.modify(this.displayName, movement, "base", -Math.round(movement.base / 2));
    }
}

class MutilatedEunuchOne extends Vantage {
    /** @inheritdoc */
    canModifyRoll(item) {
        return ["WILLENSKRAFT", "BETOREN"].includes(item.system.cid);
    }

    /** @inheritdoc */
    modifyRoll(item) {
        if (item.system.cid === "WILLENSKRAFT") return new AttributeModification(this, +1, { active: false });
        if (item.system.cid === "BETOREN") return new AttributeModification(this, -1, { active: false });
        return null;
    }
}

class MutilatedEunuchTwo extends MutilatedEunuchOne {
    /** @inheritdoc */
    modifyRoll(item) {
        if (item.system.cid === "WILLENSKRAFT") return new AttributeModification(this, +2, { active: false });
        if (item.system.cid === "BETOREN") return new AttributeModification(this, -2, { active: false });
        return null;
    }
}

class NightBlind extends Vantage {
    /** @inheritdoc */
    canModifyRoll(item) {
        return item.cid === "SINNESSCHARFE";
    }

    /** @inheritdoc */
    modifyRoll() {
        return new InfoModification(this, game.i18n.localize("tde5e.check.modifications.depends"));
    }
}

class ColoBlind extends Vantage {
    /** @inheritdoc */
    canModifyRoll(item) {
        return item.cid === "SINNESSCHARFE"
            || item.cid === "ETTIKETTE"
            || item.cid === "ALCHIMIE"
            || item.cid === "ORIENTIERUNG";
    }

    /** @inheritdoc */
    modifyRoll() {
        return new InfoModification(this, game.i18n.localize("tde5e.check.modifications.depends"));
    }
}

class LimitedSense extends Vantage {
    /** @inheritdoc */
    canModifyRoll(item) {
        return item.cid === "SINNESSCHARFE";
    }

    /** @inheritdoc */
    modifyRoll() {
        return new AttributeModification(this, -2, { active: false });
    }
}

export default {
    default: Vantage,
    BEHABIG: Sluggish,
    EINGESCHRANKTER_SINN: LimitedSense,
    FARBENBLIND: ColoBlind,
    FETTLEIBIG: FatBody,
    GLASERN: Glassy,
    MAGISCHE_EINSCHRANKUNG: MagicalConfinement,
    NACHTBLIND: NightBlind,
    NIEDRIGE_ASTRALKRAFT: LowArcaneEnergy,
    NIEDRIGE_KARMALKRAFT: LowKarmaPoints,
    NIEDRIGE_LEBENSKRAFT: LowLifePoints,
    NIEDRIGE_SEELENKRAFT: LowSpirit,
    NIEDRIGE_ZAHIGKEIT: LowToughness,
    PECH: Unlucky,
    RAUBTIERGERUCH: PredatorsScent,
    SCHLECHTES_NAMENSGEDACHTNIS: CannotRememberNames,
    UNFAHIG: Incompetence,
    "VERSTUMMELT:EINOHRIG": MutilatedEar,
    "VERSTUMMELT:EINAUGIG": MutilatedEye,
    "VERSTUMMELT:EINBEINIG": MutilatedLeg,
    "VERSTUMMELT:EUNUCH_1": MutilatedEunuchOne,
    "VERSTUMMELT:EUNUCH_2": MutilatedEunuchTwo,
    WILDE_MAGIE: WildMagic,
    ZERBRECHLICH: Fragile,
};
