import ModifierItem from "./modifierItem.js";

export class TraditionImprint extends ModifierItem {
    /** @inheritdoc */
    getSheetData() {
        const data = super.getSheetData();
        data.traditionImprintLink = `<a class="content-link" data-link data-uuid="${this.uuid}">${this.name}</a>`;
        return data;
    }
}

export default {
    default: TraditionImprint,
};
