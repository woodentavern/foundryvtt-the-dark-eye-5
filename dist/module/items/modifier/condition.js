import { AttributeModification, ResultModification } from "../../ui/rolls/rollModification.js";
import { PreparationPriority } from "../item.js";
import ModifierItem from "./modifierItem.js";

class Condition extends ModifierItem {
    preparationPriority = PreparationPriority.AFTER_PREPARATION_EARLY;

    /**
     * Generates a {@link RollModification} based on the condition's level.
     * @inheritdoc
     */
    modifyRoll() {
        if (this.value === 0) return null;
        return new AttributeModification(this, this.value * -1);
    }

    /** @inheritdoc */
    getSheetData() {
        const data = super.getSheetData();
        data.base = this.system.base ?? 0;
        data.modifiers = this.system.modifiers;
        data.effect = this.currentEffectShortText;
        return data;
    }

    /**
     * Gets the current effect's text.
     * @returns {string} The effect of this condition currrenly has.
     */
    get currentEffectShortText() {
        return (this.value > 0 && Array.isArray(this.system.levelEffectsShort))
            ? this.system.levelEffectsShort[this.value - 1]
            : "-";
    }

    /** @inheritdoc */
    get isNatural() {
        return this.system.isDefault;
    }

    /**
     * Gets the current value of this condition.
     * @returns {number} A value between 0 and the maximum condition level.
     */
    get value() {
        return Math.clamp(this.system.value, 0, this.system.maxLevel);
    }

    /**
     * @returns {ActiveEffect} The active effect instance associated with this condition.
     */
    get activeEffect() {
        if (this.value <= 0) return null;
        if (!this._activeEffect) {
            let effectData = CONFIG.statusEffects.find(effect => effect.id === this.cid);
            if (!effectData) return null;

            effectData = foundry.utils.deepClone(effectData);
            effectData.statuses = [effectData.id];
            foundry.utils.setProperty(effectData, "flags.statuscounter.config", { type: "tde5e.condition" });
            delete effectData.id;
            this._activeEffect = new ActiveEffect(effectData, { parent: this });
        }

        return this._activeEffect;
    }

    /** @inheritdoc */
    prepareDerivedData() {
        this.system.base = this.baseData.value ?? 0;
    }

    /** @inheritdoc */
    prepareActorData() {
        this.synchronizeEffect();
    }

    /**
     * Ensures that an active effect is present in the item's effects (if needed).
     */
    synchronizeEffect() {
        const { activeEffect, actor } = this;
        const exists = this.effects.has(this.cid);

        if (activeEffect) {
            if (activeEffect.statusCounter.type !== "tde5e.condition") activeEffect._statusCounter = null;
            if (!exists) {
                actor._conditionsChanged = true;
                this.effects.set(this.cid, activeEffect);
            }
        } else if (exists) {
            actor._conditionsChanged = true;
            this.effects.delete(this.cid);
        }
    }

    /**
     * Applies a modified to the value of this condition.
     * @param {string} name The name of the modifier.
     * @param {number} value The value to add to the current value.
     */
    modifyValue(name, value) {
        this.modify(name, this.system, "value", value);
    }
}

class Pain extends Condition {
    /** @inheritdoc */
    canModifyRoll() { return true; }

    /** @override */
    prepareActorData() {
        super.prepareActorData();
        this.actor.modify(this.name, this.actor.system.derived.movement, "value", this.value * -1);
    }
}

class Fear extends Condition {
    /** @inheritdoc */
    canModifyRoll() { return true; }
}

class Stupor extends Condition {
    /** @inheritdoc */
    canModifyRoll() { return true; }
}

class Encumbrance extends Condition {
    /** @inheritdoc */
    preparationPriority = PreparationPriority.AFTER_PREPARATION_LATE;

    /** @inheritdoc */
    canModifyRoll(item) {
        return item.system.check?.affectedByEncumbrance ?? true;
    }

    /** @inheritdoc */
    modifyRoll(item) {
        const modification = super.modifyRoll(item);
        if (modification) modification.active = item.system.check?.affectedByEncumbrance ?? true;
        return modification;
    }

    /** @override */
    prepareActorData() {
        super.prepareActorData();
        this.actor.modify(this.name, this.actor.system.derived.physicalInitiative, "value", this.value * -1);
        this.actor.modify(this.name, this.actor.system.derived.movement, "value", this.value * -1);
    }
}

class Rapture extends Condition {
    /** @inheritdoc */
    canModifyRoll() {
    // TODO determine if item is godly skill
        return true;
    }
}

class Confusion extends Condition {
    /** @inheritdoc */
    canModifyRoll() { return true; }

    /** @inheritdoc */
    modifyRoll(item) {
        if (this.value >= 3
            && ((item.type === TDE5E.itemTypes.Skill && item.system.group === "knowledge")
                || TDE5E.spellTypes.includes(item.type))) {
            return new ResultModification(this, false);
        }
        return super.modifyRoll(item);
    }

    /** @override */
    prepareActorData() {
        super.prepareActorData();
        this.actor.modify(this.name, this.actor.system.derived.mentalInitiative, "value", this.value * -1);
    }
}

class Animosity extends Condition {
    /** @inheritdoc */
    canModifyRoll(item) {
        return item.system.cid === "WILLENSKRAFT";
    }

    /** @inheritdoc */
    modifyRoll(item) {
        if (this.value === 4) return new ResultModification(this, false);
        return super.modifyRoll(item);
    }
}

class Drunk extends Condition {
    /** @inheritdoc */
    canModifyRoll(item) {
        return item.system.cid === "ZECHEN";
    }
}

class Trance extends Condition {
    /** @inheritdoc */
    canModifyRoll() { return true; }

    /** @inheritdoc */
    modifyRoll(item) {
        switch (this.value) {
        case 0:
        case 1:
            return null;
        case 2:
            // TODO determine if godly skill
            return item.type === TDE5E.itemTypes.LiturgicalChant ? null : super.modifyRoll(item);
        default:
            return super.modifyRoll(item);
        }
    }
}

class SikaryanLoss extends Condition {
    /** @inheritdoc */
    canModifyRoll(item) {
        return item.system.cid === "WILLENSKRAFT";
    }

    /** @inheritdoc */
    modifyRoll(item) {
        const modification = super.modifyRoll(item);
        if (modification) modification.value = (modification.value + 1) * 2;
        return modification;
    }
}

class Exhaustion extends Condition {
    /** @inheritdoc */
    canModifyRoll(item) {
        return item.system.group === "knowledge";
    }
}

class DemonicDepletion extends Condition {
    /** @inheritdoc */
    canModifyRoll() {
        // TODO determine if pact skill
        return true;
    }
}

class Paralysis extends Condition {
    preparationPriority = PreparationPriority.AFTER_PREPARATION_LATE;

    /** @inheritdoc */
    canModifyRoll() {
        return true;
    }

    /** @override */
    prepareActorData() {
        super.prepareActorData();
        const { movement } = this.actor.system.derived;
        this.actor.modify(this.name, movement, "value", this.value * -0.25 * movement.value);
    }
}

/**
 * Overrides for specific compendium entries to use a custom class.
 */
export default {
    default: Condition,
    ANIMOSITAT: Animosity,
    BELASTUNG: Encumbrance,
    BERAUSCHT: Drunk,
    BETAUBUNG: Stupor,
    DAMONISCHE_AUSZEHRUNG: DemonicDepletion,
    ENTRUCKUNG: Rapture,
    FURCHT: Fear,
    PARALYSE: Paralysis,
    SCHMERZ: Pain,
    SIKARYANVERLUST: SikaryanLoss,
    TRANCE: Trance,
    UBERANSTRENGUNG: Exhaustion,
    VERWIRRUNG: Confusion,
};
