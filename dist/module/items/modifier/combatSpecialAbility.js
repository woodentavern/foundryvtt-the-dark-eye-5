import SpecialAbility from "./specialAbility.js";
import { lookupCompendiumEntry } from "../../util.js";

export class CombatSpecialAbility extends SpecialAbility {
    /** @inheritdoc */
    static defaultIconPath = "systems/tde5e/assets/defaultItems/special_ability_combat.svg";

    /**
     * Checks if this ability is relevant for the given combatant's current state.
     * @param {TdeCombatant} combatant The combatant to check the relevance for.
     * @returns {boolean} True if the ability is currently relevant, false otherwise.
     */
    isCombatRelevant(combatant) {
        if (this.system.combat.combatTechniques.all) return true;
        const skill = combatant.actor.items.get(combatant.getFlag("tde5e", "selectedSkill")?.id);
        if (!skill || skill.type !== "combatTechnique") return false;
        return this.system.combat.combatTechniques.values
            .some(id => lookupCompendiumEntry(id).entry?.system.cid === skill.cid);
    }
}

class MilitaryTraining extends CombatSpecialAbility {
    /** @override */
    isCombatRelevant() {
        return false;
    }
}

export default {
    default: CombatSpecialAbility,
    MILITARISCHE_AUSBILDUNG: MilitaryTraining,
};
