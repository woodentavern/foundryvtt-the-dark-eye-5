import { CombatSpecialAbility } from "./combatSpecialAbility.js";
import { CustomModification, ReplaceAttributeModification, RollModification } from "../../ui/rolls/rollModification.js";
import { lookupCompendiumEntry, gridDistance, warn } from "../../util.js";

class CombatStyle extends CombatSpecialAbility {
    /**
     * Determines whether generally this combat style allows to use an offhand item of any kind.
     * @returns {boolean} True, if this style allows for offhand items. False otherwise.
     */
    isOffhandAllowed() {
        return true;
    }

    /**
     * Determines whether this specific equipment can be used in the corresponding hand in this style.
     * @param {WeaponItem} mainhand An equipable item.
     * @returns {boolean} True, if this mainhand weapon is allowed by the combatStyle. False otherwise.
     */
    canUseAsMainhand(mainhand) {
        return mainhand.type !== TDE5E.itemTypes.Shield;
    }

    /**
     * Determines whether this specific equipment can be used in the corresponding hand in this style.
     * Can be used to filter out shields or weapons and even specific instances of those.
     * @param {WeaponItem} offhand An equipable item.
     * @returns {boolean} True, if this offhand weapon is allowed by the combatStyle. False otherwise.
     */
    // eslint-disable-next-line no-unused-vars -- Keep as documentation.
    canUseAsOffhand(offhand) {
        return true;
    }

    /**
     * Determines whether this specific loadout combination can be used in this style.
     * @param {WeaponItem} mainhand An equipable item.
     * @param {WeaponItem} offhand An equipable item.
     * @returns {boolean} True, if this loadout is allowed by the combatStyle. False otherwise.
     */
    canUseLoadout(mainhand, offhand) {
        if (!mainhand && offhand) return false;
        return this.canUseAsMainhand(mainhand) && this.canUseAsOffhand(offhand);
    }

    /**
     * Determines whether the given technique is compatible with this combat style.
     * @param {CombatTechnique} combatTechnique Any combat technique.
     * @returns {boolean} True if this combatTechnique is compatible with the combatStyle, false otherwise.
     */
    appliesToCombatTechnique(combatTechnique) {
        if (!combatTechnique) return false;
        return this.system.combat.combatTechniques.hasCid(combatTechnique.cid);
    }

    /**
     * Checks if this ability is relevant for the given combatant's current state.
     * A relevant ability will be displayed in the context suggetion dialog.
     * @param {TdeCombatant} combatant The combatant to check the relevance for.
     * @returns {boolean} True if the ability is currently relevant, false otherwise.
     */
    isCombatRelevant(combatant) {
        if (this.system.combat.combatTechniques.all) return true;
        const skill = combatant.actor.items.get(combatant.getFlag("tde5e", "selectedSkill")?.id);
        if (!skill || skill.type !== "combatTechnique") return false;
        return this.system.combat.combatTechniques.values
            .some(id => lookupCompendiumEntry(id).entry?.system.cid === skill.cid);
    }

    /**
     * Handles combat lifecycle events.
     * Note that this is only called for the first GM. Other clients will not receive these events.
     * @param {TdeCombat} combat The combat that fired the event.
     * @param {object} events The name and arguments of the event.
     * @param {TdeCombatant?} events.turnEnd The combatant whose turn was ended.
     */
    // eslint-disable-next-line no-unused-vars -- Keep as documentation.
    onCombatProgressed(combat, events) {}
}

class BellumIustum extends CombatStyle {
    /** @override */
    canModifyRoll(item) {
        return item.system.cid === "SCHLAGWAFFEN";
    }

    /**
     * Generates a {@link RollModification} to replace the last attribute of
     *  RAUFEN with the primary attribute of the caster's tradition.
     * @inheritdoc
     */
    modifyRoll() {
        const primaryAttribute = this.actor.blessedTraditions[0]?.system.primaryAttribute ?? "charisma";
        return new ReplaceAttributeModification(
            this,
            this.actor.system.attributes[primaryAttribute].value,
            2,
            primaryAttribute,
        );
    }

    /** @override */
    isCombatRelevant() {
        return false;
    }
}

class PugnaMagicae extends CombatStyle {
    /** @override */
    canModifyRoll(item) {
        return item.system.cid === "SCHLAGWAFFEN";
    }

    /**
     * Generates a {@link RollModification} to replace the last attribute of
     *  RAUFEN with the primary attribute of the caster's tradition.
     * @inheritdoc
     */
    modifyRoll() {
        const primaryAttribute = this.actor.magicalTraditions[0]?.system.primaryAttribute ?? "sagacity";
        return new ReplaceAttributeModification(
            this,
            this.actor.system.attributes[primaryAttribute].value,
            2,
            primaryAttribute,
        );
    }

    /** @override */
    isCombatRelevant() {
        return false;
    }
}

class NoOffhandCombatStyle extends CombatStyle {
    /** @inheritdoc */
    isOffhandAllowed() {
        return false;
    }
}

class Bladestorm extends CombatStyle {
    /** @inheritdoc */
    canUseAsOffhand(offhand) {
        return offhand.type !== "shield";
    }
}

class Gladiator extends CombatStyle {
    /** @inheritdoc */
    canUseAsOffhand(offhand) {
        return offhand.type === "shield";
    }
}

class Arrowdancer extends NoOffhandCombatStyle {
    /** @inheritdoc */
    canModifyRoll(item) {
        return item.cid === "SCHUSSWAFFEN";
    }

    /** @inheritdoc */
    modifyRoll() {
        const defensive = this.actor.uniqueItems.get("PFEILTANZER_DEFENSIV");
        const offensive = this.actor.uniqueItems.get("PFEILTANZER_OFFENSIV");
        if (!defensive || !offensive) {
            if (this.isOwner) warn("Missing status effects for Arrowdancer style. Add them from the library.", true);
            return null;
        }

        let activeUuid = this.uuid;
        if (defensive.active) activeUuid = defensive.uuid;
        else if (offensive.active) activeUuid = offensive.uuid;
        return new CustomModification(this, activeUuid, RollModification.MOD_PRIORITIES.BEFORE_ATTRIBUTES, {
            select: true,
            values: {
                [this.uuid]: game.i18n.localize("tde5e.combat.maneuverQuality.neutral"),
                [defensive.uuid]: defensive.name.substring(this.name.length + 2),
                [offensive.uuid]: offensive.name.substring(this.name.length + 2),
            },
        });
    }

    /** @inheritdoc */
    async onCombatProgressed(combat, events) {
        if (!events.move || !this.actor) return;

        const combatant = combat.getCombatantByActor(this.actor);
        const position = combatant?.token?.object.center;
        if (!position) return;
        const { isAlly } = combatant;

        const [allies, enemies] = combat.combatants.reduce((combatants, c) => {
            if (c.isDefeated || c === combatant) return combatants;
            if (gridDistance(position, c.token.object.center, canvas.grid) <= 5) {
                combatants[(c.isAlly === isAlly) ? 0 : 1]++;
            }
            return combatants;
        }, [0, 0]);

        const defensive = this.actor.uniqueItems.get("PFEILTANZER_DEFENSIV");
        if (defensive) await defensive.setActive(enemies > allies);
        const offensive = this.actor.uniqueItems.get("PFEILTANZER_OFFENSIV");
        if (offensive) await offensive.setActive(enemies < allies);
    }
}

export default {
    default: CombatStyle,
    BELLUM_IUSTUM: BellumIustum,
    EISENFAUST: NoOffhandCombatStyle,
    GLADIATOR: Gladiator,
    KLINGENSTURM: Bladestorm,
    KNOCHENBRECHER: NoOffhandCombatStyle,
    PFEILTANZER: Arrowdancer,
    PUGNA_MAGICAE: PugnaMagicae,
    SCHARFRICHTER: NoOffhandCombatStyle,
    WIRBELWIND: NoOffhandCombatStyle,
    KOPFE_DER_HYDRA: NoOffhandCombatStyle,
};
