import TdeItem from "../item.js";

/**
 * Base class for all items that can potentially modify other items or checks.
 */
export default class ModifierItem extends TdeItem {
    /**
     * Generates information about how a roll of the given item is modified.
     *  This method should never be called for items that return false when
     *  checked with {@link canModifyRoll}.
     * @param {TdeItem} item The item of the rolled skill.
     * @returns {RollModification?} A roll modification object or null if none should be applied.
     */
    // eslint-disable-next-line no-unused-vars -- Keep for documentation purposes.
    modifyRoll(item) {
        return null;
    }
}
