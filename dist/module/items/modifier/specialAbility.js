import ModifierItem from "./modifierItem.js";

export default class SpecialAbility extends ModifierItem {
    /** @override */
    get sheetVariantTemplate() {
        return "systems/tde5e/templates/items/variants/specialAbilityVariant.hbs";
    }

    /** @override */
    getSheetData() {
        return {
            id: this.id,
            name: this.name,
            link: this.createLink(),
            hasValidReferences: this.hasValidReferences(),
        };
    }

    /** @inheritdoc */
    hasValidReferences() {
        const validReferences = super.hasValidReferences();

        const target = foundry.utils.getProperty(this.system, "variant.selected.target");
        if (target) {
            return this.actor.items.has(target) && validReferences;
        }

        return validReferences;
    }
}
