import ModifierItem from "./modifierItem.js";
import * as rollMod from "../../ui/rolls/rollModification.js";
import { PreparationPriority } from "../item.js";

class State extends ModifierItem {
    /** @inheritdoc */
    getSheetData() {
        const data = super.getSheetData();
        data.active = this.active;
        return data;
    }

    /** @inheritdoc */
    get isNatural() {
        return this.system.isDefault;
    }

    /**
     * Gets the actor's current active state for this effect.
     * @returns {boolean} True if the state is active, false otherwise.
     */
    get active() {
        return Boolean(this.actor.effects.find(effect => effect.statuses.has(this.cid)));
    }

    /**
     * Creates or removes the state from the actor.
     * @param {boolean} state True if the state should be added, false if it should be removed.
     * @returns {Promise} A promise representing the effect creation or deletion.
     */
    setActive(state) {
        const effect = this.actor.effects.find(e => e.statuses.has(this.cid));
        if (state === !!effect) return Promise.resolve();

        this.system.active = state;
        if (effect) return effect.delete();

        let effectData = foundry.utils.deepClone(CONFIG.statusEffects.find(e => e.id === this.cid));
        if (!effectData) {
            // Global effect no longer exists, create fallback data.
            effectData = {
                icon: this.img,
                name: this.name,
                "flags.tde5e.type": this.type,
            };
        }
        effectData.statuses = [this.cid];
        delete effectData.id;
        return ActiveEffect.create(effectData, { parent: this.actor });
    }
}

class Prone extends State {
    /** @inheritdoc */
    canModifyRoll(item) {
        return item.type === "combatTechnique";
    }

    /** @inheritdoc */
    modifyRoll() {
        if (!this.active) return null;
        return this.actor.inCombat
            ? new rollMod.SkillPointModification(this, -4, { deferred: true })
            : new rollMod.AttributeModification(this, -2, { deferred: true });
    }
}

class Incapacitated extends State {
    preparationPriority = PreparationPriority.AFTER_PREPARATION_LATE;

    /** @inheritdoc */
    get active() { return super.active; }

    /** @inheritdoc */
    async setActive(state) {
        const combatants = game.combat?.combatants
            .filter(c => c.actor === this.actor)
            .map(c => ({ _id: c.id, defeated: state }));
        if (combatants?.length) await game.combat.updateEmbeddedDocuments("Combatant", combatants);

        const effect = this.actor.effects.find(e => e.statuses.has(this.cid));
        if (state === !!effect) return Promise.resolve();
        this.system.active = state;
        if (effect) return effect.delete();

        const effectData = foundry.utils.deepClone(CONFIG.statusEffects.find(e => e.id === this.cid));
        effectData.statuses = [effectData.id];
        effectData["flags.core.overlay"] = true; // Create the effect as overlay.
        delete effectData.id;
        return ActiveEffect.create(effectData, { parent: this.actor });
    }

    /** @inheritdoc */
    prepareActorData() {
        if (this.active) {
            const { movement } = this.actor.system.derived;
            this.actor.modify(this.name, movement, "value", -movement.value);
        }
    }
}

class Cramped extends State {
    /** @inheritdoc */
    canModifyRoll(item) {
        return item.type === "combatTechnique";
    }

    /** @inheritdoc */
    modifyRoll() {
        if (!this.active) return null;
        return new rollMod.QualityModification(this, -3);
    }
}

class Bloodlust extends State {
    /** @inheritdoc */
    canModifyRoll(item) {
        return item.type === "combatTechnique" || item.cid === "KRAFTAKT";
    }

    /** @inheritdoc */
    modifyRoll() {
        if (!this.active) return null;
        return new rollMod.AttributeModification(this, 2);
    }

    /** @inheritdoc */
    prepareActorData() {
        if (!this.active) return;

        const pain = this.actor.uniqueItems.get("SCHMERZ");
        if (!pain || pain.value >= 4) return;

        pain.modifyValue(this.name, -3);
    }
}

class Silenced extends State {
    /** @inheritdoc */
    canModifyRoll(item) {
        return item.type === TDE5E.itemTypes.Spell
            || item.type === TDE5E.itemTypes.Liturgy;
    }

    /** @inheritdoc */
    modifyRoll() {
        if (!this.active) return null;
        return new rollMod.InfoModification(this, game.i18n.localize("tde5e.check.modifications.depends"));
    }
}

class Blind extends State {
    /** @inheritdoc */
    canModifyRoll(item) {
        return item.cid === "SINNESSCHARFE"
            || item.type === TDE5E.itemTypes.Spell
            || item.type === TDE5E.itemTypes.Liturgy;
    }

    /** @inheritdoc */
    modifyRoll() {
        if (!this.active) return null;
        return new rollMod.InfoModification(this, game.i18n.localize("tde5e.check.modifications.depends"));
    }
}

class Bound extends State {
    preparationPriority = PreparationPriority.AFTER_PREPARATION_LATE;

    /** @inheritdoc */
    prepareActorData() {
        if (this.active) {
            const { movement } = this.actor.system.derived;
            this.actor.modify(this.name, movement, "value", -movement.value);
        }
    }
}

class ArrowdancerDefensive extends State {
    /**
     * Disables the embattled modifier.
     * @param {object} _result The result of the roll.
     * @param {string} _priority The current priority of the apply call.
     * @param {object[]} modifiers The active modifiers of the roll.
     */
    applyRollModification(_result, _priority, modifiers) {
        const embattledName = game.i18n.localize("tde5e.combat.modifiers.embattled.name");
        const modIndex = modifiers.findIndex(mod => mod.name === embattledName);
        if (modIndex > -1) modifiers.splice(modIndex, 1);
    }
}

class ArrowdancerOffensive extends ArrowdancerDefensive {
    /**
     * Disables the crowded modifier.
     * @inheritdoc
     */
    applyRollModification(_result, _priority, modifiers) {
        const embattledName = game.i18n.localize("tde5e.combat.modifiers.crowded.name");
        const modIndex = modifiers.findIndex(mod => mod.name === embattledName);
        if (modIndex > -1) modifiers.splice(modIndex, 1);
    }
}

export default {
    default: State,
    BLIND: Blind,
    BLUTRAUSCH: Bloodlust,
    EINGEENGT: Cramped,
    FIXIERT: Bound,
    HANDLUNGSUNFAHIG: Incapacitated,
    LIEGEND: Prone,
    PFEILTANZER_DEFENSIV: ArrowdancerDefensive,
    PFEILTANZER_OFFENSIV: ArrowdancerOffensive,
    STUMM: Silenced,
};
