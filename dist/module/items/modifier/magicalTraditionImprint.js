import { TraditionImprint } from "./traditionImprint.js";
import { lookupCompendiumEntry } from "../../util.js";

export class MagicalTraditionImprint extends TraditionImprint {
    /** @override */
    static async allowCreation(data, actor) {
        const tradition = actor.magicalTraditions;
        if (!tradition.length) return false;

        const matchesTradition = lookupCompendiumEntry(data.system.tradition).entry
            ?.system.cid === tradition[0]._source.system.cid;
        if (!matchesTradition) return false;

        const currentType = actor.itemTypes[data.type]
            .filter(imprint => imprint._source.system.group === data.system.group);
        if (currentType.length > 0 && !await promptReplace(currentType[0], data)) return false;
        return true;
    }
}

export default {
    default: MagicalTraditionImprint,
};
