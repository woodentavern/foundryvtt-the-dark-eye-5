import SpecialAbility from "./specialAbility.js";
import promptMultiSelect from "../../ui/controls/multiSelectPicker.js";
import promptReplace from "../../ui/replaceDialog.js";
import { RatingModification } from "../../ui/rolls/rollModification.js";

class MagicalSpecialAbility extends SpecialAbility {
}

class ScholarOfRashdulerDemonologs extends MagicalSpecialAbility {
    /** @inheritdoc */
    static async allowCreation(data, actor) {
        const selectionOption = {
            selectionOptions: actor.itemTypes.spell
                .filter(item => item.system.property === "demonic")
                .map(item => `${item.compendiumId.replace("tde5e.", "")}.${item.cid}`),
            maxSelectionCount: 7,
            minSelectionCount: 0,
        };

        const skills = await promptMultiSelect(selectionOption);
        data.system.variant.selected = {
            // TODO: implement use
            ids: skills,
        };

        if (!data.system.cid) return true;

        const existing = actor.uniqueItems.get(data.system.cid);
        const allow = existing ? await promptReplace(existing) : true;

        if (!allow) util.error(`Can't add item ${data.name} because it already exists.`, false, true);
        return allow;
    }
}

class FavouriteSpell extends MagicalSpecialAbility {
    /** @inheritdoc */
    canModifyRoll(item) {
        return item.id === this.system.variant.selected?.target;
    }

    /** @inheritdoc */
    modifyRoll() {
        return new RatingModification(this, 2);
    }
}

export default {
    default: MagicalSpecialAbility,
    SCHOLAR_DER_RASHDULER_DAMONOLOGEN: ScholarOfRashdulerDemonologs,
    LIEBLINGSZAUBER: FavouriteSpell,
};
