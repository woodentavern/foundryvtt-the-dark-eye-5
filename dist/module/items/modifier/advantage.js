import TdeSkillCheck from "../../ui/rolls/skillCheck.js";
import {
    AttributeModification,
    InfoModification,
    RerollModification,
    SingleAttributeModification,
    SkillPointModification,
} from "../../ui/rolls/rollModification.js";
import vantage from "./vantage.js";
import { PreparationPriority } from "../item.js";

class Advantage extends vantage.default {
    /** @override */
    get sheetHeaderClass() {
        return "bg-green";
    }
}

class Aptitude extends Advantage {
    /** @inheritdoc */
    canModifyRoll(item) {
        return item.id === this.system.variant.selected?.target;
    }

    /** @inheritdoc */
    modifyRoll() {
        return new RerollModification(this, 1, [1, 1, 1], true);
    }
}

class DangerSense extends Advantage {
    /** @inheritdoc */
    get isRollable() {
        return true;
    }

    /**
     * Attempts to roll a check for this item, if it allows doing so.
     * @param {string?} rollMode The roll mode to use for the chat message.
     * @returns {Promise} A promise representing the message creation.
     */
    async roll(rollMode) {
        // TODO these data changes are a bad idea, need to restructure
        this.system.check = { attributes: ["sagacity", "intuition", "intuition"], affectedByEncumbrance: false };
        this.system.improvement = { value: this.system.variant.level };
        const roll = new TdeSkillCheck(this);
        return roll.toMessage({ speaker: { alias: roll.data.actor.name } }, { rollMode });
    }
}

class OutstandingSense extends Advantage {
    /** @inheritdoc */
    canModifyRoll(item) {
        return item.cid === "SINNESSCHARFE";
    }

    /** @inheritdoc */
    modifyRoll() {
        return new AttributeModification(this, 1, { active: false });
    }
}

class Trustworthy extends Advantage {
    /** @inheritdoc */
    canModifyRoll(item) {
        return item.cid === "BEKEHREN_UND_UBERZEUGEN"
            || item.cid === "UBERREDEN"
            || item.cid === "HANDEL";
    }

    /** @inheritdoc */
    modifyRoll() {
        return new AttributeModification(this, 1, { active: false });
    }
}

class SenseOfDirection extends Advantage {
    /** @inheritdoc */
    canModifyRoll(item) {
        return item.cid === "ORIENTIERUNG";
    }

    /** @inheritdoc */
    modifyRoll() {
        return new AttributeModification(this, 1);
    }
}

class HighSpirit extends Advantage {
    /** @override */
    prepareActorData() {
        this.actor.modifyWithItem(this, this.actor.system.derived.spirit, "value");
    }
}

class HighToughness extends Advantage {
    /** @override */
    prepareActorData() {
        this.actor.modifyWithItem(this, this.actor.system.derived.toughness, "value");
    }
}

class HighLifePoints extends Advantage {
    preparationPriority = PreparationPriority.AFTER_MODIFIED;

    /** @override */
    prepareActorData() {
        this.actor.modifyWithItem(this, this.actor.system.resources.lifePoints, "max");
    }
}

class HighArcaneEnergy extends Advantage {
    /** @override */
    prepareActorData() {
        this.actor.modifyWithItem(this, this.actor.system.resources.arcaneEnergy, "max");
    }
}

class HighKarmaPoints extends Advantage {
    /** @override */
    prepareActorData() {
        this.actor.modifyWithItem(this, this.actor.system.resources.karmaPoints, "max");
    }
}

class Lucky extends Advantage {
    /** @override */
    prepareActorData() {
        this.actor.modifyWithItem(this, this.actor.system.resources.fatePoints, "max");
    }
}

class Nimble extends Advantage {
    /** @override */
    prepareActorData() {
        this.actor.modifyWithItem(this, this.actor.system.derived.movement, "value");
    }
}

class LightStep extends Advantage {
    /** @override */
    canModifyRoll(item) {
        return item.cid === "TANZEN";
    }

    /** @override */
    modifyRoll() {
        return new SkillPointModification(this, 1);
    }

    /** @override */
    prepareActorData() {
        this.actor.modify(this.displayName, this.actor.system.derived.movement, "value", 1);
    }
}

class Tough extends Advantage {
    preparationPriority = PreparationPriority.AFTER_MODIFIED;

    /** @override */
    prepareActorData() {
        this.actor.uniqueItems.get("SCHMERZ")?.modifyValue(this.name, -1);
    }
}

class Iron extends Advantage {
    /** @override */
    prepareActorData() {
        this.actor.modify(this.displayName, this.actor.system.derived.woundThreshold, "value", 1);
    }
}

class DwarfsNose extends Advantage {
    /** @override */
    canModifyRoll(item) {
        return item.cid === "SINNESSCHARFE";
    }

    /** @override */
    modifyRoll() {
        return new AttributeModification(this, 1, { active: false });
    }
}

class DarkSight extends Advantage {
    /** @override */
    canModifyRoll(item) {
        return item.cid === "SINNESSCHARFE";
    }

    /** @override */
    modifyRoll() {
        return new InfoModification(this, game.i18n.localize("tde5e.check.modifications.depends"));
    }
}

class NaturalWeaponTail extends Advantage {
    /** @override */
    canModifyRoll(item) {
        return item.cid === "AKROBATIK";
    }

    /** @override */
    modifyRoll(item) {
        const index = item.system.check.attributes.indexOf("agility");
        return new SingleAttributeModification(this, 1, index, { active: false });
    }
}

class GoodLooks extends Advantage {
    /** @override */
    canModifyRoll(item) {
        return item.cid === "HANDELN"
            || item.cid === "UBERREDEN"
            || item.cid === "BETOREN";
    }

    /** @override */
    modifyRoll() {
        return new AttributeModification(this, this.system.variant.level);
    }
}

class RemembersNamesWell extends Advantage {
    /** @override */
    canModifyRoll(item) {
        return item.cid === "ETIKETTE";
    }

    /** @override */
    modifyRoll() {
        return new SingleAttributeModification(this, this.system.variant.level, 0, { active: false });
    }
}

class NaturalArmor extends Advantage {
    preparationPriority = PreparationPriority.AFTER_ATTRIBUTES;

    /** @override */
    prepareActorData() {
        this.actor?.itemTypes[TDE5E.itemTypes.Armor]?.forEach(armor => {
            armor.modify(this.displayName, armor.system.protection, "mod", this.system.variant.level);
        });
    }
}

class Euphony extends Advantage {
    /** @override */
    canModifyRoll(item) {
        return item.cid === "SINGEN";
    }

    /** @override */
    modifyRoll() {
        return new AttributeModification(this, 1, { active: true });
    }
}

class AncestralBloodDryade extends Advantage {
    /** @override */
    canModifyRoll(item) {
        return item.cid === "PFLANZENKUNDE"
            || item.cid === "BETOREN"
            || item.cid === "WILLENSKRAFT";
    }

    /** @override */
    modifyRoll(item) {
        if (item.cid === "PFLANZENKUNDE") return new SkillPointModification(this, 3, { active: false });
        if (item.cid === "BETOREN") {
            return [
                new SingleAttributeModification(this, 1, 1, { active: true }),
                new SingleAttributeModification(this, 1, 2, { active: true }),
            ];
        }
        return new AttributeModification(this, -1, { active: false });
    }
}

export default {
    default: Advantage,
    "AHNENBLUT:FEENBLUT_DRYADE": AncestralBloodDryade,
    BEGABUNG: Aptitude,
    DUNKELSICHT: DarkSight,
    EISERN: Iron,
    FLINK: Nimble,
    GEFAHRENINSTINKT: DangerSense,
    GLUCK: Lucky,
    GUTAUSSEHEND: GoodLooks,
    GUTES_NAMENSGEDACHTNIS: RemembersNamesWell,
    HERAUSRAGENDER_SINN: OutstandingSense,
    HOHE_ASTRALKRAFT: HighArcaneEnergy,
    HOHE_KARMALKRAFT: HighKarmaPoints,
    HOHE_LEBENSKRAFT: HighLifePoints,
    HOHE_SEELENKRAFT: HighSpirit,
    HOHE_ZAHIGKEIT: HighToughness,
    LEICHTER_GANG: LightStep,
    NATURLICHER_RUSTUNGSSCHUTZ: NaturalArmor,
    NATURLICHE_WAFFE_SCHWANZ: NaturalWeaponTail,
    RICHTUNGSSINN: SenseOfDirection,
    VERTRAUENERWECKEND: Trustworthy,
    WOHLKLANG: Euphony,
    ZAHER_HUND: Tough,
    ZWERGENNASE: DwarfsNose,
};
