import { TraditionImprint } from "./traditionImprint.js";
import { AttributeModification, SkillPointModification } from "../../ui/rolls/rollModification.js";

export class BlessedTraditionImprint extends TraditionImprint {
}

class MordernPerspective extends BlessedTraditionImprint {
    /** @inheritdoc */
    canModifyRoll(item) {
        return item.type === "liturgy" && item._source.system.group === "ceremony";
    }

    /** @inheritdoc */
    modifyRoll() {
        return new AttributeModification(this, -1);
    }
}

class TraditionalPerspective extends BlessedTraditionImprint {
    /** @inheritdoc */
    canModifyRoll(item) {
        return item.type === "liturgy"
            && (item._source.system.group === "ceremony"
            || item._source.system.group === "liturgicalChant");
    }

    /** @inheritdoc */
    modifyRoll() {
        return [new SkillPointModification(this, 1), new AttributeModification(this, -1, { active: false })];
    }
}

export default {
    default: BlessedTraditionImprint,
    MODERNE_SICHTWEISE: MordernPerspective,
    MYSTERIENKULT: BlessedTraditionImprint,
    PRAGMATISMUS: BlessedTraditionImprint,
    TRADITIONELLE_DENKRICHTUNG: TraditionalPerspective,
};
