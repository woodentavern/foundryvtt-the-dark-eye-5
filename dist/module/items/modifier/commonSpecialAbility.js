import {
    AttributeModification,
    RatingModification,
    SkillPointModification,
    InfoModification,
    SingleAttributeModification,
} from "../../ui/rolls/rollModification.js";
import { PreparationPriority } from "../item.js";
import SpecialAbility from "./specialAbility.js";

class CommonSpecialAbility extends SpecialAbility {
}

class AbilitySpecialisation extends CommonSpecialAbility {
    preparationPriority = PreparationPriority.AFTER_MODIFIED;

    /** @inheritdoc */
    canModifyRoll(item) {
        return item.id === this.system.variant.selected?.target;
    }

    /** @inheritdoc */
    modifyRoll() {
        const item = this.actor.items.get(this.system.variant.selected.target);
        const attributeMedian = item.system.check.attributes
            .map(attr => this.actor.system.attributes[attr].value)
            .sort()[1];
        const currentValue = item.system.improvement.value;
        const maxBonus = Math.min(this.system.variant.level * 2, (attributeMedian - currentValue) + 2);
        return new RatingModification(this, Math.max(maxBonus, 0), { active: false });
    }
}

class TreeMover extends CommonSpecialAbility {
    preparationPriority = PreparationPriority.AFTER_DERIVED;

    /** @override */
    prepareActorData() {
        const actorData = this.actor.system;
        if (this.actor.type !== "player") return; // Only players have inventory data.
        this.actor.modify(this.name, actorData.derived.carryingCapacity, "mod", actorData.attributes.strength.base);
    }
}

class Statecraft extends CommonSpecialAbility {
    /** @inheritdoc */
    canModifyRoll(item) {
        return item.cid === "GESCHICHTSWISSEN"
            || item.cid === "BEKEHREN_UND_UBERZEUGEN"
            || item.cid === "UBERREDEN";
    }

    /** @inheritdoc */
    modifyRoll() {
        return new SkillPointModification(this, 3, { active: false });
    }
}

class AreaKnowledge extends CommonSpecialAbility {
    /** @inheritdoc */
    canModifyRoll(item) {
        return item.cid === "GASSENWISSEN"
            || item.cid === "ORIENTIERUNG";
    }

    /** @inheritdoc */
    modifyRoll() {
        return new AttributeModification(this, 1, { active: false });
    }
}

class NoWork extends CommonSpecialAbility {
    /** @inheritdoc */
    canModifyRoll(item) {
        return item.system.group === "craft" && item.system.improvement.value <= 3;
    }

    /** @inheritdoc */
    modifyRoll() {
        return new AttributeModification(this, 1, { active: true });
    }
}

class WayOfTheWanderer extends CommonSpecialAbility {
    /** @inheritdoc */
    canModifyRoll(item) {
        return item.cid === "GASSENWISSEN"
            || item.cid === "UBERREDEN"
            || item.cid === "VERBERGEN";
    }

    /** @inheritdoc */
    modifyRoll() {
        return new InfoModification(this, game.i18n.localize("tde5e.check.modifications.rerollBetter"));
    }
}

class SpecializedKnowdlege extends CommonSpecialAbility {
    /** @inheritdoc */
    canModifyRoll(item) {
        return item.type === "skill"
            && item.system.group === "knowledge"
            && item.specializations?.length;
    }

    /** @inheritdoc */
    modifyRoll() {
        return new InfoModification(this, game.i18n.localize("tde5e.check.modifications.depends"));
    }
}

class WayOfTheSchoolar extends CommonSpecialAbility {
    /** @inheritdoc */
    canModifyRoll(item) {
        return item.type === "skill"
            && item.system.group === "knowledge"
            && item.specializations?.length;
    }

    /** @inheritdoc */
    modifyRoll(item) {
        const specCount = item.specializations.length;
        if (!specCount) return null;

        const values = Object.entries(item.system.check.attributes)
            .map(([, value]) => this.actor.system.attributes[value].value);
        const min = Math.min(...values);
        const index = values.indexOf(min);

        return new SingleAttributeModification(this, specCount, index);
    }
}

export default {
    default: CommonSpecialAbility,
    BAUMSCHLEPPER: TreeMover,
    FACHWISSEN: SpecializedKnowdlege,
    FOKUSGEBIET: AbilitySpecialisation,
    KEINE_ARBEIT: NoWork,
    ORTSKENNTNIS: AreaKnowledge,
    STAATSKUNST: Statecraft,
    WEG_DES_GELEHRTEN: WayOfTheSchoolar,
    WEG_DES_STREUNERS: WayOfTheWanderer,
};
