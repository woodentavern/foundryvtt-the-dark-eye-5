import SpecialAbility from "./specialAbility.js";

class ArtifactAbility extends SpecialAbility {
    /** @inheritdoc */
    getSheetData() {
        const data = super.getSheetData();
        data.traditionGroup = this.system.tradition?.system.group ?? "";
        return data;
    }
}

export default {
    default: ArtifactAbility,
};
