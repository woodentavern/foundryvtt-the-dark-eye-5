import * as util from "../util.js";

/**
 * Enables integration with the Elevation Ruler module if it is active.
 */
export default function registerElevationRuler() {
    if (game.modules.get("elevationruler")?.active) {
        util.info("Found ElevationRuler, applying integration.", false, util.logStyle.integration);
        enableElevationRuler();
    }
}

/**
 *  Hooks into foundry to register distance coloring for the ruler.
 */
function enableElevationRuler() {
    Hooks.once("ready", () => {
        // Define colored segments when dragging
        CONFIG.elevationruler.SPEED.CATEGORIES = [
            { name: "freeMovement", color: Color.from(0x00ff00), multiplier: 1 },
            { name: "first", color: Color.from(0xbfff00), multiplier: 2 },
            { name: "second", color: Color.from(0xffff00), multiplier: 3 },
            { name: "third", color: Color.from(0xffbf00), multiplier: 4 },
            { name: "fourth", color: Color.from(0xff8000), multiplier: 5 },
            { name: "maximum", color: Color.from(0xff4000), multiplier: Number.POSITIVE_INFINITY },
        ];

        // Derive speed from underlying actor
        CONFIG.elevationruler.SPEED.tokenSpeed = token => {
            if (!token || !token.actor) return 0;
            return token.actor.system.derived.movement.value ?? 8;
        };
    });
}
