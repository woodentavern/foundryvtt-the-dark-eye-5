import * as util from "../util.js";

/**
 * Enable integration with the Babele module if it is active.
 */
export default function registerBabele() {
    if (game.modules.get("babele")?.active && typeof Babele !== "undefined") {
        util.info("Found Babele, registering compendium localization.", false, util.logStyle.integration);
        enableLocalizedCompendiums();
    }
}

/**
 * Registers compendium localizations.
 */
function enableLocalizedCompendiums() {
    game.babele.setSystemTranslationsDir("lang/packs");
}
