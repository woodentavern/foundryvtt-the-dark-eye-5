import * as util from "../util.js";

/**
 * Enables integration with Bar Brawl.
 */
export default function registerBarBrawl() {
    if (game.modules.get("barbrawl")?.active) {
        util.info("Found Bar Brawl, applying integration.", false, util.logStyle.integration);
        enableBarBrawl();
    }
}

/**
 * Collection of visibility keys.
 * Barbrawl does not expose this, so we define it here.
 * Partly overlaps with CONFIG.TOKEN_DISPLAY_MODES.
 */
const BAR_VISIBILITY = {
    INHERIT: -1,
    NONE: 0,
    ALWAYS: 50,
    HOVER_CONTROL: 35,
    HOVER: 30,
    CONTROL: 10,
};

/**
 * Registers hooks to create prototype tokens with default resource bars.
 */
function enableBarBrawl() {
    Hooks.on("preCreateActor", actor => {
        const defaultBars = actor.prototypeToken.getFlag("barbrawl", "resourceBars");
        if (!foundry.utils.isEmpty(defaultBars)) return;

        let update = null;

        if (actor.type === "player") update = createPrototypeBarsData(BAR_VISIBILITY.HOVER_CONTROL);
        else update = createPrototypeBarsData(BAR_VISIBILITY.ALWAYS);

        actor.updateSource(update, { recursive: false });
    });
}

/**
 * Creates an update object that defines the default bars of an actor.
 * @param {number} lifePointsVisibilityOwner The visibility (see BAR_VISIBILITY) of the life bar for the owner.
 * @returns {Object} An update object.
 */
function createPrototypeBarsData(lifePointsVisibilityOwner) {
    return {
        "prototypeToken.bar1.attribute": "resources.lifePoints",
        "prototypeToken.bar2.attribute": "resources.adrenaline",
        "prototypeToken.flags.barbrawl.resourceBars": {
            bar1: {
                order: 0,
                id: "bar1",
                attribute: "resources.lifePoints",
                mincolor: "#ffa07a",
                maxcolor: "#ff0000",
                position: "bottom-inner",
                otherVisibility: BAR_VISIBILITY.ALWAYS,
                ownerVisibility: lifePointsVisibilityOwner,
                subdivisions: 3,
                subdivisionsOwner: false,
                gmVisibility: -1,
            },
            bar2: {
                order: 0,
                id: "bar2",
                attribute: "resources.adrenaline",
                mincolor: "#ffffc7",
                maxcolor: "#ffff00",
                position: "bottom-inner",
                otherVisibility: BAR_VISIBILITY.NONE,
                ownerVisibility: BAR_VISIBILITY.HOVER_CONTROL,
            },
        },
    };
}
