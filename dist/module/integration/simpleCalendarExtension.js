import { initializeCalendarRecharge } from "../ui/chargeManagement.js";
import * as util from "../util.js";

/**
 * Enables integration with the Simple Calendar module if it is active.
 */
export default function registerSimpleCalendar() {
    if (game.modules.get("foundryvtt-simple-calendar")?.active) {
        util.info("Found Simple Calendar, applying integration.", false, util.logStyle.integration);
        enableCalendar();
    } else {
        util.warn("Simple Calendar module not enabled. Item charge management will not be initialized.");
    }
}

/**
 * Changes Simple Calendar settings to match our system.
 */
function enableCalendar() {
    Hooks.once("simple-calendar-ready", async () => {
        await SimpleCalendar.api.setTheme("classic");
    });

    initializeCalendarRecharge();
}
