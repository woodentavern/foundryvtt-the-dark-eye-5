import * as util from "../util.js";

/**
 * Enables integration with the Drag Ruler module if it is active.
 */
export default function registerDragRuler() {
    if (game.modules.get("drag-ruler")?.active) {
        util.info("Found DragRuler, applying integration.", false, util.logStyle.integration);
        enableDragRuler();
    }
}

/**
 * Creates a speed provider for Drag Ruler.
 */
function enableDragRuler() {
    Hooks.once("dragRuler.ready", SpeedProvider => {
        /**
         *  Implementation of SpeedProvider.
         *  See <a href="https://github.com/manuelVo/foundryvtt-drag-ruler/blob/develop/src/speed_provider.js"/>
         */
        class Tde5eSpeedProvider extends SpeedProvider {
            get colors() {
                return [
                    { id: "freeMovement", default: 0x12aa17, name: "tde5e.integration.dragRuler.freeMovement" },
                    { id: "fullMovement", default: 0x1e76db, name: "tde5e.integration.dragRuler.fullMovement" },
                    { id: "doubleMovement", default: 0x3d0972, name: "tde5e.integration.dragRuler.doubleMovement" },
                ];
            }

            getRanges(token) {
                const baseSpeed = token.actor.system.derived.movement.value;

                const ranges = [
                    { range: Math.round(baseSpeed * 0.5), color: "freeMovement" },
                    { range: baseSpeed, color: "fullMovement" },
                    { range: baseSpeed * 2, color: "doubleMovement" },
                ];

                // We can respect the preparations and abilities that improve movemnet here
                return ranges;
            }
        }

        // Register
        dragRuler.registerSystem("tde5e", Tde5eSpeedProvider);
    });
}
