import * as util from "../util.js";
import { renderControls } from "../ui/hotbarOverlay.js";

/**
 * Enables integration with the Monk's Hotbar module if it is active.
 */
export default function registerMonkHotbar() {
    if (game.modules.get("monks-hotbar-expansion")?.active) {
        util.info("Found Monk's Hotbar, applying integration.", false, util.logStyle.integration);
        enableMonksHotbar();
    }
}

/**
 * Registers hooks to render additional controls for extra hotbars.
 */
function enableMonksHotbar() {
    Hooks.on("renderHotbar", (app, html) => {
        if (!app._pagecollapsed) {
            // Multiple rows are visible
            app.macrolist
                .filter(list => !list.selected)
                .forEach(list => renderControls(app, html, list.macros));
        }
    });
}
