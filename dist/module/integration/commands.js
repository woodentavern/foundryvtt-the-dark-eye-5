import * as util from "../util.js";

/**
 * Enable integration with the Chat Commander module if it is active.
 */
export default function registerChatCommands() {
    if (game.modules.get("_chatcommands")?.active) {
        util.info("Found Chat Commander, registering commands.", false, util.logStyle.integration);
        enableChatCommands();
    }
}

/**
 * Registers chat commands with Chat Commander.
 */
function enableChatCommands() {
    Hooks.on("chatCommandsReady", commands => {
        const baseCommand = {
            name: "/rollability",
            module: "tde5e",
            aliases: ["%", "/ra"],
            description: game.i18n.localize("tde5e.commands.rollAbility.description"),
            icon: "<i class='fas fa-dice-d20'></i>",
            callback: (_, parameters) => rollAbility(parameters, game.settings.get("core", "rollMode")),
            autocompleteCallback:
                (menu, alias, parameters, maxEntries) => autocompleteAbility(menu, alias, parameters, maxEntries),
        };

        commands.register(baseCommand);
        commands.register({
            name: "/selfrollability",
            module: baseCommand.module,
            aliases: ["/sra", "/selfra"],
            description: game.i18n.localize("tde5e.commands.rollAbility.descriptionSelf"),
            icon: baseCommand.icon,
            callback: (_, parameters) => rollAbility(parameters, CONST.DICE_ROLL_MODES.SELF),
            autocompleteCallback: baseCommand.autocompleteCallback,
        });
        commands.register({
            name: "/gmrollability",
            module: baseCommand.module,
            aliases: ["/gra", "/gmra"],
            description: game.i18n.localize("tde5e.commands.rollAbility.descriptionGM"),
            icon: baseCommand.icon,
            callback: (_, parameters) => rollAbility(parameters, CONST.DICE_ROLL_MODES.PRIVATE),
            autocompleteCallback: baseCommand.autocompleteCallback,
        });
        commands.register({
            name: "/blindrollability",
            module: baseCommand.module,
            aliases: ["/bra", "/blindra"],
            description: game.i18n.localize("tde5e.commands.rollAbility.descriptionBlind"),
            icon: baseCommand.icon,
            callback: (_, parameters) => rollAbility(parameters, CONST.DICE_ROLL_MODES.BLIND),
            autocompleteCallback: baseCommand.autocompleteCallback,
        });
    });
}

/**
 * Handles the command to roll an actor's ability item.
 * @param {string} params The arguments of the command.
 * @param {string} rollMode The role mode to use for the resulting chat message.
 * @returns {object} An empty object to disable FoundryVTT command handling.
 */
function rollAbility(params, rollMode) {
    if (!params) {
        util.error(game.i18n.localize("tde5e.commands.rollAbility.badFormat"));
        return {};
    }

    const terms = params.split("=>");
    const ownedActors = util.getOwnedActors();

    // Identify the actor.
    let actor;
    if (terms.length === 1) {
        const controlledActor = canvas.tokens.controlled[0]?.actor;
        if (controlledActor?.isOwner) actor = controlledActor;
        else if (ownedActors.length === 1) [actor] = ownedActors;
        else {
            util.error(game.i18n.localize("tde5e.commands.rollAbility.badFormat"));
            return {};
        }
    } else if (terms.length === 2) {
        const actorName = terms[0].trim();
        actor = ownedActors.find(a => a.name.toLowerCase() === actorName.toLowerCase());
        if (!actor) {
            util.error(game.i18n.format("tde5e.commands.rollAbility.invalidActor", { name: actorName }));
            return {};
        }
    } else {
        util.error(game.i18n.localize("tde5e.commands.rollAbility.invalidName"));
        return {};
    }

    // Identify the item.
    let itemName = terms[terms.length - 1].trim();

    // Parse optional difficulty.
    const nameParts = itemName.split(" ");
    let difficulty = parseInt(nameParts[nameParts.length - 1], 10);
    if (!Number.isNaN(difficulty)) itemName = nameParts.slice(0, -1).join(" ");
    else difficulty = 0;

    const item = actor.items.find(i => i.name.toLowerCase() === itemName.toLowerCase());
    if (!item) {
        util.error(game.i18n.format("tde5e.commands.rollAbility.invalidItem", { name: itemName }));
        return {};
    }

    // Roll the item.
    item.roll(rollMode, difficulty);
    return {}; // Suppress the chat message.
}

/**
 * Creates a list of autocomplete entries matching the given parameters.
 * @param {AutocompleteMenu} menu The autocompletion menu.
 * @param {string} alias The alias used for the command.
 * @param {string} params The arguments of the command.
 * @param {number} maxEntries The maximum amount of entries in the menu.
 * @returns {HTMLElement[]} The menu entries to display.
 */
function autocompleteAbility(menu, alias, params, maxEntries) {
    if (!params) return [game.chatCommands.createInfoElement(menu.currentCommand.getDescription(alias))];

    // Build ability menu from rollable items
    let entries = [];
    const ownedActors = util.getOwnedActors();
    for (const actor of ownedActors) {
        if (entries.length > maxEntries) break;

        const actorAbilities = actor.items.map(item => matchAbility(alias, item, params)).filter(Boolean);
        if (actorAbilities.length === 0) continue;

        if (entries.length) entries.push(game.chatCommands.createSeparatorElement());
        entries.push(game.chatCommands.createInfoElement(`<i class='fas fa-male fa-fw'></i> ${actor.name}`));
        entries = entries.concat(actorAbilities);
    }

    return entries;
}

/**
 * Checks if the given item is an ability and its name is potentially identified by the given input.
 * @param {string} alias The alias used for the command.
 * @param {TdeItem} item The item to match.
 * @param {string} params The string to match against.
 * @returns {string} An HTML string containing a command element if the input matches an ability, null otherwise.
 */
function matchAbility(alias, item, params) {
    if (!item.isRollable || !item.actor) return null;

    const searchTerms = params.split(" ");
    if (searchTerms.length === 0) return null;

    let match = null;
    for (let term of searchTerms) {
        if (!term || term === "=>") continue; // Ignore empty and separation string.
        term = term.toLowerCase(); // Match case-insensitive.
        if (item.actor.name.toLowerCase().includes(term)) continue; // Match actor.

        // Match item.
        const startIndex = item.name.toLowerCase().indexOf(term);
        if (startIndex === -1) return null;

        // Highlight the user's input.
        const endIndex = startIndex + term.length;
        match = `${item.name.substring(0, startIndex)
        }<strong>${item.name.substring(startIndex, endIndex)}</strong>${
            item.name.substring(endIndex)}`;
    }

    const prefix = alias.length === 1 ? alias : `${alias} `;
    return game.chatCommands.createCommandElement(`${prefix}${item.actor.name} => ${item.name}`, match ?? item.name);
}
