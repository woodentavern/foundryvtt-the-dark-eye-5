/* eslint-disable no-empty-function -- Needed to remove some core behavior. */
import TdeSkillCheck from "../ui/rolls/skillCheck.js";
import * as util from "../util.js";

export default class TdeCombatant extends Combatant {
    /**
     * Checks if this combatant is on the player team or the NPC team.
     * @returns {boolean} True if the combatant is on the player's team, false otherwise.
     */
    get isAlly() {
        const isPlayer = this.token
            ? this.token.disposition === CONST.TOKEN_DISPOSITIONS.FRIENDLY
            : this.actor && this.players.length;
        return foundry.utils.getProperty(this, "flags.tde5e.switchTeam") ? !isPlayer : !!isPlayer;
    }

    /**
     * Gets the index of the group that the combatant is part of.
     * @returns {number} The index of the combatant's group.
     */
    get group() {
        return this.isAlly ? 0 : 1;
    }

    /** @override */
    get visible() {
        if (this.hidden) return this.isOwner;
        if (this.parent.isPreparing) return game.user.isGM || !this.hidden;
        return this.isOwner || this.isAlly;
    }

    /** @override */
    get isDefeated() {
        return super.isDefeated || this.actor?.uniqueItems.get("SCHMERZ")?.value >= 4;
    }

    /**
     * Fetches the combatant's currently selected skill from the actor.
     * @returns {TdeItem?} The skill item or null if none exists.
     */
    get currentSkill() {
        const selection = this.getFlag("tde5e", "selectedSkill");
        if (selection?.id) return this.actor?.items.get(selection.id);
        return null;
    }

    /**
     * Fetches the combatant's currently active loadout from the actor.
     * @returns {Loadout?} The loadout of the current skill or null if none is active.
     */
    get activeLoadout() {
        const id = this.getFlag("tde5e", "selectedLoadout");
        if (!id) return null;
        return this.actor?.system.loadouts.find(ld => ld.id === id);
    }

    /**
     * Extends the combatant preparation to add resources, skills and other
     *  required state information.
     * @override
     */
    prepareDerivedData() {
        if (!this.parent) return;

        super.prepareDerivedData();
        if (!this.actor) {
            util.warn(`Combatant ${this.name} does not have a valid actor, skipping preparation.`);
            this.resources = this.availableSkills = this.availableLoadouts = [];
            return;
        }

        this.availableLoadouts = util.createSelectOptions(this.actor.system.loadouts);
        const loadout = this.activeLoadout;

        // Collect available skills.
        let skills = this.actor.items.filter(item => item.isCombatSkill && item._source.system.improvement.value > 0);
        if (loadout) skills = skills.filter(item => item.canUseLoadout(loadout));
        skills.sort((a, b) => b.system.improvement.value - a.system.improvement.value || a.name.localeCompare(b.name));
        this.availableSkills = util.createSelectOptions(skills);

        const selectedSkill = this.getFlag("tde5e", "selectedSkill");
        if (selectedSkill?.id) {
            // Make sure that the selected skill is on the list.
            if (!this.availableSkills[selectedSkill.id]) {
                this.availableSkills[selectedSkill.id] = selectedSkill.name;
            }
        } else if (skills.length > 0 && selectedSkill?.name !== "-") {
            // Use the first available skill unless none was explicitly selected.
            foundry.utils.setProperty(this, "flags.tde5e.selectedSkill", {
                id: skills[0].id,
                name: skills[0].name,
            });
        }

        this._prepareEquipmentDetails();
    }

    /**
     * Replaces Foundry's method to include multiple resources instead of just one.
     * @override
     */
    updateResource() {
        this.resource = null; // Clear Foundry field.
        this.resources = [];

        if (!this.actor) return;
        this._prepareResource("lifePoints");
        this._prepareResource("adrenaline");
        if (this.actor.canUseMagic) this._prepareResource("arcaneEnergy");
        if (this.actor.canUseKarma) this._prepareResource("karmaPoints");
    }

    /**
     * Prepares an actor's resource for display in the combat tracker.
     * @param {string} resourceName The property name of the resource.
     */
    _prepareResource(resourceName) {
        const resource = this.actor.system.resources[resourceName];
        const resourceTitle = game.i18n.localize(`tde5e.actor.resources.${resourceName}Short`);
        this.resources.push({
            css: resourceName,
            percentage: Math.round(Math.clamp(resource.value / resource.max, 0, 1) * 100),
            text: `${resourceTitle} ${resource.value}/${resource.max}`,
        });
    }

    /**
     * Prepares additional information for displaying the details of the
     *  equipment that the combatant's actor is currently using.
     */
    _prepareEquipmentDetails() {
        const lines = [];
        const loadout = this.activeLoadout;
        if (loadout) {
            const { adrenaline, combatStyle } = loadout;
            if (combatStyle) lines.push(combatStyle.displayName);
            lines.push(`${game.i18n.localize("tde5e.actor.resources.adrenaline")}: ${adrenaline.signedString()}`);

            [loadout.mainhand, loadout.offhand].forEach(weapon => {
                if (!weapon) return;
                lines.push(`${weapon.name}: ${weapon.damageText} ${[...weapon.system.damage.types]
                    .map(type => game.i18n.localize(`tde5e.damageTypes.${type}`))
                    .join("/")}`);
            });
        }

        const armor = this.actor.activeArmor;
        if (armor) lines.push(`${armor.name}: ${armor.protectionText}`);
        this.equipmentDetails = lines.join("\n");
    }

    /**
     * Checks if the combatant can still act.
     * @returns {boolean} True if the combatant can act, false otherwise.
     */
    canAct() {
        return !(this.initiative === 0 || this.isDefeated);
    }

    /**
     * Consumes the given amount of action points.
     * @param {number} amount The amount of action points to consume.
     * @returns {Promise} A promise representing the combatant update.
     */
    consumeActionPoints(amount) {
        return this.update({ initiative: Math.max(0, this.initiative - amount) });
    }

    /**
     * Adds a bonus action point to this combatant.
     * @returns {Promise} A promise representing the combatant update.
     */
    addActionPoint() {
        return this.update({ initiative: this.initiative + 1 });
    }

    /**
     * Moves this turn to the back of the current cycle.
     * @returns {Promise} A promise representing the combatant update.
     */
    delayAction() {
        if (this.getFlag("tde5e", "delay")) return Promise.resolve();

        const combat = this.parent;
        if (combat.turns[0] === this) {
            // Make sure that the new combatant for that turn has an action.
            const nextTurn = combat.findNextActingTurn();
            if (nextTurn === -1) return Promise.resolve();
        }

        return this.setFlag("tde5e", "delay", true);
    }

    /**
     * Changes the team of this combatant.
     * @returns {Promise} A promise representig the combatant update.
     */
    switchTeam() {
        return this.setFlag("tde5e", "switchTeam", !this.getFlag("tde5e", "switchTeam"));
    }

    /**
     * Changes the active loadout of this combatant.
     * @param {string} id The ID of the new loadout.
     * @returns {Promise} A promise representing the combatant update.
     */
    changeLoadout(id) {
        const update = { "flags.tde5e": { selectedLoadout: id } };

        // Spend an action point for changing mid round.
        if (!this.parent.isPreparing) update.initiative = this._source.initiative - 1;

        // Adjust the selected skill if the current one cannot be used with the new loadout.
        const loadout = this.actor?.system.loadouts.find(ld => ld.id === id);
        if (loadout && !this.currentSkill?.canUseLoadout(loadout)) {
            const ct = loadout.combatTechnique;
            update["flags.tde5e"].selectedSkill = ct ? { id: ct.id, name: ct.name } : { id: "" };
        }

        return this.update(update);
    }

    /**
     * Changes the current skill of the combatant, resetting initiative and rolls as needed.
     * @param {string} id The ID of the new skill.
     * @param {string} name The name of the new skill.
     * @returns {Promise} A promise representing the combatant update.
     */
    changeSkill(id, name) {
        const claimedPoints = this.getFlag("tde5e", "claimedPoints");
        const update = {
            initiative: claimedPoints && claimedPoints > 0 ? claimedPoints : null,
            "flags.tde5e.selectedSkill": { id, name },
        };

        if (this.parent.isPreparing) update["flags.tde5e.selectedSkill"].result = null;
        return this.update(update, { render: false });
    }

    /**
     * Associates the given skill check with this combatant.
     * @param {TdeSkillCheck} roll The skill check that should be associated.
     * @param {string} messageId The ID of the chat message containing the roll.
     * @param {boolean=} force Flag indicating whether the roll should override all other skill checks.
     * @returns {Promise} A promise representing the combatant update.
     */
    connectSkillCheck(roll, messageId, force) {
        if (!force) {
            // Combatant already has a skill, only override it with valid combat skills.
            const selection = this.getFlag("tde5e", "selectedSkill");
            if (selection?.id && this.initiative !== null && !(roll.data.skill.id in this.availableSkills)) {
                return Promise.resolve();
            }
        }

        // Update the turn's skill.
        const result = foundry.utils.deepClone(roll._total.base ?? roll._total);
        result.base = null; // Make sure that the update resets the base roll.
        result.modifiedSkillPoints = roll._total.remainingSkillPoints;
        const update = {
            "flags.tde5e.selectedSkill": {
                messageId,
                id: roll.data.skill.id,
                name: roll.data.skill.name,
                result,
            },
        };
        if (this.parent.isPreparing) update.initiative = result.qualityLevel;

        return this.update(update);
    }

    /**
     * Creates a chat message for the combatant's currently selected skill. The roll will be evaluated if it wasn't
     *  already, but display the existing result otherwise.
     * @returns {Promise} A promise representing the chat message creation.
     */
    displaySkillCheck() {
        const skill = this.currentSkill;
        if (!skill) return Promise.resolve();

        const { result } = this.getFlag("tde5e", "selectedSkill");
        if (!result) return skill.roll(CONST.DICE_ROLL_MODES.SELF);

        const roll = TdeSkillCheck.fromResult(skill, result);
        return roll.toMessage(undefined, { rollMode: CONST.DICE_ROLL_MODES.SELF });
    }

    /**
     * Refreshes the actor & token of the combatant when it enters or exits the combat or when the encounter ends.
     */
    refresh() {
        const { actor } = this;
        if (actor && actor.uniqueItems.has("BELASTUNGSGEWOHNUNG")) {
            actor._initialize();
            this.token.object.drawEffects();
        }
    }

    /**
     * Extends Foundry's method to initialize custom combat data.
     * @override
     */
    async _preCreate(data, options, user) {
        await super._preCreate(data, options, user);
        this.updateSource({
            initiative: data.initiative ?? (this.parent.isPreparing ? null : 0),
            hidden: this.token?.disposition !== CONST.TOKEN_DISPOSITIONS.FRIENDLY,
        });

        if (foundry.utils.hasProperty(data, "flags.tde5e")) return;
        const defaultLoadout = this.actor?.system.loadouts[0];
        const defaultSkill = defaultLoadout?.combatTechnique;
        const defaultPoints = this.actor?.system.settings?.bonusActionPoints ?? 1;
        this.updateSource({
            "flags.tde5e": {
                selectedSkill: defaultSkill ? { id: defaultSkill.id, name: defaultSkill.name } : { id: "" },
                selectedLoadout: defaultLoadout?.id,
                tieInitiative: 0,
                maxInitiative: 6 + defaultPoints,
                delay: false,
                switchTeam: false,
            },
        });
    }

    /** Get rid of FoundryVTT's insane combat cloning process. */

    static async _preCreateOperation() {}

    static async _preUpdateOperation() {}

    static async _preDeleteOperation() {}
}
