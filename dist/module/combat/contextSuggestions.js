import CombatTechnique from "../items/rollable/combatTechnique.js";
import { stripHtml } from "../util.js";

/**
 * ID of the combatant that currently has combat tracker suggestions. This is
 *  used to determine whether a re-render should show the same suggestions or
 *  generate new ones.
 */
let activeCombatantId;

/**
 * Initializes the suggestion of combat applications in the combat tracker and
 *  the token HUD.
 */
export default function initializeSuggestions() {
    // Hook to extend the token HUD with suggestions.
    Hooks.on("renderTokenHUD", (hud, html) => {
        if (game.combat?.round > 0) {
            const combatant = game.combat.remainingTurns.find(t => t.tokenId === hud.object.id)
                ?? game.combat.turns.find(t => t.tokenId === hud.object.id);
            if (combatant?.isOwner && combatant.actor) {
                // if (!hud.options.scrollY.includes(".sticky-table")) hud.options.scrollY.push(".sticky-table");
                attachToTokenHud(html, combatant);
            }
        }
    });

    // Hook to extend the combat tracker with suggestions.
    Hooks.on("renderCombatTracker", (tracker, html) => {
        const combat = tracker.viewed;
        if (!combat || combat.isHistorical) return;

        let combatant;
        let animate = true;
        if (activeCombatantId) {
            // Restore previously selected combatant.
            combatant = combat.turns.find(t => t.id === activeCombatantId);
            animate = false;
        } else if (combat.isActionPhase) {
            // During action phase, show suggestions for the active combatant.
            const currentCombatant = combat.combatant;

            // Don't show the GM suggestions for player characters.
            if (game.user.isGM ? currentCombatant.isNPC : currentCombatant.isOwner) {
                combatant = currentCombatant;
            }
        } else if (combat.isPreparing && !game.user.isGM) {
            // During preparation, show suggestions for player characters to their owners.
            combatant = combat.combatants.find(c => c.isOwner && c.actor);
        }

        if (combatant?.actor && !combatant.isBonusSlot) attachToCombatTracker(html, combatant, animate);

        // Register event listener that allows opening suggestions for other combatants.
        html.on("click", "li.combatant", event => {
            const el = event.currentTarget;

            // Target already has suggestions.
            if (el.querySelector(".combat-suggestions")) return;

            // Remove other suggestion to avoid cluttering the tracker.
            const oldSuggestions = tracker.element.find(".combat-suggestions");
            if (oldSuggestions.length) oldSuggestions.slideUp(300, function remove() { $(this).remove(); });

            // Retreive the combatant and make sure that it is a valid target.
            const cmb = combat.turns.find(c => c.id === el.dataset.combatantId);
            if (!cmb?.isOwner || !cmb.actor) return;

            // Add the suggestions to the target element.
            renderSuggestions(cmb, true).hide().appendTo(el).slideDown(300);
        });
    });

    // Hook to reset the combatant with open suggestions.
    Hooks.on("updateCombat", (_combat, changes) => {
        if ("turn" in changes || "round" in changes
            || foundry.utils.hasProperty(changes, "flags.tde5e.phase")) activeCombatantId = null;
    });

    // Hook to refresh the token HUD when a skill selection changes.
    Hooks.on("updateCombatant", (combatant, changes) => {
        const flagChanges = changes?.flags?.tde5e;
        if (flagChanges?.selectedSkill?.id && combatant.token.object.hasActiveHUD) canvas.tokens.hud.render();
        if (flagChanges?.delay !== undefined) activeCombatantId = null;
    });
}

/**
 * Renders and adds suggestions for the given combatant to the given HTML.
 * @param {jQuery} html The HTML of the combat tracker.
 * @param {TdeCombatant} combatant The combatant to render suggestions for.
 * @param {boolean} animate Flag indicating whether the suggestions should be animated.
 */
function attachToCombatTracker(html, combatant, animate) {
    const el = html.find(`li.combatant[data-combatant-id="${combatant.id}"]`);
    if (animate) renderSuggestions(combatant, true).hide().appendTo(el).slideDown(300);
    else el.append(renderSuggestions(combatant, false));
}

/**
 * Renders and adds suggestions for the given combatant to the given HTML.
 * @param {jQuery} html The HTML of the token HUD.
 * @param {TdeCombatant} combatant The combatant to render suggestions for.
 */
function attachToTokenHud(html, combatant) {
    html.find(".col.right").append(renderSuggestions(combatant, false, true));
}

/**
 * Builds the suggestions for the given combatant based on its state.
 * @param {TdeCombatant} combatant The combatant to build the suggestions for.
 * @param {boolean} changeActive Indicates whether the currently selected combatant is changed.
 * @param {boolean=} forceScroll Indicates whether selected skills are manually scrolled into view. Defaults to false.
 * @returns {jQuery} An JQuery object containing the suggestions.
 */
function renderSuggestions(combatant, changeActive, forceScroll = false) {
    if (changeActive) {
        activeCombatantId = combatant.id;
        forceScroll = true;
    }

    const suggestions = $("<div class='combat-suggestions'></div>");

    const preparing = combatant.parent.isPreparing;
    const selected = combatant.getFlag("tde5e", "selectedSkill");
    if (preparing || !selected?.result) {
        // No skill selected, prompt for skills.
        suggestions.append(buildLoadouts(combatant));
        if (Object.keys(combatant.availableSkills).length > 1) suggestions.append(buildSkills(combatant, forceScroll));
        suggestions.append(buildAbilities(combatant));
    } else if (combatant.parent.combatant === combatant) {
        // Combatant is current turn, prompt for attacks.
        suggestions.append(buildArmor(combatant));
        suggestions.append(buildOffenses(combatant, selected.id));
    } else {
        // Has skill but not current action, prompt for reactions.
        suggestions.append(buildArmor(combatant));
        suggestions.append(buildReactions(combatant, selected.id));
    }

    return suggestions;
}

/**
 * Constructs an overview for the combatant's loadouts.
 * @param {TdeCombatant} combatant The combatant to determine the loadouts for.
 * @returns {string} An HTML string containing the loadout overview.
 */
function buildLoadouts(combatant) {
    const el = $("<div class='sticky-table'></div>");
    const loadoutTable = $(`<table><tr><th>${game.i18n.localize("tde5e.actor.loadout.name")}</th>
        <th>${game.i18n.localize("tde5e.types.combatTechnique")}</th>
        <th>${game.i18n.localize("tde5e.item.skill.skillValueShort")}</th></tr></table>`);

    const selectedLoadout = combatant.getFlag("tde5e", "selectedLoadout");
    for (const loadout of combatant.actor.system.loadouts.filter(ld => combatant.availableLoadouts[ld.id])) {
        const { combatTechnique } = loadout;
        const loadoutRow = $(`<tr data-id=${loadout.id}>
            <td>${loadout.name}</td>
            <td><a class="usable roll offset" name="${combatTechnique.uuid}">${combatTechnique.name}</a></td>
            <td>${combatTechnique.system.improvement.value}</td>
        </tr>`);
        if (selectedLoadout === loadout.id) loadoutRow.addClass("selected");

        loadoutTable.append(loadoutRow);
    }

    // Add listener to change the selected loadout.
    loadoutTable.on("click", "tr", function selectLoadout(event) {
        if (event.target.tagName === "A" || event.target.tagName === "I") return; // Ignore clicks on actual links.
        combatant?.changeLoadout(this.dataset.id);
    });

    return el.append(loadoutTable);
}

/**
 * Constructs an overview for the combatant's skills which can be used for the
 *  check of the turn.
 * @param {TdeCombatant} combatant The combatant to determine the skills for.
 * @param {boolean} forceScroll Indicates whether selected skills are manually scrolled into view.
 * @returns {string} An HTML string containing the skill overview.
 */
function buildSkills(combatant, forceScroll) {
    const el = $("<div class='sticky-table'></div>");
    const skillTable = $(`<table><tr><th>${game.i18n.localize("tde5e.types.combatTechnique")}</th>
        <th>${game.i18n.localize("tde5e.item.skill.check")}</th>
        <th>${game.i18n.localize("tde5e.item.skill.skillValueShort")}</th></tr></table>`);

    const selectedSkill = combatant.getFlag("tde5e", "selectedSkill");
    for (const skill of Object.keys(combatant.availableSkills).map(id => combatant.actor.items.get(id))) {
        const skillRow = $(`<tr>
            <td><a class="content-link" data-uuid="${skill.uuid}">
                <i class="far fa-file-alt"></i></a> ${skill.displayName}
            </td>
            <td><a class="usable roll offset" name="${skill.uuid}">${skill.system.check.attributes
    .map(at => game.i18n.localize(`tde5e.actor.attributesShort.${at}`))
    .join("/")}</a></td>
                    <td>${skill.system.improvement.value}</td>
                </tr>`);
        if (selectedSkill?.id === skill.id) {
            skillRow.addClass("selected");
            if (forceScroll) setTimeout(() => skillRow[0].scrollIntoView(false));
        }

        skillTable.append(skillRow);
    }

    // Add listener to change the selected skill.
    skillTable.on("click", "tr", function selectSkill(event) {
        if (event.target.tagName === "A" || event.target.tagName === "I") return; // Ignore clicks on actual links.
        const link = this.querySelector("a.content-link");
        if (!link) return;

        combatant?.changeSkill(
            link.dataset.uuid.substring(link.dataset.uuid.lastIndexOf(".") + 1),
            link.nextSibling.textContent.trim(),
        );
    });

    return el.append(skillTable);
}

/**
 * Constructs an overview for the combatant's attacks, tactical maneuvers and
 *  current weapon loadout.
 * @param {TdeCombatant} combatant The combatant to determine the attacks for.
 * @param {string} skillId The ID of the skill that the combatant rolled.
 * @returns {string} An HTML string containing the offensive overview.
 */
function buildOffenses(combatant, skillId) {
    const { actor } = combatant;
    const skill = actor.items.get(skillId);
    const isCombatTechnique = skill instanceof CombatTechnique;

    let overview = "";
    const loadout = combatant.activeLoadout;
    if (loadout) {
        // Show weapon details.
        overview += `<p>
            <strong>${game.i18n.localize("tde5e.actor.loadout.name")}</strong>: <span>`;
        const weapons = [loadout.mainhand, loadout.offhand].filter(Boolean);
        if (weapons.length) {
            overview += weapons.map(w => {
                const damageTypes = [...w.system.damage.types]
                    .map(type => `<img class="damage-type-img" src="${TDE5E.damageTypeIcons[type].icon}"/>`)
                    .join("");
                return `${w.name}, <a class="usable roll" name="${w.uuid}">${w.damageText}</a>${damageTypes}`;
            }).join(" - ");
        } else overview += game.i18n.localize("tde5e.general.none");
        overview += "</span></p>";
    }

    // Show relevant passive abilities.
    overview += buildAbilities(combatant);

    if (isCombatTechnique) {
        // Show available attacks.
        let attacks = actor.itemTypes.action.filter(ca => ca.system.isAttack && skill.canUseManeuver(ca, loadout));

        // Only allow specific attacks when bloodlust is active.
        if (actor.uniqueItems.get("BLUTRAUSCH")?.active) {
            attacks = attacks.filter(ca => ["WUCHTSCHLAG", "HAMMERSCHLAG", "EINFACHER_ANGRIFF"].includes(ca.cid));
        }

        overview += buildApplications(combatant, "tde5e.item.maneuver.type.melee", attacks);
    }

    // Show available tactical applications.
    let tacticals = actor.itemTypes.action.filter(ca => !ca.system.isAttack);
    if (loadout) tacticals = tacticals.filter(ca => loadout.combatTechnique.canUseManeuver(ca, loadout));

    return overview + buildApplications(combatant, "tde5e.item.maneuver.type.tactical", tacticals);
}

/**
 * Constructs an overview for the combatant's reactions.
 * @param {TdeCombatant} combatant The combatant to determine the reactions for.
 * @param {string} skillId The ID of the skill that the combatant rolled.
 * @returns {string} An HTML string containing the defensive overview.
 */
function buildReactions(combatant, skillId) {
    const overview = $("<div></div>");

    // Show relevant passive abilities.
    overview.append(buildAbilities(combatant));

    // Skip reactions when the combatant doesn't have an action.
    if (combatant.initiative === 0) {
        overview.append(game.i18n.localize("tde5e.combat.noActionPoints"));
        if (game.user.isGM) {
            const addAction = $(`<button>
                    <i class="fas fa-plus fa-fw"></i>${game.i18n.localize("tde5e.combat.context.addActionPoint")}
                </button>`);
            overview.append(addAction);
            addAction.click(() => combatant.addActionPoint());
        }

        return overview;
    }

    // Make sure the current skill allows defending.
    const skill = combatant.actor.items.get(skillId);
    if (!(skill instanceof CombatTechnique)) {
        return overview.append(game.i18n.localize("tde5e.combat.badDefenseSkill"));
    }

    // Show available reactions.
    const loadout = combatant.activeLoadout;
    const reactions = combatant.actor.itemTypes.reaction.filter(r => skill.canUseManeuver(r, loadout));
    return overview.append(buildApplications(combatant, "tde5e.types.reaction", reactions));
}

/**
 * Constructs an overview for the combatant's current armor.
 * @param {TdeCombatant} combatant The combatant to determine the armor for.
 * @returns {string} An HTML string containing armor information.
 */
function buildArmor(combatant) {
    const armor = combatant.actor.activeArmor;
    let armorInfo = `<p><strong>${game.i18n.localize("tde5e.types.armor.name")}</strong><br>`;
    if (armor) {
        armorInfo += `${armor.name}, ${["pierce", "slash", "blunt"]
            .map(damageType => {
                const { icon } = TDE5E.damageTypeIcons[damageType];
                const value = armor.system.protection[damageType];
                const title = game.i18n.localize(`tde5e.damageTypes.${damageType}`);
                return `<span title="${title}"><img class="damage-type-img" src="${icon}"/>${value}</span>`;
            }).join(" ")}`;
    } else {
        armorInfo += game.i18n.localize("tde5e.general.none");
    }

    return `${armorInfo}</p>`;
}

/**
 * Constructs a table of combat applications with their effective skill points and adrenaline.
 * @param {TdeCombatant} combatant The combatant to read the loadout modifiers from.
 * @param {string} group The name of the application group.
 * @param {Maneuver} maneuvers The maneuvers to include.
 * @returns {string} An HTML string containing the combat application table.
 */
function buildApplications(combatant, group, maneuvers) {
    const actionPoints = combatant.initiative;
    const availableApps = [];

    // Filter applications that require more resources than the actor has.
    for (const app of maneuvers) {
        if (app.system.cost.resources.actionPoints > actionPoints) continue;
        availableApps.push({
            name: app.displayName,
            uuid: app.uuid,
            rule: stripHtml(app.system.rule),
            actionPoints: app.system.cost.resources.actionPoints,
        });
    }

    // Sort the applications by skill points and then by name.
    availableApps.sort((a1, a2) => (a2.actionPoints - a1.actionPoints) || a1.name.localeCompare(a2.name));

    // Create table with the sorted applications.
    let table = `<div class="sticky-table"><table>
        <tr><th>${game.i18n.localize(group)}</th>
        <th>${game.i18n.localize("tde5e.combat.actionPointsShort")}</th></tr>`;
    for (const app of availableApps) {
        table += `<tr title="${app.rule}">
            <td>
                <a class="content-link" data-link data-uuid="${app.uuid}">
                    <i class="far fa-file-alt"></i>
                </a> ${app.name}
            </td>
            <td>${app.actionPoints}</td>
        </tr>`;
    }
    return `${table}</table></div>`;
}

/**
 * Constructs a list of relevant combat special abilities for the given combatant.
 * @param {TdeCombatant} combatant The combatant to gather relevant abilities for.
 * @returns {string} An HTML string containing the ability overview.
 */
function buildAbilities(combatant) {
    let abilityInfo = `<p><strong>${game.i18n.localize("tde5e.types.combatAbility.name")}</strong><br>`;
    const abilities = combatant.actor.itemTypes.combatAbility
        .filter(ability => ability.isCombatRelevant(combatant))
        .map(ability => ability.createLink());
    if (abilities.length) abilityInfo += abilities.join(", ");
    else abilityInfo += game.i18n.localize("tde5e.general.none");

    abilityInfo += "</p>";
    return abilityInfo;
}
