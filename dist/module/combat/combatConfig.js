/**
 * Form application that implements per-combat configuration options.
 */
export default class TdeCombatConfig extends FormApplication {
    /**
     * Mirrors the default options of the combat tracker configuration
     *  (@see {@link https://foundryvtt.com/api/CombatTrackerConfig.html}), but
     *  uses our own HTML template instead of the original one.
     * @returns {object} The merged default options of the dialog.
     * @override
     */
    static get defaultOptions() {
        return foundry.utils.mergeObject(super.defaultOptions, {
            id: "combat-config",
            title: game.i18n.localize("COMBAT.Settings"),
            classes: ["sheet", "combat-sheet"],
            template: "systems/tde5e/templates/combat/combatConfig.hbs",
            width: 420,
        });
    }

    /**
     * Prepares the display data of the dialog to include our internal flags.
     * @returns {object} The data required for rendering the dialog.
     * @override
     */
    getData() {
        return {
            data: this.object.flags.tde5e,
            options: this.options,
            title: this.title,
        };
    }

    /**
     * Specifies the title of the dialog. The encounter number is generated from the index within the combats of the
     *  current scene.
     * @returns {string} The title of the configuration dialog.
     * @override
     */
    get title() {
        const combats = game.combats.contents.filter(c => c.scene === canvas.scene.id);
        const combatIndex = combats.findIndex(c => c === this.object);
        return `${game.i18n.localize("COMBAT.Encounter")} ${combatIndex + 1}`;
    }

    /**
     * Updates the associated encounter with the form data settings.
     * @returns {Promise.<void>} A promise representing the update operation.
     * @override
     */
    async _updateObject(_event, formData) {
        return this.object.update(formData);
    }
}
