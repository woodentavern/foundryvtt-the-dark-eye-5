import { addMessageListener } from "../chat.js";
import { redirectActionPointClaim } from "../socket.js";
import TdeSkillCheck from "../ui/rolls/skillCheck.js";
import { warn } from "../util.js";
import TdeCombatConfig from "./combatConfig.js";

/**
 * Extends the Foundry combat tracker to implement system specific interactions.
 */
export default class TdeCombatTracker extends CombatTracker {
    // ***********************************************************
    //                       Data preparation
    // ***********************************************************

    /**
     * Extends the default options to replace the template used to render the tracker.
     * @returns {object} The adjusted default options.
     * @override
     */
    static get defaultOptions() {
        const options = super.defaultOptions;
        options.scrollY.push(".sticky-table");
        options.template = "systems/tde5e/templates/combat/combatTracker.hbs";
        return options;
    }

    /**
     * Extends the data collection to include more turn specific data and adjust the CSS classes.
     * @param {object} options The options for the rendering step.
     * @returns {object} The data required to render the tracker.
     * @override
     */
    async getData(options) {
        const data = await super.getData(options);
        if (!data.hasCombat) return data;

        // Don't process turns for the historical view.
        if (data.combat.isHistorical) {
            data.isHistorical = true;
            data.hasHistory = data.combat.flags.tde5e.historicalRound > 1;
            return data;
        }

        // Store some basic state information.
        data.isPreparing = data.combat.isPreparing;
        data.isDistributing = data.combat.isDistributing;
        data.isActionPhase = data.combat.isActionPhase;
        data.isRoundEnding = data.combat.isRoundEnding;
        data.hasHistory = data.round > 1 || (data.round === 1 && !data.isPreparing);
        data.control &&= data.isActionPhase;

        const actionPoints = data.combat.getFlag("tde5e", "groupPoints") ?? [0, 0];
        const maxActionPoints = data.combat.getFlag("tde5e", "maxGroupPoints") ?? actionPoints;
        data.groups = [{
            name: "Allies",
            color: "green",
            actionPoints: actionPoints[0],
            maxActionPoints: maxActionPoints[0],
            visible: false,
        }, {
            name: "Enemies",
            color: "red",
            actionPoints: actionPoints[1],
            maxActionPoints: maxActionPoints[1],
            visible: false,
        }];

        data.playerCount = 0;
        data.readyCount = 0;
        data.ready = true;

        // UI specific post-processing for every turn.
        for (const turnData of data.turns) {
        // for (let i = 0; i < data.turns.length; i++) {
            const combatant = data.combat.combatants.get(turnData.id);
            turnData.combatant = combatant;
            const classes = [];
            turnData.active &&= data.isActionPhase;
            if (!combatant) continue;

            // CSS classes that apply to all combatant types.
            if (turnData.active) classes.push("active");
            classes.push(combatant.isAlly ? "ally" : "enemy");
            if (turnData.owner) data.groups[combatant.isAlly ? 0 : 1].visible = true;

            turnData.canChangeLoadout = (data.isPreparing || (combatant.initiative > 1 && turnData.active))
                && turnData.owner && Object.keys(combatant.availableLoadouts).length > 1;
            turnData.canChangeSkill = data.isPreparing && turnData.owner
                && Object.keys(combatant.availableSkills).length > 1;

            if (data.isPreparing) {
                if (combatant.players.length) { // NPCs are always ready because they will roll automatically.
                    data.playerCount++;
                    if (turnData.hasRolled) data.readyCount++; // Make sure every player rolled something.
                }
            }

            this.prepareSkillCheck(turnData, combatant);
            if (combatant.token) this.prepareStatusEffects(turnData, combatant);

            // Adjust the CSS classes.
            if (turnData.hidden) classes.push("hidden");
            if (turnData.defeated) {
                classes.push("defeated");
                classes.push("no-action");
                turnData.initiative = 0;
            } else if (combatant.initiative === 0 && !data.isPreparing) {
                classes.push("no-action");
            }
            turnData.css = classes.join(" ");
        }

        data.ready = data.playerCount === data.readyCount;
        return data;
    }

    /**
     * Prepares additional information for displaying the details of the skill check that the turn is using.
     * @param {object} turnData The UI data of the turn to prepare the check details for.
     * @param {TdeCombatant} combatant The combatant of the turn.
     */
    prepareSkillCheck(turnData, combatant) {
        let result = combatant.getFlag("tde5e", "selectedSkill")?.result;
        if (!turnData.hasRolled || !result) return;
        let resultString = TdeSkillCheck.toDisplayResult(result);
        if (result.base) result = result.base;

        // Create list of attributes.
        let attributeString = "";
        for (const attribute of result.attributes) {
            const attrName = game.i18n.localize(`tde5e.actor.attributes.${attribute.name}`);
            attributeString += `${attrName}: ${attribute.value} / ${attribute.roll}\n`;
        }

        // Create result line.
        resultString += `\n${game.i18n.localize("tde5e.check.qualityShort")} ${result.qualityLevel}, `
            + `${game.i18n.localize("tde5e.check.modType.skillPoints")} ${result.remainingSkillPoints}, `
            + `${game.i18n.localize("tde5e.check.modType.rating")} ${result.value}`;

        turnData.checkDetails = attributeString + resultString;
        turnData.isCritical = result.success && result.isCrit;
        turnData.isBotch = !result.success && result.isCrit;
    }

    /**
     * Prepares displayed status effects for the given turn.
     * @param {object} turn The turn to prepare the effects for.
     * @param {TdeCombatant} combatant The combatant of the turn.
     */
    prepareStatusEffects(turn, combatant) {
        const statusEffects = [...turn.effects]
            .map(effect => foundry.utils.deepClone(CONFIG.statusEffects.find(status => status.img === effect) ?? null))
            .filter(Boolean);

        // Make sure that all effects are loaded.
        if (statusEffects.length === turn.effects.size && CONFIG.statusEffects.ready) {
            // Separate statuses and conditions.
            const statuses = [];
            let conditions = [];
            const counters = window.EffectCounter?.getAllCounters(combatant.token) ?? [];
            for (const effect of statusEffects) {
                const counter = counters.find(c => c.path === effect.icon);
                if (counter) {
                    effect.counter = counter.getDisplayValue(combatant.token);
                    effect.counterColor = counter.font.fill;
                }

                if ((foundry.utils.getProperty(effect, "flags.tde5e.type") === TDE5E.itemTypes.Condition // Conditions.
                    && (effect.id !== "BRAND" || effect.id !== "BLUTUNG")) // Exclude burning and bleeding.
                    || effect.id === "penalty") { // Include generic penalty.
                    conditions.push(effect);
                } else {
                    statuses.push(effect);
                }
            }

            // Merge the conditions into one to save space.
            if (conditions.length > 3 && window.EffectCounter) conditions = this.mergeConditions(conditions);
            turn.effects = conditions.concat(statuses);
        } else {
            // Convert the effect set to the required format.
            turn.effects = [...turn.effects].map(effect => ({ icon: effect, label: "", counterColor: "#fff" }));
        }
    }

    /**
     * Merges conditions that apply a penalty to the actor's actions into one
     *  locally stored penalty condition to save space.
     * @param {object[]} conditions An array of conditions on the combatant.
     * @returns {object[]} The summarized array of conditions.
     */
    mergeConditions(conditions) {
        const reducedConditions = [];

        // Accumulate relevant condition names and levels.
        const descriptions = [];
        let levels = 0;
        for (const condition of conditions) {
            if (condition.counter === "") condition.counter = 1;
            levels += condition.counter ?? 0;
            descriptions.push(`${condition.label} ${condition.counter}`);
        }

        let penalty = conditions.find(e => e.id === "penalty");
        if (!penalty) penalty = foundry.utils.deepClone(CONFIG.statusEffects.find(e => e.id === "penalty"));

        penalty.counter = levels;
        penalty.label = descriptions.join("\n");
        penalty.counterColor = "#fff";
        reducedConditions.push(penalty);
        return reducedConditions;
    }

    // ***********************************************************
    //                  Listeners & context menus
    // ***********************************************************

    /**
     * Registers additional listeners to modify bonus actions and skill selections.
     * @param {jQuery.Element} html The JQuery element of the tracker.
     */
    activateListeners(html) {
        super.activateListeners(html);
        if (!this.viewed) return;

        // Replace combat tracker config with our own configuration dialog.
        html.find(".combat-settings").off("click").click(ev => {
            ev.preventDefault();
            new TdeCombatConfig(this.viewed).render(true);
        });

        if (this.viewed.isHistorical) return; // Skip the rest for the historical view.

        if (this.viewed.isDistributing) {
            html.find("input.addGroupPoint").click(this.addGroupPoint.bind(this));
            html.find("input.removeGroupPoint").click(this.removeGroupPoint.bind(this));
        }

        if (!this.viewed.isPreparing) {
            html.find("div.action-points.owner")
                .on("mouseenter", "div.action-point", this.highlightActionPoints.bind(this))
                .on("click", "div.action-point", this.modifyActionPoints.bind(this))
                .on("mouseleave", "div.action-point", this.resetHover.bind(this));
            html.find("div.resource-diamond-wrapper.adrenaline.owner")
                .on("mouseenter", this.highlightAdrenaline.bind(this))
                .on("click", this.convertAdrenaline.bind(this))
                .on("mouseleave", this.resetHover.bind(this));
        }

        addMessageListener(html);
        html.find(".token-selection select.skill-select").change(this.updateCombatantSkill.bind(this));
        html.find(".token-selection select.loadout-select").change(this.updateCombatantLoadout.bind(this));
    }

    /**
     * Adds a group action point to the group identified by the event's element.
     * @param {jQuery.Event} event The JQuery event of the button click.
     */
    addGroupPoint(event) {
        event.preventDefault();
        this.viewed.modifyGroupPoints(parseInt(event.currentTarget.dataset.group, 10), 1, true);
    }

    /**
     * Subtracts a group action point from the group identified by the event's element.
     * @param {jQuery.Event} event The JQuery event of the button click.
     */
    removeGroupPoint(event) {
        event.preventDefault();
        this.viewed.modifyGroupPoints(parseInt(event.currentTarget.dataset.group, 10), -1, true);
    }

    /**
     * Highlights hovered action points to indicate whether they can be claimed or consumed.
     * @param {jQuery.Event} event The JQuery event of the hover.
     */
    highlightActionPoints(event) {
        const pointElement = event.currentTarget;
        const combatant = this._resolveCombatant(pointElement.closest("li.combatant"));
        if (!combatant) return;

        if (pointElement.classList.contains("empty")) {
            // Hovering empty action point, prepare for claiming a group point.
            if (!this.viewed.isDistributing) return; // No claiming outside of distribution phase.

            const availablePoints = this.viewed.getGroupPoints(combatant.group);
            if (!availablePoints) return;

            const requestedPoints = this._getAvailablePoints(pointElement, availablePoints, true);
            requestedPoints.forEach(point => point.classList.add("flashing-good"));

            // Highlight group points that would be claimed.
            const groupPointElement = this.element[0]
                .querySelector(`div.group:nth-child(${combatant.group + 1})`)
                .querySelector("div.action-point:not(.empty)");
            const claimedPoints = this._getAvailablePoints(groupPointElement, requestedPoints.length);
            claimedPoints.forEach(point => point.classList.add("flashing-bad"));
        } else {
            // Hovering available action point, prepare for spending a point.
            const availablePoints = this.viewed.isDistributing
                ? (combatant.getFlag("tde5e", "claimedPoints") ?? 0)
                : combatant.initiative;
            if (!availablePoints) return;

            const offeredPoints = this._getAvailablePoints(pointElement, availablePoints);
            offeredPoints.forEach(point => point.classList.add("flashing-bad"));

            if (this.viewed.isDistributing) {
                // Highlight group points that would be refunded.
                const groupPointElement = this.element[0]
                    .querySelector(`div.group:nth-child(${combatant.group + 1})`)
                    .querySelector("div.action-point.empty:last-of-type");
                const refundedPoints = this._getAvailablePoints(groupPointElement, offeredPoints.length, true);
                refundedPoints.forEach(point => point.classList.add("flashing-good"));
            }
        }
    }

    /**
     * Searches up to the given amount of siblings of the given element for empty or non-empty action points.
     * @param {HTMLElement} pointElement The element of the originating action point.
     * @param {number} maxAmount The maximum amount of points to return.
     * @param {boolean} empty Indicates whether to search forward for empty points or backward for non-empty points.
     * @returns {HTMLElement[]} The array of available action point elements.
     */
    _getAvailablePoints(pointElement, maxAmount, empty = false) {
        if (!pointElement) return [];

        const requestedPoints = [];
        let isEmpty = false;
        do {
            requestedPoints.push(pointElement);
            pointElement = empty ? pointElement.previousElementSibling : pointElement.nextElementSibling;
            if (!pointElement) break;
            isEmpty = pointElement.classList.contains("empty");
        } while (empty ? isEmpty : !isEmpty);

        return requestedPoints.slice(Math.min(requestedPoints.length, maxAmount) * -1);
    }

    /**
     * Claims or consumes the action points highlighted by @see highlightActionPoints.
     * @param {jQuery.Event} event The JQuery event of the click.
     * @returns {Promise} A promise representing the combat or combatant update.
     */
    async modifyActionPoints(event) {
        const combatant = this._resolveCombatant(event.currentTarget.closest("li.combatant"));
        const previousClaim = combatant.getFlag("tde5e", "claimedPoints") ?? 0;

        const claimingPoints = event.delegateTarget.querySelectorAll("div.action-point.flashing-good").length;
        if (claimingPoints) {
            // Points are positive, claim them from the group pool.
            const claimedPoints = Math.min(claimingPoints, this.viewed.getGroupPoints(combatant.group));
            if (claimedPoints <= 0) return Promise.resolve();

            await combatant.update({
                initiative: combatant.initiative + claimedPoints,
                "flags.tde5e.claimedPoints": previousClaim + claimedPoints,
            }, { render: false });
            return redirectActionPointClaim(this.viewed.id, combatant.group, claimedPoints * -1);
        }
        // Points are negative, consume them from the combatant.
        const consumingPoints = event.delegateTarget.querySelectorAll("div.action-point.flashing-bad").length;
        if (consumingPoints <= 0) return Promise.resolve();

        const initiative = Math.max(0, combatant.initiative - consumingPoints);
        const update = { initiative };

        if (this.viewed.isDistributing) {
            // When in distribution phase, restore the points to the group pool.
            update["flags.tde5e.claimedPoints"] = previousClaim - consumingPoints;
            await combatant.update(update, { render: false });
            return redirectActionPointClaim(this.viewed.id, combatant.group, consumingPoints);
        } if (this.viewed.isActionPhase && initiative === 0 && !this.viewed.nextCombatant.canAct()) {
            await combatant.update(update, { render: false });
            return this.viewed.nextTurn();
        }
        return combatant.update(update);
    }

    /**
     * Highlights an action point if it can be converted from adrenaline.
     * @param {jQuery.Event} event The JQuery event of the hover.
     */
    highlightAdrenaline(event) {
        const combatantEl = event.currentTarget.closest("li.combatant");
        const combatant = this._resolveCombatant(combatantEl);
        const adr = combatant.actor.system.resources.adrenaline.value;
        if (adr < 10 || combatant.initiative >= combatant.getFlag("tde5e", "maxInitiative")) return;

        combatantEl.querySelector("div.action-points > div.action-point.empty")?.classList.add("flashing-good");
    }

    /**
     * Attempts to convert adrenaline into action points when the resource is clicked.
     * @param {jQuery.Event} event The JQuery event of the click.
     * @returns {Promise} A promise representing the combatant update.
     */
    async convertAdrenaline(event) {
        const combatant = this._resolveCombatant(event.currentTarget.closest("li.combatant"));
        const adr = combatant.actor.system.resources.adrenaline.value;
        if (adr < 10) {
            warn(game.i18n.localize("tde5e.combat.controls.missingAdrenaline"), true);
            return Promise.resolve();
        }

        if (combatant.initiative >= combatant.getFlag("tde5e", "maxInitiative")) {
            warn(game.i18n.localize("tde5e.combat.controls.exceededPoints"), true);
            return Promise.resolve();
        }

        await combatant.update({ initiative: combatant.initiative + 1 }, { render: false });
        return combatant.actor.update({ "system.resources.adrenaline.value": adr - 10 });
    }

    /**
     * Resets highlighted action points when the mouse leaves.
     */
    resetHover() {
        this.element[0].querySelectorAll("div.action-point")
            .forEach(point => point.classList.remove("flashing-good", "flashing-bad"));
    }

    /**
     * Handles combatant skill selection changes by updating the associated flag.
     * @param {jQuery.Event} event The JQuery event of the skill select.
     */
    updateCombatantSkill(event) {
        const { combatantId } = event.currentTarget.closest("li.combatant").dataset;
        this.viewed.combatants.get(combatantId)?.changeSkill(
            event.currentTarget.value,
            event.currentTarget.options[event.currentTarget.selectedIndex].text,
        );
    }

    /**
     * Handles combatant loadout selection changes by updating the parent actor's active loadout.
     * @param {jQuery.Event} event The JQuery event of the loadout select.
     */
    updateCombatantLoadout(event) {
        const { combatantId } = event.currentTarget.closest("li.combatant").dataset;
        this.viewed.combatants.get(combatantId)?.changeLoadout(event.currentTarget.value);
    }

    /**
     * Extend the combatant context menu with our own entries to allow system-specific combatant modifications.
     * @returns {object[]} The extended array of context menu entries.
     * @override
     */
    _getEntryContextOptions() {
        if (!game.combat || game.combat.isHistorical) return [];
        const entries = super._getEntryContextOptions();
        entries.push({
            name: "tde5e.combat.context.switchTeam",
            icon: "<i class=\"fas fa-exchange-alt\"></i>",
            callback: target => this._resolveCombatant(target).switchTeam(),
        });
        entries.push({
            name: "tde5e.combat.context.showRoll",
            icon: "<i class=\"fas fa-dice\"></i>",
            condition: target => this._resolveCombatant(target).players.length === 0,
            callback: target => this._resolveCombatant(target).displaySkillCheck(),
        });
        entries.push({
            name: "tde5e.combat.context.addActionPoint",
            icon: "<i class=\"fas fa-plus\"></i>",
            callback: target => this._resolveCombatant(target).addActionPoint(),
        });
        entries.push({
            name: "tde5e.combat.context.consumeActionPoint",
            icon: "<i class=\"fas fa-minus\"></i>",
            condition: target => this._resolveCombatant(target).initiative > 0,
            callback: target => this._resolveCombatant(target).consumeActionPoints(1),
        });
        entries.push({
            name: "tde5e.combat.context.delayAction",
            icon: "<i class=\"fas fa-sort-amount-down\"></i>",
            condition: target => this._resolveCombatant(target).initiative > 0,
            callback: target => this._resolveCombatant(target).delayAction(),
        });
        return entries;
    }

    /**
     * Resolves a combatant using the given target element from the currently viewed combat.
     * @param {HTMLElement|jQuery.Element} target The UI element containing the combatant's ID.
     * @returns {TdeCombatant} The combatant, if it exists.
     */
    _resolveCombatant(target) {
        if (target.length) [target] = target;
        return this.viewed.combatants.get(target.dataset.combatantId);
    }

    /**
     * Extend the combatant configuration to display our own dialog.
     * @param {jQuery.Element} li The JQuery element of the combatant.
     * @override
     */
    _onConfigureCombatant(li) {
        const combatant = this.viewed.combatants.get(li.data("combatantId"));
        new CombatantConfig(combatant, {
            template: "systems/tde5e/templates/combat/combatantConfig.hbs",
            top: Math.min(li[0].offsetTop, window.innerHeight - 350),
            left: window.innerWidth - 720,
            width: 400,
        }).render(true);
    }

    // ***********************************************************
    //                Utilities & shortcut methods
    // ***********************************************************

    /**
     * Creates an HTML snapshot of the current combat tracker state with disabled inputs and links.
     * @param {jQuery.Element} html The JQuery element containing the combat tracker.
     * @returns {string} The adjusted HTML content of the combat tracker.
     */
    static createHistoricalView(html) {
        const element = html.find("#combat-tracker").clone();
        element.find("select, input").prop("disabled", true);
        element.find("a").attr("disabled", "");
        element.find("li.combatant.active")
            .removeClass("active")
            .children("div.combat-suggestions").remove();
        return element.html();
    }
}
