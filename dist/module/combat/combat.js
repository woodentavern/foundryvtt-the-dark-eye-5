import { sendTieMessage } from "../chat.js";
import { redirectPhaseEnd } from "../socket.js";
import TdeSkillCheck from "../ui/rolls/skillCheck.js";
import * as util from "../util.js";
import TdeCombatTracker from "./combatTracker.js";
import initializeSuggestions from "./contextSuggestions.js";

/**
 * Extends Foundry's combat to implement our own system.
 */
export default class TdeCombat extends Combat {
    /**
     * Creates a new combat instance.
     * @param {object} data The data of the instance.
     * @param {object} context The context of the instance creation.
     */
    constructor(data, context) {
        super(data, context);
        this.previous ??= this.current; // Ensure that there is a previous turn.
    }

    // ***********************************************************
    //                       Turn preparation
    // ***********************************************************

    /**
     * Replaces the preparation of combatants into a coherent turn order to
     *  separate top and bonus action preparation.
     * @param {boolean} skipFirst Indicates whether the first turn should stay in place regardless of its order.
     * @returns {object[]} An array of fully prepared turns.
     * @override
     */
    setupTurns(skipFirst = true) {
        if (this.isHistorical) return [];
        if (this.isDistributing) skipFirst = false;

        const turns = [...this.combatants.values()];
        if (this.isPreparing) {
            // Sort by name during preparation.
            turns.sort((t1, t2) => t1.name.localeCompare(t2.name));
        } else if (skipFirst && this.turns.length > 1 && this.turns[0].canAct()) {
            // Preserve the current first turn and sort the rest.
            const firstTurn = this.turns[0];
            const index = turns.indexOf(firstTurn);
            turns.splice(index, 1);
            turns.sort(this._sortCombatants);
            turns.unshift(firstTurn);
        } else {
            turns.sort(this._sortCombatants);
        }

        // Update the current turn.
        this.turn = 0;
        const currentCombatant = this.isActionPhase ? turns[0] : null;
        this.previous = this.current;
        this.current = {
            round: this.round,
            turn: this.turn,
            phase: this.flags.tde5e?.phase,
            combatantId: currentCombatant ? currentCombatant.id : null,
            tokenId: currentCombatant ? currentCombatant.tokenId : null,
        };

        this.turns = turns;
        return turns;
    }

    /** @override */
    debounceSetup = foundry.utils.debounce((skipFirst, turnEvents = true) => {
        this.setupTurns(skipFirst);
        if (ui.combat.viewed === this) ui.combat.render();
        if (turnEvents) this._manageTurnEvents();
    }, 50);

    /**
     * Replaces Foundry's turn sorting to account for delayed turns and ties.
     * @param {TdeCombatant} a The first combatant to compare.
     * @param {TdeCombatant} b The second combatant to compare.
     * @returns {number} A positive number if the second combatant should be sorted before the first combatant,
     *  0 if they are even, a negative number otherwise.
     * @override
     */
    _sortCombatants(a, b) {
        // Sort by skill points during actions.
        let orderA = a.isDefeated ? 0 : (a.initiative ?? 0);
        let orderB = b.isDefeated ? 0 : (b.initiative ?? 0);
        if (orderA === 0 || orderB === 0) return orderB - orderA; // At least one doesn't have an action.

        // Break ties with initiative rolls.
        if (orderA === orderB) {
            orderA += (a.getFlag("tde5e", "tieInitiative") ?? 0) / 1000;
            orderB += (b.getFlag("tde5e", "tieInitiative") ?? 0) / 1000;
        }

        // Invert order for delayed actions.
        if (a.getFlag("tde5e", "delay")) orderA *= -1;
        if (b.getFlag("tde5e", "delay")) orderB *= -1;

        return orderB - orderA;
    }

    // ***********************************************************
    //              Round, phase & turn manipulation
    // ***********************************************************

    /**
     * Checks whether the current combat state allows continueing to the next turn.
     * This is not the case when the current turn would not change by continueing.
     * @returns {boolean} True if the next turn can be started, false otherwise.
     */
    get canContinue() {
        return this.isActionPhase && this.canDelay && this.nextCombatant.initiative > this.combatant.initiative;
    }

    /**
     * Checks whether the current combat state allows delaying the current turn.
     * This is not the case when it's the last turn or the only turn that has not delayed yet.
     * @returns {boolean} True if the current turn can be delayed, false otherwise.
     */
    get canDelay() {
        if (!this.isActionPhase) return false;
        const next = this.nextCombatant;
        return next.canAct() && !next.getFlag("tde5e", "delay");
    }

    /**
     * Checks if the combat is currently in the preparation phase.
     * @returns {boolean} True if the combat is preparing, false otherwise.
     */
    get isPreparing() {
        return this.flags.tde5e?.phase === "preparation";
    }

    /**
     * Checks if the combat is currently in the action point distribution phase.
     * @returns {boolean} True if action points can be distributed, false otherwise.
     */
    get isDistributing() {
        return this.flags.tde5e?.phase === "distribution";
    }

    /**
     * Checks if the combat is currently in the action phase.
     * @returns {boolean} True if the actions are ongoing, false otherwise.
     */
    get isActionPhase() {
        return this.flags.tde5e?.phase === "actions";
    }

    /**
     * Checks if the current combat round is ending.
     * @returns {boolean} True if there are no more turns left in the current round, false otherwise.
     */
    get isRoundEnding() {
        return this.flags.tde5e?.phase === "roundEnd";
    }

    /**
     * Checks if the combat is currently in a historical view.
     * @returns {boolean} True if the viewed turn is the current turn, false otherwise.
     */
    get isHistorical() {
        return this.flags.tde5e?.phase === "history";
    }

    /**
     * Leaves the historical view to display the current round.
     * @returns {Promise} A promise representing the combat tracker rendering.
     */
    async returnToCurrent() {
        this.flags.tde5e.phase = this.currentPhase ?? "roundEnd";
        this.flags.tde5e.historicalRound = null;
        delete this.currentPhase;
        return ui.combat.render();
    }

    /**
     * Ends the preparation phase by rolling missing NPC skill checks and generating action points.
     * @returns {Promise} A promise representing the operations.
     */
    async endPreparation() {
        await this.rollNPC(); // Make sure all NPCs have rolled a check.

        // Generate personal and group action points.
        const combatantUpdates = this.combatants
            .filter(c => c.canAct())
            .map(c => [c, { _id: c.id }]);
        const groupPoints = this.distributeActionPoints(combatantUpdates);
        await this.updateEmbeddedDocuments("Combatant", combatantUpdates.map(u => u[1]), { render: false });

        // Check which phase to advance to.
        let nextPhase = "distribution";
        if (!combatantUpdates.length) {
            nextPhase = "roundEnd";
            this._queueHistoryCreation();
        } else if (groupPoints.every(p => p === 0)) {
            nextPhase = "actions";
            this._queueHistoryCreation();
        }

        return this.update({
            "flags.tde5e": {
                phase: nextPhase,
                groupPoints,
                maxGroupPoints: groupPoints,
            },
        });
    }

    /**
     * Ends the distribution phase by resolving tied action points and creating a historical view of the tracker.
     * @returns {Promise} A promise representing the combat updates.
     */
    async endDistribution() {
        // Resolve ties and update combatants.
        const combatantUpdates = this.combatants
            .filter(c => c.canAct())
            .map(c => [c, { _id: c.id }]);
        await this.resolveTies(combatantUpdates);
        await this.updateEmbeddedDocuments("Combatant", combatantUpdates.map(u => u[1]), { render: false });

        // If any ties were rolled, send a chat message.
        const tiedCombatants = combatantUpdates
            .filter(u => u[1].hasOwnProperty("flags.tde5e.tieInitiative"))
            .map(u => u[0]);
        if (tiedCombatants.length) sendTieMessage(tiedCombatants);

        this._queueHistoryCreation();

        // Advance to action phase.
        return this.update({ "flags.tde5e.phase": "actions" });
    }

    /**
     * Ends the action phase.
     * @returns {Promise} A promise representing the combat update.
     */
    endActions() {
        return this.setFlag("tde5e", "phase", "roundEnd");
    }

    /**
     * Resolves ties by rolling the involved combatants' physical initiatives until a valid separation is achieved.
     * @param {object[]} turnUpdates The array of turns to check for ties and their updates.
     * @returns {Promise} A promise representing the asynchronous rolls.
     */
    async resolveTies(turnUpdates) {
        const groupedTurns = util.groupBy(turnUpdates, "0.initiative");
        for (const tiedTurns of Object.values(groupedTurns)) {
            if (tiedTurns.length <= 1) continue;

            const initiatives = new Set();
            for (let i = 1; initiatives.size !== tiedTurns.length; i *= 100) {
                initiatives.clear();
                await Promise.all(tiedTurns.map(async ([turn, update]) => {
                    const roll = await new Roll(`d6+${turn.actor?.system.derived.physicalInitiative.value ?? 12}`)
                        .evaluate();
                    const currentIni = update["flags.tde5e.tieInitiative"] ?? 0;
                    const ini = roll.total / i + currentIni;
                    update["flags.tde5e.tieInitiative"] = ini;
                    initiatives.add(ini);
                }));
            }
        }
    }

    /**
     * Distributes the rolled quality levels into personal and group action points.
     * @param {object[]} turnUpdates The array of turns to distribute action points to and their updates.
     * @returns {number[]} The array of action points per group.
     */
    distributeActionPoints(turnUpdates) {
        const groupPoints = [0, 0];
        for (const [turn, update] of turnUpdates) {
            const groupContribution = Math.floor(turn.initiative / 2);
            const defaultPoints = turn.actor?.system.settings?.bonusActionPoints ?? 1;
            const ownPoints = defaultPoints + turn.initiative - groupContribution;
            groupPoints[turn.isAlly ? 0 : 1] += groupContribution;
            update.initiative = ownPoints;
        }

        return groupPoints.map((points, index) => {
            const reduction = groupPoints
                .filter((_, i) => index !== i) // Skip self for reduction.
                .map(p => p / this.getFlag("tde5e", "groupReductionRatio"))
                .reduce((acc, p) => acc + p, 0);
            return Math.max(0, points - Math.round(reduction));
        });
    }

    /**
     * Registers a one time rendering hook to create a historical view of the round.
     */
    _queueHistoryCreation() {
        Hooks.once("renderCombatTracker", (tracker, html) => {
            const historicalView = TdeCombatTracker.createHistoricalView(html);
            const combat = tracker.viewed;
            combat.update({ [`flags.tde5e.history.${combat.round}`]: historicalView }, { render: false });
        });
    }

    /**
     * Adds or subtracts a number of group action points from the group with the given index.
     * @param {number} groupIndex The index of the group to modify.
     * @param {number} delta The amount of points to add or subtract.
     * @param {boolean=} modifyMax Indicates whether the maximum group points should also be changed. Defaults to false.
     * @returns {Promise} A promise representing the combat update.
     */
    async modifyGroupPoints(groupIndex, delta, modifyMax = false) {
        if (delta === 0 || !util.hasValue(groupIndex) || groupIndex < 0) return Promise.resolve();

        const currentPoints = this.getFlag("tde5e", "groupPoints");
        if (delta < 0) delta = Math.max(delta, currentPoints[groupIndex] * -1);
        currentPoints[groupIndex] += delta;

        if (modifyMax) {
            const maxPoints = this.getFlag("tde5e", "maxGroupPoints") ?? currentPoints;
            maxPoints[groupIndex] += delta;
            await this.update({
                "flags.tde5e": {
                    groupPoints: currentPoints,
                    maxGroupPoints: maxPoints,
                },
            });
        } else {
            await this.setFlag("tde5e", "groupPoints", currentPoints);
        }
        return this.autoforward();
    }

    /**
     * Gets the amount of available action points for the group with the given index.
     * @param {number} groupIndex The index of the group.
     * @returns {number} The group's action points or 0 if it doesn't exist.
     */
    getGroupPoints(groupIndex) {
        return this.getFlag("tde5e", "groupPoints")[groupIndex] ?? 0;
    }

    /**
     * Advance to the next turn (or round, if there are no remaining turns).
     * @returns {Promise} A promise representing the combat update.
     * @override
     */
    async nextTurn() {
        if (this.isPreparing) {
            // End preparation instead of advancing.
            if (!game.user.isGM) return Promise.resolve();
            return this.endPreparation();
        }

        if (this.isDistributing) {
            // End distribution instead of advancing.
            if (!game.user.isGM) return Promise.resolve();
            return this.endDistribution();
        }

        // Determine the next turn number.
        const next = this.findNextActingTurn();
        if (next === -1) return redirectPhaseEnd(this.id, "actions");

        // Update the combat.
        const advanceTime = CONFIG.time.turnTime * next;
        return this.nextCombatant.initiative <= this.combatant.initiative
            ? this.combatant.delayAction()
            : this.update({ turn: 0 }, { advanceTime, diff: false, nextTurn: true });
    }

    /**
     * Disable returning to the previous turn as the action point order can't be reversed.
     * @override
     */
    async previousTurn() { return Promise.resolve(); }

    /**
     * Advance to the next round, creating a history snapshot of the previous
     *  round if we're not already looking at one.
     * @returns {Promise} A promise representing the combat update.
     * @override
     */
    async nextRound() {
        if (this.isHistorical) {
            // Move forward in the history.
            this.flags.tde5e.historicalRound++;
            if (!this.flags.tde5e.history[this.flags.tde5e.historicalRound]) return this.returnToCurrent();
            return ui.combat.render();
        }

        if (!this.isRoundEnding) {
            if (!await Dialog.confirm({
                title: game.i18n.localize("tde5e.combat.nextRoundDialog.title"),
                content: game.i18n.localize("tde5e.combat.nextRoundDialog.description"),
            })) return Promise.resolve();
        }

        await this.resetCombatants();
        await this.distributeAdrenaline();

        // Foundry behavior to advance the game time.
        let advanceTime = this.remainingTurns.length * CONFIG.time.turnTime;
        advanceTime += CONFIG.time.roundTime;

        // Reset round, turn and phase.
        return this.update({
            round: this.round + 1,
            turn: 0,
            "flags.tde5e.phase": "preparation",
        }, { advanceTime });
    }

    /**
     * Enter a historical view of the previous round. This does not alter the
     *  current combat state and is only applied locally.
     * @override
     */
    previousRound() {
        let round = Number.isNumeric(this.flags.tde5e.historicalRound)
            ? this.flags.tde5e.historicalRound - 1
            : this.round - 1;

        if (!this.isHistorical) {
            this.currentPhase = this.flags.tde5e.phase; // Retain the current phase for jumping back to it.
            round += 1;
        }

        this.flags.tde5e.phase = "history";
        this.flags.tde5e.historicalRound = round;
        ui.combat.render();
    }

    // ***********************************************************
    //                  Combatant manipulation
    // ***********************************************************

    /**
     * Gathers the combatants of the opposing team, including players that have
     *  been switched. This does not include bonus actions.
     * @returns {object[]} An array of enemy combatants.
     */
    get enemies() {
        return this.combatants.filter(combatant => !combatant.isAlly && !combatant.defeated);
    }

    /**
     * Gathers the combatants of the player team, including NPCs that have been
     *  switched. This does not include bonus actions.
     * @returns {object[]} An array of allied combatants.
     */
    get allies() {
        return this.combatants.filter(combatant => combatant.isAlly && !combatant.defeated);
    }

    /**
     * Gathers all turns that have an action and are not over yet.
     * @returns {TdeCombatant[]} An array of turns that can still act.
     */
    get remainingTurns() {
        if (this.isPreparing) return this.turns;
        return this.turns.filter(t => t.canAct());
    }

    /**
     * Resets all combatants and removes defeated characters.
     * @returns {Promise} A promise representing the combatant updates.
     */
    async resetCombatants() {
        const combatantUpdates = [];
        const combatantRemovals = [];
        for (const combatant of this.combatants) {
            if (combatant.isDefeated) {
                // Queue for removal.
                combatantRemovals.push(combatant.id);
                continue;
            }

            // Reset to default values.
            combatantUpdates.push({
                _id: combatant.id,
                initiative: null,
                flags: {
                    tde5e: {
                        claimedPoints: 0,
                        tieInitiative: 0,
                        delay: false,
                        selectedSkill: {
                            result: null,
                        },
                    },
                },
            });
        }

        // Apply the batched updates.
        if (combatantRemovals.length) await this.deleteEmbeddedDocuments("Combatant", combatantRemovals);
        return this.updateEmbeddedDocuments("Combatant", combatantUpdates);
    }

    /**
     * Distributes adrenaline for all non-defeated turns based on their actor's active loadout.
     * @returns {Promise} A promise representing the actor updates.
     */
    distributeAdrenaline() {
        return Promise.all(this.turns.filter(t => !t.defeated).map(t => {
            const { actor } = t;
            if (!actor) return Promise.resolve();

            const bonus = this.getFlag("tde5e", "adrenalineBonus") ?? 0;
            const loadoutAdrenaline = t.activeLoadout?.adrenaline ?? 0;
            const adrenalineGain = loadoutAdrenaline + bonus;

            if (adrenalineGain <= 0) return Promise.resolve();

            const { adrenaline } = actor.system.resources;
            if (adrenaline.value >= adrenaline.max) return Promise.resolve();

            const newAdr = Math.clamp(adrenaline.value + adrenalineGain, 0, adrenaline.max);
            return actor.update({ "system.resources.adrenaline.value": newAdr });
        }));
    }

    /**
     * Rolls or rerolls the skill checks of the turns identified by the given
     *  array of IDs and updates the results.
     * @param {string[]} ids The array of turn IDs to reroll.
     * @returns {Promise.<TdeCombat>} A promise representing the combatant updates.
     * @override
     */
    async rollInitiative(ids) {
        if (!Array.isArray(ids)) ids = [ids];

        const rolls = [];
        const updates = [];
        for (const id of ids) {
            // Find the combatant.
            const combatant = this.combatants.get(id);
            if (!combatant || !combatant.isOwner || !combatant.actor) continue;

            // Create a copy of the selected skill.
            const selection = { selectedSkill: foundry.utils.deepClone(combatant.flags.tde5e.selectedSkill ?? {}) };

            if (!selection.selectedSkill.id) {
                selection.selectedSkill.name = "-";
                updates.push(this._createInitiativeUpdate(combatant, selection, null));
                continue;
            }

            // Roll the selected skill.
            const skillItem = combatant.actor.items.get(selection.selectedSkill.id);
            if (!skillItem) {
                util.error(`Actor ${combatant.actor.name} does not contain item ${selection.selectedSkill.id}.`);
                continue;
            }

            // For users, display the roll in chat instead of updating the result directly.
            if (combatant.actor.hasPlayerOwner) {
                skillItem.roll();
                continue;
            }

            selection.selectedSkill.name = skillItem.name;
            rolls.push(this._evaluateInitiativeRoll(combatant, skillItem, selection));
        }

        updates.push(...await Promise.all(rolls));
        if (!updates.length) return this;

        // Apply the batched updates.
        await this.updateEmbeddedDocuments("Combatant", updates);
        await this.autoforward();
        return this;
    }

    /**
     * Rolls the given skill and creates a combatant update from the result.
     * @param {TdeCombatant} combatant The combatant to roll the skill for.
     * @param {TdeItem} skill The item of the skill to roll.
     * @param {object} selection The combatant's skill selection data.
     * @returns {Promise.<object>} A promise representing the combatant update.
     */
    async _evaluateInitiativeRoll(combatant, skill, selection) {
        const roll = await new TdeSkillCheck(skill).evaluate();
        let result = roll._total;
        if (result.base) {
            const skillPoints = result.remainingSkillPoints;
            result = result.base;
            result.modifiedSkillPoints = skillPoints;
        }
        selection.selectedSkill.result = result;
        return this._createInitiativeUpdate(combatant, selection, result);
    }

    /**
     * Creates a combatant update from the given skill check result.
     * @param {TdeCombatant} combatant The combatant to create the update for.
     * @param {object} selection The combatant's skill selection data.
     * @param {object?} result An optional skill check result.
     * @returns {object} The combatant update.
     */
    _createInitiativeUpdate(combatant, selection, result) {
        const actionPoints = result?.qualityLevel ?? 0;
        const claimedPoints = combatant.getFlag("tde5e", "claimedPoints") ?? 0;
        return {
            _id: combatant.id,
            initiative: actionPoints + claimedPoints,
            flags: { tde5e: selection },
        };
    }

    /**
     * Override Foundry's method to include combatants with 0 initiative.
     * @see {@link https://foundryvtt.com/api/Combat.html#rollNPC} for additional details.
     * @override
     */
    async rollNPC(...args) {
        const npcs = this.turns.filter(t => (!t.actor || !t.players.length) && !Number.isNumeric(t.initiative));
        return this.rollInitiative(npcs.map(t => t.id), ...args);
    }

    /* -------------------------------------------- */

    /**
     * Override Foundry's method to include combatants with 0 initiative.
     * @see {@link https://foundryvtt.com/api/Combat.html#rollAll} for additional details.
     * @override
     */
    async rollAll(...args) {
        const unrolled = this.turns.filter(t => t.isOwner && !Number.isNumeric(t.initiative));
        return this.rollInitiative(unrolled.map(t => t._id), ...args);
    }

    /**
     * Associates the given skill check with the next available combatant that belongs to the actor of the check.
     * @param {ChatMessage} message The chat message containing the roll.
     * @param {boolean=} force Flag indicating whether the roll should override all other skill checks.
     *  Defaults to false.
     * @returns {Promise} A promise representing the combatant update.
     */
    async connectRoll(message, force = false) {
        if (!this.round || !message) return Promise.resolve();
        const skillCheck = message.rolls.find(roll => roll instanceof TdeSkillCheck);
        if (!skillCheck) return Promise.resolve();

        // Find first combatant associated with the roll.
        const combatant = this.turns.find(c => c.isOwner && c.actor?.uuid === skillCheck.data.actor.uuid);
        await combatant?.connectSkillCheck(skillCheck, message.id, force);
        return this.autoforward();
    }

    /**
     * Checks if the current phase is considered complete. If so (and if the settings allow autoforwarding), the phase
     *  is ended.
     * @returns {Promise} A promise representing the phase change update (if any were triggered).
     */
    autoforward() {
        if (game.settings.get("tde5e", "autoforwardCombat")) {
            if (this.isPreparing && this.turns.every(c => c.initiative !== null)) {
                return redirectPhaseEnd(this.id, "preparation");
            }

            if (this.isDistributing && this.getFlag("tde5e", "groupPoints")?.every(p => p === 0)) {
                return redirectPhaseEnd(this.id, "distribution");
            }
        }

        return Promise.resolve();
    }

    /**
     * Extends Foundry's method to initialize custom combat data.
     * @override
     */
    async _preCreate(data, options, user) {
        await super._preCreate(data, options, user);
        this.updateSource({
            round: 1,
            turn: 0,
            "flags.tde5e": {
                phase: "preparation",
                history: {},
                groupReductionRatio: 2,
                adrenalineBonus: 2,
            },
        });
    }

    /**
     * Extends FoundryVTT's method to process phase updates.
     * @override
     */
    _onUpdate(data, options) {
        if (foundry.utils.hasProperty(data, "flags.tde5e.phase")) {
            this.setupTurns(false);
            Hooks.once("renderCombatTracker", (_tracker, html) => {
                html.find("nav > h4.phase").addClass("flash-text");
            });
        } else if (options.nextTurn) {
            this.setupTurns(false);
            this.updateCombatantActors();

            // Play sounds
            if (game.user.character) {
                if (this.combatant?.actorId === game.user.character._id) this._playCombatSound("yourTurn");
                else if (this.nextCombatant?.actorId === game.user.character._id) this._playCombatSound("nextUp");
            }
        } else if (data.active === true && this.isActive) {
            ui.combat.initialize({ combat: this });
        } else if ("scene" in data) {
            ui.combat.initialize();
        }

        if (options.turnEvents !== false) this._manageTurnEvents();
    }

    /**
     * Extends Foundry's method to create multiple combatants depending on the actor settings.
     * @override
     */
    async createEmbeddedDocuments(documentName, data, options) {
        if (documentName !== "Combatant") return super.createEmbeddedDocuments(documentName, data, options);

        const duplicates = [];
        for (const combatantData of data) {
            if (!combatantData.actorId) continue;

            const actor = game.actors.get(combatantData.actorId);
            if (!actor) continue;

            // Add additional combatants based on the actor configuration.
            const count = foundry.utils.getProperty(actor, "system.settings.combatantCount");
            for (let i = 1; i < count; i++) duplicates.push(combatantData);
        }

        return super.createEmbeddedDocuments(documentName, data.concat(duplicates), options);
    }

    /**
     * Replaces Foundry's method to prevent current turn modifications.
     * @inheritdoc
     */
    _onUpdateDescendantDocuments(_parent, collection, combatants, changes, options) {
        if (collection !== "combatants") return;
        if (game.users.activeGM?.isSelf && this.isRoundEnding
            && combatants.some(c => c.canAct() && c.initiative !== null)) {
            this.setFlag("tde5e", "phase", "actions");
        } else {
            const firstDone = changes.some(c => c._id === this.current.combatantId
                && (c.initiative === 0 || foundry.utils.getProperty(c, "flags.tde5e.delay")));
            this.debounceSetup(!firstDone, options.turnEvents);
        }
    }

    /**
     * Replaces Foundry's method to prevent changing the current turn. The adjustment is not needed since new
     *  combatants should always be at the end of the turn order until the next preparation phase.
     * @inheritdoc
     */
    _onCreateDescendantDocuments(_parent, collection, _documents, _data, options) {
        if (collection !== "combatants") return;
        this.debounceSetup(true, options.turnEvents);
    }

    /* -------------------------------------------- */

    /**
     * Replaces Foundry's method to adjust how the next acting turn is determined.
     * @inheritdoc
     */
    _onDeleteDescendantDocuments(_parent, collection, documents, ids, options, userId) {
        if (collection !== "combatants") return;
        const removedFirst = ids.includes(this.turns[0].id);
        if (game.users.activeGM?.isSelf && removedFirst && !this.turns[1]?.canAct()) {
            this.setFlag("tde5e", "phase", "roundEnd");
        } else {
            this.debounceSetup(!removedFirst, options.turnEvents);
        }

        if (userId !== game.userId) return;

        // Reset combatant's adrenaline.
        for (const combatant of documents) {
            combatant.actor?.update({ "system.resources.adrenaline.value": 0 });
        }
    }

    /**
     * Extends Foundry's method to clear the combatants' adrenaline value.
     * @inheritdoc
     */
    _onDelete(options, userId) {
        super._onDelete(options, userId);
        if (userId !== game.userId) return;
        this.combatants.forEach(c => c.actor?.update({ "system.resources.adrenaline.value": 0 }));
    }

    /**
     * Replaces Foundry's method to account for combat phases when handling lifecycle events.
     * @returns {Promise} A promise representing the lifecycle events.
     */
    async _manageTurnEvents() {
        if (!game.users.activeGM?.isSelf || !this.started) return;

        const isActions = this.current.phase === "actions";
        const wasActions = this.previous.phase === "actions";

        const advanceRound = this.current.round > (this.previous.round ?? -1);
        const advanceTurn = this.current.combatantId !== this.previous.combatantId || (isActions !== wasActions);
        if (!(advanceTurn || advanceRound)) return;

        const previousCombatant = this.combatants.get(this.previous.combatantId);
        if (wasActions && previousCombatant) await this._onEndTurn(previousCombatant);
        if (advanceRound && (this.previous.round !== null)) await this._onEndRound();
        if (advanceRound) await this._onStartRound();
        if (isActions && this.combatant.canAct()) await this._onStartTurn(this.combatant);
    }

    /**
     * Notifies combat styles that a combatant has moved.
     * @param {TdeCombatant} combatant The combatant that moved.
     */
    _onMoveCombatant(combatant) {
        for (const c of this.combatants) {
            const style = c.activeLoadout?.combatStyle;
            if (style) style.onCombatProgressed(this, { move: combatant });
        }
    }

    // ***********************************************************
    //              Utilities & shortcut methods
    // ***********************************************************

    /**
     * Shortcut for delaying the current turn.
     * @returns {Promise} A promise representing the combatant update.
     * @see {TdeCombatant.delayAction}
     */
    async delayTurn() {
        return this.combatant.delayAction();
    }

    /**
     * Calculates the total quality level of the given combatants skill checks.
     * @param {object[]} combatants The combatants to evaluate.
     * @returns {number} The accumulated quality level of the combatants.
     */
    accumulateQuality(combatants) {
        return combatants.reduce(
            (ql, combatant) => ql + TdeSkillCheck.skillPointsToQuality(combatant.initiative ?? 0),
            0,
        );
    }

    /**
     * Searches the turns for the next turn that has an action.
     * @returns {number} The index of the next turn that has an action or null if there is none.
     */
    findNextActingTurn() {
        const next = this.turns.slice(1).findIndex(t => t.canAct());
        return next === -1 ? next : next + 1;
    }

    // ***********************************************************
    //                      Static methods
    // ***********************************************************

    /**
     * Sets up hooks for keeping the model current.
     */
    static ready() {
        // Adds the result of rolled skill checks during combat to the owner's combatant.
        Hooks.on("createChatMessage", (message, _options, userId) => {
            if (game.user.id !== userId || !message.isRoll) return;
            game.combat?.connectRoll(message);
        });
        Hooks.on("updateChatMessage", (message, _diffData, _options, userId) => {
            if (game.user.id !== userId || !message.isRoll) return;
            game.combat?.connectRoll(message);
        });

        initializeSuggestions();
    }
}
