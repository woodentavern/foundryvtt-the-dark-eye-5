/* eslint-disable no-bitwise -- Allow utilities with bitwise operators. */
/* eslint-disable no-console -- Class contains logging wrappers */

export const logStyle = {
    base: [
        "color: #fff",
        "background-color: #444",
        "padding: 2px 4px",
        "border-radius: 2px",
    ].join(";"),
    warning: [
        "padding: 2px 4px",
        "border-radius: 2px",
        "color: #eee",
        "background-color: #CCD120",
    ].join(";"),
    failure: [
        "padding: 2px 4px",
        "border-radius: 2px",
        "color: #eee",
        "background-color: #CD0000",
    ].join(";"),
    integration: [
        "padding: 2px 4px",
        "border-radius: 2px",
        "color: #fff",
        "background-color: #189400",
    ].join(";"),
    migration: [
        "padding: 2px 4px",
        "border-radius: 2px",
        "color: #fff",
        "background-color: #782570",
    ].join(";"),
};

/**
 *  The prefix every message should be prefixed with.
 */
const notificationPrefix = "The Dark Eye | ";

/**
 * Converts a single number to its romanized form, e.g. XIV for 14.
 * @param {number} num The number to convert.
 * @returns {string} The romanized representation of the number.
 */
export function romanize(num) {
    const roman = {
        M: 1000,
        CM: 900,
        D: 500,
        CD: 400,
        C: 100,
        XC: 90,
        L: 50,
        XL: 40,
        X: 10,
        IX: 9,
        V: 5,
        IV: 4,
        I: 1,
    };
    let romanizedNumber = "";

    for (const i of Object.keys(roman)) {
        const q = Math.floor(num / roman[i]);
        num -= q * roman[i];
        romanizedNumber += i.repeat(q);
    }

    return romanizedNumber;
}

/**
 * Calculates how often the key appeares in the data array.
 * @param {Array} data An array of data.
 * @param {*} key Any key that can occure in the array.
 * @returns {number} The number of times the key occures in the data.
 */
export function countOccurences(data, key) {
    if (!Array.isArray(data)) data = [data];
    let count = 0;
    data.forEach(element => {
        if (element === key) count++;
    });
    return count;
}

/**
 * Compares the contents of the two given arrays for equality.
 * If either value is not an array, they are compared directly, so you can use this for all value types.
 * @param {*} array1 The array to compare with.
 * @param {*} array2 The array to compare.
 * @returns {boolean} True if the contents of the arrays are equal, false otherwise.
 */
export function arrayEquals(array1, array2) {
    if (!Array.isArray(array1) || !Array.isArray(array2)) return array1 === array2;
    if (array1.length !== array2.length) return false;
    return array1.every((entry1, index) => {
        const entry2 = array2[index];
        if (!entry2) return false;
        return typeof (entry1) === "object"
            ? foundry.utils.isEmpty(foundry.utils.diffObject(entry1, entry2))
            : entry1 === entry2;
    });
}

/**
 * Returns a distinct array of the given array.
 * @param {Array} array An array of data.
 * @returns {Array} An array that only contains each value once.
 */
export function distinct(array) {
    if (!Array.isArray(array)) return array;
    return [...new Set(array)];
}

/**
 * Rounds the given numbers to the given amount of decimals.
 * @param {number} num The number to round.
 * @param {number} decimals The amount of decimals to include at most.
 * @returns {number} A correctly rounded number.
 */
export function roundTo(num, decimals) {
    return Number(`${Math.round(`${num}e${decimals}`)}e-${decimals}`);
}

/**
 * Converts the given number to a localized string with the given amount of decimals.
 * @param {number} num The number to convert.
 * @param {number=} decimals The amount of decimals to display. Defaults to 2.
 * @returns {string} The localized string representation of the number.
 */
export function toLocaleFixed(num, decimals = 2) {
    return num.toLocaleString(undefined, { minimumFractionDigits: decimals, maximumFractionDigits: decimals });
}

/**
 * Attempts to parse the given value as an integer.
 * @param {*} value The value to parse.
 * @returns {*} The numeric value if it starts with an integer. The original value otherwise.
 */
export function tryParseInt(value) {
    const numValue = parseInt(value, 10);
    return Number.isNaN(numValue) ? value : numValue;
}

/**
 * Returns true if the given value is defined, i.e. not null or undefined.
 * @param {any} val Any value.
 * @returns {boolean} False if the value is null or undefined, true otherwise.
 */
export function hasValue(val) {
    return !(typeof (val) === "undefined" || val === null);
}

/**
 * Groups the given array of objects by the value of the property identified using the given name.
 * @param {object[]} array The array of objects to group.
 * @param {string} property The name of the property to group by.
 * @param {any?} fallback The default value used if the property has no defined value.
 * @returns {object} An object where each key represents a unique group with the array of grouped items as value.
 */
export function groupBy(array, property, fallback = null) {
    return array.reduce((acc, obj) => {
        const key = foundry.utils.getProperty(obj, property) ?? fallback;
        if (acc.hasOwnProperty(key)) acc[key].push(obj);
        else acc[key] = [obj];
        return acc;
    }, {});
}

/**
 * Accumulates the value of the given property from the given array of objects.
 * @param {object[]} array The array of objects to read the value from.
 * @param {string} property The name of the property to accumulate.
 * @returns {number} The accumulated value.
 */
export function sumBy(array, property) {
    if (!array || array.length === 0) return 0;
    return array.reduce((acc, obj) => acc + (foundry.utils.getProperty(obj, property) ?? 0), 0);
}

/**
 * Takes the given string and capitalizes it.
 * @param {string} text Any defined string.
 * @returns {string} A capitalized version of the string.
 */
export function capitalize(text) {
    return text.charAt(0).toUpperCase() + text.slice(1);
}

/**
 * Generates a system CID by transforming the given item name.
 * @param {string} name The name of the item.
 * @returns {string} The CID of the item.
 */
export function generateCid(name) {
    return name.toUpperCase()
        .replace(/ I$/, " 1")
        .replace(/ II$/, " 2")
        .replace(/ III$/, " 3")
        .replaceAll(/[-–]/g, "")
        .replaceAll(/\s/g, "_")
        .replaceAll(/_?\/_?/g, "_ODER_")
        .replaceAll(/[!?)(,:\][.'’]/g, "")
        .replaceAll(/Ä/g, "A")
        .replaceAll(/Ü/g, "U")
        .replaceAll(/Ö/g, "O")
        .replaceAll(/ß/g, "SS")
        .replaceAll(/&/g, "UND");
}

/**
 * Generates a system ID by transforming the given item CID.
 * @param {string} cid The CID of the item.
 * @returns {string} The ID of the item.
 */
export function generateId(cid) {
    if (!cid || cid.length < 2) cid = "00";
    let id = cid.substring(0, 2);
    for (let i = 0; i < 14; i++) id += Math.floor(Math.random() * 10);
    return id;
}

/**
 * Removes any HTML tags from the given string, retaining their inner content.
 * @param {string} text The text to strip HTML tags from.
 * @returns {string} The text without HTML tags.
 */
export function stripHtml(text) {
    if (!text) return "";
    return text.replace(/(<([^>]+)>)/ig, "");
}

/**
 * Generates a hash for the given string.
 * @see https://github.com/bryc/code/blob/master/jshash/experimental/cyrb53.js
 * @param {string} str The input to generate the hashcode for.
 * @param {number} seed An optional seed to allow variation within the same string.
 * @returns {number} The hashcode of the string.
 */
export function hashcode(str, seed = 0) {
    let h1 = 0xdeadbeef ^ seed;
    let h2 = 0x41c6ce57 ^ seed;
    for (let i = 0, ch; i < str.length; i++) {
        ch = str.charCodeAt(i);
        h1 = Math.imul(h1 ^ ch, 2654435761);
        h2 = Math.imul(h2 ^ ch, 1597334677);
    }
    h1 = Math.imul(h1 ^ (h1 >>> 16), 2246822507);
    h1 ^= Math.imul(h2 ^ (h2 >>> 13), 3266489909);
    h2 = Math.imul(h2 ^ (h2 >>> 16), 2246822507);
    h2 ^= Math.imul(h1 ^ (h1 >>> 13), 3266489909);
    return 4294967296 * (2097151 & h2) + (h1 >>> 0);
}

/**
 * Serializes the given array of documents into an object map with the documents' IDs as keys and the names as values.
 * @param {Document[]} documents The array of documents to serialize.
 * @returns {object} An object containing the document IDs and names.
 */
export function createSelectOptions(documents) {
    return documents.reduce((options, entity) => {
        options[entity.id] = entity.displayName ?? entity.name;
        return options;
    }, {});
}

/**
 * Fetches a compendium pack and index entry for the entity identified by the
 *  given ID. The "tde5e" prefix and variants are normalized.
 * @param {string} uuid The fully qualified ID of the item to link, including the compendium and the indexed ID.
 * @returns {object} An object containing the source pack and the index entry of the target.
 */
export function lookupCompendiumEntry(uuid) {
    // Support legacy data format.
    if (uuid.startsWith("tde5e.") || uuid.startsWith("world.")) uuid = `Compendium.${uuid}`;

    // Resolve collection and document.
    const { collection, documentId } = foundry.utils.parseUuid(uuid);
    const index = collection?.index ?? collection;
    return collection ? {
        pack: collection,
        entry: index?.get(documentId),
    } : {};
}

/**
 * Retreives all regular and token actors of the current scene that are owned by the current user.
 * Note that the token actors are lazily created and may not be available when this method is called.
 * @returns {TdeActor[]} An array of actors owned by the current user.
 */
export function getOwnedActors() {
    return Array.from(game.actors.values()) // Global actors.
        .concat(Object.values(game.actors.tokens)) // Token actors.
        .filter(actor => actor?.isOwner); // Owned actors.
}

/**
 * Determines the main gamemaster of the current session.
 * @returns {User?} The primary gamemaster, if one is available.
 */
export function getPrimaryGm() {
    const gms = game.users.filter(user => user.isGM && user.active);
    if (!gms.length) return null;

    gms.sort(user => user.id);
    return gms[0];
}

/**
 * Determines if the current user is the primary gamemaster.
 * @see getPrimaryGm
 * @returns {boolean} True if the current user is the primary GM, false otherwise.
 */
export function isPrimaryGm() {
    return getPrimaryGm()?.isSelf ?? false;
}

/**
 * Retreives global metadata information for a system property.
 * @param {object} type The collection and type to search the model for.
 * @param {string} type.collection The collection containing the type.
 * @param {string} type.type The name of the type.
 * @param {string} path The path of the property within the document.
 * @returns {object} The global metadata of the property or an empty object if it wasn't found.
 */
export function getGlobalMetadata(type, path) {
    if (!path.startsWith("system.")) {
        error("Global metadata is only available for system properties.", null, false);
        return {};
    }

    path = path.substring(7);
    const model = CONFIG[type.collection].dataModels[type.type];
    if (!model) return {};

    const field = model.schema.getField(path);
    const metadata = {
        type: field instanceof foundry.data.fields.NumberField ? "number" : "text",
        label: field.label,
        hint: field.hint,
        alwaysEditable: field.alwaysEditable ?? false,
        hidden: field.hidden ?? false,
        locked: field.locked ?? false,
        noMigration: field.noMigration ?? false,
    };

    if (metadata.label) metadata.label = game.i18n.localize(metadata.label);
    if (metadata.hint) metadata.hint = game.i18n.localize(metadata.hint);
    if (field.model?.prototype.valueToString) metadata.transform = value => field.model.prototype.valueToString(value);
    else if (field.valueToString) metadata.transform = value => field.valueToString(value);
    return metadata;
}

/**
 * Traverses the parents of the given model to find the actor containing it.
 * @param {DataModel} model The data model to traverse.
 * @returns {Actor?} The parent actor of the model or null if there is none.
 */
export function findParentActor(model) {
    while (model) {
        if (model instanceof foundry.abstract.Document) return model instanceof Actor ? model : model.actor;
        model = model.parent;
    }

    return null;
}

/**
 * Calculates the raw distance between two points, ignoring the grid.
 * @param {Point} origin The position of the source.
 * @param {Point} target The position of the target.
 * @param {Grid} grid The grid containing distance information.
 * @returns {number} The raw distance between the points.
 */
export function gridDistance(origin, target, grid) {
    return (new Ray(origin, target).distance / grid.size) * grid.distance;
}

/**
 * Logs the given message as an error and optionally displays a UI notification.
 * @param {string} text The message to log.
 * @param {Error?} exception An optional exception to log.
 * @param {boolean=} toUi Determines whether a notification is emitted in addition to the console message.
 *  Defaults to true.
 * @param {string?} style An optional string of CSS styles to apply to the message. @see logStyle
 */
export function error(text, exception = null, toUi = true, style = null) {
    text = notificationPrefix + text;
    if (toUi) ui.notifications.error(text, { permanent: true, console: !!style });
    else if (!style) console.error(text);
    if (style) console.log(`%c${text}`, style);
    if (exception) console.error(exception, exception.stack);
}

/**
 * Logs the given message as a warning and optionally displays a UI notification.
 * @param {string} text The message to log.
 * @param {boolean=} toUi Determines whether a notification is emitted in addition to the console message.
 *  Defaults to false.
 * @param {string?} style An optional string of CSS styles to apply to the message. @see logStyle
 */
export function warn(text, toUi = false, style = null) {
    text = notificationPrefix + text;
    if (toUi) ui.notifications.warn(text, { permanent: true, console: !!style });
    else if (!style) console.warn(text);
    if (style) console.log(`%c${text}`, style);
}

/**
 * Logs the given informational message and optionally displays a UI notification.
 * @param {string} text The message to log.
 * @param {boolean=} toUi Determines whether a notification is emitted in addition to the console message.
 *  Defaults to false.
 * @param {string?} style An optional string of CSS styles to apply to the message. @see logStyle
 */
export function info(text, toUi = false, style = null) {
    text = notificationPrefix + text;
    if (toUi) ui.notifications.info(text, { console: !!style });
    else if (!style) console.info(text);
    if (style) console.log(`%c${text}`, style);
}

/**
 * Logs the given message if the user is in debug mode.
 * @param {string} text The message to log.
 */
export function debug(text) {
    if (TDE5E.debugMode) console.debug(notificationPrefix + text);
}

/**
 * Localizes the name of the given type without considering its group.
 * @param {string} type The type to localize.
 * @param {string?} group An optional group to localize.
 * @returns {string} The translated type name.
 */
export function localizeType(type, group = null) {
    const text = foundry.utils.getProperty(game.i18n.translations, `tde5e.types.${type}`);
    if (typeof text === "object") return group ? text[group] ?? text.name : text.name;
    return text;
}
