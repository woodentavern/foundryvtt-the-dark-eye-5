import { stopEvent } from "./jsUtils.js";
import EntityLibrary from "./ui/library.js";

/**
 * Registers global keyboard shortcuts.
 */
export default function registerShortcuts() {
    $(document).on("keydown.tde5e", ev => {
        if (ev.ctrlKey && ev.which === 73) {
            stopEvent(ev);
            new EntityLibrary().render(true);
        }

        return true;
    });
}
