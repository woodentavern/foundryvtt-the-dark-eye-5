/**
 * Simple mutex to prevent parallel execution of critical sections.
 * Note that this implementation doesn't provide any error handling or timeouts.
 * @example
 * const mutex = new Mutex();
 * const unlock = await mutex.lock();
 * try {
 *     await doWork();
 * } finally {
 *     unlock();
 * }
 * @example
 * const mutex = new Mutex();
 * await mutex.wrap(async () => doWork());
 */
export default class Mutex {
    /**
     * The currently blocking promise.
     */
    current = Promise.resolve();

    /**
     * Creates a promise that is resolved when the currently blocking operation completes.
     * @returns {Promise} A new promise that resolves when the mutex is free.
     */
    async lock() {
        let unlock = () => {};
        const next = new Promise(resolve => { unlock = () => resolve(); });
        const blocker = this.current.then(() => unlock);
        this.current = next;
        return blocker;
    }

    /**
     * Runs the given callback in a non-parallel section.
     * @param {function(): Promise} callback The asynchronous method to execute within the mutex.
     * @returns {Promise.<object?>} A promise representing the return value of the callback.
     */
    async wrap(callback) {
        const unlock = await this.lock();
        try {
            return await callback();
        } finally {
            unlock();
        }
    }
}
