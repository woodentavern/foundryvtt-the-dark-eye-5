import TdeActor from "./actor.js";

export default class NpcActor extends TdeActor {
    /** @override */
    async _preCreate(data, options, user) {
        this.updateSource({ "token.actorLink": true });
        return super._preCreate(data, options, user);
    }

    /** @override */
    async _setupItems() {
        const conditions = await game.packs.get(TDE5E.compendiums.Conditions)
            .getDocuments({ system: { isDefault__in: [true] } });
        const skills = await game.packs.get(TDE5E.compendiums.Skills)
            .getDocuments({ system: { cid__in: TDE5E.defaultAbilities.skills.npc } });
        const combat = (await game.packs.get(TDE5E.compendiums.Combat)
            .getDocuments({ system: { requirementText__in: [""] } }))
            .filter(item => item.system.purchase.cost[0] === 0);
        const states = await game.packs.get(TDE5E.compendiums.States)
            .getDocuments({ system: { isDefault__in: [true] } });
        const equipment = await game.packs.get(TDE5E.compendiums.Equipment)
            .getDocuments({ system: { cid__in: ["WAFFENLOS", "UNGERUSTET"] } });

        return conditions.concat(skills, combat, states, equipment);
    }

    /** @override */
    _onCreateDescendantDocuments(parent, collection, documents, data, options, userId) {
        super._onCreateDescendantDocuments(parent, collection, documents, data, options, userId);

        // Add new loadout in simpleLoadout-mode when a combat technique is added
        if (game.user.id === userId && this.system.settings.useSimpleLoadout && collection === "Item") {
            const cTs = documents.filter(item => item.type === TDE5E.itemTypes.CombatTechnique);
            if (cTs.length) {
                const loadouts = foundry.utils.deepClone(this.system.loadouts);
                cTs.forEach(cT => {
                    const wasPreviouslyUsed = loadouts.some(ld => ld.combatTechnique === cT._id);

                    if (!wasPreviouslyUsed) {
                        loadouts.push({
                            id: foundry.utils.randomID(),
                            name: `Loadout #${loadouts.length + 1}`,
                            order: (loadouts.length + 1),
                            combatTechnique: cT?._id,
                            combatStyle: null,
                            mainhand: null,
                            offhand: null,
                        });
                    }
                });

                if (this.system.loadouts.length < loadouts.length) {
                    this.update({ "system.loadouts": loadouts });
                }
            }
        }
    }
}
