/**
 * Form application that implements per-actor configuration options.
 */
export default class TdeActorConfig extends FormApplication {
    /** @override */
    static get defaultOptions() {
        return foundry.utils.mergeObject(super.defaultOptions, {
            id: "actor-config",
            template: "systems/tde5e/templates/actors/actorConfig.hbs",
            width: 420,
        });
    }

    /**
     * Prepares the display data of the dialog to include our internal flags.
     * @returns {object} The data required for rendering the dialog.
     * @override
     */
    getData() {
        return { data: this.object.toObject() };
    }

    /**
     * Specifies the title of the dialog with the actor's name.
     * @returns {string} The title of the configuration dialog.
     * @override
     */
    get title() {
        return `${this.object.name}: ${game.i18n.localize("tde5e.actor.config.title")}`;
    }

    /**
     * Updates the associated actor with the form data settings.
     * @returns {Promise.<void>} A promise representing the update operation.
     * @override
     */
    async _updateObject(_event, formData) {
        return this.object.update(formData);
    }
}
