/**
 * Dialog class for setting improvable properties to a fixed value.
 */
export default class ImprovementDialog extends Dialog {
    /**
     * The improvement context for the property.
     * @type {ImprovementContext}
     */
    improvement;

    /**
     * The minimum value of the property.
     * @type {number}
     */
    minimumValue;

    /**
     * Creates and configures the dialog for improving a property.
     * @param {TdeItem | TdeActor} document The item or actor to improve.
     * @param {DataField} field The field to improve.
     * @param {ImprovementContext} improvement An improvement context for the field.
     */
    constructor(document, field, improvement) {
        const name = document.displayName ?? document.name;
        const description = game.i18n.format("tde5e.actor.improvementDialog.hint", {
            name,
            property: document.getPropertyMetadata(`system.${improvement.path}`).label,
        });

        super({
            title: game.i18n.format("tde5e.actor.improvementDialog.title", { name }),
            content: `<p>${description}</p>
                <p><input type="number" name="level" value="${improvement.maximumValue}" step="1"/></p>`,
            buttons: {
                ok: {
                    icon: "<i class='fas fa-angles-up'></i>",
                    label: game.i18n.localize("tde5e.actor.improvementDialog.button"),
                },
            },
            default: "ok",
        }, { focus: false });

        this.improvement = improvement;
        this.minimumValue = field.min;
    }

    /** @inheritdoc */
    activateListeners(html) {
        super.activateListeners(html);
        html.find("input[name='level']")
            .select()
            .change(ev => this.validate(ev.currentTarget));
    }

    /**
     * Extends FoundryVTT's method to validate the input before submitting.
     * @inheritdoc
     */
    submit(button, event) {
        const input = this.element[0].querySelector("input[name='level']");
        if (this.validate(input)) {
            this.improvement.apply(input.valueAsNumber);
            super.submit(button, event);
        }
    }

    /**
     * Checks whether the property is improvable to the given level.
     * @param {HTMLInputElement} input The element containing the target value.
     * @returns {boolean} True if the validation succeeded, false otherwise.
     */
    validate(input) {
        const button = this.element[0].querySelector(".default");
        const warning = button.querySelector(".fa-circle-exclamation");
        const value = input.valueAsNumber;
        const canImprove = value < this.minimumValue
            ? game.i18n.format("tde5e.actor.improvementDialog.minLevel", { min: this.minimumValue })
            : this.improvement.canImprove(value);

        if (canImprove === true) {
            button.disabled = false;
            button.title = "";
            if (warning) {
                warning.remove();
                setTimeout(() => button.focus());
            }
            return true;
        }

        button.disabled = true;
        button.title = typeof (canImprove) === "string"
            ? canImprove
            : game.i18n.format("tde5e.actor.improvementDialog.maxLevel", { max: this.improvement.maximumValue });
        if (!warning) {
            button.insertAdjacentHTML("beforeend", "<i style='color: darkred;' class='fas fa-circle-exclamation'></i>");
        }
        input.select();
        return false;
    }
}
