import BaseActorSheet from "./actorSheet.js";

export default class PlayerActorSheet extends BaseActorSheet {
    /**
     * Gets the textual representation of the experience level.
     * @param {number} experienceTotal The amount of experience spent.
     * @returns {string} The name of the experience level.
     */
    getExperienceLevel(experienceTotal) {
        if (experienceTotal < 1000) return "inexperienced";
        if (experienceTotal < 1100) return "average";
        if (experienceTotal < 1200) return "experienced";
        if (experienceTotal < 1400) return "competent";
        if (experienceTotal < 1700) return "masterful";
        if (experienceTotal < 2100) return "brilliant";
        return "legendary";
    }

    prepareTabs(data) {
        super.prepareTabs(data);
        this.prepareInventoryTabData(data);
    }

    prepareCharacterData(data) {
        super.prepareCharacterData(data);
        const experience = data.system.adventurePoints;
        const level = this.getExperienceLevel(experience.total);
        experience.description = game.i18n.localize(`tde5e.experienceLevels.${level}`);
    }
}
