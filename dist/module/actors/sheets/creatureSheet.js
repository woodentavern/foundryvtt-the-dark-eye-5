import NpcActorSheet from "./npcSheet.js";

export default class CreatureSheet extends NpcActorSheet {
    /** @override */
    static get defaultOptions() {
        return foundry.utils.mergeObject(super.defaultOptions, {
            classes: ["sheet", "tde-actor", "tde-creature"],
        });
    }

    /** @override */
    get template() {
        return this.actor.limited
            ? "systems/tde5e/templates/actors/limited-creature-sheet.hbs"
            : "systems/tde5e/templates/actors/creature-sheet.hbs";
    }

    /** @override */
    activateListeners(html) {
        super.activateListeners(html);

        [...$(".flip-box")].forEach(element => {
            const name = $(element).attr("name");
            const button = $(`a.flip-button[name="${name}"]`);
            if (button) {
                button[0].addEventListener("click", () => this.onClickFlip(name), false);
            }
        });

        html.on("click", "a.add-creaturefact", this.onClickAddCreatureFact.bind(this));
        html.on("click", "a.copy-creaturefact", this.onClickCopyCreatureFact.bind(this));
        html.on("click", "a.delete-creaturefact", this.onClickDeleteCreatureFact.bind(this));
        html.on("click", "a.add-specialrule", this.onClickAddSpecialRule.bind(this));
        html.on("click", "a.delete-specialrule", this.onClickDeleteSpecialRule.bind(this));
    }

    /** @inheritdoc */
    prepareTabs(data) {
        super.prepareTabs(data);
        this.prepareCreatureData(data);
    }

    /**
     * Prepares all creature specific values.
     * @param {object} data The container
     */
    prepareCreatureData(data) {
        data.creatureData = {};

        // Creature Facts
        data.creatureData.anyFactsVisible = data.system.creatureFacts.facts.some(fact => fact.isVisible);
        data.creatureData.facts = data.system.creatureFacts.facts;

        // Special rules
        data.creatureData.anyRulesVisible = data.system.specialRules.some(rule => rule.isVisible);
        data.creatureData.specialRules = data.system.specialRules;

        // CreatureType
        data.creatureData.creatureType = this.actor.creatureType?.getSheetData();

        // Size category
        data.creatureData.sizeCategory = `<select class="actor-property" name='system.sizeCategory'>${
            HandlebarsHelpers.selectOptions(
                game.i18n.translations.tde5e.creatureSizes,
                { hash: { selected: data.system.sizeCategory ?? "none" } },
            )
        }</select>`;
    }

    /**
     * Called when any copy creature fact button is clicked.
     * @param {Event} event  The click event.
     */
    async onClickCopyCreatureFact(event) {
        const { type } = event.currentTarget.dataset;
        let { facts } = this.actor._source.system.creatureFacts;

        if (type === "upto") {
            const index = parseInt(event.currentTarget.dataset.factIndex, 10);
            facts = facts.slice(0, index);
        }

        const factHtml = this._formatCreateFacts(facts);
        this._copyToClipboard(factHtml);
    }

    /**
     * Copies the given text to clipboard using JQuery-shenanigans.
     * @param {string} text Any text.
     */
    _copyToClipboard(text) {
        const tempElement = $("<input>");
        $("body").append(tempElement);
        tempElement.val(text).select();
        document.execCommand("copy");
        tempElement.remove();
    }

    /**
     * Transforms an array of creature fact-objects from the data into a single html element.
     * @param {object[]} facts An array of facts or null.
     * @returns {string} A single p-Element that contains the text of the facts.
     */
    _formatCreateFacts(facts) {
        const text = facts?.map(fact => fact.description).join("<br>");
        return `<p>${text}</p>`;
    }

    /**
     * Called when the delete creature fact button is clicked.
     * @param {Event} event The click event.
     */
    async onClickDeleteCreatureFact(event) {
        const index = parseInt(event.currentTarget.dataset.factIndex, 10);
        if (!await Dialog.confirm({
            title: game.i18n.localize("tde5e.actor.creatureFacts.deleteConfirmTitle"),
            content: game.i18n.localize("tde5e.actor.creatureFacts.deleteConfirmContent"),
        })) return;

        await this.actor.deleteCreatureFact(index);
    }

    /**
     * Called when the add creature fact button is clicked.
     */
    async onClickAddCreatureFact() {
        await this.actor.addNewCreatureFact();
    }

    /**
     * Called when the delete creature fact button is clicked.
     * @param {Event} event The click event.
     */
    async onClickDeleteSpecialRule(event) {
        const index = parseInt(event.currentTarget.dataset.ruleIndex, 10);
        if (!await Dialog.confirm({
            title: game.i18n.localize("tde5e.actor.specialRules.deleteConfirmTitle"),
            content: game.i18n.localize("tde5e.actor.specialRules.deleteConfirmContent"),
        })) return;

        await this.actor.deleteSpecialRule(index);
    }

    /**
     * Called when the add creature fact button is clicked.
     */
    async onClickAddSpecialRule() {
        await this.actor.addNewSpecialRule();
    }
}
