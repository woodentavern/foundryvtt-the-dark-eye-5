import { redirectNoteUpdate } from "../../socket.js";
import BaseActorSheet from "./actorSheet.js";
import { LootClaimMessage } from "../../chatMessage/lootClaimMessage.js";

export default class NpcActorSheet extends BaseActorSheet {
    /** @inheritdoc */
    get template() {
        return this.actor.limited
            ? "systems/tde5e/templates/actors/limited-npc-sheet.hbs"
            : "systems/tde5e/templates/actors/npc-sheet.hbs";
    }

    /** @inheritdoc */
    prepareCombatTabData(data) {
        if (data.system.settings.useSimpleLoadout) {
            // Create a selection list for damage types.
            data.damageTypes = Object.entries(TDE5E.damageTypeIcons).reduce((types, type) => {
                types[type[0]] = game.i18n.localize(`tde5e.damageTypes.${type[0]}`);
                return types;
            }, {});
        }

        super.prepareCombatTabData(data);
    }

    /** @inheritdoc */
    prepareChangelogTab(data) {
        super.prepareChangelogTab(data);
        data.changelogTab.adventurePoints = {
            available: "∞",
            total: data.changelogTab.adventurePoints.total,
            remaining: 1000,
        };
    }

    /** @inheritdoc */
    activateListeners(html) {
        super.activateListeners(html);
        if (!this.isEditable) { // Excludes owner and GM.
            if (this.object.testUserPermission(game.user, "LIMITED")) { // Includes limited and above.
                // Restore some functionality for limited and observer permissions.
                html.find(".editor-content[data-edit='system.notes']").each((_i, div) => this._activateEditor(div));
                this.options.editable = true;
            }
            return;
        }

        html.find(".btn-create-claimable-chat-item").click(this.onClickCreateClaimableChatItem.bind(this));

        if (this.actor.system.settings.useSimpleLoadout) {
            html.find("input.create-weapon").click(this.createPseudoWeapon.bind(this));
        }
    }

    /**
     * Creates a pseudo weapon for the combat technique identified by the data of the clicked button.
     * @param {jQuery.Event} event The event of the button.
     */
    async createPseudoWeapon(event) {
        const index = parseInt(event.currentTarget.dataset.index, 10);
        const loadout = this.actor.system.loadouts[index];
        const combatTechnique = loadout?.combatTechnique;
        if (!combatTechnique) return;

        // Create weapon.
        const weapon = await this.actor.createEmbeddedDocuments("Item", [{
            type: combatTechnique.isRanged ? "rangedWeapon" : "meleeWeapon",
            name: game.i18n.localize("tde5e.actor.sheet.pseudoWeapon"),
            system: { group: combatTechnique._stats.compendiumSource },
        }]);

        // Create loadout.
        const loadouts = foundry.utils.deepClone(this.actor._source.system.loadouts);
        loadouts[index].mainhand = weapon[0].id;
        this.actor.update({ "system.loadouts": loadouts });
    }

    /**
     * Replaces the base method to include loot container data.
     * @override
     */
    onClickAddInventoryItem(event) {
        event.preventDefault();
        const element = event.currentTarget;
        const containerId = element.dataset.container;

        if (!containerId) {
            util.error("Could not find container id", true, false);
            return;
        }

        const itemData = {
            name: "-",
            img: "systems/tde5e/assets/defaultItems/inventory_item.svg",
            type: TDE5E.itemTypes.InventoryItem,
            system: { container: containerId },
            flags: {},
        };

        // Add default loot flags when added to loot
        const container = this.actor.system.inventoryContainer.find(c => c.id === containerId);
        if (container.type === "loot") {
            foundry.utils.setProperty(itemData, "flags.tde5e.loot", foundry.utils.deepClone(TDE5E.defaultLootItemData));
        }

        this.actor.createEmbeddedDocuments("Item", [itemData]);
    }

    /** @inheritdoc */
    _moveItemAmongContainsers(containerItemMap, sourceContainerId, destContainerId, item, { oldIndex, newIndex }) {
        const updateList = super._moveItemAmongContainsers(
            containerItemMap,
            sourceContainerId,
            destContainerId,
            item,
            { oldIndex, newIndex },
        );

        const { type } = this.actor.system.inventoryContainer[destContainerId];
        // If item moved to loot container, add loot properties to flags
        if (updateList.length && sourceContainerId !== destContainerId && type === "loot") {
            const presentValues = foundry.utils.getProperty(item, "flags.tde5e.loot") ?? {};
            const newValues = foundry.utils.mergeObject(presentValues, TDE5E.defaultLootItemData, {
                overwrite: false,
                insertKeys: true,
                insertValues: true,
            });
            if (newValues !== presentValues) {
                updateList.push({ _id: item.id, "flags.tde5e.loot": newValues });
            }
        }
        return updateList;
    }

    /**
     * Handles the click on an element.
     * Creates a claimable chat item with the content of the calling loot container.
     * @param {Event} event The click event.
     */
    onClickCreateClaimableChatItem(event) {
        const element = event.currentTarget;
        const containerId = element.dataset.container;
        const itemsToClaim = this.actor.items
            .filter(item => item._source.system.container === containerId)
            .map(item => item.toObject());

        const { currency } = this.actor.system.inventoryContainer[containerId];

        LootClaimMessage.createLootClaimMessage(this.name, itemsToClaim, currency);
    }

    /**
     * Attempts to redirect form updates for the actor to a user with a higher permission level to allow limited users
     *  to change the notes.
     * @param {jQuery.Event} event The event that triggered the update.
     * @param {object} formData The raw form data of the update.
     * @returns {Promise} A promise representing the update.
     */
    async _updateObject(event, formData) {
        if (this.actor.isOwner) return super._updateObject(event, formData);
        const diffData = diffObject(this.actor.toObject(), expandObject(formData));
        if (Object.keys(diffData).length === 0) return Promise.resolve();
        diffData._id = this.object._id;
        return redirectNoteUpdate(diffData._id, diffData);
    }
}
