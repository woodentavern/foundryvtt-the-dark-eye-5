import * as util from "../../util.js";
import TdeActorConfig from "../actorConfig.js";
import { activateModifierControl } from "../../ui/controls/modifierControl.js";
import GlobalContextMenu from "../../ui/globalContextMenu.js";
import { requestItemTransfer } from "../../socket.js";
import TdeActor from "../actor.js";
import { parseArrays } from "../../formDataParsing.js";
import UnitControl from "../../ui/controls/unitControl.js";
import { migrateActor, migrateActorData } from "../../migration.js";
import { on, stopEvent } from "../../jsUtils.js";
import { ActorLogEntry } from "../logEntry.js";
import ImprovementDialog from "../improvementDialog.js";
import LoadoutModel from "../model/partial/loadoutModel.js";

export default class BaseActorSheet extends ActorSheet {
    /**
     * Indicates whether changelog entries that aren't improvements should be hidden.
     * @type {boolean}
     */
    onlyShowImprovements;

    /** @override */
    static get defaultOptions() {
        return foundry.utils.mergeObject(super.defaultOptions, {
            tabs: [{ navSelector: ".tabs", contentSelector: ".sheet-body", initial: "description" }],
            classes: ["sheet", "tde-actor"],
            width: 875,
            height: 800,
        });
    }

    /** @override */
    get template() {
        return this.actor.limited
            ? "systems/tde5e/templates/actors/limited-player-sheet.hbs"
            : `systems/tde5e/templates/actors/${this.actor.type}-sheet.hbs`;
    }

    /**
     * Indicates whether improvement elements should be displayed.
     * @type {boolean}
     */
    get isImproving() {
        return this.actor.getFlag("tde5e", "improving");
    }

    /**
     * Gets an icon representing the actor's location.
     * @returns {string} An HTML string containing the source icon.
     */
    getSourceIcon() {
        const { compendium } = this.object;
        if (compendium) {
            const icon = compendium.collection.startsWith("world.") ? "fa-atlas" : "fa-book";
            return `<i class="fas ${icon}" title="${compendium.metadata.label}"></i>`;
        }
        return "<i class='fas fa-user'></i>";
    }

    /**
     *  Is handed down to the HTML for the corresponding sheet.
     *  Will be called every time a sheet is rendered.
     *  @override
     */
    getData() {
        // Basic data
        const { isOwner } = this.document;
        const actorCopy = foundry.utils.deepClone(this.actor);

        const defaultSorting = (a, b) => a.name.localeCompare(b.name);
        const sortOverride = {
            rangedWeapon: (a, b) => a.system.group?.name.localeCompare(b.system.group) || a.name.localeCompare(b.name),
            meleeWeapon: (a, b) => a.system.group?.name.localeCompare(b.system.group) || a.name.localeCompare(b.name),
            armor: (a, b) => a.totalProtection - b.totalProtection || a.name.localeCompare(b.name),
            action: (a, b) => a.system.cost.resources.actionPoints - (b.system.cost.resources.actionPoints)
                || a.name.localeCompare(b.name),
            reaction: (a, b) => a.system.cost.resources.actionPoints - (b.system.cost.resources.actionPoints)
                || a.name.localeCompare(b.name),
            inventoryItem: null,
        };

        // Convert items to usuable data for the ui and sort items if needed
        const itemTypes = {};
        for (const [key, value] of Object.entries(this.actor.itemTypes)) {
            itemTypes[key] = value.map(item => item.getSheetData());
            const sorting = sortOverride[key];
            // Sort only not null itemTypes
            if (sorting !== null) itemTypes[key].sort(sorting ?? defaultSorting);
        }

        // Create ID set from stored closed details elements.
        const closedDetails = new Set(game.settings.get("tde5e", "closedDetails")?.[this.object.uuid] ?? []);

        const data = {
            isOwner,
            limited: this.document.limited,
            options: this.options,
            editable: this.isEditable,
            cssClass: isOwner ? "editable" : "locked",
            isPlayer: actorCopy.type === "player",
            isGM: game.user.isGM,
            showInventory: this.actor._source.system.settings.hasLoot ?? true,
            config: TDE5E,
            actor: actorCopy,
            system: actorCopy.system,
            source: this.actor._source,
            itemTypes,
            closedDetails,
        };

        this.prepareTabs(data);
        if (this.actor.limited) this.position.height = 450;
        return data;
    }

    /**
     * Processes and prepares data that is needed for the ui.
     * This is the central method that will call all other preparations to be executed.
     * @param {object} data The data object.
     */
    prepareTabs(data) {
        this.prepareCharacterData(data);
        this.prepareSkillsTabData(data);
        this.prepareCombatTabData(data);
        // Prepare karma before to filter out liturgicalChants
        if (data.character.canUseKarma) this.prepareKarmaTabData(data);
        if (data.character.canUseMagic) this.prepareMagicTabData(data);
        if (data.showInventory) this.prepareInventoryTabData(data);
        this.prepareChangelogTab(data);
    }

    /**
     * Prepares actor specific data.
     * @param {object} data data object.
     */
    prepareCharacterData(data) {
        /** @type {TdeActor} */
        const { actor } = this;
        data.character = {
            canUseMagic: actor.canUseMagic,
            canUseKarma: actor.canUseKarma,
            race: actor.race?.getSheetData(),
            culture: actor.culture?.getSheetData(),
            attributes: [],
        };

        // Prepare attributes.
        let attributeSum = 0;
        for (const [key, attr] of Object.entries(data.system.attributes)) {
            attributeSum += attr.base;

            if (attr.value > attr.base) attr.classMod = "attr-increased";
            else if (attr.value < attr.base) attr.classMod = "attr-lowered";

            attr.name = game.i18n.localize(`tde5e.actor.attributes.${key}`);
            attr.nameShort = game.i18n.localize(`tde5e.actor.attributesShort.${key}`);
        }
        data.character.attributeSum = attributeSum;

        // Prepare resources.
        if (!data.character.canUseMagic) delete data.system.resources.arcaneEnergy;
        if (!data.character.canUseKarma) delete data.system.resources.karmaPoints;
        if (!actor.canUseFate) delete data.system.resources.fatePoints;
        for (const [key, resource] of Object.entries(data.system.resources)) {
            resource.name = game.i18n.localize(`tde5e.actor.resources.${key}`);
            resource.percentage = Math.round(Math.clamp(resource.value / resource.max, 0, 1) * 100);
            const resourceText = game.i18n.localize(`tde5e.actor.resources.${key}Short`);
            resource.text = `${resourceText} ${resource.value}/${resource.max}`;
        }

        // Prepare derived values.
        delete data.system.derived.painThresholds;
        for (const [key, value] of Object.entries(data.system.derived)) {
            value.name = game.i18n.localize(`tde5e.actor.derived.${key}`);
        }

        // Prepare status effects.
        data.character.effects = actor.effects.map(effect => {
            const counter = effect.statusCounter;
            return {
                icon: effect.img,
                name: effect.name,
                count: counter?.visible ? counter.displayValue : "",
            };
        });

        // Split artifactAbilities into magical and blessed
        data.itemTypes.artifactAbility = util.groupBy(data.itemTypes.artifactAbility, "traditionGroup");

        // Concat multiple types that represent magicalAbilities as a hole
        if (data.character.canUseMagic) {
            data.character.magicalAbilitiesAndSubtypes = []
                .concat(data.itemTypes.magicalAbility)
                .concat(data.itemTypes.spellGlyphAbility)
                .sort((item1, item2) => item1.name.localeCompare(item2.name));
        }
    }

    /**
     * Prepares ui data for the SkillsSheet.
     * This processes all necesarry items into arrays that can easily be processed and enrich the data with
     *  values to reduce translation calls in the sheet itself.
     * @param {object} data All tab-specific data should nest in this object.
     */
    prepareSkillsTabData(data) {
        data.itemTypes.skill = util.groupBy(data.itemTypes.skill, "system.group");
    }

    /**
     * Prepares ui data for the CombatSheet.
     * This processes all necesarry items into arrays that can easily be processed and enrich the data with
     *  values to reduce translation calls in the sheet itself.
     * @param {object} data All tab-specific data should nest in this object.
     */
    prepareCombatTabData(data) {
        data.itemTypes.action = data.itemTypes.action.reduce((actions, action) => {
            let key;
            if (action.system.isAttack) {
                key = action.system.isRanged ? "ranged" : "melee";
                if (action.system.isRanged === null) {
                    // Action can be used at any range, add it to both groups.
                    actions.ranged ??= [];
                    actions.ranged.push(action);
                }
            } else {
                key = "tactical";
            }

            actions[key] ??= [];
            actions[key].push(action);
            return actions;
        }, {});
        data.itemTypes.armor = util.groupBy(data.itemTypes.armor, "system.group");
        data.loadouts = this.actor.system.loadouts.map(ld => ld.getSheetData());

        const isUnused = item => !this.actor.system.loadouts
            .some(ld => ld.mainhand?.id === item.id || ld.offhand?.id === item.id);
        data.unusedWeapons = {
            meleeWeapon: data.itemTypes.meleeWeapon.filter(isUnused),
            rangedWeapon: data.itemTypes.rangedWeapon.filter(isUnused),
            shield: data.itemTypes.shield.filter(isUnused),
        };
        data.unusedWeapons.hasAny = (data.unusedWeapons.meleeWeapon.length
            + data.unusedWeapons.rangedWeapon.length
            + data.unusedWeapons.shield.length) > 0;
    }

    /**
     * Prepares ui data for the MagicSheet.
     * This processes all necesarry items into arrays that can easily be processed and enrich the data with
     *  values to reduce translation calls in the sheet itself.
     * @param {object} data All tab-specific data should nest in this object.
     */
    prepareMagicTabData(data) {
        const arcaneTraditions = data.itemTypes.tradition.filter(tradition => tradition.system.group === "magical");
        const arcaneImprints = data.itemTypes.traditionImprint.filter(imprint => imprint.system.group === "magical");
        data.magicTab = {
            traditionText: arcaneTraditions.map(item => item.traditionLink).join(", "),
            primaryAttributeText: arcaneTraditions.map(t => t.primaryAttribute).join(", "),
            traditionImprintText: arcaneImprints.map(ti => ti.traditionImprintLink).join(", "),
        };
    }

    /**
     * Prepares ui data for the KarmaSheet.
     * This processes all necesarry items into arrays that can easily be processed and enrich the data with
     *  values to reduce translation calls in the sheet itself.
     * @param {object} data All tab-specific data should nest in this object.
     */
    prepareKarmaTabData(data) {
        const blessedTraditions = data.itemTypes.tradition.filter(tradition => tradition.system.group === "blessed");
        const blessedImprints = data.itemTypes.traditionImprint.filter(imprint => imprint.system.group === "blessed");
        data.karmaTab = {
            traditionText: blessedTraditions.map(item => item.traditionLink).join(", "),
            primaryAttributeText: blessedTraditions.map(t => t.primaryAttribute).join(", "),
            traditionImprintText: blessedImprints.map(ti => ti.traditionImprintLink).join(", "),
        };
    }

    /**
     * Prepares ui data for the InventorySheet.
     * This processes all necesarry items into arrays that can easily be processed and enrich the data with
     *  values to reduce translation calls in the sheet itself.
     * @param {object} container All tab-specific data should nest in this object.
     */
    prepareInventoryTabData(container) {
        const containers = this.processInventoryContainers(container);

        const weighText = game.i18n.localize("tde5e.inventory.weight");
        const encumbranceByWeight = (this.actor.uniqueItems.get("BELASTUNG")?.system.modifiers
            ?.find(mod => mod[0] === weighText) ?? ["", 0])[1];

        container.inventoryTab = {
            containers,
            weightEncumbrance: encumbranceByWeight,
        };
    }

    /**
     * Prepares inventory items and their respective containers.
     * @param {object} data All tab-specific data should nest in this object.
     * @returns {object[]} An object-array containing containers with their items.
     */
    processInventoryContainers(data) {
        const inventoryItems = TdeActor.getInventoryRelatedItems(data.itemTypes);
        const containerData = foundry.utils.deepClone(this.actor.system.inventoryContainer);
        for (const container of containerData) {
            container.items = [];
            container.icon = `<a class="${container.icon ?? TDE5E.inventoryContainerIconsDefault} container-icon"></a>`;
            container.canBeDeleted = container.id !== "DEFAULT"
                && (this.actor.type === "player" || !container.type === "loot");

            if (container.type === "loot") {
                container.canBeDeleted = this.actor.type === "player";
            } else if (container.type === "trade") {
                const zoneLabels = foundry.utils.getProperty(game.i18n.translations, "tde5e.region.tradeZone") ?? {};
                container.tradeZones = foundry.utils.deepClone(zoneLabels);
                Object.keys(zoneLabels)
                    .forEach(zone => { container.tradeZones[zone] = `${zone.toUpperCase()} - ${zoneLabels[zone]}`; });
            }
        }

        // Fill containers with items
        const defaultContainer = containerData.find(c => c.id === "DEFAULT");
        if (!defaultContainer) util.error(`Actor ${this.actor.name} is missing a default inventory container.`);

        for (const item of inventoryItems) {
            const itemContainer = containerData.find(c => c.id === item.system.container) ?? defaultContainer;
            if (itemContainer) itemContainer.items.push(item);
        }

        containerData.forEach(c => {
            c.items.sort(this._getInventorySorter(c.sortingField));
            if (!c.sortAscending) c.items.reverse();

            if (c.type === "trade") {
                let totalPrice = 0;
                let totalBought = 0;
                let totalSold = 0;
                c.items.forEach(item => {
                    totalPrice += item.system.price;
                    totalBought += item.buyPrice ?? 0;
                    totalSold += item.sellPrice ?? 0;
                });

                c.totalPrice = util.toLocaleFixed(totalPrice / 100);
                c.totalBought = util.toLocaleFixed(totalBought / 100);
                c.totalSold = util.toLocaleFixed(totalSold / 100);
            }
        });

        // Return sorted containers
        return containerData.sort((c1, c2) => c1.order - c2.order);
    }

    /**
     * Creates a function to sort inventory items by the given field.
     * @param {string} field The id of the field to sort.
     * @returns {Function} The function to sort the items with.
     */
    _getInventorySorter(field) {
        switch (field) {
        case "type":
            return (i1, i2) => i1.type.localeCompare(i2.type)
                || i1._source.system.group?.localeCompare(i2._source.system.group)
                || i1.name.localeCompare(i2.name);
        case "quantity":
            return (i1, i2) => (i1.system.quantity - i2.system.quantity) || i1.name.localeCompare(i2.name);
        case "totalWeight":
            return (i1, i2) => (i1.totalWeight - i2.totalWeight) || i1.name.localeCompare(i2.name);
        case "price":
            return (i1, i2) => (i1.system.price - i2.system.price) || i1.name.localeCompare(i2.name);
        case "buyPrice":
            return (i1, i2) => (i1.buyPrice - i2.buyPrice) || i1.name.localeCompare(i2.name);
        case "sellPrice":
            return (i1, i2) => (i1.sellPrice - i2.sellPrice) || i1.name.localeCompare(i2.name);
        default:
            return (i1, i2) => i1.name.localeCompare(i2.name);
        }
    }

    /**
     * Prepares UI data for the changelog.
     * @param {object} data Sheet data to nest tab-specific data in.
     */
    prepareChangelogTab(data) {
        const entries = [this.actor]
            .concat(this.actor.items.contents)
            .reduce((logs, doc) => {
                let docLog = doc.system.changelog ?? [];

                // Filter non-improvements when they're hidden.
                if (this.onlyShowImprovements) docLog = docLog.filter(entry => entry.cost);
                if (!docLog.length) return logs;

                // Create sorted UI data for changes.
                const logEntries = docLog
                    .sort((entry1, entry2) => entry2.time - entry1.time)
                    .map(entry => ({
                        uuid: entry.documentUuid,
                        path: entry.path,
                        name: entry.displayName,
                        original: entry.toDisplayValue(entry.original),
                        footer: entry.showFooterValue,
                        history: entry.history
                            .map(change => ({
                                value: entry.toDisplayValue(change.value),
                                time: foundry.utils.timeSince(change.time),
                            }))
                            .reverse(),
                        cost: entry.cost,
                        userCost: entry.cost + entry.userCostDelta,
                    }));

                logs.push(logEntries);
                return logs;
            }, [])
            .flat(1);

        // Calculate adventure points.
        const totalCost = util.sumBy(entries, "userCost");
        const adventurePoints = this.object.system.adventurePoints?.remaining ?? 0;

        data.improving = this.isImproving;
        data.improvable = data.isGM || game.settings.get("tde5e", "improving");
        data.changelogTab = {
            onlyImprovements: this.onlyShowImprovements,
            entries,
            adventurePoints: {
                available: adventurePoints,
                total: totalCost,
                remaining: adventurePoints - totalCost,
            },
        };
    }

    /**
     * Extends Foundry's method to change to a specific tab after rendering the sheet.
     * @override
     */
    async _render(force = false, options = {}) {
        await super._render(force, options);
        if (options.activeTab) this._tabs[0].activate(options.activeTab);
    }

    /**
     * Extends Foundry's method to render improvement elements.
     * @override
     */
    async _renderInner(data) {
        const html = await super._renderInner(data);
        if (this.isImproving) this._renderImprovements(html, data.changelogTab.adventurePoints.remaining);
        return html;
    }

    /**
     * Extends Foundry's method to adjust the display of the title.
     * @override
     */
    async _renderOuter() {
        const html = await super._renderOuter();
        const title = html.find("h4.window-title");
        title.find("a.document-id-link").remove();
        title.before(this.getSourceIcon());
        GlobalContextMenu.create(this, title.parent(), null, this._getHeaderContextOptions());
        return html;
    }

    /**
     * Adds improvement elements to the sheet.
     * @param {jQuery.Element} html The rendered HTML of the sheet.
     * @param {number} remainingAp The amount of remaining adventure points.
     */
    _renderImprovements(html, remainingAp) {
        const improvableValues = html[0].querySelectorAll(".improvable");
        const apLocalized = game.i18n.localize("tde5e.actor.adventurePoints.short");
        for (const improvable of improvableValues) {
            const [doc, path] = this._resolveImprovementDocument(improvable);
            const improvement = doc?.system.getImprovement(path);
            if (!improvement) continue;

            const canImprove = improvement.canImprove();
            if (!canImprove) continue;

            const cost = improvement.calculateImprovementCost();
            const improvementEl = document.createElement("div");
            improvementEl.classList.add("improve");
            improvementEl.innerHTML = `<i class="fas fa-angles-up"></i> ${cost} ${apLocalized}`;
            if (typeof (canImprove) === "string") {
                improvementEl.title = canImprove;
                improvementEl.setAttribute("disabled", true);
            } else if (cost > remainingAp) {
                improvementEl.title = game.i18n.format("tde5e.actor.changelog.prerequisite.ap", { cost });
                improvementEl.setAttribute("disabled", true);
            }

            improvable.append(improvementEl);
        }
    }

    /**
     * Retrieves the target document and property path of the given improvable element.
     * @param {HTMLElement} element The element to read the data from.
     * @returns {Array} A tuple containing the document and property.
     */
    _resolveImprovementDocument(element) {
        const { id, path } = element.dataset;
        const doc = id ? this.actor.items.get(id) : this.actor;
        return [doc, path];
    }

    /**
     * Extends the method to replace the sheet configuration button with an actor configuration button.
     * @returns {object[]} The adjusted list of header buttons.
     * @override
     */
    _getHeaderButtons() {
        const buttons = super._getHeaderButtons().filter(button => button.class !== "configure-sheet");

        if (game.user.isGM) {
            buttons.unshift({
                label: game.i18n.localize("tde5e.actor.config.title"),
                class: "actor-config",
                icon: "fas fa-cog",
                onclick: this.configure.bind(this),
            });

            buttons.unshift({
                class: "actor-improving",
                icon: "fas fa-cart-plus",
                onclick: () => {
                    this.actor.update({ "flags.tde5e.improving": !this.isImproving });
                },
            });
        }

        return buttons;
    }

    /**
     * Builds an array of options for the sheet header context menu.
     * @returns {Array[]} An array of context menu options.
     */
    _getHeaderContextOptions() {
        const entries = [
            {
                name: game.i18n.localize("tde5e.sheetHeader.copyId"),
                icon: "<i class='far fa-clipboard'></i>",
                callback: () => {
                    game.clipboard.copyPlainText(this.object.id);
                    ui.notifications.info(game.i18n.format("DOCUMENT.IdCopiedClipboard", {
                        label: game.i18n.localize(this.object.constructor.metadata.label),
                        type: "ID",
                        id: this.object.id,
                    }));
                },
            },
            {
                name: game.i18n.localize("tde5e.sheetHeader.copyUuid"),
                icon: "<i class='far fa-clipboard'></i>",
                callback: () => {
                    game.clipboard.copyPlainText(this.object.uuid);
                    ui.notifications.info(game.i18n.format("DOCUMENT.IdCopiedClipboard", {
                        label: game.i18n.localize(this.object.constructor.metadata.label),
                        type: "UUID",
                        id: this.object.uuid,
                    }));
                },
            },
        ];

        if (TDE5E.debugMode) {
            entries.push({
                name: game.i18n.localize("tde5e.sheetHeader.copyItem"),
                icon: "<i class='far fa-copy'></i>",
                callback: () => {
                    game.clipboard.copyPlainText(JSON.stringify(this.object.toObject(), null, 2));
                    ui.notifications.info(game.i18n.format(
                        "tde5e.notification.copySourceToClipboard",
                        { name: this.object.name },
                    ));
                },
            });

            if (!this.object.compendium) {
                entries.push({
                    name: game.i18n.localize("tde5e.settings.forceMigration.name"),
                    icon: "<i class='far fa-file-arrow-up'></i>",
                    callback: async () => {
                        const { name, id } = this.object;
                        const worldData = game.data[this.object.collectionName].find(d => d._id === id);
                        if (!worldData) {
                            util.error(`Unable to find source data for actor ${id}`);
                            return;
                        }
                        await migrateActor(this.object, worldData);
                        await migrateActorData(this.object);
                        util.info(game.i18n.format("tde5e.notification.actorMigrated", { name }), true);
                    },
                });
            }
        }

        return entries;
    }

    /**
     * Activate event listeners using the prepared sheet HTML
     * @param {HTML} html The prepared HTML object ready to be rendered into the DOM
     */
    activateListeners(html) {
        super.activateListeners(html);

        if (!this.isEditable) return;

        html.on("click", "input.equipment-select", this.onClickSelectArmor.bind(this));

        // Toggle status effects instead of updating them.
        html.on("change", "input.status-toggle", event => {
            const input = event.currentTarget;
            const state = this.actor.items.get(input.id);
            if (!state || state.img === "icons/svg/mystery-man.svg") return;
            stopEvent(event);
            state.setActive(input.checked);
        });

        // Loadout listeners
        const loadouts = html.find(".loadout-table");
        if (loadouts.length) {
            html.find(".loadout-add").click(this.onClickCreateLoadout.bind(this));
            loadouts.on("click", ".loadout-delete", this.onClickDeleteLoadout.bind(this));
            loadouts.on("click", ".loadout-move-up", this.onClickMoveLoadoutUp.bind(this));
            loadouts.on("click", ".loadout-move-down", this.onClickMoveLoadoutDown.bind(this));
        }

        this._intiializeInventoryListeners(html);
        this._initializeChangelogListeners(html);

        activateModifierControl(html, this.actor);
        if (this.options.enableUnitControl) UnitControl.activate(html);
    }

    _intiializeInventoryListeners(html) {
        GlobalContextMenu.create(
            this,
            html,
            "a.btn-add-container",
            this._getCreateContainerContextOptions(),
            { eventName: "click" },
        );

        const inventory = html.find(".inventory-table.container");
        if (inventory.length) {
            // Handle inventory container actions
            inventory.on("click", "a.btn-remove-container", this.onClickDeleteContainer.bind(this));
            inventory.on("click", "a.btn-move-container-up", this.onClickMoveContainerUp.bind(this));
            inventory.on("click", "a.btn-move-container-down", this.onClickMoveContainerDown.bind(this));

            // Add or remove items.
            inventory.on("click", "button.create-item", this.onClickCreateInventoryItem.bind(this));
            inventory.on("click", "a.remove-item", this.onClickDeleteInventoryItem.bind(this));
            inventory.on("click", "a.move-item", this.onClickTransferInventoryItem.bind(this));
            inventory.on("click", "a.roll-item", this.onClickRollInventoryItem.bind(this));
            inventory.on("click", "a.activate-item", this.onClickActivateInventoryItem.bind(this));
            inventory.on("click", "a.edit-item-note", this.onClickEditItemNote.bind(this));
            inventory.on("change", "input.silverthalers-input", this.onChangeSilverthalerPrice.bind(this));

            inventory.on("click", ".sorting-field", this.onClickChangeSorting.bind(this));

            // Handle drag listener for inventory items
            inventory.find(".item-drag").each((i, item) => {
                item.setAttribute("draggable", true);
                item.addEventListener(
                    "dragstart",
                    event => this._setDataTransferForItemMoveBewteenContainers(event, this.actor),
                );
            });

            const sheet = this;
            // Handle styling & drop for containers
            inventory.each((_, item) => {
                let dragOverContainers = $();

                const queryItem = $(item);
                queryItem.on("dragenter", event => {
                    if (dragOverContainers.length === 0) event.currentTarget.classList.add("focused-container");
                    dragOverContainers = dragOverContainers.add(event.target);
                });
                queryItem.on("dragleave drop", event => {
                    // Handle drop before drop-preview is removed
                    if (event.type === "drop") sheet.onDropInventoryItemOnInventoryContainers(event);
                    dragOverContainers = dragOverContainers.not(event.target);
                    if (dragOverContainers.length === 0) {
                        event.currentTarget.classList.remove("focused-container");
                    }
                });
            });

            // Handle click on container icon
            inventory.on("click", "a.container-icon", this.onClickChangeContainerIcon.bind(this));
        }
    }

    /**
     * Builds an array of options for creating inventory containers.
     * @returns {Array[]} An array of context menu options.
     */
    _getCreateContainerContextOptions() {
        const containerTypes = [
            {
                name: game.i18n.localize("tde5e.inventory.containerType.default"),
                icon: "<i class='fas fa-box-open'></i>",
                callback: () => this.createInventoryContainer(),
            },
            {
                name: game.i18n.localize("tde5e.inventory.containerType.trade"),
                icon: "<i class='fas fa-balance-scale'></i>",
                callback: () => this.createInventoryContainer("trade", "fas fa-balance-scale"),
            },
        ];

        if (game.user.isGM) {
            containerTypes.push({
                name: game.i18n.localize("tde5e.inventory.containerType.loot"),
                icon: "<i class='fas fa-gem'></i>",
                callback: () => this.createInventoryContainer("loot", "fas fa-gem"),
            });
        }

        return containerTypes;
    }

    _initializeChangelogListeners(html) {
        const changelog = html.find("div.tab.changelog");
        if (changelog.length) {
            changelog.find("input.only-improvements").change(event => {
                this.onlyShowImprovements = event.currentTarget.checked;
                this.render();
            });
            changelog.find("button.approve-all").click(() => ActorLogEntry.approveAll(this.actor));
            on(html[0], "click", ".undo-entry", event => this.resolveChangelogEntry(event)?.undo());
            on(changelog[0], "click", ".approve-entry", event => this.resolveChangelogEntry(event)?.approve());
            on(
                changelog[0],
                "change",
                "input.change-cost",
                event => this.resolveChangelogEntry(event)?.changeCost(event.delegateTarget.value),
            );
        }

        if (this.isImproving) {
            html.on("click", "div.improve", event => {
                const [doc, path] = this._resolveImprovementDocument(event.currentTarget.closest(".improvable"));
                const improvement = doc.system.getImprovement(path);
                if (!improvement) return;

                if (event.altKey) {
                    // Open dialog for quickly maxing numerical fields.
                    const field = doc.system.schema.getField(improvement.path);
                    if (improvement && field instanceof foundry.data.fields.NumberField) {
                        new ImprovementDialog(doc, field, improvement).render(true);
                        return;
                    }
                }

                improvement.apply();
            });
        }
    }

    /**
     * Extends Foundry's method to reset the scroll position when changing the tab.
     * @override
     */
    _onChangeTab(event, tabs, active) {
        super._onChangeTab(event, tabs, active);
        this.form.parentElement.scrollTop = 0;
    }

    /**
     * Handles a remove loadout button click by removing the associated loadout.
     * @param {Event} event The click event of the button.
     */
    async onClickDeleteLoadout(event) {
        const loadouts = foundry.utils.deepClone(this.actor._source.system.loadouts);
        const index = parseInt(event.currentTarget.dataset.index, 10);
        const loadout = loadouts[index];
        if (!loadout) return;

        const confirmed = await Dialog.confirm({
            title: game.i18n.localize("tde5e.actor.loadout.deleteTitle"),
            content: game.i18n.format("tde5e.actor.loadout.deleteContent", { name: loadout.name }),
        });
        if (!confirmed) return;

        loadouts.splice(index, 1);
        this.actor.update({ "system.loadouts": loadouts });
    }

    /**
     * Handles an add loadout button click by creating a new loadout for this actor.
     */
    onClickCreateLoadout() {
        const loadouts = foundry.utils.deepClone(this.actor._source.system.loadouts);
        loadouts.push(LoadoutModel.createDefault(this.actor));
        this.actor.update({ "system.loadouts": loadouts });
    }

    /**
     * Handles an order button click by moving the loadout up one spot.
     * @param {Event} event The click event of the button.
     */
    onClickMoveLoadoutUp(event) {
        event.preventDefault();
        const loadouts = foundry.utils.deepClone(this.actor._source.system.loadouts);
        const index = parseInt(event.currentTarget.dataset.index, 10);
        if (index <= 0) return;

        const loadout = loadouts[index];
        loadouts[index] = loadouts[index - 1];
        loadouts[index - 1] = loadout;
        this.actor.update({ "system.loadouts": loadouts });
    }

    /**
     * Handles an order button click by moving the loadout down one spot.
     * @param {Event} event The click event of the button.
     */
    onClickMoveLoadoutDown(event) {
        event.preventDefault();
        const loadouts = foundry.utils.deepClone(this.actor._source.system.loadouts);
        const index = parseInt(event.currentTarget.dataset.index, 10);
        if (index >= loadouts.length - 1) return;

        const loadout = loadouts[index];
        loadouts[index] = loadouts[index + 1];
        loadouts[index + 1] = loadout;
        this.actor.update({ "system.loadouts": loadouts });
    }

    /**
     * Handles the click on an element.
     * Selects The item to be part of the active equipment of the actor.
     * @param {Event} event The triggering event.
     */
    onClickSelectArmor(event) {
        const source = event.currentTarget;
        const itemId = source.dataset.itemPath;
        // check will have the value after it is clicked
        const newValue = source.checked;

        if (!itemId) {
            event.preventDefault();
            util.error(`Could not find item ID for weapon/armor ${source.outerHTML}.`, true, false);
            return;
        }

        const currentlySelectedArmors = this.actor.itemTypes[TDE5E.itemTypes.Armor]
            .filter(armor => armor.system.isSelected);

        // Dont allow deselct of only one armor
        if (!newValue && currentlySelectedArmors.length === 1) {
            event.preventDefault();
            return;
        }
        const targetArmor = this.actor.items.get(itemId);

        const updates = [];
        // Deselect all
        for (const armor of currentlySelectedArmors) {
            updates.push({ _id: armor.id, system: { isSelected: false } });
        }

        // Select single
        updates.push({ _id: targetArmor.id, system: { isSelected: true } });
        this.actor.updateEmbeddedDocuments("Item", updates);
    }

    /**
     * Handles the click on an element.
     * Moves an inventory container one step down, meaning it will be displayed lower on the sheet.
     * @param {Event} event The click event.
     */
    onClickMoveContainerDown(event) {
        event.preventDefault();
        const id = event.currentTarget.dataset.container;
        const containers = foundry.utils.deepClone(this.actor.system.inventoryContainer);
        const index = containers.findIndex(c => c.id === id);
        if (index >= containers.length - 1) return;

        const container = containers[index];
        containers[index] = containers[index + 1];
        containers[index + 1] = container;
        this.actor.update({ "system.inventoryContainer": containers }, { diff: false });
    }

    /**
     * Handles the click on an element.
     * Moves an inventory container one step up, meaning it will be displayed higher on the sheet.
     * @param {Event} event The click event.
     */
    onClickMoveContainerUp(event) {
        event.preventDefault();
        const id = event.currentTarget.dataset.container;
        const containers = foundry.utils.deepClone(this.actor.system.inventoryContainer);
        const index = containers.findIndex(c => c.id === id);
        if (index <= 0) return;

        const container = containers[index];
        containers[index] = containers[index - 1];
        containers[index - 1] = container;
        this.actor.update({ "system.inventoryContainer": containers }, { diff: false });
    }

    /**
     * Handles the click on an element.
     * Adds a fresh inventory item to container
     * @param {Event} event The click event on the button.
     */
    onClickCreateInventoryItem(event) {
        event.preventDefault();
        this.actor.createEmbeddedDocuments("Item", [{
            name: "-",
            img: "systems/tde5e/assets/defaultItems/inventory_item.svg",
            type: TDE5E.itemTypes.InventoryItem,
            system: { container: event.currentTarget.dataset.container },
        }]);
    }

    /**
     * Changes the price of an item in Silverthalers (instead of the usual Kreutzers).
     * @param {jQuery.Event} event The JQuery event of the change.
     */
    onChangeSilverthalerPrice(event) {
        stopEvent(event);
        const item = this.actor.items.get(event.currentTarget.dataset.itemId);
        const price = Math.round(event.currentTarget.valueAsNumber * 100);
        item?.update({ "system.price": price });
    }

    /**
     * Removes the inventory item associated with the button of the event.
     * @param {jQuery.Event} event The event fired by the remove button.
     */
    onClickDeleteInventoryItem(event) {
        const item = this.actor.items.get(event.currentTarget.closest(".item-element").dataset.documentId);
        if (!item) return;

        item.deleteConfirm();
        stopEvent(event);
    }

    /**
     * Displays a dropdown menu for selecting the actor that the item associated
     *  with the button should be moved to.
     * @param {jQuery.Event} event The event fired by the move button.
     */
    onClickTransferInventoryItem(event) {
        // Resolve the item.
        const item = this.actor.items.get(event.currentTarget.closest(".item-element").dataset.documentId);
        if (!item) return;

        let validTargets = game.actors.filter(actor => actor.visible && actor.id !== this.actor.id);
        if (!validTargets.length) {
            util.info("You don't know anyone to give this item to.", true);
            return;
        }
        if (validTargets.length > 10) {
            util.info("List size exceeded - restricting target actor list to current scene.");
            const sceneActors = canvas.scene.tokens.map(token => token.actor);
            validTargets = validTargets.filter(actor => sceneActors.includes(actor));
        }

        new GlobalContextMenu($(event.currentTarget), null, validTargets.map(actor => ({
            name: actor.name,
            icon: `<img class='actor-preview' src='${actor.img}'>`,
            callback: () => requestItemTransfer(item.uuid, actor.id),
        }))).toggle($(event.currentTarget));

        stopEvent(event);
    }

    /**
     * Rolls the inventory item associated with the button of the event.
     * @param {jQuery.Event} event The event fired by the remove button.
     */
    onClickRollInventoryItem(event) {
        const item = this.actor.items.get(event.currentTarget.closest(".item-element").dataset.documentId);
        if (!item?.isRollable) return;

        item.roll();
        stopEvent(event);
    }

    /**
     * Opens the itemsheet associated with the inventory item and focuses the notes element.
     * @param {Event} event The triggering event.
     */
    onClickEditItemNote(event) {
        const source = event.currentTarget;
        const { itemId } = source.dataset;

        if (!itemId) util.error("Item-Id not found! Cannot edit item notes.");
        this.actor.items.get(itemId).sheet.render(true, { focusNotes: true });
    }

    /**
     * Activates the inventory item associated with the button of the event.
     * @param {jQuery.Event} event The event fired by the remove button.
     */
    onClickActivateInventoryItem(event) {
        const item = this.actor.items.get(event.currentTarget.closest(".item-element").dataset.documentId);
        item?.activate();
        stopEvent(event);
    }

    /**
     * Adds a new container for inventory items with the given type.
     * @param {string=} type The type of the container. Defaults to "default".
     * @param {string=} icon The icon of the container. Defaults to "fas fa-box-open".
     */
    createInventoryContainer(type = "default", icon = "fas fa-box-open") {
        const containers = this.actor.system.inventoryContainer.slice();
        containers.push({
            id: foundry.utils.randomID(),
            name: game.i18n.localize(`tde5e.inventory.containerType.${type}`),
            type,
            icon,
        });
        this.actor.update({ "system.inventoryContainer": containers }, { diff: false, skipChangelog: true });
    }

    /**
     * Displays a dropdown menu for selecting the icon for a container.
     * @param {jQuery.Event} event The event fired by the button.
     */
    onClickChangeContainerIcon(event) {
        stopEvent(event);

        // Resolve the item.
        const el = $(event.currentTarget);
        const containerEl = event.currentTarget.closest(".inventory-table.container");
        if (!containerEl) util.error("Failed to find container header when selecting icon.");

        const id = containerEl.dataset.container;
        if (!id) util.error(`Failed to find container ID for container ${containerEl}`);

        GlobalContextMenu.create(this, el, null, TDE5E.inventoryContainerIcons.map(icon => ({
            name: "", // icon
            icon: `<i class="container-icon-preview ${icon}"></i>`,
            callback: () => this.changeInventoryContainerIcon(id, icon),
        })), { css: "icon-grid" }).toggle(el);
    }

    /**
     * Handles the click on an element.
     * Removes a specified container from the actor.
     * @param {Event} event The click event on the button.
     */
    onClickDeleteContainer(event) {
        event.preventDefault();
        const id = event.currentTarget.dataset.container;
        const containers = foundry.utils.deepClone(this.actor.system.inventoryContainer);
        containers.splice(containers.findIndex(c => c.id === id), 1);

        // Reset items in container to default container
        const inventoryItems = this.actor.itemTypes[TDE5E.itemTypes.InventoryItem]
            .concat(
                this.actor.itemTypes[TDE5E.itemTypes.Armor],
                this.actor.itemTypes[TDE5E.itemTypes.MeleeWeapon],
                this.actor.itemTypes[TDE5E.itemTypes.RangedWeapon],
                this.actor.itemTypes[TDE5E.itemTypes.Shield],
            );

        const itemUpdates = inventoryItems
            .filter(item => item.system.container === id)
            .map(item => ({ _id: item.id, "system.container": "DEFAULT" }));

        // Update actor and embedded documents
        this.actor.update(
            { "system.inventoryContainer": containers },
            { render: false, diff: false, skipChangelog: true },
        ).then(() => {
            this.actor.updateEmbeddedDocuments("Item", itemUpdates, { render: true, skipChangelog: true });
        });
    }

    /**
     * Handles the click on an element.
     * Changes the sorting field or order for a single container.
     * @param {jQuery.Event} event The click event on the element.
     */
    onClickChangeSorting(event) {
        event.preventDefault();
        const fieldId = event.currentTarget.dataset.id;
        if (!fieldId) return;

        const containerId = event.currentTarget.closest(".container.inventory-table")?.dataset?.container;
        if (!containerId) util.error("Failed to find container for sorting in HTML", true, false);

        const allContainers = foundry.utils.deepClone(this.actor._source.system.inventoryContainer);
        const container = allContainers.find(c => c.id === containerId);

        if (container.sortingField === fieldId) {
            container.sortAscending = !container.sortAscending;
        } else {
            container.sortingField = fieldId;
            container.sortAscending = true;
        }

        this.actor.update({ "system.inventoryContainer": allContainers }, { render: true });
    }

    /**
     * Handles the drop event for all inventory containers.
     * If an item in the oinventory is drag & dropped onto one of the containers,
     *  it it processed and is moved to that container.
     * @param {jQuery.Event} event The drop event.
     */
    onDropInventoryItemOnInventoryContainers(event) {
        event = event.originalEvent;
        const data = JSON.parse(event.dataTransfer.getData("text/plain"));
        const srcContainer = data.container;
        const { type, tabName } = data;
        const destContainer = event.currentTarget.dataset.container;

        if (type !== "Item") return;
        if (!util.hasValue(destContainer)) {
            util.error("Failed to find container in HTML", true, false);
            return;
        }
        // Handle move of item inside of actor
        if (util.hasValue(srcContainer) && tabName === "inventory") {
            stopEvent(event);
            if (this.actor.id !== data.actorId) return;
            const itemId = data.data._id;
            this._moveItemInInventory(itemId, destContainer);
        // Dropping external items onto actor
        } else if (data.uuid) {
            stopEvent(event);
            this._addExternalItemToInventory(data.uuid, destContainer);
        }
    }

    /**
     *  Moves an item of this actor to another container.
     * @param {string} itemId The id of an item.
     * @param {string?} destContainer The id of the container to add the item to.
     */
    async _moveItemInInventory(itemId, destContainer = "DEFAULT") {
        const item = this.actor.items.get(itemId);
        const srcContainer = item.system.container;
        if (destContainer === srcContainer) return;
        await item.update({ "system.container": destContainer });
    }

    /**
     *  Adds an item to this actor using given UUID.
     * @param {string} uuid The UUID of an item.
     * @param {string?} destContainer The id of the container to add the item to.
     */
    async _addExternalItemToInventory(uuid, destContainer = "DEFAULT") {
        const itemData = (await fromUuid(uuid)).toObject();
        if (foundry.utils.hasProperty(TDE5E.inventoryRelatedItemTypes, itemData.type)) {
            itemData.system.container = destContainer;
        }
        await this.actor.createEmbeddedDocuments("Item", [itemData]);
    }

    /**
     * Resolves a log entry from this sheet's actor.
     * @param {jQuery.Event} event The event of the entry button.
     * @returns {ActorLogEntry} The log entry associated with the button.
     */
    resolveChangelogEntry(event) {
        const entryEl = event.delegateTarget.closest(".log-entry");
        const { path, uuid } = entryEl.dataset;
        if (path === "create" || path === "delete") {
            return this.actor.system.changelog.find(entry => entry.path === path && entry.documentUuid === uuid);
        }

        const document = fromUuidSync(uuid);
        return document?.system.changelog.find(entry => entry.path === path);
    }

    /**
     * Opens a new window to configure the actor.
     */
    configure() {
        new TdeActorConfig(this.actor, {
            top: this.position.top + 40,
            left: this.position.left + ((this.position.width - 400) / 2),
        }).render(true);
    }

    /**
     * Preprocesses or filters the given form input event based on the element's class.
     * @override
     */
    _onChangeInput(event) {
        const classes = event.currentTarget.classList;
        if (classes.contains("item-property")) return this.changeItemValue(event);
        if (classes.contains("actor-property")) return this.changeActorValue(event);
        return super._onChangeInput(event);
    }

    /**
     * Extends Foundry's submit pipeline to check if the currently focused
     *  element is a secondary input (one with the same property as another
     *  on the sheet) that should override the primary value.
     * @param {jQuery.Event} event The event of the submit operation.
     * @param {object} param1 The options of the operation.
     * @param {object} param1.updateData the data for the update.
     * @param {boolean} param1.preventClose Prevent the closing of the window.
     * @param {boolean} param1.preventRender Prevent the render of window.
     * @returns {Promise} A promise representing the update data.
     * @override
     */
    async _onSubmit(event, { updateData = null, preventClose = false, preventRender = false } = {}) {
        const focus = document.activeElement;
        if (focus && focus.classList.contains("secondary")) {
            updateData ||= {};
            updateData[focus.dataset.name] = focus.value;
        }
        return super._onSubmit(event, { updateData, preventClose, preventRender });
    }

    /**
     * Extends Foundry's edit listener to display an error if the user lacks
     *  the required permissions.
     * @param {jQuery.Event} event The event of the edit operation.
     * @override
     */
    _onEditImage(event) {
        if (!game.user.can("FILES_BROWSE")) {
            util.error("You lack the permissions to upload images.", false, true);
            return;
        }
        super._onEditImage(event);
    }

    /**
     * Changes the icon a given container.
     * @param {string} id The ID of the container.
     * @param {string} newIcon The new icon code for that container.
     */
    changeInventoryContainerIcon(id, newIcon) {
        const containers = foundry.utils.deepClone(this.actor.system.inventoryContainer);
        containers.find(c => c.id === id).icon = newIcon;
        this.actor.update({ "system.inventoryContainer": containers }, { diff: false, skipChangelog: true });
    }

    /**
     * Handles changes on input elements by immediately applying the change to
     *  this actor's property defined by the path in the element's dataset.
     * @param {jQuery.Event} event The JQuery change event.
     */
    changeActorValue(event) {
        const input = event.currentTarget;

        const propertyPath = input.name || input.dataset.name;
        if (!propertyPath) {
            util.error(`Could not find name for input ${input.outerHTML}.`);
            return;
        }

        const updateObject = {};
        const value = BaseActorSheet.parseInput(input);
        updateObject[propertyPath] = value;
        if (propertyPath.includes("[")) parseArrays(updateObject, this.actor._source, true);
        else foundry.utils.setProperty(updateObject, propertyPath, value);

        this.actor.update(updateObject, {
            diff: false,
            render: !(propertyPath.startsWith("system.bio") || propertyPath.startsWith("system.currency")),
        });
    }

    /**
     * Handles changes on input elements by applying the change to the property
     *  defined by the item ID within the element's name and the property path
     *  from the element's dataset.
     * @param {jQuery.Event} event The JQuery change event.
     */
    changeItemValue(event) {
        const input = event.currentTarget;

        let propertyPath = input.dataset.itemPath;
        if (!propertyPath) {
            util.error(`Could not find data-item-path for input ${input.outerHTML}.`);
            return;
        }

        const pathSegments = propertyPath.split(".");
        const itemId = pathSegments.shift();
        const item = this.actor.items.get(itemId);
        if (!item) {
            util.error(`Could not find item with ID ${itemId} in actor ${this.actor.name}.`);
            return;
        }

        propertyPath = pathSegments.join(".");
        const updateObject = {};
        const value = BaseActorSheet.parseInput(input);
        updateObject[propertyPath] = value;
        if (propertyPath.includes("[")) parseArrays(updateObject, item._source, true);
        else foundry.utils.setProperty(item, propertyPath, value);

        // Prevent rendering the sheet unless the refresh attribute is set.
        if (input.dataset.refresh) item.update(updateObject, { diff: false });
        else this._backgroundUpdate(() => item.update(updateObject, { diff: false }));
    }

    /**
     * Prevents rendering this sheet while the given operation runs.
     * @param {function():Promise} operation The asynchronous operation to execute. Must return a promise.
     * @returns {Promise} A promise representing the operation.
     */
    async _backgroundUpdate(operation) {
        const priorState = this._state;
        this._state = Application.RENDER_STATES.RENDERING;
        await operation();
        this._state = priorState;
    }

    /**
     *  Sets the data for the dragstart event for an inventory item in a container
     *   that will later be movable to other containers.
     * @param {Event} event The dragstart event.
     * @param {TdeActor} actor The parent of event. Usually it is equivalent to <i>this.actor</i>.
     */
    _setDataTransferForItemMoveBewteenContainers(event, actor) {
        event.stopPropagation();

        const target = event.currentTarget;
        const tabName = target.getAttribute("data-tab-name");
        const container = target.getAttribute("data-container");
        const item = this.actor.items.get(target.getAttribute("name"));

        const content = {
            type: "Item",
            uuid: item.uuid,
            tabName,
            container,
            actorId: actor.id,
            data: item.toObject(),
        };

        event.dataTransfer.setData("text/plain", JSON.stringify(content));
    }

    /**
     * Extends Foundry's method to store the currently closed detail elements.
     * @override
     */
    async close(options = {}) {
        // Query relevant elements while the sheet is still open.
        const closedDetails = [...this.element[0].querySelectorAll("details:not([open])")]
            .map(el => el.dataset.detailsId)
            .filter(Boolean);

        await super.close(options);

        // Adjust settings after closing to improve responsiveness.
        const storage = game.settings.get("tde5e", "closedDetails");
        if (closedDetails.length) storage[this.object.uuid] = closedDetails;
        else if (storage.hasOwnProperty(this.object.uuid)) delete storage[this.object.uuid];
        else return;

        game.settings.set("tde5e", "closedDetails", storage); // No await because we don't care when this completes.
    }

    /**
     * Reads, parses and returns a correctly typed input from the given element.
     * @param {HTMLInputElement} targetInput The input to read the value from.
     * @returns {number|boolean|string} The parsed value of the input.
     */
    static parseInput(targetInput) {
        switch (targetInput.type) {
        case "number": return (!targetInput.value.includes("."))
            ? parseInt(targetInput.value, 10)
            : parseFloat(targetInput.value, 10);
        case "checkbox": return targetInput.checked;
        case "select-one": return (targetInput?.dataset?.type?.toLowerCase() === "number")
            ? parseInt(targetInput.value, 10)
            : targetInput.value;
        case "select-multiple": return $(targetInput).val();
        default: return targetInput.value;
        }
    }
}
