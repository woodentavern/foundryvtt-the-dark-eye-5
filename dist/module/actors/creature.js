import NpcActor from "./npc.js";

export default class CreatureActor extends NpcActor {
    /** @override */
    async _preCreate(data, options, user) {
        this.updateSource({ "token.actorLink": false });
        return super._preCreate(data, options, user);
    }

    /** @override */
    async _setupItems() {
        const conditions = await game.packs.get(TDE5E.compendiums.Conditions)
            .getDocuments({ system: { isDefault__in: [true] } });
        const skills = await game.packs.get(TDE5E.compendiums.Skills)
            .getDocuments({ system: { cid__in: TDE5E.defaultAbilities.skills.creature } });
        const combat = await game.packs.get(TDE5E.compendiums.Combat)
            .getDocuments({ system: { cid__in: TDE5E.defaultAbilities.combat.creature } });
        const states = await game.packs.get(TDE5E.compendiums.States)
            .getDocuments({ system: { isDefault__in: [true] } });
        const equipment = await game.packs.get(TDE5E.compendiums.Equipment)
            .getDocuments({ system: { cid__in: ["UNGERUSTET"] } });

        return conditions.concat(skills, combat, states, equipment);
    }

    /**
     * Returns the creatureType or null if none is present.
     * There can only ever be one creatureType per creature.
     * @returns {string} The creaturetype.
     */
    get creatureType() {
        const type = this.itemTypes[TDE5E.itemTypes.CreateType];
        if (!type || type.length === 0) return null;
        if (type.length > 1) util.warn(`More than one creatureType present in ${this.name}`, true, false);

        return type[0];
    }

    /**
     * @override
     */
    get sizeCategory() {
        return this.system.sizeCategory;
    }

    /**
     * Deletes a creature specialRule identified by the given id.
     * @param {string} index The index of the rule to delete.
     */
    async deleteSpecialRule(index) {
        const rules = foundry.utils.deepClone(this._source.system.specialRules);
        rules.splice(index, 1);
        await this.update({ "system.specialRules": rules }, { diff: false });
    }

    /**
     * Adds a new, empty creature specialRule to the actor.
     */
    async addNewSpecialRule() {
        const rules = foundry.utils.deepClone(this._source.system.specialRules);
        rules.push({ name: game.i18n.localize("tde5e.actor.specialRules.name") });
        await this.update({ "system.specialRules": rules }, { diff: false });
    }

    /**
     * Deletes a creature fact identified by the given id.
     * @param {string} index The index of the fact to delete.
     */
    async deleteCreatureFact(index) {
        const facts = foundry.utils.deepClone(this._source.system.creatureFacts.facts);
        facts.splice(index, 1);
        await this.update({ "system.creatureFacts.facts": facts }, { diff: false });
    }

    /**
     * Adds a new, empty creature fact to the actor.
     */
    async addNewCreatureFact() {
        const facts = foundry.utils.deepClone(this._source.system.creatureFacts.facts);
        facts.push({ quality: "QS x" });
        await this.update({ "system.creatureFacts.facts": facts }, { diff: false });
    }
}
