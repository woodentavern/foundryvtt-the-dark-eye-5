import { stopEvent } from "../jsUtils.js";
import { arrayEquals, error, lookupCompendiumEntry } from "../util.js";
import AnyField from "./model/partial/anyField.js";

/**
 * @typedef HistoryEntry
 * Represents a single property change within the log entry.
 * @property {number} time The timestamp of the change in milliseconds.
 * @property {*} value The property value set at the given time.
 */

/**
 * Model class for entries in the actor's changelog.
 * @property {string} uuid The UUID of the modified document.
 * @property {string} path The path to the changed property within the modified document.
 * @property {*} original The original property value before any logged changes were applied.
 * @property {HistoryEntry[]} history An array of up to 10 value and timestamp pairs representing property value
 *  changes since the original. For improvements, this should only contain the most recent value.
 */
export class ActorLogEntry extends foundry.abstract.DataModel {
    /**
     * The cutoff timespan for relevant entries in milliseconds.
     * @type {number}
     */
    static relevanceCutoff = 24 * 60 * 60 * 1000;

    /** @inheritdoc */
    static defineSchema() {
        const { fields } = foundry.data;
        return {
            path: new fields.StringField({ required: true, blank: false, readonly: true }),
            original: new AnyField(),
            history: new fields.ArrayField(new fields.SchemaField({
                time: new fields.NumberField({
                    integer: true,
                    positive: true,
                    required: true,
                    nullable: false,
                }),
                value: new AnyField(),
            })),
            cost: new fields.NumberField({ integer: true }),
            userCostDelta: new fields.NumberField({ integer: true }),
        };
    }

    /**
     * Creates a new log entry for inclusion in an actor's changelog.
     * @param {object} data The data of the entry.
     * @param {object=} options The options of the creation. Defaults to an empty object.
     * @param {boolean=} typed Flag indicating that the type was already resolved. Defaults to false.
     */
    constructor(data, options = {}, typed = false) {
        if (!typed) {
            switch (data.path) {
            case "create": return new CreationLogEntry(data, options);
            case "delete": return new DeletionLogEntry(data, options);
            default: if (data.cost) return new ImprovementLogEntry(data, options);
            }
        }

        super(data, options);
    }

    /**
     * Gets the time of the most recent change for this property.
     * @returns {number} The time of the last change in milliseconds.
     */
    get time() {
        return this.history[0].time;
    }

    /**
     * Gets the most recent value for this property.
     * @returns {*} The value of the last change.
     */
    get currentValue() {
        return this.history[0].value;
    }

    /**
     * Gets the UUID of the document that this change is for.
     * @returns {string} The UUID of the changed document.
     */
    get documentUuid() {
        return this.parent.parent.uuid;
    }

    /**
     * Creates a link and property name for this entry.
     * @returns {string} An HTML string containing the entry's parent name, link and property name.
     */
    get displayName() {
        const document = this.parent.parent;
        const name = document.createLink ? document.createLink() : document.name;
        const property = this.propertyDisplayName;
        return property ? `${name} <i class="fas fa-chevron-right"></i> ${property}` : name;
    }

    /**
     * Gets the localized display name of the entry's property.
     * @returns {string} The human readable name of the changed property.
     */
    get propertyDisplayName() {
        switch (this.path) {
        case "system.improvement.value":
        case "system.variant.level":
            return "";
        default:
            return this.parent.parent.getPropertyMetadata(this.path).label;
        }
    }

    /**
     * Indicates whether the current value of this change should be displayed in the improvement footer.
     * @returns {boolean} True if the value should be displayed in the footer, false otherwise.
     */
    get showFooterValue() {
        return true;
    }

    /**
     * Formats a value of this change in a human readable way.
     * @param {*} value The value to format.
     * @returns {string} A string containing the change value in a human readable format.
     */
    toDisplayValue(value) {
        if (this.path === "system.enhancements" && this.cost) {
            return value.filter(e => e.owned).map(e => e.name).join(", ");
        }

        if (Array.isArray(value)) {
            return `${value.length} ${game.i18n.localize("tde5e.general.entries")}`;
        }

        if (typeof (value) === "string") {
            return value.length > 200
                ? `${value.length} ${game.i18n.localize("tde5e.general.characters")}` // Display character count.
                : value.replace(/\s|(<(\/)?p>)/g, " "); // Remove whitespace and paragraphs.
        }

        return value;
    }

    /**
     * Adds this entry to the given changelog.
     * @param {ActorLogEntry[]} log The changelog to add this entry to.
     */
    addToLog(log) {
        const entryIndex = log.findIndex(e => e.path === this.path);
        if (entryIndex === -1) log.push(this);
        else this.mergeInto(log[entryIndex]);
    }

    /**
     * Merges changes from this entry into the given existing entry.
     * @param {ActorLogEntry} entry The entry to merge own changes into.
     */
    mergeInto(entry) {
        if (this.history.length === 1
            && arrayEquals(this.currentValue, entry.history.length === 1 ? entry.original : entry.history[1]?.value)) {
            // If a single new value matches the target's previous value, consider it an undo operation.
            entry.history.shift();
            if (entry.history.length) entry.history[0].time = this.time;
        } else {
            // Otherwise, prepend the history.
            entry.history.unshift(...this.history);
        }

        const cutoff = this.time - ActorLogEntry.relevanceCutoff;
        const newHistory = entry.history.filter(c => c.time > cutoff);
        if (newHistory.length > 10) newHistory.length = 10;
        entry.updateSource({ history: newHistory });
    }

    /**
     * Checks if this entry is still relevant after the given time.
     * @param {number} time The cutoff time in milliseconds.
     * @returns {boolean} True if the entry is still relevant for the actor, false otherwise.
     */
    isRelevant(time) {
        return this.history.length > 0 && this.time > time;
    }

    /**
     * Attempts to reverse changes tracked by this entry.
     * @returns {Promise} A promise representing the document update.
     */
    async undo() {
        const doc = this.parent.parent;
        const previousValue = this.history[1]?.value ?? this.original;
        if (foundry.utils.getProperty(doc._source, this.path) === previousValue) return this.approve();
        return this.parent.parent.update({ [this.path]: previousValue });
    }

    /**
     * Removes this entry from the parent's changelog.
     * @returns {Promise} A promise representing the document update.
     */
    approve() {
        const doc = this.parent.parent;
        const log = doc.system.changelog
            .filter(entry => entry !== this)
            .map(entry => entry.toObject());
        return doc.update({ "system.changelog": log }, { skipChangelog: true });
    }

    /**
     * Applies all pending changes of the given actor and clears the changelog.
     * @param {TdeActor} actor The actor to approve changes for.
     * @returns {Promise} A promise representing the actor update.
     */
    static async approveAll(actor) {
        let totalAp = actor.system.changelog.reduce((total, entry) => total + entry.cost + entry.userCostDelta, 0);
        const itemUpdates = [];
        for (const item of actor.items) {
            if (!item.system.changelog.length) continue;

            totalAp += item.system.changelog.reduce((total, entry) => total + entry.cost + entry.userCostDelta, 0);
            itemUpdates.push({ _id: item.id, "system.changelog": [] });
        }

        const confirmed = await Dialog.confirm({
            title: game.i18n.localize("tde5e.actor.changelog.approveAll.title"),
            content: game.i18n.format("tde5e.actor.changelog.approveAll.hint", { ap: totalAp }),
        });
        if (!confirmed) return;

        await actor.update({
            "system.adventurePoints.spent": (actor.system.adventurePoints?.spent ?? 0) + totalAp,
            "system.changelog": [],
        }, { skipChangelog: true, render: !itemUpdates.length });
        if (itemUpdates.length) await actor.updateEmbeddedDocuments("Item", itemUpdates, { skipChangelog: true });
    }

    /**
     * Adds and merges changelog entries for the changes of the given update and appends the changelog to the update.
     * @param {Document} doc The document that is being updated.
     * @param {object} update The data of the update.
     * @param {object=} flatChanges Flattened data of the update. Will be created internally if not given.
     */
    static prepareUpdate(doc, update, flatChanges = foundry.utils.flattenObject(update)) {
        const time = flatChanges["_stats.modifiedTime"] ?? Date.now();
        const logEntries = Object.keys(flatChanges).reduce((entries, propertyPath) => {
            const currentValue = foundry.utils.getProperty(doc._source, propertyPath);
            if (!doc.trackProperty(propertyPath, currentValue)) return entries;

            const newValue = flatChanges[propertyPath];
            if (currentValue === newValue) return entries;

            const apCost = doc.system.getImprovement(propertyPath)?.calculateImprovementCost(newValue) ?? 0;
            entries.push(new ActorLogEntry({
                path: propertyPath,
                original: currentValue,
                history: [{ time, value: newValue }],
                cost: apCost,
                userCostDelta: 0,
            }));
            return entries;
        }, []);

        if (logEntries.length) {
            foundry.utils.setProperty(update, "system.changelog", this.prepareChangelog(doc, logEntries));
        }
    }

    /**
     * Adds the given entries to the given document's changelog and prunes entries that should no longer be included.
     * @param {Document} doc The document to prepare the changelog for.
     * @param {ActorLogEntry[]} entries The entries to add to the log.
     * @returns {ActorLogEntry[]} An updated and pruned changelog.
     */
    static prepareChangelog(doc, entries) {
        let changelog = doc.system.changelog ?? [];
        if (!entries.length) return changelog;

        changelog = changelog.map(entry => new ActorLogEntry(entry.toObject()));
        entries.forEach(e => e.addToLog(changelog));

        const cutoff = Date.now() - this.relevanceCutoff;
        changelog = changelog.filter(entry => entry.isRelevant(cutoff));
        return changelog.map(entry => entry.toObject(false));
    }
}

export class ImprovementLogEntry extends ActorLogEntry {
    /**
     * Creates a new log entry for inclusion in an actor's changelog.
     * @param {object} data The data of the entry.
     * @param {object} options The options of the creation.
     */
    constructor(data, options) {
        super(data, options, true);
    }

    /**
     * Merges changes from this entry into the given existing entry.
     * @param {ImprovementLogEntry} entry The entry to merge own changes into.
     * @override
     */
    mergeInto(entry) {
        entry.updateSource({
            history: this._source.history,
            cost: entry.cost + this._source.cost,
            userCostDelta: entry.userCostDelta + this._source.userCostDelta,
        });
    }

    /**
     * Changes the adventure point cost of this entry.
     * This does not modify the calculated cost, but tracks a delta value.
     * @param {number} apCost The new adventure point cost.
     * @returns {Promise} A promise representing the actor update.
     */
    changeCost(apCost) {
        this.updateSource({ userCostDelta: apCost - this._source.cost });
        return this.parent.parent.update(
            { "system.changelog": this.parent.parent._source.system.changelog },
            { skipChangelog: true, diff: false },
        );
    }

    /**
     * Checks if the given entry is still relevant.
     * @returns {boolean} True if the entry is still relevant for the actor, false otherwise.
     * @override
     */
    isRelevant() {
        return this.history.length > 0 && this.cost && this.original !== this.currentValue;
    }

    /**
     * Removes this entry from the parent's changelog and subtracts the spent adventure points from the actor.
     * @returns {Promise} A promise representing the document updates.
     * @override
     */
    async approve() {
        const doc = this.parent.parent;
        const log = doc.system.changelog
            .filter(entry => entry !== this)
            .map(entry => entry.toObject());
        const update = { "system.changelog": log };

        const cost = this.cost + this.userCostDelta;
        if (doc instanceof Actor) {
            update["system.adventurePoints.spent"] = (doc.system.adventurePoints?.spent ?? 0) + cost;
        } else {
            const { actor } = doc;
            if (actor) {
                const ap = actor.system.adventurePoints?.spent ?? 0;
                await actor.update(
                    { "system.adventurePoints.spent": ap + cost },
                    { skipChangelog: true, render: false },
                );
            }
        }

        return doc.update(update, { skipChangelog: true });
    }

    /**
     * Checks if the given actor has any improvements that require GM approval.
     * @param {TdeActor} actor The actor to search.
     * @returns {boolean} True if the actor has unapproved improvements, false otherwise.
     */
    static hasPendingImprovements(actor) {
        return actor.hasPlayerOwner && actor.system.changelog.some(entry => entry.cost > 0);
    }

    /**
     * Sends a notification containing the names of actors that have unapproved improvements.
     */
    static notifyPendingImprovements() {
        const pendingActors = game.actors
            .filter(ImprovementLogEntry.hasPendingImprovements)
            .map(actor => actor.name)
            .join(", ");

        if (pendingActors) {
            const message = game.i18n.format("tde5e.actor.changelog.notificationMulti", { actors: pendingActors });
            ui.notifications.info(`The Dark Eye | ${message}`, { permanent: true });
        }
    }

    /**
     * Extends the rendered actor directory to add icons for unapproved improvements.
     * @param {Application} _app The application containing the directory.
     * @param {jQuery.Element} html The HTML element of the directory.
     */
    static displayPendingImprovements(_app, html) {
        if (!game.user.isGM) return;
        ImprovementLogEntry.renderImprovementToggles(html);

        // Find relevant actors.
        const pendingActors = game.actors.filter(ImprovementLogEntry.hasPendingImprovements);
        if (!pendingActors.length) return;

        // Create a link to the improvements for each one.
        const list = html[0].querySelector("ol.directory-list");
        const title = game.i18n.localize("tde5e.actor.changelog.title");
        pendingActors.forEach(actor => {
            const el = list.querySelector(`li.actor[data-entry-id="${actor.id}"] h4`);
            if (!el) return;

            const improvementLink = document.createElement("a");
            improvementLink.classList.add("actor-improvements");
            improvementLink.title = title;
            improvementLink.innerHTML = "<i class='fas fa-cart-plus'></i>";
            improvementLink.addEventListener("click", async event => {
                await actor.sheet.render(true, { activeTab: "changelog" });
                stopEvent(event);
            });

            el.append(improvementLink);
        });
    }

    /**
     * Extends the rendered actor directory with buttons to toggle improvements globally.
     * @param {jQuery.Element} html The HTML element of the directory.
     */
    static renderImprovementToggles(html) {
        const header = html[0].querySelector(".header-actions.action-buttons");
        const improving = game.settings.get("tde5e", "improving");

        // Create button to distribute adventure points to all players.
        const apDistribution = document.createElement("button");
        apDistribution.innerHTML = "<i class='fas fa-cart-plus'></i>";
        apDistribution.title = game.i18n.localize(`tde5e.actor.distributionDialog.title${improving ? "End" : ""}`);
        if (improving) apDistribution.classList.add("improving");
        apDistribution.addEventListener("click", () => {
            if (improving) {
                game.settings.set("tde5e", "improving", false);
                return;
            }

            const apPrompt = new Dialog({
                title: apDistribution.title,
                content: `<p>${game.i18n.localize("tde5e.actor.distributionDialog.hint")}</p>
                    <p><input type="number" name="ap" value="0"/></p>`,
                buttons: {
                    ok: {
                        icon: "<i class='fas fa-check'></i>",
                        label: game.i18n.localize("tde5e.actor.distributionDialog.button"),
                        callback: content => {
                            const input = content[0].querySelector("input[name='ap']");
                            const ap = input.valueAsNumber;
                            Promise.all(game.actors.filter(actor => actor.hasPlayerOwner).map(actor => actor.update({
                                "system.adventurePoints.total": actor.system.adventurePoints.total + ap,
                                "flags.tde5e.improving": true,
                            }))).then(() => game.settings.set("tde5e", "improving", true));
                        },
                    },
                },
                default: "ok",
            });
            apPrompt.render(true);
        });
        header.append(apDistribution);
    }
}

export class CreationLogEntry extends ImprovementLogEntry {
    /** @inheritdoc */
    get documentUuid() {
        return this.currentValue;
    }

    /**
     * Resolves the item that was created.
     * @returns {TdeItem?} The item or null, if it cannot be accessed.
     */
    get item() {
        const uuid = this.currentValue;
        if (!uuid.startsWith("Compendium.")) return fromUuidSync(uuid);

        const parentUuid = this.parent.parent.uuid;
        if (uuid.startsWith(parentUuid)) {
            return this.parent.parent.items.get(uuid.split(".").at(-1));
        }

        const {
            id, collection, embedded, doc,
        } = foundry.utils.parseUuid(uuid);
        if (embedded.length) return null;

        return doc ?? collection.get(id) ?? collection.index?.get(id);
    }

    /** @inheritdoc */
    get displayName() {
        const { item } = this;
        const name = item?.name ?? this.currentValue;
        const classes = ["content-link", "nobg"];
        if (!item) classes.push("broken");
        return "<i class='fas fa-file-plus'></i>"
            + `<a class="${classes.join(" ")}" data-link data-uuid="${this.currentValue}">${name}</a>`;
    }

    /** @inheritdoc */
    get propertyDisplayName() {
        return "";
    }

    /** @inheritdoc */
    get showFooterValue() {
        return false;
    }

    /** @inheritdoc */
    toDisplayValue(value) {
        return value ? game.i18n.localize("tde5e.actor.changelog.create") : "";
    }

    /** @inheritdoc */
    addToLog(log) {
        // Remove delete entry and set cost to the difference.
        const deleteIndex = log.findIndex(entry => entry.path === "delete" && entry.original === this.currentValue);
        if (deleteIndex > -1) {
            this.updateSource({ cost: this.cost + log[deleteIndex].cost });
            log.splice(deleteIndex, 1);
            if (!this.cost) return; // Cost matches, drop both entries.
        }

        // Merge into existing entry if one exists.
        const entryIndex = log.findIndex(e => e.path === this.path && e.currentValue === this.currentValue);
        if (entryIndex === -1) log.push(this);
        else this.mergeInto(log[entryIndex]);
    }

    /** @inheritdoc */
    isRelevant() {
        return true;
    }

    /** @inheritdoc */
    async undo() {
        // Sheet should render with changelog update.
        return this.item?.delete({ render: false });
    }
}

export class DeletionLogEntry extends ImprovementLogEntry {
    /** @inheritdoc */
    get documentUuid() {
        return this.original;
    }

    /** @inheritdoc */
    get displayName() {
        const index = this.currentValue
            ? lookupCompendiumEntry(this.currentValue)?.entry
            : null;
        if (!index) return `<i class='fas fa-trash'></i>${this.currentValue ?? this.original}`;
        return "<i class='fas fa-trash'></i>"
            + `<a class="content-link nobg" data-uuid="${this.currentValue}">${index.name}</a>`;
    }

    /** @inheritdoc */
    get propertyDisplayName() {
        return "";
    }

    /** @inheritdoc */
    get showFooterValue() {
        return false;
    }

    /** @inheritdoc */
    toDisplayValue(value) {
        return value?.startsWith("Actor.") ? "" : game.i18n.localize("tde5e.actor.changelog.delete");
    }

    /** @inheritdoc */
    addToLog(log) {
        const createIndex = log.findIndex(entry => entry.path === "create" && entry.currentValue === this.original);
        let creationCost = createIndex > -1 ? log[createIndex].cost : 0;

        const itemLog = this.document?.system.changelog;
        delete this.document;
        if (itemLog?.length) creationCost += itemLog.reduce((c, change) => c + change.cost, 0);

        if (createIndex > -1) {
            log.splice(createIndex, 1);
            if (-creationCost === this.cost) return; // Cost matches, drop both entries.
        }

        if (creationCost) this.updateSource({ cost: this.cost + creationCost }); // Set cost to the difference.

        // Merge into existing entry if one exists.
        const entryIndex = log.findIndex(e => e.path === this.path && e.original === this.original);
        if (entryIndex === -1) log.push(this);
        else this.mergeInto(log[entryIndex]);
    }

    /** @inheritdoc */
    isRelevant() {
        return true;
    }

    /** @inheritdoc */
    async undo() {
        const uuid = this.original;
        const sourceItem = this.currentValue ? await fromUuid(this.currentValue) : null;
        if (!sourceItem) {
            error(game.i18n.format("tde5e.actor.changelog.undoError", { name: uuid }));
            return Promise.resolve();
        }

        const createData = game.items.fromCompendium(sourceItem, { clearFolder: true });
        createData._id = uuid.substring(uuid.lastIndexOf(".") + 1);
        return this.parent.parent.createEmbeddedDocuments(sourceItem.documentName, [createData], { keepId: true });
    }
}
