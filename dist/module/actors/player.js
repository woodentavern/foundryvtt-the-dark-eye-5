import TdeActor from "./actor.js";

export default class PlayerActor extends TdeActor {
    /** @override */
    get canUseFate() {
        return !(this.uniqueItems.get("PECH")?.system.variant.level === 3);
    }

    /** @inheritdoc */
    prepareDerivedValues() {
        const actorData = this.system;
        actorData.adventurePoints.remaining = actorData.adventurePoints.total - actorData.adventurePoints.spent;
        super.prepareDerivedValues();
    }

    /**
     * Prepares the weight calculation for the actor's inventory.
     */
    prepareInventory() {
        // Calculate carrying capacity.
        const carryingCapacity = this.system.derived.carryingCapacity.value;

        // Accumulate inventory item weights with container factors.
        const inventoryItems = this.itemTypes[TDE5E.itemTypes.InventoryItem]
            .concat(
                this.itemTypes[TDE5E.itemTypes.Armor],
                this.itemTypes[TDE5E.itemTypes.MeleeWeapon],
                this.itemTypes[TDE5E.itemTypes.RangedWeapon],
                this.itemTypes[TDE5E.itemTypes.Shield],
            );

        this.system.inventoryContainer.forEach(container => { container.totalWeight = 0; });
        for (const item of inventoryItems) {
            if (item.isNatural) continue;
            const container = this.system.inventoryContainer.find(c => c.id === item.system.container);
            if (container) container.totalWeight += item.totalWeight;
        }

        // Store values into model.
        this.system.inventory = {
            overburdenThreshold: (carryingCapacity + 4) * 40,
            totalWeight: this.system.inventoryContainer
                .reduce((weight, container) => (container.applyWeight ? weight + container.totalWeight : weight), 0),
        };
    }

    /** @inheritdoc */
    prepareFinalValues() {
        super.prepareFinalValues();
        this.prepareInventory();
        // Adjust encumbrance with small bonus for rounding errors.
        const { inventory } = this.system;
        const threshold = inventory.overburdenThreshold - 0.1;
        if (inventory.totalWeight > threshold) {
            const levels = Math.ceil((inventory.totalWeight - threshold) / 160);
            this.uniqueItems.get("BELASTUNG")?.modifyValue(game.i18n.localize("tde5e.inventory.weight"), levels);
        }
    }

    /** @override */
    async _preCreate(data, options, user) {
        this.updateSource({
            "prototypeToken.actorLink": true,
            "prototypeToken.disposition": CONST.TOKEN_DISPOSITIONS.FRIENDLY,
        });
        return super._preCreate(data, options, user);
    }

    /** @override */
    async _setupItems() {
        const conditions = await game.packs.get(TDE5E.compendiums.Conditions)
            .getDocuments({ system: { isDefault__in: [true] } });
        const skills = (await game.packs.get(TDE5E.compendiums.Skills).getDocuments())
            .filter(item => item._source.system.isDefault); // No query because almost all of them should be default.
        const combat = (await game.packs.get(TDE5E.compendiums.Combat)
            .getDocuments({ system: { requirementText__in: [""] } }))
            .filter(item => item.system.purchase.cost[0] === 0);
        const states = await game.packs.get(TDE5E.compendiums.States)
            .getDocuments({ system: { isDefault__in: [true] } });
        const equipment = await game.packs.get(TDE5E.compendiums.Equipment)
            .getDocuments({ system: { cid__in: ["WAFFENLOS", "UNGERUSTET"] } });
        const ammo = await game.packs.get(TDE5E.compendiums.Equipment)
            .getDocuments({ type__in: [TDE5E.itemTypes.Ammunition], system: { isSpecial__in: [false] } });
        const abilities = await game.packs.get(TDE5E.compendiums.CommonSA)
            .getDocuments({ system: { cid__in: ["ERSTER", "NEUER_WURF"] } });

        return conditions.concat(skills, combat, states, equipment, ammo, abilities);
    }
}
