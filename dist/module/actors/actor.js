import { PreparationPriority } from "../items/item.js";
import Mutex from "../mutex.js";
import * as util from "../util.js";
import { ActorLogEntry } from "./logEntry.js";

export default class TdeActor extends Actor {
    /**
     * Property for caching the grouped items.
     * @see itemTypes
     * @type {Map.<string, TdeItem[]>?}
     */
    _itemTypes = null;

    /**
     * Flag indicating that cached collections need to be rest on the next preparation.
     * @type {boolean}
     */
    invalidateCache = false;

    /**
     * Mutex for sequencing changelog operations.
     * @type {Mutex}
     */
    changelogMutex = new Mutex();

    constructor(data, context, typed = false) {
        if (!typed) {
            const cls = TDE5E.actorTypeClasses[data.type];
            if (cls) return new cls(data, context, true);
        }
        super(data, context);
    }

    /**
     * Shortcut to access the unmodified system data of the actor.
     * @returns {object} The actor's system data without modifications.
     */
    get baseData() {
        return this._source.system;
    }

    /** @inheritdoc */
    _onCreate(data, options, userId) {
        super._onCreate(data, options, userId);
        if (game.user.id === userId) this._initializeLoadout(data);
    }

    /**
     * Create a new loadout on actor creation.
     * @param {object} data The actor data.
     */
    _initializeLoadout(data) {
        const melee = data.items.find(it => it.system.cid === "SCHLAGWAFFEN");
        const weaponId = data.items.find(it => it.system.cid === "WAFFENLOS")?._id;
        const loadout = {
            id: foundry.utils.randomID(),
            name: `${melee?.name ?? "Fists"} #1`,
            order: 0,
            isSelected: true,
            combatTechnique: melee?._id,
            combatStyle: null,
            mainhand: weaponId,
            offhand: null,
        };

        this.update({ "system.loadouts": [loadout] }, { skipChangelog: true });
    }

    /**
     * Invalidates the cached {@link itemTypes} after creating an item, but before preparing the actor.
     * @param {Document} parent The current parent of the documents.
     * @param {string} collection The collection name of the embedded document.
     * @inheritdoc
     */
    _preCreateDescendantDocuments(parent, collection, data, options, userId) {
        super._preCreateDescendantDocuments(parent, collection, data, options, userId);
        if (collection === "items") this.invalidateCache = true;
    }

    /**
     * Creates a changelog entry when an embedded document is created.
     * @param {Document} parent The current parent of the documents.
     * @param {string} collection The collection name of the embedded document.
     * @param {Document[]} documents The array of documents that were created.
     * @inheritdoc
     */
    _onCreateDescendantDocuments(parent, collection, documents, data, options, userId) {
        super._onCreateDescendantDocuments(parent, collection, documents, data, options, userId);
        if (collection === "items" && userId === game.user.id && !options.skipChangelog) {
            const now = Date.now();
            const logEntries = documents.map(doc => new ActorLogEntry({
                path: "create",
                original: null,
                history: [{ time: now, value: doc.uuid }],
                cost: doc.system.adventurePointValue,
                userCostDelta: 0,
            }));
            this.changelogMutex.wrap(
                () => this.update({ "system.changelog": ActorLogEntry.prepareChangelog(this, logEntries) }),
            );
        }
    }

    /**
     * Invalidates the cached {@link itemTypes} after deleting an item, but before preparing the actor.
     * @param {Document} parent The current parent of the documents.
     * @param {string} collection The collection name of the embedded document.
     * @inheritdoc
     */
    _preDeleteDescendantDocuments(parent, collection, ...args) {
        super._preDeleteDescendantDocuments(parent, collection, ...args);
        if (collection === "items") this.invalidateCache = true;
    }

    /**
     * Creates a changelog entry when an embedded document is removed.
     * @param {Document} parent The current parent of the documents.
     * @param {string} collection The collection name of the embedded document.
     * @param {Document[]} documents The array of documents that were deleted.
     * @inheritdoc
     */
    _onDeleteDescendantDocuments(parent, collection, documents, ids, options, userId) {
        super._onDeleteDescendantDocuments(parent, collection, documents, ids, options, userId);
        if (collection === "items" && userId === game.user.id && !options.skipChangelog) {
            const now = Date.now();
            const logEntries = documents.map(doc => {
                const entry = new ActorLogEntry({
                    path: "delete",
                    original: doc.uuid,
                    history: [{ time: now, value: doc._stats.compendiumSource ?? null }],
                    cost: -doc.system.adventurePointValue,
                    userCostDelta: 0,
                });
                entry.document = doc;
                return entry;
            });
            this.changelogMutex.wrap(
                () => this.update({ "system.changelog": ActorLogEntry.prepareChangelog(this, logEntries) }),
            );
        }
    }

    /**
     * Invalidate cached item lists to ensure later calls will regenerate them.
     */
    _invalidateItemLists() {
        this._itemTypes = this._uniqueItems = null;
        this.invalidateCache = false;
    }

    /**
     * Prepares the data of the given items in the context of this actor.
     * @param {TdeItem[]?} items The items to prepare.
     */
    prepareEmbeddedItems(items) {
        items?.forEach(item => item.prepareActorData());
    }

    /**
     * Called first in the preparation chain.
     * @override
     */
    prepareData() {
        this.isPrepared = true;
        if (this.invalidateCache || this.isToken) this._invalidateItemLists();
        super.prepareData();
    }

    /**
     * Called after embedded documents and base data is prepared.
     * @override
     */
    prepareDerivedData() {
        // Group items by preparation order.
        this.preparingItems = util.groupBy(this.items, "preparationPriority");

        this.prepareEmbeddedItems(this.preparingItems[PreparationPriority.BEFORE_ATTRIBUTES]);
        this.prepareAttributes();

        this.prepareEmbeddedItems(this.preparingItems[PreparationPriority.AFTER_ATTRIBUTES]);
        this.prepareDerivedValues();

        this.prepareEmbeddedItems(this.preparingItems[PreparationPriority.AFTER_DERIVED]);
        this.prepareModifiedValues();

        this.prepareEmbeddedItems(this.preparingItems[PreparationPriority.AFTER_MODIFIED]);
        this.prepareFinalValues();

        this.prepareEmbeddedItems(this.preparingItems[PreparationPriority.AFTER_PREPARATION_EARLY]);
        this.prepareEmbeddedItems(this.preparingItems[PreparationPriority.AFTER_PREPARATION_LATE]);
        delete this.preparingItems;
    }

    /**
     * Prepares attributes of the actor with base and modified values.
     */
    prepareAttributes() {
        Object.entries(this.system.attributes).forEach(entry => {
            const attr = entry[1];
            attr.value = attr.base;
            this.modifyWithUser(attr, "value");
        });
    }

    /**
     * Prepares base values and resources derived from attributes.
     */
    prepareDerivedValues() {
        const actorData = this.system;

        // Resources
        const resourceData = actorData.resources;
        resourceData.lifePoints.base = actorData.attributes.constitution.base * 2;
        this.modifyWithBought(resourceData.lifePoints);
        resourceData.adrenaline.base = actorData.attributes.sagacity.base + actorData.attributes.constitution.base;
        this.modifyWithBought(resourceData.adrenaline);
        if (this.canUseFate) resourceData.fatePoints.base = 3;
        if (this.canUseMagic) {
            resourceData.arcaneEnergy.base = 20;
            this.modifyWithBought(resourceData.arcaneEnergy);
        }
        if (this.canUseKarma) {
            resourceData.karmaPoints.base = 20;
            this.modifyWithBought(resourceData.karmaPoints);
        }

        // Derived stats
        const derivedData = actorData.derived;
        derivedData.spirit.base = Math.round((actorData.attributes.courage.base
            + actorData.attributes.sagacity.base
            + actorData.attributes.intuition.base) / 6);
        derivedData.toughness.base = Math.round((actorData.attributes.constitution.base * 2
            + actorData.attributes.strength.base) / 6);
        derivedData.physicalInitiative.base = Math.round((actorData.attributes.constitution.base
            + actorData.attributes.agility.base) / 2);
        derivedData.mentalInitiative.base = Math.round((actorData.attributes.sagacity.base
            + actorData.attributes.intuition.base) / 2);
        derivedData.carryingCapacity.base = actorData.attributes.strength.base * 2;
        derivedData.movement.base ??= 8;
    }

    /**
     * Prepares values derived from other derived values or resources.
     */
    prepareModifiedValues() {
        const actorData = this.system;

        const resourceData = actorData.resources;
        const resources = [resourceData.lifePoints, resourceData.adrenaline];
        if (this.canUseFate) resources.push(resourceData.fatePoints);
        if (this.canUseMagic) resources.push(resourceData.arcaneEnergy);
        if (this.canUseKarma) resources.push(resourceData.karmaPoints);
        resources.forEach(resource => {
            resource.max = resource.base;
            this.modifyWithUser(resource, "max");
        });

        const derivedData = actorData.derived;
        [derivedData.spirit, derivedData.toughness, derivedData.physicalInitiative, derivedData.mentalInitiative,
            derivedData.movement, derivedData.carryingCapacity, derivedData.woundThreshold]
            .forEach(derived => {
                derived.value = derived.base;
                this.modifyWithUser(derived, "value");
            });
    }

    /**
     * Prepares values derived from modified values.
     */
    prepareFinalValues() {
        const thresholds = this.creatureType?.system.ignoresPainFromLowHealth
            ? [0, 0]
            : [
                Math.round(this.system.resources.lifePoints.max * 0.5),
                Math.min(this.system.resources.lifePoints.max, 5),
            ];
        this.system.derived.painThresholds = thresholds;

        // Adjust pain based on life points.
        const lifePoints = this.system.resources.lifePoints.value;
        if (lifePoints < thresholds[0]) {
            const value = lifePoints < thresholds[1] ? 4 : 1;
            this.uniqueItems.get("SCHMERZ")
                ?.modifyValue(game.i18n.localize("tde5e.actor.derived.painThreshold"), value);
        }
    }

    /**
     * Applies a modifier to the given property of the given object and adds a
     *  descriptive entry in the parent's modifier list.
     * @param {string} name The name of the modifier.
     * @param {object} parent The parent of the value to modify.
     * @param {string} propertyName The name of the property within the parent to modify.
     * @param {number} value The value to add to the property. This is not required to be a number,
     *  but must support the + operator.
     */
    modify(name, parent, propertyName, value) {
        if (!value) return;
        parent.modifiers ??= [];
        parent[propertyName] += value;
        parent.modifiers.push([name, value]);
    }

    /**
     * Applies a user defined modifier.
     * @see modify
     * @param {object} parent The parent of the value to modify.
     * @param {string} propertyName The name of the property within the parent to modify.
     */
    modifyWithUser(parent, propertyName) {
        if (!parent.mod) return;
        this.modify(game.i18n.localize("tde5e.general.user"), parent, propertyName, parent.mod);
    }

    /**
     * Applies a modifier for bought resources.
     * @see modify
     * @param {object} parent The parent of the value to modify.
     */
    modifyWithBought(parent) {
        if (!parent.bought) return;
        this.modify(game.i18n.localize("tde5e.values.bought"), parent, "base", parent.bought);
    }

    /**
     * Applies the item's level as modifier for the given property.
     * @param {TdeItem} item The item to modify the property with.
     * @param {object} parent The parent of the value to modify.
     * @param {string} propertyName The name of the property within the parent to modify.
     * @param {boolean=} invert Indicates whether the value should be subtracted instead of added.
     *  Defaults to false.
     */
    modifyWithItem(item, parent, propertyName, invert = false) {
        let value = item.system.variant.level;
        if (invert) value *= -1;
        this.modify(item.displayName, parent, propertyName, value);
    }

    /**
     * Stores a modifier that is applied at the specified time.
     * @param {string} name The name of the modifier.
     * @param {object} parent The parent of the value to modify.
     * @param {string} propertyName The name of the property within the parent to modify.
     * @param {number} value The value to add to the property.
     * @param {number=} priority The @see PreparationPriority when the modifier should be applied. This
     *  should be later than the preparation priority of the calling item.
     *  Defaults to @see PreparationPriority.AFTER_PREPARATION_LATE
     */
    modifyLater(name, parent, propertyName, value, priority = PreparationPriority.AFTER_PREPARATION_LATE) {
        this.preparingItems[priority].push({
            prepareActorData: () => this.modify(name, parent, propertyName, value),
        });
    }

    // ***********************************************************
    //                      Getters & Setters
    // ***********************************************************

    /**
     * Caching wrapper for the list of items grouped by their type.
     * @returns {Map.<string, TdeItem[]>} An item list grouped by their type.
     * @override
     */
    get itemTypes() {
        if (!this._itemTypes) this._itemTypes = super.itemTypes;
        return this._itemTypes;
    }

    /**
     * Retrieves all items of the given type from the actor.
     * @param {string} type The type of the items to fetch.
     * @param {string?} cid An optional CID to filter the items with.
     * @returns {TdeItem[]} An array of items.
     */
    getItems(type, cid) {
        let items = this.itemTypes[type] ?? [];
        if (cid) items = items.filter(i => i.system.cid === cid || (i.system.cid?.startsWith(`${cid}:`) ?? false));
        return items;
    }

    /**
     * Caching wrapper for items mapped by their compendium ID. If a variant is
     *  present, it is appended to the key so the format is "itemId:variantId".
     * @returns {Map.<string, TdeItem[]>} An item list mapped by their CID.
     */
    get uniqueItems() {
        if (!this._uniqueItems && this.isPrepared) {
            this._uniqueItems = new Map(this.items.map(item => [item.system.cid, item]));
        }
        return this._uniqueItems;
    }

    /**
     * Determines whether the actor can use fate points.
     * @returns {boolean} True if the actor has fate points, false otherwise.
     */
    get canUseFate() {
        return !!this.system.settings.hasFatePoints;
    }

    /**
     * Determines whether the actor can use arcaneEnergy base abilities and should have the corresponding resource.
     * @returns {boolean} True, if the user can use magic, false otherwiese.
     */
    get canUseMagic() {
        return this.uniqueItems.has("ZAUBERER");
    }

    /**
     * Determines whether the actor can use karma base abilities and should have the corresponding resource.
     * @returns {boolean} True, if the user can use magic, false otherwiese.
     */
    get canUseKarma() {
        return this.uniqueItems.has("GEWEIHTER");
    }

    /**
     * Gets the one and only culture the actor can have.
     * Returns null if no culture is found.
     * @returns {TdeItem} The current culture.
     */
    get culture() {
        const culture = this.itemTypes[TDE5E.itemTypes.Culture];
        if (!culture?.length) return null;
        if (culture.length > 1) util.warn(`More than one culture present in ${this.name}`);
        return culture[0];
    }

    /**
     * Gets the one and only race the actor can have.
     * Returns null if no race is found.
     * @returns {TdeItem} The current race.
     */
    get race() {
        const race = this.itemTypes[TDE5E.itemTypes.Race];
        if (!race?.length) return null;
        if (race.length > 1) util.warn(`More than one race present in ${this.name}`);
        return race[0];
    }

    /**
     * Gets all magical traditions that are owned by the actor.
     * @returns {TdeItem[]} A list of magical traditons.
     */
    get magicalTraditions() {
        return this.getItems(TDE5E.itemTypes.Tradition).filter(tradition => tradition.system.group === "magical");
    }

    /**
     * Gets all blessed traditions that are owned by the actor.
     * @returns {TdeItem[]} A list of blessed traditons.
     */
    get blessedTraditions() {
        return this.getItems(TDE5E.itemTypes.Tradition).filter(tradition => tradition.system.group === "blessed");
    }

    /**
     * Retreives the currently selected armor (if any).
     * @returns {Armor?} The active armor of the actor.
     */
    get activeArmor() {
        return this.getItems(TDE5E.itemTypes.Armor).find(armor => armor.system.isSelected);
    }

    /**
     * Retreives the selected loadout of this actor's first combatant.
     * @returns {Loadout?} The actor's active loadout or null when out of combat.
     */
    get activeLoadout() {
        return game.combat?.combatants.find(c => c.actor === this)?.activeLoadout;
    }

    /**
     * Safely checks if the given actor is in combat, even if the combat tracker is
     *  not initialized yet.
     * @returns {boolean} True if the actor is in combat, false otherwise.
     */
    get inCombat() {
        if (game.combats) return game.combat?.round > 0 && game.combat.combatants.some(c => c.actor === this);

        // Combat is not initialized, so we need to fall back to game data.
        const activeCombat = game.data.combats.find(c => c.active);
        return activeCombat && activeCombat.round > 0; // We don't check combatant here because tokens aren't ready.
    }

    /**
     * Gets the size category for this actor according to the translation 'tde5e.creatureSizes'.
     * @returns {string} The size category for this actor.
     */
    get sizeCategory() {
        return "medium";
    }

    // ***********************************************************
    //                    Creation functions
    // ***********************************************************

    /**
     * Creation method that is called before the actor is created. This method may be used to override data that will
     *  be merged during the creation.
     * @param {object} data A data object that can be used to override values on creation.
     * @param {object} options The options of the actor creation.
     * @returns {object} The final data that the actor will be created with.
     * @override
     */
    async _preCreate(data, options, user) {
        if (data.system?.changelog?.length) this.updateSource({ "system.changelog": [] });
        if (data.items?.length) {
            const cleanedItems = data.items.map(item => {
                item.system.changelog = [];
                return item;
            });
            this.updateSource({ items: cleanedItems });
            return super._preCreate(data, options, user); // Actor is actually a copy.
        }

        // Construct default items.
        const items = (await this._setupItems())
            .map(item => this._preprocessItem(item.collection.fromCompendium(item)));
        this.updateSource({ items });
        return super._preCreate(data, options, user);
    }

    /**
     * Preprocesses items before the actor is fully created.
     * @param {object} data The object data of a compendium item.
     * @returns {object} The original data, potentially modified.
     */
    _preprocessItem(data) {
        // TODO: Remove after moving selection to actor
        if (data.system.cid === "UNGERUSTET") {
            data.system.isSelected = true;
        }
        return data;
    }

    /**
     * Fetches compendium documents used to create the default items.
     * @returns {Promise.<Document[]>} A promise representing an array of items that should be created.
     * @abstract
     */
    async _setupItems() { return Promise.resolve(); }

    /**
     * Extends FoundryVTT's method to react to resource changes.
     * @override
     */
    async _preUpdate(changes, options, user) {
        const flatChanges = foundry.utils.flattenObject(changes);

        if (!options.skipChangelog) ActorLogEntry.prepareUpdate(this, changes, flatChanges);

        // Determine if the actor was incapacitated.
        let incapacitate = false;
        const lifePoints = flatChanges["system.resources.lifePoints.value"];
        if (util.hasValue(lifePoints)) {
            const previousValue = this.system.resources.lifePoints.value;
            incapacitate = lifePoints <= 0 && previousValue > 0;
        }

        // Validate loadout updates.
        for (const loadoutUpdate of flatChanges["system.loadouts"] ?? []) {
            const loadout = this.system.loadouts.find(l => l.id === loadoutUpdate.id);
            if (loadout) loadout.resetWithUpdate(loadoutUpdate);
        }

        await super._preUpdate(changes, options, user);

        // Apply incapacitated after the update completes (because it's an embedded item update).
        if (incapacitate) await this.uniqueItems.get("HANDLUNGSUNFAHIG").setActive(true);
    }

    /**
     * Calculates the amount of adventure points needed to improve the property with the given path from a source to a
     * target value. This may also be negative if the new target is lower than the source value.
     * @param {string} property The property to determine the improvement costs for.
     * @param {number} oldValue The previous value of the property.
     * @param {number} newValue The new value of the property.
     * @returns {number} The AP required to improve the property.
     */
    _calculateApCost(property, oldValue, newValue) {
        if (oldValue === newValue) return 0;

        const column = this.getPropertyMetadata(property).improvement ?? false;
        if (typeof (column) !== "string") return 0;

        const reverse = newValue < oldValue;
        if (reverse) {
            const tmp = oldValue;
            oldValue = newValue;
            newValue = tmp;
        }

        const cost = TDE5E.improvementTable[column.toUpperCase()]
            .slice(oldValue + 1, newValue + 1) // Ignore activation cost
            .reduce((sum, level) => sum + level, 0);
        return reverse ? -cost : cost;
    }

    /**
     * Migrates the actor to the current system version.
     * @param {object} data The raw database object before any validation.
     * @returns {object} The update data to apply.
     */
    // eslint-disable-next-line no-unused-vars -- Keep for documentation purposes.
    migrate(data) {
        return {};
    }

    /**
     * Extends FoundryVTT's method to refresh the actor's tokens when the
     *  conditions were modified by the last preparation.
     * @override
     */
    _onUpdate(data, options, userId) {
        super._onUpdate(data, options, userId);
        this.refreshConditions();
    }

    /**
     * Redraws the actor's tokens and associated combatants if any conditions were changed.
     */
    refreshConditions() {
        if (!this._conditionsChanged) return;
        this._conditionsChanged = false;

        // Redraw token effects.
        let combatChanged = false;
        this.getActiveTokens().forEach(token => {
            token.drawEffects();
            if (token.inCombat) combatChanged = true;
            if (token.hasActiveHUD) canvas.tokens.hud.refreshStatusIcons();
        });

        if (combatChanged) ui.combat.render();
    }

    /**
     * Checks if the given status effect should be available for the token.
     * @param {object} effect The status effect data.
     * @returns {boolean} True if the effect should be selectable, false otherwise.
     */
    canHaveStatus(effect) {
        if (!effect) return false;
        if (foundry.utils.getProperty(effect, "flags.tde5e.isDefault")) return true;

        const type = foundry.utils.getProperty(effect, "flags.tde5e.type");
        if (!type) return false;

        return !!this.itemTypes[type]?.find(item => item.cid === effect.id);
    }

    /**
     * Prevent toggling conditions and update their value instead.
     * @inheritdoc
     */
    toggleStatusEffect(statusId, { active, overlay = false } = {}) {
        if (overlay) return super.toggleStatusEffect(statusId, { active, overlay });

        const status = CONFIG.statusEffects.find(e => e.id === statusId);
        if (foundry.utils.getProperty(status, "flags.tde5e.type") === "condition") {
            const condition = this.uniqueItems.get(statusId);
            if (!condition) {
                util.error(`Unable to toggle missing condition ${statusId}.`);
            } else {
                active ??= condition._source.system.value === 0;
                let value = 0;
                if (active) {
                    const api = game.modules.get("statuscounter")?.api;
                    const data = api?.unqueueCreation(condition, statusId);
                    if (data) value = data.value;
                }
                return condition.update({ "system.value": value });
            }
        }

        return super.toggleStatusEffect(statusId, { active, overlay });
    }

    // ***********************************************************
    //           Helper functions for easy access of data
    // ***********************************************************

    /**
     * Gets an array of inventory related items.
     * This excludes items that are "natural", i.e. are inherrant to the owner and cannot be transfered or
     *  or removed.
     * @param {object} itemTypes A map that holds all relevant itemType names and a corresponding list with items.
     * @returns {TdeItem[]} An array with all items (in enriched format) that should be displayed in the inventory.
     */
    static getInventoryRelatedItems(itemTypes) {
        const inventoryItems = Object.entries(TDE5E.inventoryRelatedItemTypes)
            .map(([type, hasSubtypes]) => {
                if (hasSubtypes) return Object.values(itemTypes[type]);
                return itemTypes[type];
            })
            .flat(2)
            .filter(item => !item.isNatural);

        return inventoryItems;
    }

    /**
     * Attempts to find a local copy of the compendium item with the given UUID.
     * @param {string} uuid The compendium UUID of the item.
     * @returns {TdeItem?} The item if a local copy exists, null otherwise.
     */
    getLocalItem(uuid) {
        const index = util.lookupCompendiumEntry(uuid).entry;
        if (!index) return null;

        let localItem = this.getItems(index.type).find(item => item._stats.compendiumSource === uuid);
        if (!localItem && index.system?.cid) [localItem] = this.getItems(index.type, index.system.cid);
        return localItem;
    }

    /**
     * Retrieves metadata information for the property with the given path.
     * @param {string} path The path of the property.
     * @returns {object?} An object containing metadata information or an empty object if the property doesn't exist.
     */
    getPropertyMetadata(path) {
        let systemMetadata;
        if (path.startsWith("system.")) systemMetadata = this.system.schema.getField(path.substring(7));
        else if (path.startsWith("flags.")) systemMetadata = TDE5E.flagMetadata[path];
        else systemMetadata = this.schema.getField(path);
        if (!systemMetadata) return { label: path };
        return {
            label: systemMetadata.label ? game.i18n.localize(systemMetadata.label) : path,
            hint: systemMetadata.hint ? game.i18n.localize(systemMetadata.hint) : "",
            improvement: systemMetadata.improvement,
        };
    }

    /**
     * Determines if a log entry should be created for a property change.
     * @param {string} path The path of the changed property.
     * @param {*} value The old value of the changed property.
     * @returns {boolean} True if the change should have a log entry, false otherwise.
     */
    trackProperty(path, value) {
        if (value === undefined) return false;

        let metadata;
        if (path.startsWith("system.")) metadata = this.system.schema.getField(path.substring(7));
        else if (path.startsWith("flags.tde5e.")) metadata = TDE5E.flagMetadata[path];

        return metadata ? !metadata.skipChangelog : false;
    }
}
