import TdeActorModel from "./abstract/actorModel.js";

/**
 * Data model for actors with the NPC type.
 */
export default class NpcData extends TdeActorModel {
    /** @inheritdoc */
    static defineSchema() {
        const { fields } = foundry.data;

        const actorModel = super.defineSchema();
        actorModel.settings.fields.hasFatePoints = new fields.BooleanField();
        actorModel.settings.fields.useSimpleLoadout = new fields.BooleanField();
        actorModel.settings.fields.hasLoot = new fields.BooleanField();

        actorModel.inventoryContainer.initial.push({
            id: "LOOT",
            name: game.i18n.localize("tde5e.inventory.containerType.loot"),
            type: "loot",
        });

        return Object.assign(actorModel, {
            bio: new fields.SchemaField({
                socialStatus: new fields.StringField({
                    choices: ["notFree", "free", "lesserNoble", "noble", "aristocracy"],
                    initial: "free",
                }),
            }),
        });
    }
}
