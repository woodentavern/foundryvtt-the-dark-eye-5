import { FieldImprovement } from "../../../items/model/partial/fieldImprovement.js";
import { ActorLogEntry } from "../../logEntry.js";
import AttributeField from "../partial/attributeField.js";
import ContainerField from "../partial/containerField.js";
import LoadoutModel from "../partial/loadoutModel.js";
import ModSchemaField from "../partial/modSchemaField.js";
import ResourceField from "../partial/resourceField.js";

/**
 * Common data model for system actors.
 */
export default class TdeActorModel extends foundry.abstract.DataModel {
    /** @inheritdoc */
    static defineSchema() {
        const { fields } = foundry.data;

        const fatePoints = new ResourceField("fatePoints", 3);
        delete fatePoints.fields.bought;

        const loadouts = new fields.ArrayField(new fields.EmbeddedDataField(LoadoutModel));
        loadouts.skipChangelog = true;

        const inventoryContainer = new fields.ArrayField(
            new ContainerField(),
            { initial: [{ id: "DEFAULT", name: game.i18n.localize("tde5e.inventory.personalContainer") }] },
        );
        inventoryContainer.skipChangelog = true;

        const changelog = new fields.ArrayField(new fields.EmbeddedDataField(ActorLogEntry));
        changelog.skipChangelog = true;
        changelog.noMigration = true;

        return {
            resources: new fields.SchemaField({
                lifePoints: new ResourceField("lifePoints", 16, "D"),
                adrenaline: new ResourceField("adrenaline", 16, "B"),
                arcaneEnergy: new ResourceField("arcaneEnergy", 20, "D"),
                karmaPoints: new ResourceField("karmaPoints", 20, "D"),
                fatePoints,
            }),
            attributes: new fields.SchemaField({
                courage: new AttributeField("courage"),
                sagacity: new AttributeField("sagacity"),
                intuition: new AttributeField("intuition"),
                charisma: new AttributeField("charisma"),
                dexterity: new AttributeField("dexterity"),
                agility: new AttributeField("agility"),
                constitution: new AttributeField("constitution"),
                strength: new AttributeField("strength"),
            }),
            derived: new fields.SchemaField({
                physicalInitiative: new ModSchemaField(),
                mentalInitiative: new ModSchemaField(),
                spirit: new ModSchemaField(),
                toughness: new ModSchemaField(),
                movement: new ModSchemaField(),
                carryingCapacity: new ModSchemaField(),
                woundThreshold: new AttributeField(null, 10),
            }),
            settings: new fields.SchemaField({
                combatantCount: new fields.NumberField({
                    integer: true,
                    positive: true,
                    initial: 1,
                    nullable: false,
                }),
                bonusActionPoints: new fields.NumberField({
                    integer: true,
                    initial: 1,
                    nullable: false,
                }),
            }),
            loadouts,
            inventoryContainer,
            notes: new fields.HTMLField({ label: "tde5e.actor.bio.notes" }),
            gmNotes: new fields.StringField({ label: "tde5e.actor.bio.gmNotes" }),
            changelog,
        };
    }

    /**
     * Creates lists of bar and value attributes that may be used to render token resources.
     * @returns {object} The attributes that can be used by tokens.
     */
    static getTokenAttributes() {
        return {
            bar: [
                "resources.lifePoints", "resources.adrenaline", "resources.arcaneEnergy", "resources.karmaPoints",
                "resources.fatePoints",
            ],
            value: [
                "attributes.courage.mod", "attributes.sagacity.mod", "attributes.intuition.mod",
                "attributes.charisma.mod", "attributes.dexterity.mod", "attributes.agility.mod",
                "attributes.constitution.mod", "attributes.strength.mod",
                "derived.physicalInitiative.mod", "derived.mentalInitiative.mod", "derived.spirit.mod",
                "derived.toughness.mod", "derived.movement.mod", "derived.carryingCapacity.mod",
                "derived.woundThreshold.mod",
            ],
        };
    }

    /** @inheritdoc */
    static migrateData(source) {
        if (source.hasOwnProperty("inventoryContainer") && !Array.isArray(source.inventoryContainer)) {
            source.inventoryContainer = Object.entries(source.inventoryContainer).map(([id, container]) => {
                container.id = id;
                return container;
            });
        }

        if (source.changelog?.length) {
            const filteredCost = [0, 0, 0];
            source.changelog = source.changelog.filter(entry => {
                if (entry.operation) return false;
                if (!entry.uuid) return true;
                if (!entry.cost && !entry.userCostDelta) return false;

                filteredCost[0]++;
                filteredCost[1] += entry.cost;
                filteredCost[2] += entry.userCostDelta;
                return false;
            });

            if (filteredCost[0]) {
                source.changelog.push({
                    path: "system.changelog",
                    original: source.changelog.length + filteredCost[0],
                    history: [{ time: Date.now(), value: source.changelog.length }],
                    cost: filteredCost[1],
                    userCostDelta: filteredCost[2],
                });
            }
        }

        if (source.hasOwnProperty("currency") && typeof (source.currency) === "object") {
            source.currency = source.currency.ducats * 1000
                + source.currency.silverthalers * 100
                + source.currency.halers * 10
                + source.currency.kreutzers;
        }

        if (source.hasOwnProperty("inventoryContainer")) {
            for (const container of source.inventoryContainer) {
                if (container.currency && typeof (container.currency) === "object") {
                    container.currency = (container.currency.ducats ?? 0) * 1000
                        + (container.currency.silverthalers ?? 0) * 100
                        + (container.currency.halers ?? 0) * 10
                        + (container.currency.kreutzers ?? 0);
                }

                if (container.hasOwnProperty("isLoot") && !container.hasOwnProperty("type")) {
                    container.type = container.isLoot ? "loot" : "default";
                    delete container.isLoot;
                }

                if (container.hasOwnProperty("weightFactor") && !container.hasOwnProperty("applyWeight")) {
                    container.applyWeight = container.weightFactor > 0;
                }
            }
        }

        return super.migrateData(source);
    }

    /**
     * Retrieves an instance for calculating improvement related information for the given property.
     * @param {string} path The path of the property to improve.
     * @returns {FieldImprovement?} An improvement instance for the property or null if it isn't improvable.
     */
    getImprovement(path) {
        if (!path.startsWith("system.")) return null;
        return FieldImprovement.create(this, path.substring(7));
    }
}
