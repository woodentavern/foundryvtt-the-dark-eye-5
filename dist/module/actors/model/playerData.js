import TdeActorModel from "./abstract/actorModel.js";

/**
 * Data model for actors with the player type.
 */
export default class PlayerData extends TdeActorModel {
    /** @inheritdoc */
    static defineSchema() {
        const { fields } = foundry.data;
        return Object.assign(super.defineSchema(), {
            bio: new fields.SchemaField({
                family: new fields.StringField(),
                sex: new fields.StringField(),
                profession: new fields.StringField(),
                age: new fields.NumberField({
                    integer: true,
                    positive: true,
                    initial: 18,
                    nullable: false,
                }),
                hairColor: new fields.StringField(),
                eyeColor: new fields.StringField(),
                height: new fields.NumberField({
                    integer: true,
                    positive: true,
                    initial: 170,
                    nullable: false,
                }),
                weight: new fields.NumberField({
                    integer: true,
                    positive: true,
                    initial: 50,
                    nullable: false,
                }),
                socialStatus: new fields.StringField({
                    choices: ["notFree", "free", "lesserNoble", "noble", "aristocracy"],
                    initial: "free",
                }),
                placeOfBirth: new fields.StringField(),
                dateOfBirth: new fields.StringField(),
                title: new fields.StringField(),
                characteristics: new fields.StringField(),
            }),
            adventurePoints: new fields.SchemaField({
                total: new fields.NumberField({
                    label: "tde5e.actor.adventurePoints.name",
                    integer: true,
                    min: 0,
                    initial: 1000,
                    nullable: false,
                }),
                spent: new fields.NumberField({
                    label: "tde5e.actor.adventurePoints.spentLong",
                    integer: true,
                    min: 0,
                    initial: 0,
                    nullable: false,
                }),
            }),
            currency: new fields.NumberField({ integer: true, initial: 75000, nullable: false }),
        });
    }

    /** @inheritdoc */
    static getTokenAttributes() {
        const attributes = super.getTokenAttributes();
        attributes.value.push("adventurePoints.total", "adventurePoints.spent", "currency");
        return attributes;
    }

    /** @inheritdoc */
    static migrateData(source) {
        if ((source.bio?.age ?? 1) <= 0) source.bio.age = 18;
        if ((source.bio?.height ?? 1) <= 0) source.bio.height = 170;
        if ((source.bio?.weight ?? 1) <= 0) source.bio.weight = 50;
        return super.migrateData(source);
    }
}
