import TdeActorModel from "./abstract/actorModel.js";

/**
 * Data model for actors with the creature type.
 */
export default class CreatureData extends TdeActorModel {
    /** @inheritdoc */
    static defineSchema() {
        const { fields } = foundry.data;

        const actorModel = super.defineSchema();
        actorModel.settings.fields.hasFatePoints = new fields.BooleanField();
        actorModel.settings.fields.useSimpleLoadout = new fields.BooleanField({ initial: true });
        actorModel.settings.fields.hasLoot = new fields.BooleanField();
        actorModel.settings.fields.hasFacts = new fields.BooleanField({ initial: true });
        actorModel.settings.fields.hasSpecialRules = new fields.BooleanField({ initial: true });

        actorModel.inventoryContainer.initial.push({
            id: "LOOT",
            name: game.i18n.localize("tde5e.inventory.containerType.loot"),
            type: "loot",
        });

        return Object.assign(actorModel, {
            specialRules: new fields.ArrayField(new fields.SchemaField({
                name: new fields.StringField(),
                description: new fields.StringField(),
                isVisible: new fields.BooleanField(),
            })),
            groupSize: new fields.StringField(),
            sizeCategory: new fields.StringField({
                choices: ["huge", "large", "medium", "small", "tiny"],
                initial: "medium",
            }),
            creatureFacts: new fields.SchemaField({
                check: new fields.StringField(),
                facts: new fields.ArrayField(new fields.SchemaField({
                    quality: new fields.StringField(),
                    description: new fields.StringField(),
                    isVisible: new fields.BooleanField(),
                })),
            }),
            behavior: new fields.SchemaField({
                fight: new fields.StringField(),
                flight: new fields.StringField(),
            }),

            size: new fields.SchemaField({
                min: new fields.NumberField({
                    integer: false,
                    min: 0,
                    initial: 1,
                    nullable: false,
                }),
                max: new fields.NumberField({
                    integer: false,
                    min: 0,
                    initial: 10,
                    nullable: false,
                }),
            }),
            weight: new fields.SchemaField({
                min: new fields.NumberField({
                    integer: false,
                    min: 0,
                    initial: 1,
                    nullable: false,
                }),
                max: new fields.NumberField({
                    integer: false,
                    min: 0,
                    initial: 10,
                    nullable: false,
                }),
            }),
        });
    }

    /** @inheritdoc */
    static migrateData(source) {
        if (source.hasOwnProperty("specialRules") && !Array.isArray(source.specialRules)) {
            source.specialRules = Object.values(source.specialRules);
        }

        if (source.creatureFacts?.hasOwnProperty("facts") && !Array.isArray(source.creatureFacts.facts)) {
            source.creatureFacts.facts = Object.values(source.creatureFacts.facts);
        }

        return super.migrateData(source);
    }
}
