import { findParentActor } from "../../../util.js";

/**
 * Data field representing a reference to another item within the same actor.
 */
export default class ItemIdField extends foundry.data.fields.DocumentIdField {
    /** @inheritdoc */
    static get _defaults() {
        return foundry.utils.mergeObject(super._defaults, { blank: false, readonly: false, required: false });
    }

    /** @inheritdoc */
    initialize(value, model) {
        return value ? () => findParentActor(model).items.get(value) : null;
    }
}
