/**
 * Data field representing a resource.
 */
export default class ResourceField extends foundry.data.fields.SchemaField {
    /**
     * Creates a field for a resource.
     * @param {string} name The name of the resource.
     * @param {number=} value The initial value of the resource. Defaults to 0.
     * @param {string?} improvementColumn An optional column for improving the resource.
     * @param {object?} additionalFields Optional fields to merge with the predefined ones.
     */
    constructor(name, value = 0, improvementColumn = null, additionalFields = {}) {
        const { fields } = foundry.data;

        additionalFields.value = new fields.NumberField({
            label: `tde5e.actor.resources.${name}`,
            integer: true,
            initial: value,
            nullable: false,
        });

        const bought = new fields.NumberField({
            label: `tde5e.actor.resources.${name}Bought`,
            integer: true,
            min: 0,
            initial: 0,
            nullable: false,
        });
        if (improvementColumn) bought.improvement = { column: improvementColumn };
        additionalFields.bought = bought;

        additionalFields.mod = new fields.NumberField({ integer: true, initial: 0, nullable: false });

        super(additionalFields);
    }
}
