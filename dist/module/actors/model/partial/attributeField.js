import ModSchemaField from "./modSchemaField.js";

/**
 * Data field to represent an attribute value and its modifiers.
 */
export default class AttributeField extends ModSchemaField {
    /**
     * Creates a new field for an improvable attribute.
     * @param {string} name The name of the attribute represented by this field.
     * @param {object?} additionalFields Optional fields to merge with the predefined ones.
     * @param {number?} initial Default: 8. The initial value of the attribute.
     */
    constructor(name, initial = 8, additionalFields = {}) {
        additionalFields.base = new foundry.data.fields.NumberField({
            label: `tde5e.actor.attributes.${name}`,
            integer: true,
            positive: true,
            initial,
        });
        additionalFields.base.improvement = { type: "attribute", column: "E" };
        super(additionalFields);
    }
}
