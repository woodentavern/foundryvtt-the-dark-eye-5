/**
 * Data field representing a modifiable value.
 */
export default class ModSchemaField extends foundry.data.fields.SchemaField {
    /**
     * Creates a new field for a schema with a modifier.
     * @param {object?} additionalFields Optional fields to merge with the predefined ones.
     */
    constructor(additionalFields = {}) {
        additionalFields.mod = new foundry.data.fields.NumberField({ integer: true, initial: 0, nullable: false });
        super(additionalFields);
    }
}
