/**
 * Data field to represent an inventory container.
 */
export default class ContainerField extends foundry.data.fields.SchemaField {
    /**
     * Creates a new field for an inventory container.
     * @param {object?} additionalFields Optional fields to merge with the predefined ones.
     */
    constructor(additionalFields = {}) {
        const { fields } = foundry.data;

        additionalFields.id = new fields.StringField({ required: true, blank: false });
        additionalFields.name = new fields.StringField();
        additionalFields.sortingField = new fields.StringField({
            choices: [
                "buyPrice",
                "name",
                "price",
                "quantity",
                "sellPrice",
                "totalWeight",
                "type",
            ],
            initial: "name",
            blank: false,
        });
        additionalFields.sortAscending = new fields.BooleanField({ initial: true, required: false });
        additionalFields.applyWeight = new fields.BooleanField({ initial: true, required: false });
        additionalFields.icon = new fields.StringField({ nullable: true, initial: null });
        additionalFields.type = new fields.StringField({ choices: ["default", "loot", "trade"], initial: "default" });
        additionalFields.currency = new fields.NumberField({ integer: true });
        additionalFields.currencyRequirement = new fields.StringField();

        super(additionalFields);
    }
}
