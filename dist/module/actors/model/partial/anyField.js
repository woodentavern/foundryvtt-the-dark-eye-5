/**
 * Data field that can contain arbitrary data.
 */
export default class AnyField extends foundry.data.fields.DataField {
    /** @inheritdoc */
    static get _defaults() {
        return foundry.utils.mergeObject(super._defaults, { nullable: true });
    }

    /** @inheritdoc */
    _cast(value) {
        return value;
    }
}
