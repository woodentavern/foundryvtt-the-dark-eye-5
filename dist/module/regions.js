import { hasValue } from "./util.js";

class ChangeElevationRegionBehaviorType extends foundry.data.regionBehaviors.RegionBehaviorType {
    static LOCALIZATION_PREFIXES = ["TYPES.RegionBehavior.changeElevation", "BEHAVIOR.TYPES.base"];

    /** @inheritdoc */
    static defineSchema() {
        return {
            events: this._createEventsField(),
            setTo: new foundry.data.fields.BooleanField({
                initial: false,
                label: "tde5e.regionTypes.changeElevation.setToLabel",
                hint: "tde5e.regionTypes.changeElevation.setToHint",
            }),
            elevationValue: new foundry.data.fields.NumberField({
                integer: true,
                initial: 1,
                nullable: false,
                label: "tde5e.regionTypes.changeElevation.elevationValueLabel",
                hint: "tde5e.regionTypes.changeElevation.elevationValueHint",
            }),
        };
    }

    /** @inheritdoc */
    async _handleRegionEvent(event) {
        let elevation = foundry.utils.getProperty(event, "data.token.elevation");
        if (!hasValue(elevation)) return;
        elevation = (this.setTo)
            ? this.elevationValue
            : elevation + this.elevationValue;
        await event.data.token.update({ elevation });
    }
}

/**
 *  Register all RegionBehaviorTypes.
 */
export default function registerRegions() {
    CONFIG.RegionBehavior.dataModels.changeElevation = ChangeElevationRegionBehaviorType;
    CONFIG.RegionBehavior.typeIcons.changeElevation = "fa-solid fa-arrow-down-up-across-line";
}
