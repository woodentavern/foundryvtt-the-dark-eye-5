import { migrateActor, migrateCompendium, migrateItem } from "./migration.js";
import { loadStatusEffects } from "./tokens/statusEffects.js";

/**
 * Patch FoundryVTT methods to build our own indexes as early as possible.
 */
export function initializeCompendium() {
    CONFIG.Item.compendiumIndexFields.push(
        "system.cid",
        "system.group",
        "flags.tde5e.tags",
        "system.variant.available",
        "system.parents",
    );
    CONFIG.Actor.compendiumIndexFields.push("flags.tde5e.tags");

    // Load conditions and refresh affected actors.
    Hooks.on("setup", async () => {
        await loadStatusEffects();
        await preloadCompendiums();
        TDE5E.compendiumsReady = true;

        buildIndexReferences();
        buildItemReferences();
        if (game.canvas.initializing) {
            await game.canvas.initializing;

            // Reprepare active token and world actors.
            const tokens = game.canvas.tokens.placeables;
            const actors = new Set(tokens.map(token => token.actor).concat(game.actors.contents));
            for (const actor of actors) {
                if (!actor) continue;
                actor.itemTypes.condition.forEach(condition => condition.synchronizeEffect());
                actor.refreshConditions();
            }
        }
    });

    // Add migration header button.
    Hooks.on("getCompendiumHeaderButtons", (app, buttons) => {
        if (!TDE5E.debugMode) return;

        let migrator;
        switch (app.collection.metadata.type) {
        case "Item":
            migrator = migrateItem;
            break;
        case "Actor":
            migrator = migrateActor;
            break;
        default:
            return;
        }

        buttons.unshift({
            class: "migrate",
            icon: "far fa-file-arrow-up",
            label: game.i18n.localize("tde5e.settings.forceMigration.name"),
            onclick: async () => {
                const compendium = app.collection;
                ui.notifications.info(game.i18n.format(
                    "tde5e.notification.startMigration",
                    { name: compendium.metadata.label },
                ));
                await migrateCompendium(compendium, migrator);
                ui.notifications.info(game.i18n.format(
                    "tde5e.notification.compendiumMigrated",
                    { name: compendium.metadata.label },
                ));
            },
        });
    });
}

/**
 * Loads index properties required for synchronous functionality.
 * @returns {Promise} A promise representing the index loading.
 */
function preloadCompendiums() {
    return Promise.all(game.packs.map(pack => pack.getIndex()));
}

/**
 * Adds child references to index entries that are referenced as another entry's parent.
 */
function buildIndexReferences() {
    const indexes = game.packs.filter(pack => pack.documentName === "Item").map(pack => pack.index);
    for (const index of indexes) {
        for (const entry of index) {
            for (const parent of entry.system?.parents ?? []) {
                const parentEntry = fromUuidSync(parent, { strict: false });
                if (parentEntry) {
                    parentEntry.references ??= new Set();
                    parentEntry.references.add(entry.uuid);
                }
            }
        }
    }
}

/**
 * Adds child references to index entries and world items that are referenced as another item's parent.
 */
function buildItemReferences() {
    for (const actor of game.actors) {
        for (const item of actor.items) {
            item.buildReferences();
        }
    }
}

/**
 * Gets the first occurence of the given packid (prefix.compendiumName.compendiumItemId) from the cached indexes.
 * @param {string} packId The packid in the form of "prefix.compendiumName.compendiumItemId"
 * @returns {object?} The index for the given packId or null.
 */
export function findIndexByPackId(packId) {
    const [prefix, packName, id] = packId.split(".");
    const pack = game.packs.get(`${prefix}.${packName}`);

    if (!pack) return null;
    return pack.index.get(id);
}

/**
 * Gets all occurences of the type from the cached indexes.
 * @param {string} type Any foundry item-type.
 * @returns {object[]} An array of all indexes matching the type.
 */
export function filterIndexesByType(type) {
    const result = [];
    for (const pack of getRelevantPacks(type)) {
        for (const index of pack.index.values()) {
            if (index.type === type) result.push({ uuid: `${pack.collection}.${index._id}`, entry: index });
        }
    }
    return result;
}

/**
 * Retreives a list of compendiums that can contain the given type, including
 *  system compendiums configured for it and all world compendiums.
 * @param {string} type The type to look for.
 * @returns {CompendiumCollection[]} The array of packs that can contain the type.
 */
function getRelevantPacks(type) {
    const systemPacks = TDE5E.indexedProperties[type].sources.map(source => game.packs.get(source));
    const worldPacks = game.packs.filter(pack => pack.metadata.packageType === "world" && pack.documentName === "Item");
    return systemPacks.concat(worldPacks);
}
