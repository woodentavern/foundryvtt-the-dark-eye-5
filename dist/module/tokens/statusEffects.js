import { warn } from "../util.js";

/**
 * Adds custom status counter types. This must be called before the counters
 *  are rendered (which happens during the render hook).
 */
export function setupConditionCounters() {
}

/**
 * Creates an array of active effects from the system's conditions and states.
 * @returns {Promise.<object[]>} The effect array to replace CONFIG.statusEffects with.
 */
export async function loadStatusEffects() {
    TDE5E.statusEffects.forEach(e => { e.label = game.i18n.localize(e.label); });
    CONFIG.statusEffects = (await _loadConditionsAndStates())
        .filter(condition => condition.img !== "icons/svg/mystery-man.svg")
        .map(condition => ({
            id: condition.system.cid,
            img: condition.img,
            name: condition.name,
            flags: {
                tde5e: {
                    type: condition.type,
                    isDefault: condition.system.isDefault ?? false,
                },
            },
        }))
        .concat(TDE5E.statusEffects);

    // Register hooks to update the effect array on item changes.
    Hooks.on("createItem", item => refreshEffect(item, true, false));
    Hooks.on("deleteItem", item => refreshEffect(item, false, true));
    Hooks.on("updateItem", item => refreshEffect(item, true, true));

    CONFIG.statusEffects.ready = true;
    CONFIG.specialStatusEffects.DEFEATED = "HANDLUNGSUNFAHIG";
    CONFIG.specialStatusEffects.BLIND = "BLIND";
    CONFIG.specialStatusEffects.INVISIBLE = "UNSICHTBAR";
    setupStatusIconCounters();
}

/**
 * Retrieves all conditions and states from all compendiums.
 * @returns {Promise.<object[]>} A promise representing all states and conditions.
 */
async function _loadConditionsAndStates() {
    let values = [];

    // Load from compendiums.
    const sources = new Set(TDE5E.indexedProperties.condition.sources.concat(TDE5E.indexedProperties.state.sources));
    for (const [key, pack] of [...game.packs.entries()]) {
        if (!sources.has(key) && (key.startsWith("tde5e.")
            || !pack.index.some(entry => entry.type === "condition" || entry.type === "state"))) continue;
        const documents = await pack.getDocuments({ type__in: [TDE5E.itemTypes.Condition, TDE5E.itemTypes.State] });
        values = values.concat(documents.map(doc => doc.toObject()));
    }

    // Load from global item data because collection isn't ready yet.
    values = values.concat(game.data.items
        .filter(item => item.system.cid && (item.type === "condition" || item.type === "state")));

    return values;
}

/**
 * Updates the global status effect array based on the given item.
 * @param {TdeItem} item The item that was changed.
 * @param {boolean} insert Indicates whether a new effect should be created.
 * @param {boolean} remove Indicates whether the old effect should be removed (if present).
 */
function refreshEffect(item, insert, remove) {
    if (item.isEmbedded || !item.cid || (item.type !== "condition" && item.type !== "state")) return;

    if (remove) {
        const index = CONFIG.statusEffects.findIndex(effect => effect.id === item.cid);
        if (index >= 0) CONFIG.statusEffects.splice(index, 1);
    }

    if (insert) {
        CONFIG.statusEffects.push({
            id: item.cid,
            img: item.img,
            name: item.name,
            flags: {
                tde5e: {
                    type: item.type,
                    isDefault: false,
                },
            },
        });
    }
}

/**
 * Configures default counter types for the loaded status effects and registers
 *  hooks to synchronize actor conditions.
 */
function setupStatusIconCounters() {
    const api = game.modules.get("statuscounter")?.api;
    if (!api) {
        warn("The Dark Eye | Status Icon Counters not initialized.");
        return;
    }

    class TdeConditionCounter extends api.StatusCounter {
        static get label() {
            return game.i18n.localize("tde5e.types.condition");
        }

        static allowType(parent) {
            return parent.getFlag("tde5e", "type") === "condition";
        }

        static create(document, data) {
            // Prevent manual creation of effects with condition counters.
            data.config.type = "default";
            return super.create(document, data);
        }

        get visible() {
            return true;
        }

        get displayValue() {
            return this.parent.parent.value ?? 0;
        }

        get _sourceValue() {
            return this.parent.parent._source.system.value;
        }

        setValue(value) {
            value = Math.clamp(value, 0, this.parent.parent.system.maxLevel);
            return this.parent.parent.update({ "system.value": value });
        }

        _deleteParent() {
            return this.setValue(0);
        }
    }

    api.addCounterType(
        "tde5e.condition",
        TdeConditionCounter,
        CONFIG.statusEffects
            .filter(e => foundry.utils.getProperty(e, "flags.tde5e.type") === "condition")
            .map(e => e.id),
    );
}

/**
 * Extends the token HUD rendering to remove status effect toggles for statuses
 *  that the actor can't have.
 * @param {TokenHUD} hud The HUD object being rendered.
 * @param {jQuery.Object} html The rendered JQuery element of the HUD.
 */
export function filterAvailableEffects(hud, html) {
    const { actor } = hud.object;
    if (!actor) return;

    html.find(".status-effects img.effect-control").filter((_index, effect) => {
        if (effect.classList.contains("active")) return false;
        return !actor.canHaveStatus(CONFIG.statusEffects.find(status => status.id === effect.dataset.statusId));
    }).remove();
}
