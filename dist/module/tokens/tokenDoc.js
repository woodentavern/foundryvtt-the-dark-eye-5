/**
 * Extended token document für the TDE5E system.
 */
export default class TdeTokenDocument extends TokenDocument {
    /**
     * Extends FoundryVTT's method to refresh the combatant when any resource has changed.
     * @override
     */
    _onRelatedUpdate(update = {}, options = {}) {
        const c = this.combatant;
        if (c && (update.system || {}).hasOwnProperty("resources")) c.updateResource();
        return super._onRelatedUpdate(update, options);
    }
}
