/**
 * Creates a new chatMessage with the specified type. If the type is not recognized or empty the
 *  default type for chatMessage will be used for type resolution.
 * @param {object} data The chatMessage data.
 * @param {string?} type The type of chatMessage @see {TDE5E.chatMessageTypeClasses}.
 * @returns {Promise} A promise representing the message creation.
 */
export async function createTypedChatMessage(data, type = "default") {
    foundry.utils.setProperty(data, "flags.tde5e.type", type);
    const cls = CONFIG.ChatMessage.documentClass.resolveClass(type);
    return cls.create(data, {});
}

/**
 * Sends a message informing the players that a tie between the given combatants
 *  has been resolved. The message is only revealed to the players if there are
 *  player characters and no hidden combatants involved.
 * @param {TdeCombatant[]} combatants The array of tied combatants.
 * @returns {Promise} A promise representing the message creation.
 */
export function sendTieMessage(combatants) {
    // Create the HTML content.
    let isHidden = false;
    let hasPlayers = false;
    let content = `<div class='tiebreaker-message'>${game.i18n.localize("tde5e.combat.resolvedTies")}: `;
    const combatantStrings = [];
    combatants.forEach(combatant => {
        if (combatant.hidden) isHidden = true;
        if (combatant.players.length) hasPlayers = true;
        combatantStrings.push(`<span>${combatant.name} (${combatant.getFlag("tde5e", "tieInitiative")})</span>`);
    });
    content += `${combatantStrings.join(", ")}</div>`;

    // Create the message with the correct roll mode.
    const cls = getDocumentClass("ChatMessage");
    const msgData = {
        user: game.user.id,
        type: CONST.CHAT_MESSAGE_TYPES.OTHER,
        content,
    };
    if (!hasPlayers || isHidden) msgData.whisper = cls.getWhisperRecipients("GM").map(u => u.id);

    return cls.create(msgData);
}

/**
 * Adds listeners to the given HTML to scroll to messages using a link.
 * @param {jQuery.Element} html The HTML element to add the listener on.
 */
export function addMessageListener(html) {
    html.on("click", "a.message-link", event => {
        event.preventDefault();
        const messageId = event.currentTarget.dataset.id;
        if (!messageId) return;

        let chat = Object.values(ui.windows).find(w => w instanceof ChatLog);
        if (!chat) {
            chat = ui.sidebar.tabs.chat;
            if (ui.sidebar.activeTab !== "chat") ui.sidebar.activateTab("chat");
        }

        const messageEl = chat.element[0]?.querySelector(`li.chat-message[data-message-id="${messageId}"]`);
        if (!messageEl) return;
        messageEl.scrollIntoView();
        if (!messageEl.classList.contains("flash-border")) {
            messageEl.classList.add("flash-border");
            messageEl.addEventListener("animationend", () => messageEl.classList.remove("flash-border"));
        }
    });
}
