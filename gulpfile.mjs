/* eslint-disable eslint-comments/no-unlimited-disable -- Stop whining, it's a build script. */
/* eslint-disable -- Don't lint the build script. */
import gulp from "gulp";
import less from "gulp-less";
import git from "gulp-git";

import fs from "fs-extra";
import path from "path";
import chalk from "chalk";
import archiver from "archiver";
import stringify from "json-stringify-pretty-compact";
import XMLHttpRequest from "xhr2";
import yargs from "yargs";

import { compilePack, extractPack} from "@foundryvtt/foundryvtt-cli";

function getManifest() {
	const json = { root: 'dist' };

	const modulePath = path.join(json.root, 'module.json');
	const systemPath = path.join(json.root, 'system.json');

	if (fs.existsSync(modulePath)) {
        json.file = fs.readJsonSync(modulePath);
		json.name = 'module.json';
	} else if (fs.existsSync(systemPath)) {
        json.file = fs.readJsonSync(systemPath);
		json.name = 'system.json';
	} else {
		return;
	}

	return json;
}

function getDataPath(config) {
    if (!config.dataPath) throw Error("No dataPath defined in foundryconfig.json.");

    const localAppDataPath = process.env.LOCALAPPDATA;
    let resolvedDataPath;
    if (!localAppDataPath) {
        console.warn(chalk.yellow("Can't auto-resolve data path, make sure to set an absolute path in foundryconfig.json > dataPath."));
        resolvedDataPath = config.dataPath;
    } else {
        resolvedDataPath = config.dataPath.replace("${env:AppData}", localAppDataPath);
    }

    if (!fs.existsSync(path.join(resolvedDataPath, 'Data'))) {
        throw Error('User Data path invalid, no Data directory found at ' + resolvedDataPath);
    }

    return resolvedDataPath;
}

/********************/
/*		BUILD		*/
/********************/

/**
 * Build Less
 */
export function buildLess() {
	return gulp
        .src('src/styles/tde5e.less')
		.pipe(less())
		.pipe(gulp.dest('dist/'));
}

/********************/
/*		CLEAN		*/
/********************/

/**
 * Remove built files from `dist` folder
 * while ignoring source files
 */
export async function clean() {
	const name = "tde5e";
	const files = [];

	// If the project uses TypeScript
	if (await fs.exists(path.join('src', `${name}.ts`))) {
		files.push(
			'module',
			`${name}.js`
		);
	}

	// If the project uses Less
	if (await fs.exists(path.join('src', 'styles', `${name}.less`))) {
		files.push('fonts', `${name}.css`);
	}

    console.log(chalk);

	console.log(' ', chalk.yellow('Files to clean:'));
	console.log('   ', chalk.blueBright(files.join('\n    ')));

	// Attempt to remove the files
	try {
		for (const filePath of files) {
			await fs.remove(path.join('dist', filePath));
		}
		return Promise.resolve();
	} catch (err) {
		Promise.reject(err);
	}
}

/********************/
/*		LINK		*/
/********************/

/**
 * Link build to User Data folder
 */
export async function link() {
    const config = await fs.readJson('foundryconfig.json');
	let destDir, name;
	try {
		const sourceModulePath = path.resolve('.', 'src', 'module.json');
		const distModulePath = path.resolve('.', 'dist', 'module.json');
		const sourceSystemPath = path.resolve('.', 'src', 'system.json');
		const distSystemPath = path.resolve('.', 'dist', 'system.json');
		if (await fs.exists(distModulePath) || await fs.exists(sourceModulePath)) {
			destDir = 'modules';
            name = (await fs.readJson(distModulePath)).id;
		} else if (await fs.exists(distSystemPath) || await fs.exists(sourceSystemPath)) {
			destDir = 'systems';
            name = (await fs.readJson(distSystemPath)).id;
		} else {
			throw Error(`Could not find ${chalk.blueBright('module.json')} or ${chalk.blueBright('system.json')}`);
		}

		let linkDir = path.join(getDataPath(config), "Data", destDir, name);
        if (yargs().argv.clean || yargs().argv.c) {
			console.log(chalk.yellow(`Removing build in ${chalk.blueBright(linkDir)}`));
			await fs.remove(linkDir);
		} else if (!await fs.exists(linkDir)) {
			console.log(chalk.green(`Copying build to ${chalk.blueBright(linkDir)}`));
			await fs.symlink(path.resolve('./dist'), linkDir);
		}
		return Promise.resolve();
	} catch (err) {
		Promise.reject(err);
	}
}

/*********************/
/*		PACKAGE		 */
/*********************/

/**
 * Package build
 */
async function packageBuild() {
	const manifest = getManifest();

    // Remove the package dir without doing anything else
    if (yargs().argv.clean || yargs().argv.c) {
        console.log(chalk.yellow('Removing all packaged files'));
        await fs.remove('package');
        return;
    }

    // Ensure there is a directory to hold all the packaged versions
    await fs.ensureDir('package');

    await new Promise((resolve, reject) => {
        try {
            // Initialize the zip file
            const zipName = `${manifest.file.id}-v${manifest.file.version}.zip`;
            const zipFile = fs.createWriteStream(path.join('package', zipName));
            const zip = archiver('zip', { zlib: { level: 9 } });

            zipFile.on('close', () => {
                console.log(chalk.green(zip.pointer() + ' total bytes'));
                console.log(chalk.green(`Zip file ${zipName} has been written`));
                resolve();
            });

            zip.on('error', (err) => {
                throw err;
            });

            zip.pipe(zipFile);

            // Add the directory with the final code
            zip.directory('dist/', manifest.file.id);
            zip.finalize();
        } catch (err) {
            reject(err);
        }
    });
}

/**
 * Creates a new version by incrementing the previous number, generating a new
 *  download link and tagging the current commit using Git.
 * Note that this requires a Git instance accessible through the command line.
 */
async function createVersion() {
    // Validate data.
    const manifest = getManifest();
    const config = await fs.readJson('foundryconfig.json');
    if (!manifest || !config) console.log(chalk.red("Manifest or configuration file not found."));
    if (!config.rawUrl || !config.apiUrl) console.log(chalk.red("Invalid repository URLs."));

    // Generate new version number.
    const sysVersion = manifest.file.version.split('.');
    const coreVersion = manifest.file.compatibility.verified.split('.');
    if (coreVersion[0] > sysVersion[0]) {
        // Create new major version.
        manifest.file.version = `${coreVersion[0]}.0`;
    } else {
        manifest.file.version = `${sysVersion[0]}.${parseInt(sysVersion[1]) + 1}`;
    }

    const versionTag = "v" + manifest.file.version;
    console.log("New version will be " + versionTag);

    // Generate URLs.
    manifest.file.manifest = `${config.rawUrl}/master/${manifest.root}/${manifest.name}`;
    const fileName = `${manifest.file.id}-v${manifest.file.version}.zip`;
    manifest.file.download = `${config.apiUrl}/packages/generic/tde5e/${manifest.file.version}/${fileName}`;

    // Save manifest file.
    const manifestFilePath = path.join(manifest.root, manifest.name);
    console.log("Writing new manifest file to " + manifestFilePath);
    await fs.writeFile(manifestFilePath, stringify(manifest.file, { indent: 4 }), "utf8");

    // Tag the repository.
    git.tag(versionTag, "", function (e) { if (e) throw e; });
}

/**
 * Publishes the most recently created version by uploading the package and
 *  committing the manifest file using Git.
 * Note that this requires a Git instance accessible through the command line.
 */
async function publish() {
    // Validate data.
    const manifest = getManifest();
    const config = await fs.readJson('foundryconfig.json');
    const token = await fs.readJson('deploytoken.json');
    if (!manifest || !config || !token) console.log(chalk.red("Manifest or configuration or token file not found."));

    // Upload package file.
    const fileName = `${manifest.file.id}-v${manifest.file.version}.zip`;
    if (!await fs.exists("package/" + fileName)) console.log(chalk.red("Package " + fileName + " does not exist."));

    console.log("Uploading file " + fileName + " via GitLab API");
    var request = new XMLHttpRequest();
    request.open("PUT", `${config.apiUrl}/packages/generic/tde5e/${manifest.file.version}/${fileName}`, true);
    request.setRequestHeader("Content-Type", "application/zip");
    request.setRequestHeader("Content-Disposition", `attachment; filename="${fileName}"`);
    request.setRequestHeader("Authorization", "Basic " + Buffer.from(`${token.name}:${token.password}`).toString("base64"));
    request.addEventListener("loadend", function (ev) {
        console.log(ev.currentTarget.response);
    });
    request.send(await fs.readFile("package/" + fileName));

    // Commit (only) system.json.
    console.log("Committing system.json");
    gulp.src("dist/system.json").pipe(git.commit("Update manifest to v" + manifest.file.version, {
        disableAppendPaths: true,
        args: "-o ./dist/system.json"
    }));
}

export const build = gulp.series(clean, buildLess);
export const pack = gulp.series(importPacks, build, packageBuild);
export const release = gulp.series(createVersion, pack, publish);

/**
 * Exports the system's compendiums into lose files using the FoundryVTT CLI.
 * The files are renamed to match the CID-ID.json naming scheme.
 */
export async function exportPacks() {
    const manifest = getManifest();
    if (!manifest) return console.log(chalk.red("Manifest not found."));

    for (let pack of manifest.file.packs) {
        const packName = pack.name;
        const packPath = path.join("dist", "packs", packName);
        console.log(chalk.blue(`Unpacking compendium ${packPath}...`));

        const exportPath = path.join("src", "packs", "_export", packName);
        await extractPack(packPath, exportPath, {
            log: false,
            jsonOptions: { space: 4 },
            transformName: entry => {
                let cid = entry.system?.cid;
                if (!cid && entry._key.startsWith("!folders")) cid = "_FOLDER";
                return cid ? `${cid}-${entry._id}.json` : `${entry._id}.json`;
            },
            transformEntry: entry => {
                if (!entry._key.startsWith("!folders")) delete entry.sort;
                delete entry.ownership;
                delete entry._stats;
                if (entry.system?.changelog) delete entry.system.changelog;
                if (entry.flags) {
                    if (entry.flags.tde5e?.tags?.length) entry.flags.tde5e = { tags: entry.flags.tde5e.tags };
                    else delete entry.flags;
                }
            }
        });
    }
}

/**
 * Imports lose JSON files into the system's compendiums using the FoundryVTT CLI.
 */
export async function importPacks() {
    const manifest = getManifest();
    if (!manifest) return console.log(chalk.red("Manifest not found."));

    for (let pack of manifest.file.packs) {
        const packName = pack.name;
        const packPath = path.join("dist", "packs", packName);
        console.log(chalk.blue(`Repacking compendium ${packPath}...`));

        const importPath = path.join("src", "packs", packName);
        await compilePack(importPath, packPath, { log: false });
    }
}